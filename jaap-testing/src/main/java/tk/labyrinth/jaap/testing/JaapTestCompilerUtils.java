package tk.labyrinth.jaap.testing;

import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Stream;

public class JaapTestCompilerUtils {

	private static Path buildPathFromTypeName(String sourceFolder, String typeName) {
		return Path.of(sourceFolder, typeName.replace(".", "/") + ".java");
	}

	/**
	 * Returns {@link Writer} with the same destination as would be chosen if no {@link Writer}
	 * is specified for {@link JavaCompiler.CompilationTask}, which is {@link System#err}.
	 * This can be seen in <b>com.sun.tools.javac.api.JavacTool#getTask</b> method, line 169.<br>
	 *
	 * @return {@link OutputStreamWriter} wrapping {@link System#err}
	 */
	public static Writer getDefaultJavacWriter() {
		return new OutputStreamWriter(System.err);
	}

	public static Stream<JavaFileObject> getJavaFileObjects(StandardJavaFileManager fileManager, Stream<Path> paths) {
		return StreamUtils.from(fileManager.getJavaFileObjects(paths.toArray(Path[]::new)))
				.map(JavaFileObject.class::cast);
	}

	public static Stream<Path> sourceFilesToPaths(Stream<String> sourceFiles) {
		return sourceFiles.map(Path::of);
	}

	public static Stream<Path> sourceNamesToPaths(String sourceFolder, Stream<String> sourceNames) {
		return sourceNames.map(sourceName -> buildPathFromTypeName(sourceFolder, sourceName));
	}

	public static Stream<Path> sourceResourcesToPaths(Stream<String> sourceResources) {
		return sourceResources.map(sourceResource -> {
			try {
				URL sourceResourceUrl = ClassLoader.getSystemResource(sourceResource);
				Objects.requireNonNull(sourceResourceUrl, "sourceResource = " + sourceResource);
				return Path.of(sourceResourceUrl.toURI());
			} catch (URISyntaxException ex) {
				throw new RuntimeException(ex);
			}
		});
	}

	public static Stream<Path> sourceTypesToPaths(String sourceFolder, Stream<Class<?>> sourceTypes) {
		return sourceNamesToPaths(sourceFolder, sourceTypes.map(Class::getCanonicalName));
	}
}
