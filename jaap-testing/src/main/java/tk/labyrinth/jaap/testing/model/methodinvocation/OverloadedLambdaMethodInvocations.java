package tk.labyrinth.jaap.testing.model.methodinvocation;

import java.util.function.Function;
import java.util.function.LongFunction;
import java.util.function.Supplier;

@SuppressWarnings({"Convert2MethodRef", "rawtypes"})
public class OverloadedLambdaMethodInvocations {

	static {
		int i = 0;
		//
		Supplier one = () -> String.valueOf(i);
		Function two = val -> String.valueOf(val);
		Function<Long, ?> three = val -> String.valueOf(val);
		LongFunction<?> four = val -> String.valueOf(val);
		Function five = val -> String.valueOf((long) val);
	}
}
