package tk.labyrinth.jaap.testing.junit5.parameter;

import tk.labyrinth.jaap.core.AnnotationProcessingRound;

/**
 * Extensible tool for enriching test entities with annproc objects.
 *
 * @param <T> Type
 */
public interface JaapVariableResolver<T> {

	/**
	 * TODO: Replace with obtaining type from type variable.
	 *
	 * @return non-null
	 */
	Class<T> getParameterType();

	/**
	 * @return whether handled variable require annproc context.
	 */
	boolean isInternal();

	T resolveVariable(AnnotationProcessingRound round);
}
