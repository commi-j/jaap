package tk.labyrinth.jaap.testing.junit5.parameter;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;

import javax.annotation.processing.ProcessingEnvironment;

@AutoService(JaapVariableResolver.class)
public class ProcessingEnvironmentVariableResolver implements JaapVariableResolver<ProcessingEnvironment> {

	@Override
	public Class<ProcessingEnvironment> getParameterType() {
		return ProcessingEnvironment.class;
	}

	@Override
	public boolean isInternal() {
		return true;
	}

	@Override
	public ProcessingEnvironment resolveVariable(AnnotationProcessingRound round) {
		return round.getProcessingEnvironment();
	}
}
