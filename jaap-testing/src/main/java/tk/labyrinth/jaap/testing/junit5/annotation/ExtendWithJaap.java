package tk.labyrinth.jaap.testing.junit5.annotation;

import org.junit.jupiter.api.extension.ExtendWith;
import tk.labyrinth.jaap.testing.junit5.JaapExtension;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Shorthand for {@link ExtendWith @ExtendWith}({@link JaapExtension JaapExtension.class}).
 *
 * @author Commitman
 */
@Documented
@ExtendWith(JaapExtension.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExtendWithJaap {
	// empty
}
