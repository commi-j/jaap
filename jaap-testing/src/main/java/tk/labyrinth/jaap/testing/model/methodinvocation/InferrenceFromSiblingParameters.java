package tk.labyrinth.jaap.testing.model.methodinvocation;

import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

@SuppressWarnings("Convert2MethodRef")
public class InferrenceFromSiblingParameters {

	static {
		function(
				UUID.randomUUID(),
				foo -> foo.toString());
		function(
				List.<Integer>of(),
				foo -> {
					BigDecimal.valueOf(foo.get(0));
					return "bar";
				});
		List<Integer> list = function(
				List.of(),
				foo -> {
					BigDecimal.valueOf(foo.get(0));
					return "bar";
				});
	}

	public static <T> T function(T value, Function<T, String> consumer) {
		throw new NotImplementedException();
	}
}
