package tk.labyrinth.jaap.testing.core;

import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class TestCallbackAnnotationProcessor extends CallbackAnnotationProcessor {

	private final Map<String, Object> state = new HashMap<>();

	@Nullable
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		return (T) state.get(key);
	}

	public <T> void put(String key, @Nullable T value) {
		state.put(key, value);
	}
}
