package tk.labyrinth.jaap.testing.model.lambdaexpression;

import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import java.util.List;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

@SuppressWarnings({"Convert2MethodRef", "ResultOfMethodCallIgnored", "rawtypes"})
public class LambdaParameterTypeInferences {

	static {
		{
			// From Target.
			//
			Stream
					.empty()
					.map(val -> self(val));
			Stream
					.<Integer>empty()
					.map(val -> self(val));
		}
		{
			// From Expected Type.
			//
			UnaryOperator uo = val -> self(val);
			UnaryOperator<Integer> uoi = val -> self(val);
			IntUnaryOperator iuo = val -> self(val);
		}
		{
			// From Sibling Argument.
			//
			function(
					12,
					val -> self(val));
			function(
					List.<Integer>of(),
					val -> val.stream().map(elem -> self(elem)));
		}
	}

	public static <T> void function(T value, Function<T, ?> function) {
		throw new NotImplementedException();
	}

	public static Integer self(Integer i) {
		return i;
	}

	public static Object self(Object o) {
		return o;
	}

	public static int self(int i) {
		return i;
	}
}
