package tk.labyrinth.jaap.testing.junit5.annotation;

import tk.labyrinth.jaap.testing.CompilationException;
import tk.labyrinth.jaap.testing.junit5.JaapExtension;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks test methods extended with {@link JaapExtension} to expect {@link CompilationException} to be thrown.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ExpectCompilationFailure {
	// empty
}
