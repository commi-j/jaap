package tk.labyrinth.jaap.testing.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AllTypes {

	public Integer IntegerObjectType;

	public int intPrimitiveType;

	public List<String> listOfStringsType;

	public Map.Entry<Integer, Long> mapEntryOfIntegerAndLongType;

	public Object objectType;

	public int[] primitiveArrayType;

	public void voidType() {
		// no-op
	}

	public static List<Class<?>> getAsClasses() {
		return Stream
				.concat(
						Stream.of(AllTypes.class.getDeclaredFields())
								.map(Field::getType),
						Stream.of(AllTypes.class.getDeclaredMethods())
								.filter(declaredMethod -> !Modifier.isStatic(declaredMethod.getModifiers()))
								.map(Method::getReturnType))
				.collect(Collectors.toList());
	}

	public static List<Type> getAsTypes() {
		return Stream
				.concat(
						Stream.of(AllTypes.class.getFields())
								.map(Field::getGenericType),
						Stream.of(AllTypes.class.getMethods())
								.map(Method::getGenericReturnType))
				.collect(Collectors.toList());
	}
}
