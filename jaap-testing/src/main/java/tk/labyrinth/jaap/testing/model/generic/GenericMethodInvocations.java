package tk.labyrinth.jaap.testing.model.generic;

import java.util.Optional;
import java.util.Random;

/**
 * Different methods are available on {@link Optional#get()} based on its type parameter.
 */
@SuppressWarnings({"ConstantConditions", "ResultOfMethodCallIgnored"})
public class GenericMethodInvocations {

	static {
		{
			Optional<Long> optional = Optional.empty();
			optional.get().intValue();
		}
		{
			Optional<Random> optional = Optional.empty();
			optional.get().nextInt();
		}
	}
}
