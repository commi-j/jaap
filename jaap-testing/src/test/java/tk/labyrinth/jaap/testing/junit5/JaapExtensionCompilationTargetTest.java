package tk.labyrinth.jaap.testing.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.RoundEnvironment;
import java.util.List;
import java.util.stream.Collectors;

@CompilationTarget(sourceNames = "tk.labyrinth.jaap.testing.test.model.package-info")
@ExtendWithJaap
public class JaapExtensionCompilationTargetTest {

	@CompilationTarget(append = true, sourceNames = "tk.labyrinth.jaap.testing.test.model.pkg.package-info")
	@Test
	void testMethodDeclaredAppendsTypeDeclared(RoundEnvironment environment) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.testing.test.model",
						"tk.labyrinth.jaap.testing.test.model.pkg"),
				environment.getRootElements().stream()
						.map(Object::toString)
						.sorted()
						.collect(Collectors.toList()));
	}

	@CompilationTarget(sourceNames = "tk.labyrinth.jaap.testing.test.model.pkg.package-info")
	@Test
	void testMethodDeclaredOverridesTypeDeclared(RoundEnvironment environment) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.testing.test.model.pkg"),
				environment.getRootElements().stream()
						.map(Object::toString)
						.sorted()
						.collect(Collectors.toList()));
	}
}
