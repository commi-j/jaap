package tk.labyrinth.jaap.testing;

import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.tools.JavaFileObject;
import java.util.stream.Stream;

class JaapTestCompilerUtilsTest {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testGetJavaFileObjectsWithSourceResource() {
		JavaFileObject javaFileObject = JaapTestCompilerUtils.getJavaFileObjects(testCompiler.getFileManager(),
				JaapTestCompilerUtils.sourceResourcesToPaths(Stream.of("JavaHelloWorld.java")))
				.collect(CollectorUtils.findOnly());
		//
		ContribAssertions.assertEndsWith("jaap-testing/target/test-classes/JavaHelloWorld.java",
				javaFileObject.toUri().toString());
	}

	@Test
	void testGetJavaFileObjectsWithSourceType() {
		JavaFileObject javaFileObject = JaapTestCompilerUtils.getJavaFileObjects(testCompiler.getFileManager(),
				JaapTestCompilerUtils.sourceTypesToPaths(JaapTestCompiler.SOURCE_FOLDER,
						Stream.of(JaapTestCompilerUtilsTest.class)))
				.collect(CollectorUtils.findOnly());
		//
		ContribAssertions.assertEndsWith("jaap-testing/src/test/java/tk/labyrinth/jaap/testing/JaapTestCompilerUtilsTest.java",
				javaFileObject.toUri().toString());
	}
}
