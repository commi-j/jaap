package tk.labyrinth.jaap.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.lang.model.element.Element;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class JaapTestCompilerTest {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testRunWithMissingSourceType() {
		ContribAssertions.assertThrows(
				() -> testCompiler.run(CompilationTarget.ofSourceTypes(Object.class)),
				fault -> {
					Assertions.assertEquals(CompilationException.class, fault.getClass());
					List<String> output = ((CompilationException) fault).getOutput();
					//
					Assertions.assertEquals(2, output.size());
					//
					// Full line is: error: error reading src/test/java/java/lang/Object.java; ${baseDir}/jaap-testing/src/test/java/java/lang/Object.java
					// We don't check the ${baseDir} part.
					ContribAssertions.assertStartsWith(
							"error: error reading src/test/java/java/lang/Object.java;",
							output.get(0).replace("\\", "/"));
					ContribAssertions.assertEndsWith(
							"/jaap-testing/src/test/java/java/lang/Object.java",
							output.get(0).replace("\\", "/"));
					//
					Assertions.assertEquals("1 error", output.get(1));
				});
	}

	@Test
	void testRunWithSourceResource() {
		AtomicBoolean triggered = new AtomicBoolean(false);
		List<String> output = testCompiler.run(CompilationTarget.ofSourceResources("JavaHelloWorld.java"), new CallbackAnnotationProcessor()
				.onFirstRound(round -> {
					ContribAssertions.assertEquals(List.of("tk.labyrinth.jaap.testing.resource.JavaHelloWorld"),
							round.getRoundEnvironment().getRootElements().stream().map(Element::toString));
					triggered.set(true);
				}));
		Assertions.assertEquals(List.of(""), output);
		Assertions.assertTrue(triggered.get());
	}
}
