#
## Origin: https://github.com/jacoco/jacoco/pull/488#issuecomment-316602731
## This script computes the result from jacoco.csv summing up 4th (INSTRUCTION_MISSED) and 5th (INSTRUCTION_COVERED) cols.
## Example input:
## tk.labyrinth.jaap:test-coverage/jaap,tk.labyrinth.misc4j.lang.struct,HierarchyUtils,17,66,2,8,3,17,3,5,1,2
## Example output:
## 59.7921 % covered
awk -F "," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, " instructions covered"; print 100*covered/instructions, "% covered" }' target/site/jacoco-aggregate/jacoco.csv
