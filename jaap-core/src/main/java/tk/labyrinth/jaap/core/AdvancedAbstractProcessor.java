package tk.labyrinth.jaap.core;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * {@link AbstractProcessor} with some features that are almost always required.
 */
@Slf4j
public abstract class AdvancedAbstractProcessor extends AbstractProcessor {

	protected abstract boolean doProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv);

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		try {
			return doProcess(annotations, roundEnv);
		} catch (Throwable t) {
			logger.error("", t);
			if (t instanceof RuntimeException) {
				throw new RuntimeException(t);
			} else {
				// StackOverflowError or other.
				throw new Error(t);
			}
		}
	}
}
