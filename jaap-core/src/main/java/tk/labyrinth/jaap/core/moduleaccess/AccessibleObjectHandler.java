package tk.labyrinth.jaap.core.moduleaccess;

import sun.misc.Unsafe;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

public class AccessibleObjectHandler {

	/**
	 * Mirror of the same field in the {@link AccessibleObject}.
	 */
	boolean override;

	private static long getOverrideFieldOffset(Unsafe unsafe) {
		try {
			return unsafe.objectFieldOffset(AccessibleObjectHandler.class.getDeclaredField("override"));
		} catch (NoSuchFieldException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static Unsafe getUnsafe() {
		try {
			Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
			theUnsafe.setAccessible(true);
			return (Unsafe) theUnsafe.get(null);
		} catch (ReflectiveOperationException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static void setAccessible(AccessibleObject accessibleObject, boolean accessible) {
		Unsafe unsafe = getUnsafe();
		//
		unsafe.putBoolean(accessibleObject, getOverrideFieldOffset(unsafe), accessible);
	}
}
