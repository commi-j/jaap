package tk.labyrinth.jaap.core.model;

import java.util.List;

public enum JavaKeywords {
	BOOLEAN,
	BYTE,
	CHAR,
	DOUBLE,
	FLOAT,
	INT,
	LONG,
	SHORT,
	VOID;

	public String lowercase() {
		return name().toLowerCase();
	}

	public static List<JavaKeywords> primitives() {
		return List.of(BOOLEAN, BYTE, CHAR, DOUBLE, FLOAT, INT, LONG, SHORT);
	}
}
