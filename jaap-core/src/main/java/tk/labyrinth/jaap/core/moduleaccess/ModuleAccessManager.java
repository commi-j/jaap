package tk.labyrinth.jaap.core.moduleaccess;

import java.lang.reflect.Method;
import java.util.List;

/**
 * This solution reuses the Lombok's approach from lombok.javac.apt.LombokProcessor#addOpensForLombok()
 * to lift access restrictions introduced by Java module system.
 */
public class ModuleAccessManager {

	private static volatile boolean added = false;

	/**
	 * Useful from jdk9 and up; required from jdk16 and up. This code is supposed to gracefully do nothing on jdk8 and below, as this operation isn't needed there.
	 */
	public static void addExportsToAllUnnamed() {
		// TODO: Use smarter synchroniation.
		synchronized (ModuleAccessManager.class) {
			if (!added) {
				added = true;
				//
				Module jdkCompilerModule = ModuleLayer.boot().findModule("jdk.compiler").get();
				//
				List<String> packagesToExport = List.of(
						"com.sun.tools.javac.code",
//				"com.sun.tools.javac.comp",
//				"com.sun.tools.javac.file",
//				"com.sun.tools.javac.jvm",
//				"com.sun.tools.javac.main",
						"com.sun.tools.javac.model",
//				"com.sun.tools.javac.parser",
						"com.sun.tools.javac.processing",
						"com.sun.tools.javac.tree",
						"com.sun.tools.javac.util");
				//
				try {
					Method method = Module.class.getDeclaredMethod("implAddExportsToAllUnnamed", String.class);
					AccessibleObjectHandler.setAccessible(method, true);
					//
					for (String packageToExport : packagesToExport) {
						method.invoke(jdkCompilerModule, packageToExport);
					}
				} catch (ReflectiveOperationException ex) {
					throw new RuntimeException(ex);
				}
			}
		}
	}
}
