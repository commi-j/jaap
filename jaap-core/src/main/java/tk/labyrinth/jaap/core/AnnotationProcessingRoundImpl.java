package tk.labyrinth.jaap.core;

import lombok.Value;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * Separation into interface and implementation is made as there is at least one mechanic
 * that require this entity to be proxyable (JUnit5 JaapExtension).
 *
 * @author Commitman
 * @version 1.0.0
 */
@Value
public class AnnotationProcessingRoundImpl implements AnnotationProcessingRound {

	Set<? extends TypeElement> annotations;

	int index;

	ProcessingEnvironment processingEnvironment;

	RoundEnvironment roundEnvironment;
}
