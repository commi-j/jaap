package tk.labyrinth.jaap.core;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * Aggregated data object of one round of annotation processing.
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface AnnotationProcessingRound {

	Set<? extends TypeElement> getAnnotations();

	int getIndex();

	ProcessingEnvironment getProcessingEnvironment();

	RoundEnvironment getRoundEnvironment();

	default boolean isFirst() {
		return getIndex() == 0;
	}

	default boolean isLast() {
		return getRoundEnvironment().processingOver();
	}
}
