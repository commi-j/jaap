package tk.labyrinth.jaap.core.module;

import com.sun.tools.javac.code.BoundKind;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.moduleaccess.ModuleAccessManager;

class ModuleAccessManagerTest {

	@Test
	void testAccessingNotExportedToUnnamedModulesClass() {
		Assertions.assertEquals("?", BoundKind.UNBOUND.toString());
	}

	@BeforeAll
	static void beforeAll() {
		ModuleAccessManager.addExportsToAllUnnamed();
	}
}