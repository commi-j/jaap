package tk.labyrinth.misc4j2.java.lang.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Marker interface to indicate object is a proxy.
 *
 * @author Commitman
 * @version 1.0.0
 * @see DynamicProxyHandler
 * @see InvocationHandler
 * @see Proxy
 */
public interface ProxyMarker {
	// no-op
}
