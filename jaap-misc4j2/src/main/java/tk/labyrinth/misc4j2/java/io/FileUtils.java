package tk.labyrinth.misc4j2.java.io;

import org.apache.commons.io.FilenameUtils;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class FileUtils {

	public static final int NOT_FOUND = -1;

	/**
	 * Returns child File of file.
	 *
	 * @param file non-null
	 * @param name fit
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static File child(@Nullable File file, String name) {
		File result;
		if (file != null) {
			result = new File(file, name);
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * Returns the extension of file.
	 * <pre>
	 * "wat"      -&gt; null
	 * "wat."     -&gt; ""
	 * "wat.java" -&gt; "java"
	 * </pre>
	 *
	 * @param file non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static String getExtension(@Nullable File file) {
		String result;
		if (file != null) {
			String name = file.getName();
			final int index = FilenameUtils.indexOfExtension(name);
			if (index != NOT_FOUND) {
				result = name.substring(index + 1);
			} else {
				result = null;
			}
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * @param file non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static List<String> readLines(File file) {
		return Objects.requireNonNull(IoUtils.tryReturnWithResourceSupplier(
				() -> new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)),
				reader -> reader.lines().collect(Collectors.toList())));
	}
}
