package tk.labyrinth.misc4j2.jaap.misc4j.java.util.regex;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class Regexes {

	/**
	 * <a href="https://en.wikipedia.org/wiki/Newline">https://en.wikipedia.org/wiki/Newline</a><br>
	 * Linux - LF - "\n";<br>
	 * Windows - CRLF - "\r\n";<br>
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String newline() {
		return "\r?\n";
	}
}
