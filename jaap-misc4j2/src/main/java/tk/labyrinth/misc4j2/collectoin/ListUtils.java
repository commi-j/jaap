package tk.labyrinth.misc4j2.collectoin;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.8
 */
public class ListUtils {

	/**
	 * @param first  nullable
	 * @param second nullable
	 * @param <T>    Type
	 *
	 * @return non-empty
	 *
	 * @since 1.0.4
	 */
	public static <T> List<T> collect(@Nullable T first, Stream<T> second) {
		return StreamUtils.concat(first, second).collect(Collectors.toList());
	}

	/**
	 * @param stream nullable
	 * @param <E>    Element
	 *
	 * @return nullable
	 *
	 * @since 1.0.2
	 */
	@Nullable
	public static <E> List<E> collectNullable(@Nullable Stream<E> stream) {
		return stream != null ? stream.collect(Collectors.toList()) : null;
	}

	/**
	 * @param first  non-null
	 * @param second nullable
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 */
	public static <T> List<T> concat(List<T> first, @Nullable T second) {
		Objects.requireNonNull(first, "first");
		//
		return flatten(first, Collections.singletonList(second));
	}

	/**
	 * @param first  nullable
	 * @param second non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 */
	public static <T> List<T> concat(@Nullable T first, List<T> second) {
		Objects.requireNonNull(second, "second");
		//
		return flatten(Collections.singletonList(first), second);
	}

	/**
	 * <a href="https://kotlinlang.org/docs/reference/collection-transformations.html#flattening">https://kotlinlang.org/docs/reference/collection-transformations.html#flattening</a>
	 *
	 * @param lists non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.6
	 */
	@SafeVarargs
	public static <T> List<T> flatten(List<T>... lists) {
		return Stream.of(lists).flatMap(List::stream).collect(Collectors.toList());
	}

	/**
	 * @param iterator non-null
	 * @param <T>      Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> List<T> from(Iterator<T> iterator) {
		Objects.requireNonNull(iterator, "iterator");
		//
		List<T> result = new ArrayList<>();
		iterator.forEachRemaining(result::add);
		return result;
	}

	/**
	 * @param enumeration non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> List<T> from(Enumeration<T> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		//
		return from(IteratorUtils.from(enumeration));
	}

	/**
	 * @param stream nullable
	 * @param <T>    Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.8
	 */
	@Nullable
	public static <T> List<T> fromNullable(@Nullable Stream<T> stream) {
		return stream != null ? stream.collect(Collectors.toList()) : null;
	}

	/**
	 * @param vararg nullable
	 * @param <T>    Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.8
	 */
	@Nullable
	@SafeVarargs
	public static <T> List<T> fromNullable(@Nullable T... vararg) {
		return vararg != null ? List.of(vararg) : null;
	}

	/**
	 * @param list        non-null
	 * @param mapFunction non-null, never receives nulls
	 * @param <T>         Type
	 * @param <R>         Result
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static <T, R> List<R> mapNonnull(List<T> list, Function<T, R> mapFunction) {
		return mapNullable(list, element -> element != null ? mapFunction.apply(element) : null);
	}

	/**
	 * Returns List consisting of results of applying mapFunction to elements of provided list.
	 *
	 * @param list        non-null
	 * @param mapFunction non-null, may receive nulls
	 * @param <T>         Type
	 * @param <R>         Result
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static <T, R> List<R> mapNullable(List<T> list, Function<T, R> mapFunction) {
		return list.stream().map(mapFunction).collect(Collectors.toList());
	}

	/**
	 * Returns List consisting of results of applying {@link Object#toString()} to elements of provided list.
	 *
	 * @param list non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static List<String> mapToString(List<?> list) {
		return mapNullable(list, Objects::toString);
	}

	/**
	 * @param element nullable
	 * @param count   number of times to repeat
	 * @param <T>     Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.6
	 */
	public static <T> List<T> repeat(@Nullable T element, int count) {
		return IntStream.range(0, count).mapToObj(index -> element).collect(Collectors.toList());
	}
}
