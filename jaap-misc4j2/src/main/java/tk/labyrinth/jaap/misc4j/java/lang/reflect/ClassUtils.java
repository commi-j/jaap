package tk.labyrinth.jaap.misc4j.java.lang.reflect;

import tk.labyrinth.jaap.misc4j.java.lang.JavaConstants;

import javax.annotation.CheckForNull;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.5
 */
public class ClassUtils {

	/**
	 * @param value non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> T castInferred(Object value) {
		return castNullableInferred(Objects.requireNonNull(value, "value"));
	}

	/**
	 * @param value nullable
	 * @param <T>   Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@CheckForNull
	@SuppressWarnings("unchecked")
	public static <T> T castNullableInferred(@CheckForNull Object value) {
		return (T) value;
	}

	/**
	 * @param classBinaryName non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@CheckForNull
	public static Class<?> find(String classBinaryName) {
		Class<?> result;
		{
			Class<?> keywordClass = JavaConstants.KEYWORDS.get(classBinaryName);
			if (keywordClass != null) {
				result = keywordClass;
			} else {
				try {
					result = Class.forName(classBinaryName);
				} catch (ClassNotFoundException ex) {
					result = null;
				}
			}
		}
		return result;
	}

	/**
	 * @param classBinaryName non-null
	 * @param <T>             Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@CheckForNull
	@SuppressWarnings("unchecked")
	public static <T> Class<T> findInferred(String classBinaryName) {
		return (Class<T>) find(classBinaryName);
	}

	/**
	 * Binary names: https://docs.oracle.com/javase/specs/jls/se8/html/jls-13.html#jls-13.1
	 *
	 * @param classBinaryName non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Class<?> get(String classBinaryName) {
		Class<?> result = find(classBinaryName);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: classBinaryName = " + classBinaryName);
			}
		}
		return result;
	}

	/**
	 * @param classBinaryName non-null
	 * @param <T>             Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> Class<T> getInferred(String classBinaryName) {
		Class<T> result = findInferred(classBinaryName);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: classBinaryName = " + classBinaryName);
			}
		}
		return result;
	}

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String getLongName(Class<?> cl) {
		return cl.getEnclosingClass() != null
				? getLongName(cl.getEnclosingClass()) + "." + cl.getSimpleName()
				: cl.getSimpleName();
	}

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String getSignature(Class<?> cl) {
		String result;
		if (isArray(cl)) {
			result = getSignature(cl.getComponentType()) + "[]";
		} else {
			if (!isKeyword(cl)) {
				String packageName = cl.getPackageName();
				//
				result = !packageName.isEmpty()
						? packageName + ":" + getLongName(cl)
						: getLongName(cl);
			} else {
				// Keyword types have no packages or enclosing types.
				result = cl.getSimpleName();
			}
		}
		return result;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if array, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isArray(@CheckForNull Class<?> cl) {
		return cl != null && cl.isArray();
	}

	/**
	 * Generic class is the one that has type parameters, e.g. {@link List}.<br>
	 * Counterpart of generic classes are plain classes.<br>
	 *
	 * @param javaClass nullable
	 *
	 * @return true if generic, false otherwise
	 *
	 * @see #isPlain(Class)
	 * @since 1.0.3
	 */
	public static boolean isGeneric(@CheckForNull Class<?> javaClass) {
		return javaClass != null && javaClass.getTypeParameters().length > 0;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if keyword, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isKeyword(@CheckForNull Class<?> cl) {
		return isPrimitive(cl) || isVoid(cl);
	}

	/**
	 * Generic class is the one that has no type parameters, e.g. {@link String}.<br>
	 * Counterpart of plain classes are generic classes.<br>
	 *
	 * @param javaClass nullable
	 *
	 * @return true if plain, false otherwise
	 *
	 * @see #isGeneric(Class)
	 * @since 1.0.2
	 */
	public static boolean isPlain(@CheckForNull Class<?> javaClass) {
		return javaClass != null && javaClass.getTypeParameters().length == 0;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if primitive, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isPrimitive(@CheckForNull Class<?> cl) {
		return cl != null && cl.isPrimitive();
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if void, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isVoid(@CheckForNull Class<?> cl) {
		return cl == void.class;
	}

	/**
	 * Integer, Number -> Number;<br>
	 * Number, Integer -> Number;<br>
	 * Integer, String -> null;<br>
	 *
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.4
	 */
	@CheckForNull
	public static Class<?> pickSuperclass(Class<?> first, Class<?> second) {
		Class<?> result;
		{
			if (first.isAssignableFrom(second)) {
				result = first;
			} else if (second.isAssignableFrom(first)) {
				result = second;
			} else {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param javaClass non-null
	 *
	 * @return javaClass
	 *
	 * @since 1.0.2
	 */
	public static Type requirePlain(Class<?> javaClass) {
		if (!isPlain(javaClass)) {
			throw new IllegalArgumentException("Require plain: %s".formatted(javaClass));
		}
		return javaClass;
	}

	/**
	 * Unwraps proxies.
	 *
	 * @param object non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> Class<T> resolveClass(T object) {
		Class<?> result;
		{
			if (object instanceof Proxy) {
				result = object.getClass().getInterfaces()[0];
			} else {
				result = object.getClass();
			}
		}
		return castInferred(result);
	}
}
