# Java Advanced Annotation Processing

[![RELEASES](https://img.shields.io/maven-central/v/tk.labyrinth.jaap/jaap.svg?label=Releases&style=flat-square)](https://mvnrepository.com/artifact/tk.labyrinth.jaap/jaap)
[![PIPELINE_MASTER](https://gitlab.com/commi-j/jaap/badges/master/pipeline.svg?key_text=Master&key_width=50&style=flat-square)](https://gitlab.com/commitman/jaap/-/pipelines)
[![TEST_COVERAGE_MASTER](https://gitlab.com/commi-j/jaap/badges/master/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/jaap/-/jobs)

[![SNAPSHOTS](https://img.shields.io/nexus/s/tk.labyrinth.jaap/jaap?label=Snapshots&server=https%3A%2F%2Foss.sonatype.org&style=flat-square)](https://oss.sonatype.org/#nexus-search;gav~tk.labyrinth.jaap~jaap~~~)
[![PIPELINE_DEV](https://gitlab.com/commi-j/jaap/badges/dev/pipeline.svg?key_text=Dev&key_width=35&style=flat-square)](https://gitlab.com/commitman/jaap/-/pipelines)
[![TEST_COVERAGE_DEV](https://gitlab.com/commi-j/jaap/badges/dev/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/jaap/-/jobs)

[![LICENSE](https://img.shields.io/gitlab/license/commi-j/jaap?color=q&label=License&style=flat-square)](LICENSE)

Utility library to work with <i>javax.annotation.processing</i> & <i>javax.lang.model</i> Classes.

<b>Note: This API is highly experimental and most probably will contain breaking changes with almost each release.</b>

## Maven Dependency

```xml

<dependency>
	<groupId>tk.labyrinth.jaap</groupId>
	<artifactId>jaap</artifactId>
	<version>0.5.1-SNAPSHOT</version>
</dependency>
```

## Quick Start

See examples below:

- [ClassicAnnotationProcessorExample](jaap-examples/src/main/java/tk/labyrinth/jaap/examples/ClassicAnnotationProcessorExample.java)
- [CallbackAnnotationProcessorExample](jaap-examples/src/main/java/tk/labyrinth/jaap/examples/CallbackAnnotationProcessorExample.java)

### Key Concept

JAAP introduces three main type hierarchies:

- ElementHandles - wrappers for java elements like classes, fields, methods etc.;
- TypeHandles - wrappers for java types like T, List<String>, <? extends Number> etc.;
- AnnotationHandles - wrappers for java annotations;

TODO: Describe all hierarchy.

### Entry Classes

TODO: Fill or remove?

### Working with javax.lang.model utils

- If you need to obtain instance of some type (like TypeMirror):
    - Look for class with name TypeMirrorUtils;
    - If you have some instances in current scope, look for their respective *Utils classes;

### Contacts

- Telegram: [commi-dev](https://t.me/commi_dev)