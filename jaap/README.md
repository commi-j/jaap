# Glossary

### Names

- Full - qualified
- Long - without package (but with enclosing classes)
- Simple - without enclosing class

#### TypeNames

- TypeBinaryName (JLS) - java.util.Map$Entry
- TypeCanonicalName (JLS) - java.util.Map.Entry
- TypeSimpleName (JLS) - Entry
- TypeFullName (JAAP) - TypeBinaryName || TypeCanonicalName
- TypeLongName (JAAP) - Map$Entry || Map.Entry
