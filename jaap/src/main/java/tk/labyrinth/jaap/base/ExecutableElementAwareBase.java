package tk.labyrinth.jaap.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;

import javax.lang.model.element.ExecutableElement;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
@EqualsAndHashCode
@Getter
public abstract class ExecutableElementAwareBase extends ProcessingContextAwareBase {

	private final ExecutableElement executableElement;

	public ExecutableElementAwareBase(ProcessingContext processingContext, ExecutableElement executableElement) {
		super(processingContext);
		this.executableElement = Objects.requireNonNull(executableElement, "executableElement");
	}
}
