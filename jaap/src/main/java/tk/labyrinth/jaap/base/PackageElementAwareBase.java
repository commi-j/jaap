package tk.labyrinth.jaap.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;

import javax.lang.model.element.PackageElement;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Deprecated
@EqualsAndHashCode
@Getter
public abstract class PackageElementAwareBase extends ProcessingContextAwareBase {

	private final PackageElement packageElement;

	public PackageElementAwareBase(ProcessingContext processingContext, PackageElement packageElement) {
		super(processingContext);
		this.packageElement = Objects.requireNonNull(packageElement, "packageElement");
	}
}
