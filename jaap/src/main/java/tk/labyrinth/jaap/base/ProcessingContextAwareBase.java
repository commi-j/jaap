package tk.labyrinth.jaap.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;

import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
@EqualsAndHashCode
@Getter
public abstract class ProcessingContextAwareBase implements HasProcessingContext {

	@EqualsAndHashCode.Exclude
	private final ProcessingContext processingContext;

	public ProcessingContextAwareBase(ProcessingContext processingContext) {
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
	}
}
