package tk.labyrinth.jaap.annotation.impl;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationAnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeElementHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelMethodElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;

import javax.annotation.Nullable;
import javax.lang.model.element.AnnotationValue;

@EqualsAndHashCode
@RequiredArgsConstructor
public class DefaultAnnotationTypeElementHandle implements AnnotationTypeElementHandle {

	private final MethodElementHandle methodElementHandle;

	@Override
	public AnnotationAnnotationTypeElementHandle asAnnotationListElement() {
		if (!isAnnotationListElement()) {
			throw new IllegalArgumentException(toString());
		}
		return new DefaultAnnotationListAnnotationTypeElementHandle(methodElementHandle);
	}

	@Nullable
	@Override
	public String findDefaultValueAsString() {
		// FIXME: Get rid of this cast.
		AnnotationValue defaultValue = ((LangmodelMethodElementHandle) methodElementHandle).getExecutableElement().getDefaultValue();
		return defaultValue != null ? (String) defaultValue.getValue() : null;
	}

	@Override
	public String getDefaultValueAsString() {
		String result = findDefaultValueAsString();
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"this = " + this);
		}
		return result;
	}

	@Override
	public String getName() {
		return methodElementHandle.getName();
	}

	@Override
	public boolean isAnnotationListElement() {
		boolean result;
		{
			TypeHandle returnTypeHandle = methodElementHandle.getReturnType();
			if (returnTypeHandle.isArrayType()) {
				result = returnTypeHandle.asArrayType().getComponentType().isAnnotationType();
			} else {
				result = false;
			}
		}
		return result;
	}
}
