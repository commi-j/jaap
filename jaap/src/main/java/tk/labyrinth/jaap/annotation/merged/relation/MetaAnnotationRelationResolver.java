package tk.labyrinth.jaap.annotation.merged.relation;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;

import java.util.List;

public class MetaAnnotationRelationResolver implements AnnotationRelationResolver {

	@Override
	public List<UnweightedAnnotationRelation> resolve(
			MergedAnnotationNode node,
			AnnotationTypeHandle annotationTypeHandle) {
		List<UnweightedAnnotationRelation> result;
		if (node.getAnnotationHandle() != null) {
			//
			// This is a tricky move - we just expose this annotation's type as another annotation owner to process.
			result = List.of(new UnweightedAnnotationRelation(
					false,
					this,
					"MetaAnnotation",
					node,
					MergedAnnotationNode.of(node.getAnnotationHandle().getType().toElement().asElement())));
		} else {
			result = List.of();
		}
		return result;
	}
}
