package tk.labyrinth.jaap.annotation.merged;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;
import java.util.List;

public interface MergedAnnotationAttribute {

	@Nullable
	Object findValue();

	String getName();

	Object getValue();

	List<AnnotationHandle> getValueAsAnnotationList();

	List<TypeHandle> getValueAsClassList();

	String getValueAsString();

	List<String> getValueAsStringList();

	boolean hasValue();
}
