package tk.labyrinth.jaap.annotation.merged.relation;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;

import java.util.List;
import java.util.stream.Collectors;

public class RepeatableAnnotationRelationResolver implements AnnotationRelationResolver {

	@Override
	public List<UnweightedAnnotationRelation> resolve(
			MergedAnnotationNode node,
			AnnotationTypeHandle annotationTypeHandle) {
		List<UnweightedAnnotationRelation> result;
		if (node.getAnnotationHandle() != null) {
			AnnotationHandle annotationHandle = node.getAnnotationHandle();
			result = annotationHandle.getType().isRepeater()
					? annotationHandle.getValueAsAnnotationList("value").stream()
					.map(innerAnnotationHandle -> new UnweightedAnnotationRelation(
							innerAnnotationHandle.isOfType(annotationTypeHandle),
							this,
							"Repeatable",
							MergedAnnotationNode.of(annotationHandle),
							MergedAnnotationNode.of(innerAnnotationHandle)))
					.collect(Collectors.toList())
					: List.of();
		} else {
			result = List.of();
		}
		return result;
	}
}
