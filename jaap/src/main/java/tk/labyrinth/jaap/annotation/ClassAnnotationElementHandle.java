package tk.labyrinth.jaap.annotation;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
 */
public interface ClassAnnotationElementHandle<T> {

	AnnotationElementHandle asAnnotation();

	ClassAnnotationTypeElementHandle getDescription();

	/**
	 * ArrayType or PlainType or PrimitiveType or RawType or VoidType.
	 * int[].class
	 * String.class
	 * int.class
	 * List.class
	 * void.class
	 *
	 * @return non-null
	 */
	T getValue();
}
