package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
 */
public interface EnumAnnotationTypeElementHandle {

	AnnotationElementHandle asAnnotation();

	TypeElementHandle getType();
}
