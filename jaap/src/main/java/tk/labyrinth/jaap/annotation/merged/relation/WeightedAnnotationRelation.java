package tk.labyrinth.jaap.annotation.merged.relation;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class WeightedAnnotationRelation {

	UnweightedAnnotationRelation unweighted;

	BigDecimal weight;
}
