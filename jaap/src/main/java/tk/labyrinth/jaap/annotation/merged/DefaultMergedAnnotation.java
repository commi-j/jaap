package tk.labyrinth.jaap.annotation.merged;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.relation.MergedAnnotationNode;
import tk.labyrinth.jaap.annotation.merged.relation.MergedAnnotationNodePath;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.ElementHandle;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultMergedAnnotation implements MergedAnnotation {

	private final AnnotationTypeHandle annotationTypeHandle;

	private final List<MergedAnnotationNodePath> certainPaths;

	private final ElementHandle elementHandle;

	@Override
	public MergedAnnotationAttribute getAttribute(String attributeName) {
		if (!isPresent()) {
			throw new IllegalStateException("Not present: " + this);
		}
		return new DefaultMergedAnnotationAttribute(annotationTypeHandle, certainPaths, attributeName);
	}

	@Override
	public List<TypeHandle> getAttributeValueAsClassList(String attributeName) {
		return getAttribute(attributeName).getValueAsClassList();
	}

	@Override
	public String getAttributeValueAsString(String attributeName) {
		return getAttribute(attributeName).getValueAsString();
	}

	@Override
	public List<String> getAttributeValueAsStringList(String attributeName) {
		return getAttribute(attributeName).getValueAsStringList();
	}

	@Override
	public List<MergedAnnotationAttribute> getAttributes() {
		if (!isPresent()) {
			throw new IllegalStateException("Not present: " + this);
		}
		return annotationTypeHandle.getElementDescriptions().stream()
				.map(elementDescription -> new DefaultMergedAnnotationAttribute(
						annotationTypeHandle,
						certainPaths,
						elementDescription.getName()))
				.filter(MergedAnnotationAttribute::hasValue)
				.collect(Collectors.toList());
	}

	@Override
	public ElementHandle getParent() {
		return elementHandle;
	}

	@Override
	public List<AnnotationHandle> getRelevantAnnotations() {
		return certainPaths.stream()
				.map(MergedAnnotationNodePath::getLastNode)
				.map(MergedAnnotationNode::getAnnotationHandle)
				.collect(Collectors.toList());
	}

	@Override
	public String getSignatureString() {
		String result;
		if (elementHandle.isMethodElement()) {
			// TODO: Annotation should use signature too.
			result = elementHandle.asMethodElement().getSignatureString() + annotationTypeHandle;
		} else if (elementHandle.isTypeElement()) {
			// TODO: Annotation should use signature too.
			result = elementHandle.asTypeElement().getSignatureString() + annotationTypeHandle;
		} else {
			// TODO: Field and other, less popular elements.
			throw new NotImplementedException(toString());
		}
		return result;
	}

	@Override
	public AnnotationTypeHandle getType() {
		return annotationTypeHandle;
	}

	@Override
	public boolean isPresent() {
		return !certainPaths.isEmpty();
	}
}
