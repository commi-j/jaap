package tk.labyrinth.jaap.annotation.merged;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;

import java.util.List;

/**
 * Lookup strategies:<br>
 * - Meta annotations (push) (more annotations);
 * - Annotation referencing (pull) (more annotations);
 * - Method overrides (more targets);
 * - Class inheritance (more targets);
 * - "Property" - fields, getters, setters (more targets);
 */
public interface MergedAnnotation {

	MergedAnnotationAttribute getAttribute(String attributeName);

	List<TypeHandle> getAttributeValueAsClassList(String attributeName);

	String getAttributeValueAsString(String attributeName);

	List<String> getAttributeValueAsStringList(String attributeName);

	/**
	 * Returns only attributes for which value is present.
	 * If you need all attributes use {@link AnnotationTypeHandle#getElementDescriptions()}.
	 *
	 * @return non-null
	 */
	List<MergedAnnotationAttribute> getAttributes();

	ElementHandle getParent();

	List<AnnotationHandle> getRelevantAnnotations();

	String getSignatureString();

	AnnotationTypeHandle getType();

	boolean isPresent();
}
