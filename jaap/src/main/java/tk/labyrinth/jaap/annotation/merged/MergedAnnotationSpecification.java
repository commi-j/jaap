package tk.labyrinth.jaap.annotation.merged;

import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.annotation.merged.relation.AnnotationRelationResolver;
import tk.labyrinth.jaap.annotation.merged.relation.DirectPresenceAnnotationRelationResolver;
import tk.labyrinth.jaap.annotation.merged.relation.MetaAnnotationRelationResolver;
import tk.labyrinth.jaap.annotation.merged.relation.RepeatableAnnotationRelationResolver;
import tk.labyrinth.jaap.misc4j.collectoin.SetUtils;

import java.math.BigDecimal;
import java.util.Set;

@Value
public class MergedAnnotationSpecification {

	Set<Pair<BigDecimal, AnnotationRelationResolver>> weightedRelationResolvers;

	public MergedAnnotationSpecification withMetaAnnotation() {
		return withMetaAnnotation(new BigDecimal("0.5"));
	}

	public MergedAnnotationSpecification withMetaAnnotation(BigDecimal weight) {
		return new MergedAnnotationSpecification(SetUtils.append(
				weightedRelationResolvers,
				Pair.of(weight, new MetaAnnotationRelationResolver())));
	}

	public static MergedAnnotationSpecification javaCore() {
		return new MergedAnnotationSpecification(Set.of(
				Pair.of(new BigDecimal("0.01"), new DirectPresenceAnnotationRelationResolver()),
				Pair.of(new BigDecimal("0.1"), new RepeatableAnnotationRelationResolver())));
	}

	public static MergedAnnotationSpecification metaAnnotation() {
		return javaCore().withMetaAnnotation();
	}
}
