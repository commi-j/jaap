package tk.labyrinth.jaap.annotation.merged.relation;

import lombok.Value;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;

@Value
public class MergedAnnotationNodePath {

	List<WeightedAnnotationRelation> relations;

	public MergedAnnotationNode getLastNode() {
		return relations.get(relations.size() - 1).getUnweighted().getTo();
	}

	public static MergedAnnotationNodePath of(
			MergedAnnotationNodePath mergedAnnotationNodePath,
			WeightedAnnotationRelation weightedAnnotationRelation) {
		return new MergedAnnotationNodePath(ListUtils.concat(
				mergedAnnotationNodePath.getRelations(),
				weightedAnnotationRelation));
	}

	public static MergedAnnotationNodePath of(WeightedAnnotationRelation weightedAnnotationRelation) {
		return new MergedAnnotationNodePath(List.of(weightedAnnotationRelation));
	}
}
