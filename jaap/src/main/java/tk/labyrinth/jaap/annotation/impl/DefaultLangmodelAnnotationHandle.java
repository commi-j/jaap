package tk.labyrinth.jaap.annotation.impl;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.util.AnnotationMirrorUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.ExecutableElement;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Value
public class DefaultLangmodelAnnotationHandle implements AnnotationHandle {

	AnnotationMirror annotationMirror;

	AnnotatedConstruct parent;

	ProcessingContext processingContext;

	@Nullable
	@Override
	public AnnotationElementHandle findAttribute(String attributeName) {
		{
			// Fails if attributeName is unknown for this annotation.
			ExecutableElement attributeExecutableElement = TypeElementUtils.getDeclaredMethod(
					getProcessingContext().getProcessingEnvironment(),
					ElementUtils.requireType(annotationMirror.getAnnotationType().asElement()),
					attributeName + "()");
		}
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public Object findValue(String attributeName) {
		// TODO: Rework with proper type handling.
		//
		Object result;
		{
			Object value = AnnotationMirrorUtils.findValue(annotationMirror, attributeName);
			if (value != null) {
				result = value;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public String findValueAsString(String attributeName) {
		return AnnotationMirrorUtils.findValueAsString(annotationMirror, attributeName);
	}

	@Override
	public String getSignatureString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public AnnotationTypeHandle getType() {
		return processingContext.getAnnotationTypeHandle(annotationMirror.getAnnotationType());
	}

	@Override
	public Object getValue(String attributeName) {
		return AnnotationMirrorUtils.getValue(annotationMirror, attributeName);
	}

	@Override
	public List<AnnotationHandle> getValueAsAnnotationList(String attributeName) {
		return AnnotationMirrorUtils.getValueAsAnnotationMirrorList(annotationMirror, attributeName).stream()
				.map(annotationMirror -> processingContext.getAnnotationHandle(parent, annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public TypeHandle getValueAsClass(String attributeName) {
		return processingContext.getTypeHandle(
				GenericContext.empty(),
				AnnotationMirrorUtils.getValueAsTypeMirror(annotationMirror, attributeName));
	}

	@Override
	public List<TypeHandle> getValueAsClassList(String attributeName) {
		return AnnotationMirrorUtils.getValueAsTypeMirrorList(annotationMirror, attributeName).stream()
				.map(typeMirror -> processingContext.getTypeHandle(GenericContext.empty(), typeMirror))
				.collect(Collectors.toList());
	}

	@Override
	public String getValueAsString(String attributeName) {
		return AnnotationMirrorUtils.getValueAsString(annotationMirror, attributeName);
	}

	@Override
	public List<String> getValueAsStringList(String attributeName) {
		return AnnotationMirrorUtils.getValueAsStringList(annotationMirror, attributeName);
	}

	@Override
	public String toString() {
		return "@" + annotationMirror.getAnnotationType().toString() +
				(!annotationMirror.getElementValues().isEmpty()
						? annotationMirror.getElementValues().entrySet().stream()
						.map(entry -> entry.getKey().getSimpleName() + "=" + entry.getValue())
						.collect(Collectors.joining(",", "(", ")"))
						: "");
	}
}
