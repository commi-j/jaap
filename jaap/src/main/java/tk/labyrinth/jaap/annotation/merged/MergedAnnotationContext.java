package tk.labyrinth.jaap.annotation.merged;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.relation.AnnotationRelationResolver;
import tk.labyrinth.jaap.annotation.merged.relation.MergedAnnotationNode;
import tk.labyrinth.jaap.annotation.merged.relation.MergedAnnotationNodePath;
import tk.labyrinth.jaap.annotation.merged.relation.WeightedAnnotationRelation;
import tk.labyrinth.jaap.model.element.ElementHandle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Value
public class MergedAnnotationContext {

	AnnotationTypeHandle annotationTypeHandle;

	ElementHandle elementHandle;

	MergedAnnotationSpecification specification;

	private List<WeightedAnnotationRelation> resolveNode(MergedAnnotationNode node) {
		return specification.getWeightedRelationResolvers().stream()
				.flatMap(weightedRelationResolver -> {
					BigDecimal weight = weightedRelationResolver.getLeft();
					AnnotationRelationResolver relationResolver = weightedRelationResolver.getRight();
					//
					return relationResolver.resolve(node, annotationTypeHandle).stream()
							.map(unweightedAnnotationRelation -> new WeightedAnnotationRelation(
									unweightedAnnotationRelation,
									weight));
				})
				.collect(Collectors.toList());
	}

	public MergedAnnotation getMergedAnnotation() {
		List<MergedAnnotationNodePath> certainPaths = new ArrayList<>();
		List<MergedAnnotationNodePath> pathsToProcess = new ArrayList<>();
		{
			resolveNode(MergedAnnotationNode.of(elementHandle))
					.forEach(weightedAnnotationRelation -> {
						MergedAnnotationNodePath newPath = MergedAnnotationNodePath.of(weightedAnnotationRelation);
						if (weightedAnnotationRelation.getUnweighted().isCertain()) {
							certainPaths.add(newPath);
						}
						pathsToProcess.add(newPath);
					});
		}
		while (!pathsToProcess.isEmpty()) {
			MergedAnnotationNodePath path = pathsToProcess.remove(0);
			MergedAnnotationNode node = path.getLastNode();
			//
			resolveNode(node).stream()
					//
					// Recursion guard.
					.filter(weightedAnnotationRelation -> !path.getRelations().contains(weightedAnnotationRelation))
					.forEach(weightedAnnotationRelation -> {
						MergedAnnotationNodePath newPath = MergedAnnotationNodePath.of(
								path,
								weightedAnnotationRelation);
						if (weightedAnnotationRelation.getUnweighted().isCertain()) {
							certainPaths.add(newPath);
						}
						pathsToProcess.add(newPath);
					});
		}
		return new DefaultMergedAnnotation(annotationTypeHandle, certainPaths, elementHandle);
	}
}
