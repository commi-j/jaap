package tk.labyrinth.jaap.annotation.merged.relation;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;

@Value
public class MergedAnnotationNode {

	AnnotationHandle annotationHandle;

	ElementHandle elementHandle;

	public static MergedAnnotationNode of(AnnotationHandle annotationHandle) {
		return new MergedAnnotationNode(annotationHandle, null);
	}

	public static MergedAnnotationNode of(ElementHandle elementHandle) {
		return new MergedAnnotationNode(null, elementHandle);
	}
}
