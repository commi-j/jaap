package tk.labyrinth.jaap.annotation.merged.relation;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;

import java.util.List;
import java.util.stream.Collectors;

public class DirectPresenceAnnotationRelationResolver implements AnnotationRelationResolver {

	@Override
	public List<UnweightedAnnotationRelation> resolve(
			MergedAnnotationNode node,
			AnnotationTypeHandle annotationTypeHandle) {
		List<UnweightedAnnotationRelation> result;
		if (node.getElementHandle() != null) {
			ElementHandle elementHandle = node.getElementHandle();
			result = elementHandle.getDirectAnnotations().stream()
					.map(annotationHandle -> new UnweightedAnnotationRelation(
							annotationHandle.isOfType(annotationTypeHandle),
							this,
							"DirectPresence",
							MergedAnnotationNode.of(elementHandle),
							MergedAnnotationNode.of(annotationHandle)))
					.collect(Collectors.toList());
		} else {
			result = List.of();
		}
		return result;
	}
}
