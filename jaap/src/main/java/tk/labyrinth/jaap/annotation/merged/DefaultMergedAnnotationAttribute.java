package tk.labyrinth.jaap.annotation.merged;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.relation.MergedAnnotationNodePath;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultMergedAnnotationAttribute implements MergedAnnotationAttribute {

	private final AnnotationTypeHandle annotationTypeHandle;

	private final List<MergedAnnotationNodePath> certainPaths;

	private final String name;

	@Nullable
	@Override
	public Object findValue() {
		return certainPaths.stream()
				.map(path -> path.getLastNode().getAnnotationHandle())
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.map(annotationHandle -> annotationHandle.findValue(name))
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getValue() {
		Object result = findValue();
		// TODO: Think about defaultValue of AnnotationTypeElementHandle.
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}

	@Override
	public List<AnnotationHandle> getValueAsAnnotationList() {
		return certainPaths.stream()
				.map(path -> path.getLastNode().getAnnotationHandle())
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.map(annotationHandle -> annotationHandle.getValueAsAnnotationList(name))
				.findFirst()
				// TODO: DefaultValue of AnnotationTypeElementHandle
				.orElse(null);
	}

	@Override
	public List<TypeHandle> getValueAsClassList() {
		return certainPaths.stream()
				.map(path -> path.getLastNode().getAnnotationHandle())
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.map(annotationHandle -> annotationHandle.getValueAsClassList(name))
				.findFirst()
				// TODO: DefaultValue of AnnotationTypeElementHandle
				.orElse(null);
	}

	@Override
	public String getValueAsString() {
		List<Pair<AnnotationHandle, String>> contributingAnnotationHandles = certainPaths.stream()
				.map(path -> path.getLastNode().getAnnotationHandle())
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.map(annotationHandle -> {
					String value = annotationHandle.findValueAsString(name);
					return value != null ? Pair.of(annotationHandle, value) : null;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		return contributingAnnotationHandles.stream()
				.findFirst()
				.map(Pair::getRight)
				.orElseGet(() -> annotationTypeHandle.getElementDescription(name).getDefaultValueAsString());
	}

	@Override
	public List<String> getValueAsStringList() {
		return certainPaths.stream()
				.map(path -> path.getLastNode().getAnnotationHandle())
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.map(annotationHandle -> annotationHandle.getValueAsStringList(name))
				.findFirst()
				// TODO: DefaultValue of AnnotationTypeElementHandle
				.orElse(null);
	}

	@Override
	public boolean hasValue() {
		return findValue() != null;
	}
}
