package tk.labyrinth.jaap.annotation.impl;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationAnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelMethodElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;

@EqualsAndHashCode
@RequiredArgsConstructor
public class DefaultAnnotationListAnnotationTypeElementHandle implements AnnotationAnnotationTypeElementHandle {

	private final MethodElementHandle methodElementHandle;

	@Override
	public AnnotationTypeElementHandle asAnnotation() {
		return new DefaultAnnotationTypeElementHandle(methodElementHandle);
	}

	@Override
	public AnnotationTypeHandle getType() {
		return methodElementHandle.getReturnType().asArrayType().getComponentType().asAnnotationType();
	}
}
