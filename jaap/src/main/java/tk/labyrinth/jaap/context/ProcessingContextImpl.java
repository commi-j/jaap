package tk.labyrinth.jaap.context;

import lombok.Getter;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.impl.DefaultAnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.impl.DefaultLangmodelAnnotationHandle;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactory;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;
import java.util.Objects;

public class ProcessingContextImpl implements ProcessingContext {

	@Getter
	private final LangmodelElementHandleFactory elementHandleFactory;

	@Getter
	private final ProcessingEnvironment processingEnvironment;

	@Getter
	private final LangmodelTypeHandleFactory typeHandleFactory;

	@Getter
	private final LangmodelTypeMirrorFactory typeMirrorFactory;

	public ProcessingContextImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			ProcessingEnvironment processingEnvironment,
			LangmodelTypeHandleFactory typeHandleFactory,
			LangmodelTypeMirrorFactory typeMirrorFactory) {
		this.elementHandleFactory = elementHandleFactory;
		this.processingEnvironment = processingEnvironment;
		this.typeHandleFactory = typeHandleFactory;
		this.typeMirrorFactory = typeMirrorFactory;
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclaredTypeHandle(String typeSignatureString) {
		return typeHandleFactory.findDeclared(typeSignatureString);
	}

	@Nullable
	@Override
	public MethodElementHandle findMethodElementHandle(MethodFullSignature methodFullSignature) {
		return elementHandleFactory.findMethod(methodFullSignature);
	}

	@Nullable
	@Override
	public TypeElementHandle findTypeElementHandle(TypeSignature typeSignature) {
		return elementHandleFactory.findType(typeSignature);
	}

	@Nullable
	@Override
	public TypeHandle findTypeHandle(String typeDescriptionString) {
		return typeHandleFactory.find(typeDescriptionString);
	}

	@Nullable
	@Override
	public TypeHandle findTypeHandle(TypeDescription typeDescription) {
		return typeHandleFactory.find(typeDescription);
	}

	@Nullable
	@Override
	public TypeParameterElementHandle findTypeParameterElementHandle(Class<?> type, String typeParameterSimpleName) {
		return elementHandleFactory.findTypeParameter(type, typeParameterSimpleName);
	}

	@Nullable
	@Override
	public TypeParameterElementHandle findTypeParameterElementHandle(String typeParameterSignature) {
		return elementHandleFactory.findTypeParameter(typeParameterSignature);
	}

	@Override
	public AnnotationHandle getAnnotationHandle(AnnotatedConstruct parent, AnnotationMirror annotationMirror) {
		Objects.requireNonNull(parent, "parent");
		Objects.requireNonNull(annotationMirror, "annotationMirror");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultLangmodelAnnotationHandle(annotationMirror, parent, this);
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(Class<? extends Annotation> annotationType) {
		Objects.requireNonNull(annotationType, "annotationType");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementHandle(annotationType));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(DeclaredType annotationDeclaredType) {
		Objects.requireNonNull(annotationDeclaredType, "annotationDeclaredType");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementHandle(annotationDeclaredType));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(String annotationTypeName) {
		Objects.requireNonNull(annotationTypeName, "annotationTypeName");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementHandle(annotationTypeName));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(TypeMirror annotationTypeMirror) {
		return getAnnotationTypeHandle(TypeMirrorUtils.requireDeclaredType(annotationTypeMirror));
	}

	@Override
	public ArrayTypeHandle getArrayTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getArray(genericContext, typeMirror);
	}

	@Override
	public ConstructorElementHandle getConstructorElementHandle(Element element) {
		return elementHandleFactory.getConstructor(element);
	}

	@Override
	public ConstructorElementHandle getConstructorElementHandle(ExecutableElement executableElement) {
		return elementHandleFactory.getConstructor(executableElement);
	}

	@Override
	public DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeElement typeElement) {
		return typeHandleFactory.getDeclared(genericContext, typeElement);
	}

	@Override
	public DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getDeclared(genericContext, typeMirror);
	}

	@Override
	public ElementHandle getElementHandle(Class<?> type) {
		return elementHandleFactory.get(type);
	}

	@Override
	public ElementHandle getElementHandle(Element element) {
		return elementHandleFactory.get(element);
	}

	@Override
	public ElementHandle getElementHandle(String fullElementSignature) {
		return elementHandleFactory.get(fullElementSignature);
	}

	@Override
	public FieldElementHandle getFieldElementHandle(Class<?> type, String fieldSimpleName) {
		return elementHandleFactory.getField(type, fieldSimpleName);
	}

	@Override
	public FieldElementHandle getFieldElementHandle(Element element) {
		return elementHandleFactory.getField(element);
	}

	@Override
	public FieldElementHandle getFieldElementHandle(String fieldFullName) {
		return elementHandleFactory.getField(fieldFullName);
	}

	@Override
	public FieldElementHandle getFieldElementHandle(VariableElement variableElement) {
		return elementHandleFactory.getField(variableElement);
	}

	@Override
	public FormalParameterElementHandle getFormalParameterElementHandle(VariableElement variableElement) {
		return elementHandleFactory.getFormalParameter(variableElement);
	}

	@Override
	public GenericTypeHandle getGenericTypeHandle(GenericContext genericContext, DeclaredType declaredType) {
		return typeHandleFactory.getGeneric(genericContext, declaredType);
	}

	@Override
	public MethodElementHandle getMethodElementHandle(Class<?> type, String simpleMethodSignature) {
		return elementHandleFactory.getMethod(type, simpleMethodSignature);
	}

	@Override
	public MethodElementHandle getMethodElementHandle(Element element) {
		return elementHandleFactory.getMethod(element);
	}

	@Override
	public MethodElementHandle getMethodElementHandle(ExecutableElement executableElement) {
		return elementHandleFactory.getMethod(executableElement);
	}

	@Override
	public MethodElementHandle getMethodElementHandle(String methodFullSignature) {
		return elementHandleFactory.getMethod(methodFullSignature);
	}

	@Override
	public MethodElementHandle getMethodElementHandleByName(Class<?> type, String name) {
		TypeElementHandle typeElementHandle = getTypeElementHandle(type);
		return typeElementHandle.getDeclaredMethodByName(name);
	}

	@Override
	public MethodElementHandle getMethodElementHandleByName(String methodFullName) {
		// FIXME: Move to factory, rework to be smarter.
		String[] split = methodFullName.split("#");
		TypeElementHandle typeElementHandle = getTypeElementHandle(split[0]);
		return typeElementHandle.getDeclaredMethodByName(split[1]);
	}

	@Override
	public Name getName(String name) {
		return processingEnvironment.getElementUtils().getName(name);
	}

	@Override
	public PackageElementHandle getPackageElementHandle(Element element) {
		return elementHandleFactory.getPackage(element);
	}

	@Override
	public PackageElementHandle getPackageElementHandle(PackageElement packageElement) {
		return elementHandleFactory.getPackage(packageElement);
	}

	@Override
	public PackageElementHandle getPackageElementHandle(String packageSignatureString) {
		return elementHandleFactory.getPackage(packageSignatureString);
	}

	@Override
	public PackageElementHandle getPackageElementHandleOf(Class<?> childType) {
		return elementHandleFactory.getPackageOf(childType);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Class<?> type) {
		return typeHandleFactory.getParameterized(genericContext, type);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, DeclaredType declaredType) {
		return typeHandleFactory.getParameterized(genericContext, declaredType);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getParameterized(genericContext, typeMirror);
	}

	@Override
	public PlainTypeHandle getPlainTypeHandle(GenericContext genericContext, DeclaredType declaredType) {
		return typeHandleFactory.getPlain(genericContext, declaredType);
	}

	@Override
	public PrimitiveTypeHandle getPrimitiveTypeHandle(PrimitiveType primitiveType) {
		return typeHandleFactory.getPrimitive(primitiveType);
	}

	@Override
	public PrimitiveTypeHandle getPrimitiveTypeHandle(TypeMirror typeMirror) {
		return typeHandleFactory.getPrimitive(typeMirror);
	}

	@Override
	public RawTypeHandle getRawTypeHandle(DeclaredType declaredType) {
		return typeHandleFactory.getRaw(declaredType);
	}

	@Override
	public ReferenceTypeHandle getReferenceTypeHandle(TypeMirror typeMirror) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeElementHandle getTypeElementHandle(Class<?> type) {
		return elementHandleFactory.getType(type);
	}

	@Override
	public TypeElementHandle getTypeElementHandle(Element element) {
		return elementHandleFactory.getType(element);
	}

	@Override
	public TypeElementHandle getTypeElementHandle(String typeFullName) {
		return elementHandleFactory.getType(typeFullName);
	}

	@Override
	public TypeElementHandle getTypeElementHandle(TypeElement typeElement) {
		return elementHandleFactory.getType(typeElement);
	}

	@Override
	public TypeElementHandle getTypeElementHandle(TypeMirror typeMirror) {
		return elementHandleFactory.getType(typeMirror);
	}

	@Override
	public TypeElementHandle getTypeElementHandle(TypeSignature typeSignature) {
		return elementHandleFactory.getType(typeSignature);
	}

	@Override
	public TypeHandle getTypeHandle(Class<?> type) {
		return getTypeHandle(GenericContext.empty(), type);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, Class<?> type) {
		return typeHandleFactory.get(type);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, TypeElement typeElement) {
		return typeHandleFactory.get(genericContext, typeElement);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.get(genericContext, typeMirror);
	}

	@Override
	public TypeHandle getTypeHandle(String typeDescriptionString) {
		return getTypeHandle(TypeDescription.of(typeDescriptionString));
	}

	@Override
	public TypeParameterElementHandle getTypeParameterElementHandle(Class<?> type, String typeParameterSimpleName) {
		return elementHandleFactory.getTypeParameter(type, typeParameterSimpleName);
	}

	@Override
	public TypeParameterElementHandle getTypeParameterElementHandle(String typeParameterSignature) {
		return elementHandleFactory.getTypeParameter(typeParameterSignature);
	}

	@Override
	public TypeParameterElementHandle getTypeParameterElementHandle(TypeParameterElement typeParameterElement) {
		return elementHandleFactory.getTypeParameter(typeParameterElement);
	}

	@Override
	public VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getVariable(genericContext, typeMirror);
	}

	@Override
	public VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeVariable typeVariable) {
		return typeHandleFactory.getVariable(genericContext, typeVariable);
	}
}
