package tk.labyrinth.jaap.context;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactoryImpl;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public interface ProcessingContext {

	@Nullable
	DeclaredTypeHandle findDeclaredTypeHandle(String typeSignatureString);

	@Nullable
	MethodElementHandle findMethodElementHandle(MethodFullSignature methodFullSignature);

	@Nullable
	TypeElementHandle findTypeElementHandle(TypeSignature typeSignature);

	@Nullable
	TypeHandle findTypeHandle(String typeDescriptionString);

	@Nullable
	TypeHandle findTypeHandle(TypeDescription typeDescription);

	@Nullable
	TypeParameterElementHandle findTypeParameterElementHandle(Class<?> type, String typeParameterSimpleName);

	@Nullable
	TypeParameterElementHandle findTypeParameterElementHandle(String typeParameterSignature);

	AnnotationHandle getAnnotationHandle(AnnotatedConstruct parent, AnnotationMirror annotationMirror);

	AnnotationTypeHandle getAnnotationTypeHandle(Class<? extends Annotation> annotationType);

	AnnotationTypeHandle getAnnotationTypeHandle(DeclaredType annotationDeclaredType);

	AnnotationTypeHandle getAnnotationTypeHandle(String annotationTypeName);

	AnnotationTypeHandle getAnnotationTypeHandle(TypeMirror annotationTypeMirror);

	ArrayTypeHandle getArrayTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	ConstructorElementHandle getConstructorElementHandle(Element element);

	ConstructorElementHandle getConstructorElementHandle(ExecutableElement executableElement);

	DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeElement typeElement);

	DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	ElementHandle getElementHandle(Class<?> type);

	ElementHandle getElementHandle(Element element);

	ElementHandle getElementHandle(String fullElementSignature);

	LangmodelElementHandleFactory getElementHandleFactory();

	FieldElementHandle getFieldElementHandle(Class<?> type, String fieldSimpleName);

	FieldElementHandle getFieldElementHandle(Element element);

	FieldElementHandle getFieldElementHandle(String fieldFullName);

	FieldElementHandle getFieldElementHandle(VariableElement variableElement);

	FormalParameterElementHandle getFormalParameterElementHandle(VariableElement variableElement);

	GenericTypeHandle getGenericTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	MethodElementHandle getMethodElementHandle(Class<?> type, String simpleMethodSignature);

	MethodElementHandle getMethodElementHandle(Element element);

	MethodElementHandle getMethodElementHandle(ExecutableElement executableElement);

	default MethodElementHandle getMethodElementHandle(MethodFullSignature methodFullSignature) {
		MethodElementHandle result = findMethodElementHandle(methodFullSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignature = " + methodFullSignature);
		}
		return result;
	}

	MethodElementHandle getMethodElementHandle(String methodFullSignature);

	MethodElementHandle getMethodElementHandleByName(Class<?> type, String name);

	MethodElementHandle getMethodElementHandleByName(String methodFullName);

	/**
	 * Returns {@link Name} built with underlying {@link ProcessingEnvironment}.
	 *
	 * @param name non-null
	 *
	 * @return non-null
	 */
	Name getName(String name);

	PackageElementHandle getPackageElementHandle(Element element);

	PackageElementHandle getPackageElementHandle(PackageElement packageElement);

	PackageElementHandle getPackageElementHandle(String packageSignatureString);

	PackageElementHandle getPackageElementHandleOf(Class<?> childType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Class<?> type);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	PlainTypeHandle getPlainTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	PrimitiveTypeHandle getPrimitiveTypeHandle(PrimitiveType primitiveType);

	PrimitiveTypeHandle getPrimitiveTypeHandle(TypeMirror typeMirror);

	ProcessingEnvironment getProcessingEnvironment();

	RawTypeHandle getRawTypeHandle(DeclaredType declaredType);

	ReferenceTypeHandle getReferenceTypeHandle(TypeMirror typeMirror);

	TypeElementHandle getTypeElementHandle(Class<?> type);

	TypeElementHandle getTypeElementHandle(Element element);

	TypeElementHandle getTypeElementHandle(String typeFullName);

	TypeElementHandle getTypeElementHandle(TypeElement typeElement);

	TypeElementHandle getTypeElementHandle(TypeMirror typeMirror);

	TypeElementHandle getTypeElementHandle(TypeSignature typeSignature);

	TypeHandle getTypeHandle(Class<?> type);

	TypeHandle getTypeHandle(GenericContext genericContext, Class<?> type);

	TypeHandle getTypeHandle(GenericContext genericContext, TypeElement typeElement);

	TypeHandle getTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	default TypeHandle getTypeHandle(String typeDescriptionString) {
		TypeHandle result = findTypeHandle(typeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescriptionString = " + typeDescriptionString);
		}
		return result;
	}

	default TypeHandle getTypeHandle(TypeDescription typeDescription) {
		TypeHandle result = findTypeHandle(typeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescription = " + typeDescription);
		}
		return result;
	}

	LangmodelTypeHandleFactory getTypeHandleFactory();

	LangmodelTypeMirrorFactory getTypeMirrorFactory();

	TypeParameterElementHandle getTypeParameterElementHandle(Class<?> type, String typeParameterSimpleName);

	TypeParameterElementHandle getTypeParameterElementHandle(String typeParameterSignature);

	TypeParameterElementHandle getTypeParameterElementHandle(TypeParameterElement typeParameterElement);

	VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeVariable typeVariable);

	static ProcessingContext of(AnnotationProcessingRound round) {
		return of(round.getProcessingEnvironment());
	}

	static ProcessingContext of(ProcessingEnvironment environment) {
		LangmodelElementFactory elementFactory = new LangmodelElementFactoryImpl(environment);
		LangmodelTypeMirrorFactory typeMirrorFactory = new LangmodelTypeMirrorFactoryImpl(elementFactory, environment);
		//
		LangmodelElementHandleFactoryImpl elementHandleFactory = new LangmodelElementHandleFactoryImpl(
				elementFactory,
				typeMirrorFactory);
		LangmodelTypeHandleFactoryImpl typeHandleFactory = new LangmodelTypeHandleFactoryImpl(
				elementHandleFactory,
				typeMirrorFactory);
		//
		ProcessingContext processingContext = new ProcessingContextImpl(
				elementHandleFactory,
				environment,
				typeHandleFactory,
				typeMirrorFactory);
		//
		elementHandleFactory.acceptProcessingContext(processingContext);
		typeHandleFactory.acceptProcessingContext(processingContext);
		//
		return processingContext;
	}
}
