package tk.labyrinth.jaap.context;

import lombok.Getter;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import java.util.Objects;
import java.util.stream.Stream;

public final class RoundContextImpl implements RoundContext {

	@Getter
	private final ProcessingContext processingContext;

	@Getter
	private final AnnotationProcessingRound round;

	RoundContextImpl(ProcessingContext processingContext, AnnotationProcessingRound round) {
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
		this.round = Objects.requireNonNull(round, "round");
	}

	@Override
	public Stream<DeclaredTypeHandle> getAllDeclaredTypes() {
		return getAllTypeElements().map(TypeElementHandle::toType);
	}

	@Override
	public Stream<TypeElementHandle> getAllTypeElements() {
		return getTopLevelTypeElements()
				.flatMap(topLevelTypeElement -> StreamUtils.expandRecursively(
						topLevelTypeElement,
						TypeElementHandle::getNestedTypeStream));
	}

	@Override
	public Stream<PackageElementHandle> getPackageElements() {
		return round.getRoundEnvironment().getRootElements().stream()
				.filter(PackageElement.class::isInstance)
				.map(PackageElement.class::cast)
				.map(rootElement -> getProcessingContext().getPackageElementHandle(rootElement));
	}

	@Override
	public Stream<DeclaredTypeHandle> getTopLevelDeclaredTypes() {
		return round.getRoundEnvironment().getRootElements().stream()
				.filter(TypeElement.class::isInstance)
				.map(TypeElement.class::cast)
				.map(rootElement -> getProcessingContext().getDeclaredTypeHandle(GenericContext.empty(), rootElement));
	}

	@Override
	public Stream<ElementHandle> getTopLevelElements() {
		return round.getRoundEnvironment().getRootElements().stream()
				.map(rootElement -> getProcessingContext().getElementHandle(rootElement));
	}

	@Override
	public Stream<TypeElementHandle> getTopLevelTypeElements() {
		return round.getRoundEnvironment().getRootElements().stream()
				.filter(TypeElement.class::isInstance)
				.map(TypeElement.class::cast)
				.map(rootElement -> getProcessingContext().getTypeElementHandle(rootElement));
	}
}
