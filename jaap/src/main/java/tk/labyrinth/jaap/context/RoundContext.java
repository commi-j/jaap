package tk.labyrinth.jaap.context;

import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;

import java.util.stream.Stream;

public interface RoundContext extends HasProcessingContext {

	Stream<DeclaredTypeHandle> getAllDeclaredTypes();

	Stream<TypeElementHandle> getAllTypeElements();

	Stream<PackageElementHandle> getPackageElements();

	AnnotationProcessingRound getRound();

	Stream<DeclaredTypeHandle> getTopLevelDeclaredTypes();

	/**
	 * Consists of {@link PackageElementHandle PackageElementHandles}
	 * and {@link TypeElementHandle TypeElementHandles}.
	 *
	 * @return non-null
	 */
	Stream<ElementHandle> getTopLevelElements();

	Stream<TypeElementHandle> getTopLevelTypeElements();

	static RoundContext of(AnnotationProcessingRound round) {
		return of(round, ProcessingContext.of(round.getProcessingEnvironment()));
	}

	static RoundContext of(AnnotationProcessingRound round, ProcessingContext processingContext) {
		return new RoundContextImpl(processingContext, round);
	}
}
