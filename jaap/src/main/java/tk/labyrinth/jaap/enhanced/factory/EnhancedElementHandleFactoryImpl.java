package tk.labyrinth.jaap.enhanced.factory;

import tk.labyrinth.jaap.enhanced.element.EnhancedTypeElementHandle;
import tk.labyrinth.jaap.handle.element.synthetic.SyntheticElementHandleRegistry;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactory;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;

// FIXME: Must decorate, not implement.
public class EnhancedElementHandleFactoryImpl extends LangmodelElementHandleFactoryImpl {

	private final SyntheticElementHandleRegistry syntheticElementHandleRegistry;

	public EnhancedElementHandleFactoryImpl(
			LangmodelElementFactory elementFactory,
			LangmodelTypeMirrorFactory typeMirrorFactory,
			SyntheticElementHandleRegistry syntheticElementHandleRegistry) {
		super(elementFactory, typeMirrorFactory);
		this.syntheticElementHandleRegistry = syntheticElementHandleRegistry;
	}

	@Nullable
	@Override
	public MethodElementHandle findMethod(MethodFullSignature methodFullSignature) {
		MethodElementHandle result;
		{
			MethodElementHandle annprocHandle = super.findMethod(methodFullSignature);
			if (annprocHandle != null) {
				result = annprocHandle;
			} else {
				result = syntheticElementHandleRegistry.findMethod(methodFullSignature);
			}
		}
		return result;
	}

	@Override
	public TypeElementHandle getType(TypeElement typeElement) {
		return new EnhancedTypeElementHandle(
				syntheticElementHandleRegistry,
				super.getType(typeElement));
	}
}
