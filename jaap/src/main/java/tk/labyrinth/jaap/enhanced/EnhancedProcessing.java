package tk.labyrinth.jaap.enhanced;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextImpl;
import tk.labyrinth.jaap.enhanced.factory.EnhancedElementHandleFactoryImpl;
import tk.labyrinth.jaap.handle.element.synthetic.SyntheticElementHandleRegistry;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactoryImpl;

import javax.annotation.processing.ProcessingEnvironment;

public class EnhancedProcessing {

	public static ProcessingContext createContext(
			ProcessingEnvironment processingEnvironment,
			SyntheticElementHandleRegistry syntheticElementHandleRegistry) {
		LangmodelElementFactory elementFactory = new LangmodelElementFactoryImpl(processingEnvironment);
		LangmodelTypeMirrorFactory typeMirrorFactory = new LangmodelTypeMirrorFactoryImpl(elementFactory, processingEnvironment);
		//
		EnhancedElementHandleFactoryImpl elementHandleFactory = new EnhancedElementHandleFactoryImpl(
				elementFactory,
				typeMirrorFactory,
				syntheticElementHandleRegistry);
		LangmodelTypeHandleFactoryImpl typeHandleFactory = new LangmodelTypeHandleFactoryImpl(
				elementHandleFactory,
				typeMirrorFactory);
		//
		ProcessingContext processingContext = new ProcessingContextImpl(
				elementHandleFactory,
				processingEnvironment,
				typeHandleFactory,
				typeMirrorFactory);
		//
		elementHandleFactory.acceptProcessingContext(processingContext);
		syntheticElementHandleRegistry.setProcessingContext(processingContext);
		typeHandleFactory.acceptProcessingContext(processingContext);
		//
		return processingContext;
	}
}
