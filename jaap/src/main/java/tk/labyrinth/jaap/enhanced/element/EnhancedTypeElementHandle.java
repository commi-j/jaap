package tk.labyrinth.jaap.enhanced.element;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.element.synthetic.SyntheticElementHandleRegistry;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Stream;

@Value
public class EnhancedTypeElementHandle implements TypeElementHandle {

	SyntheticElementHandleRegistry elementHandleRegistry;

	TypeElementHandle typeElementHandle;

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		return typeElementHandle.asAnnotationType();
	}

	@Override
	public ElementHandle asElement() {
		return typeElementHandle.asElement();
	}

	@Nullable
	@Override
	public AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return typeElementHandle.findDirectAnnotation(annotationType);
	}

	@Nullable
	@Override
	public TypeElementHandle findSuperclass() {
		return typeElementHandle.findSuperclass();
	}

	@Override
	public Stream<? extends TypeElementHandle> getAllTypeStream() {
		return typeElementHandle.getAllTypeStream();
	}

	@Override
	public String getCanonicalName() {
		return typeElementHandle.getCanonicalName();
	}

	@Override
	public Stream<ConstructorElementHandle> getConstructorStream() {
		return typeElementHandle.getConstructorStream();
	}

	@Override
	public Stream<FieldElementHandle> getDeclaredFieldStream() {
		return typeElementHandle.getDeclaredFieldStream();
	}

	//
	@Override
	public Stream<? extends MethodElementHandle> getDeclaredMethodStream() {
		return Stream.concat(
				typeElementHandle.getDeclaredMethodStream(),
				elementHandleRegistry.getMethodsOfType(getSignature()));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return typeElementHandle.getDirectAnnotations();
	}

	@Override
	public Stream<? extends TypeElementHandle> getDirectSupertypeStream() {
		return typeElementHandle.getDirectSupertypeStream();
	}

	@Override
	public FieldElementHandle getField(String fieldName) {
		return typeElementHandle.getField(fieldName);
	}

	@Override
	public String getLongName() {
		return typeElementHandle.getLongName();
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return typeElementHandle.getMergedAnnotationContext(annotationTypeHandle, mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return typeElementHandle.getMergedAnnotationContext(annotationType, mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			String annotationTypeSignature,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return typeElementHandle.getMergedAnnotationContext(annotationTypeSignature, mergedAnnotationSpecification);
	}

	@Override
	public Stream<? extends TypeElementHandle> getNestedTypeStream() {
		return typeElementHandle.getNestedTypeStream();
	}

	@Override
	public PackageElementHandle getPackage() {
		return typeElementHandle.getPackage();
	}

	@Override
	public String getPackageQualifiedName() {
		return typeElementHandle.getPackageQualifiedName();
	}

	@Nullable
	@Override
	public ElementHandle getParent() {
		return typeElementHandle.getParent();
	}

	@Override
	public String getQualifiedName() {
		return typeElementHandle.getQualifiedName();
	}

	@Override
	public TypeSignature getSignature() {
		return typeElementHandle.getSignature();
	}

	@Override
	public String getSimpleName() {
		return typeElementHandle.getSimpleName();
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		return typeElementHandle.getTypeParameterStream();
	}

	@Override
	public boolean isAnnotationType() {
		return typeElementHandle.isAnnotationType();
	}

	@Override
	public boolean isInterface() {
		return typeElementHandle.isInterface();
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		return typeElementHandle.selectMember(selector);
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(MethodSelector methodSelector) {
		MethodElementHandle result;
		{
			MethodElementHandle traditionalMethod = typeElementHandle.selectMethodElement(methodSelector);
			//
			if (traditionalMethod != null) {
				result = traditionalMethod;
			} else {
				result = elementHandleRegistry.findMethod(getSignature(), methodSelector);
			}
		}
		return result;
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(MethodSimpleSignature methodSimpleSignature) {
		return typeElementHandle.selectMethodElement(methodSimpleSignature);
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(String methodName, List<TypeHandle> argumentTypes) {
		return typeElementHandle.selectMethodElement(methodName, argumentTypes);
	}

	@Override
	public DeclaredTypeHandle toType() {
		return typeElementHandle.toType();
	}
}
