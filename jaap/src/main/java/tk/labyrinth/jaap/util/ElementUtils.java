package tk.labyrinth.jaap.util;

import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.element.util.PackageElementUtils;
import tk.labyrinth.jaap.handle.element.util.VariableElementUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.misc4j2.java.lang.EnumUtils;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import java.lang.reflect.Executable;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Member;
import java.util.List;

public class ElementUtils {

	@Nullable
	public static Element find(ProcessingEnvironment processingEnvironment, GenericDeclaration genericDeclaration) {
		Element result;
		{
			if (genericDeclaration instanceof Class<?> cl) {
				result = TypeElementUtils.find(processingEnvironment, cl);
			} else if (genericDeclaration instanceof Executable executable) {
				result = ExecutableElementUtils.find(processingEnvironment, executable);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(genericDeclaration));
			}
		}
		return result;
	}

	@Nullable
	public static Element find(ProcessingEnvironment processingEnvironment, Member member) {
		throw new NotImplementedException(ExceptionUtils.render(member));
	}

	@Nullable
	public static TypeParameterElement findTypeParameter(Element element, String name) {
		TypeParameterElement result;
		{
			if (element instanceof ExecutableElement executableElement) {
				result = ExecutableElementUtils.findTypeParameter(executableElement, name);
			} else if (element instanceof TypeElement typeElement) {
				result = TypeElementUtils.findTypeParameter(typeElement, name);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(element));
			}
		}
		return result;
	}

	public static String getName(Element element) {
		return element.getSimpleName().toString();
	}

	@Deprecated
	public static String getSignature(ProcessingEnvironment processingEnvironment, Element element) {
		String result;
		{
			if (element instanceof ExecutableElement executableElement) {
				result = ExecutableElementUtils.getSignatureString(processingEnvironment, executableElement);
			} else if (element instanceof PackageElement packageElement) {
				result = PackageElementUtils.getSignature(packageElement);
			} else if (element instanceof TypeElement typeElement) {
				result = TypeElementUtils.getSignature(typeElement);
			} else if (element instanceof VariableElement variableElement) {
				result = VariableElementUtils.getSignature(processingEnvironment, variableElement);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(element));
			}
		}
		return result;
	}

	public static boolean isConstructor(Element value) {
		return value != null && value.getKind() == ElementKind.CONSTRUCTOR;
	}

	public static boolean isExecutable(Element value) {
		return value instanceof ExecutableElement;
	}

	public static boolean isField(Element value) {
		return value != null && value.getKind() == ElementKind.FIELD;
	}

	public static boolean isFormalParameter(Element value) {
		return value != null && value.getKind() == ElementKind.PARAMETER;
	}

	public static boolean isInitializer(Element value) {
		return value != null && EnumUtils.in(value.getKind(), ElementKind.INSTANCE_INIT, ElementKind.STATIC_INIT);
	}

	public static boolean isMethod(Element value) {
		return value != null && value.getKind() == ElementKind.METHOD;
	}

	public static boolean isPackage(Element value) {
		return value instanceof PackageElement;
	}

	public static boolean isType(Element value) {
		return value instanceof TypeElement;
	}

	public static boolean isTypeParameter(Element value) {
		return value instanceof TypeParameterElement;
	}

	public static boolean isVariable(Element value) {
		return value instanceof VariableElement;
	}

	@Nullable
	public static Element navigate(
			ProcessingEnvironment processingEnvironment,
			Element element,
			EntitySelectorChain entitySelectorChain) {
		return ObjectUtils
				.reduce(
						Pair.of(element, entitySelectorChain),
						pair -> pair.getLeft() != null && pair.getRight() != null,
						pair -> {
							Pair<EntitySelector, EntitySelectorChain> headAndTail = pair.getRight().split();
							return Pair.of(
									selectMember(processingEnvironment, pair.getLeft(), headAndTail.getLeft()),
									headAndTail.getRight());
						})
				.getLeft();
	}

	public static ExecutableElement requireConstructor(Element element) {
		if (!isConstructor(element)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require constructor: %s".formatted(ExceptionUtils.render(element)));
		}
		return (ExecutableElement) element;
	}

	public static ExecutableElement requireExecutable(Element element) {
		if (!isExecutable(element)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require executable: %s".formatted(ExceptionUtils.render(element)));
		}
		return (ExecutableElement) element;
	}

	public static VariableElement requireField(Element element) {
		if (!isField(element)) {
			throw new IllegalArgumentException("Require field: %s".formatted(ExceptionUtils.render(element)));
		}
		return (VariableElement) element;
	}

	public static VariableElement requireFormalParameter(Element element) {
		if (!isFormalParameter(element)) {
			throw new IllegalArgumentException("Require formalParameter: %s".formatted(ExceptionUtils.render(element)));
		}
		return (VariableElement) element;
	}

	public static ExecutableElement requireInitializer(Element element) {
		if (!isInitializer(element)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require initializer: %s".formatted(ExceptionUtils.render(element)));
		}
		return (ExecutableElement) element;
	}

	public static ExecutableElement requireMethod(Element element) {
		if (!isMethod(element)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require method: %s".formatted(ExceptionUtils.render(element)));
		}
		return (ExecutableElement) element;
	}

	public static PackageElement requirePackage(Element element) {
		if (!isPackage(element)) {
			throw new IllegalArgumentException("Require package: %s".formatted(ExceptionUtils.render(element)));
		}
		return (PackageElement) element;
	}

	public static TypeElement requireType(Element element) {
		if (!isType(element)) {
			throw new IllegalArgumentException("Require type: %s".formatted(ExceptionUtils.render(element)));
		}
		return (TypeElement) element;
	}

	public static TypeParameterElement requireTypeParameter(Element element) {
		if (!isTypeParameter(element)) {
			throw new IllegalArgumentException("Require TypeParameterElement: %s"
					.formatted(ExceptionUtils.render(element)));
		}
		return (TypeParameterElement) element;
	}

	public static VariableElement requireVariable(Element element) {
		if (!isVariable(element)) {
			throw new IllegalArgumentException("Require variable: %s".formatted(ExceptionUtils.render(element)));
		}
		return (VariableElement) element;
	}

	@Nullable
	public static Element selectMember(
			ProcessingEnvironment processingEnvironment,
			@NonNull Element element,
			EntitySelector selector) {
		Element result;
		{
			if (element instanceof ExecutableElement executableElement) {
				result = ExecutableElementUtils.selectMember(processingEnvironment, executableElement, selector);
			} else if (element instanceof TypeElement typeElement) {
				result = TypeElementUtils.selectMember(processingEnvironment, typeElement, selector);
			} else if (element instanceof PackageElement) {
				throw new NotImplementedException(ExceptionUtils.renderList(List.of(element, selector)));
			} else if (element instanceof VariableElement variableElement) {
				result = VariableElementUtils.selectMember(processingEnvironment, variableElement, selector);
			} else {
				throw new UnsupportedOperationException(ExceptionUtils.render(List.of(element, selector)));
			}
		}
		return result;
	}
}
