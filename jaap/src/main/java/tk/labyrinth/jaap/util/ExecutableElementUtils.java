package tk.labyrinth.jaap.util;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;

@Deprecated
public class ExecutableElementUtils {

	public static ExecutableElement requireAnnotationTypeElement(ExecutableElement argument) {
		if (argument == null || argument.getEnclosingElement().getKind() != ElementKind.ANNOTATION_TYPE) {
			throw new IllegalArgumentException("Require annotation type element: " + argument);
		}
		return argument;
	}
}
