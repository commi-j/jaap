package tk.labyrinth.jaap.util;

import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.element.util.MethodSignatureUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.typical.jlm.TypicalJlmUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeElementUtils {

	/**
	 * Returns:<br>
	 * -1 if first is assignable to second;<br>
	 * 0 if first is the same as second;<br>
	 * 1 if second is assignable to first;<br>
	 * Throws exception if incompatible.<br>
	 *
	 * @param processingEnvironment non-null
	 * @param first                 non-null
	 * @param second                non-null
	 *
	 * @return -1, 0 or 1
	 *
	 * @throws IllegalArgumentException if incompatible
	 */
	public static int compareByAssignability(
			ProcessingEnvironment processingEnvironment,
			TypeElement first,
			TypeElement second) {
		return TypeMirrorUtils.compareByAssignability(processingEnvironment, first.asType(), second.asType());
	}

	@Nullable
	public static TypeElement find(ProcessingEnvironment processingEnvironment, Class<?> type) {
		Objects.requireNonNull(type, "type");
		//
		return find(processingEnvironment, type.getCanonicalName());
	}

	@Nullable
	public static TypeElement find(ProcessingEnvironment processingEnvironment, ElementSignature typeSignature) {
		return find(processingEnvironment, typeSignature.toString());
	}

	@Nullable
	public static TypeElement find(ProcessingEnvironment processingEnvironment, String typeFullName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeFullName, "typeFullName");
		//
		// TODO: Support qualified, not only canonical types.
		return processingEnvironment.getElementUtils().getTypeElement(TypeNameUtils.toQualified(typeFullName));
	}

	@Nullable
	public static TypeElement find(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeMirror, "typeMirror");
		//
		TypeElement result;
		{
			Element element = processingEnvironment.getTypeUtils().asElement(typeMirror);
			if (element instanceof TypeElement) {
				result = (TypeElement) element;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static VariableElement findDeclaredField(TypeElement typeElement, String fieldSimpleName) {
		return getDeclaredFields(typeElement)
				.filter(declaredField -> declaredField.getSimpleName().contentEquals(fieldSimpleName))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	public static ExecutableElement findDeclaredMethod(
			ProcessingEnvironment processingEnvironment,
			MethodFullSignature methodFullSignature) {
		ExecutableElement result;
		{
			List<ExecutableElement> declaredMethods = getDeclaredMethods(
					get(processingEnvironment, methodFullSignature.getTypeFullSignature()),
					methodFullSignature.getName());
			//
			result = declaredMethods.stream()
					.filter(declaredMethod -> Objects.equals(
							MethodSignatureUtils.createFull(processingEnvironment, declaredMethod),
							methodFullSignature))
					.collect(CollectorUtils.findOnly(true));
		}
		return result;
	}

	@Nullable
	public static ExecutableElement findDeclaredMethod(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			MethodSimpleSignature methodSimpleSignature) {
		return findDeclaredMethod(processingEnvironment, MethodSignatureUtils.createFull(typeElement, methodSimpleSignature));
	}

	@Nullable
	public static ExecutableElement findDeclaredMethodByName(
			ProcessingEnvironment processingEnvironment, Class<?> type, String methodName) {
		return getDeclaredMethodStream(get(processingEnvironment, type), methodName)
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	public static ExecutableElement findFunctionalInterfaceMethod(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement) {
		ExecutableElement result;
		{
			if (isEffectivelyAbstract(typeElement)) {
				List<ExecutableElement> abstractMethods = getAllMethodStream(processingEnvironment, typeElement)
						.filter(ExecutableElementUtils::isEffectivelyAbstract)
						.toList();
				//
				result = abstractMethods.size() == 1 ? abstractMethods.get(0) : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static TypeParameterElement findTypeParameter(TypeElement typeElement, String name) {
		return ParameterizableUtils.findTypeParameter(typeElement, name);
	}

	public static TypeElement get(ProcessingEnvironment processingEnvironment, Class<?> type) {
		TypeElement result = find(processingEnvironment, type);
		if (result == null) {
			throw new IllegalArgumentException("No TypeElement resolved: type = " + type);
		}
		return result;
	}

	public static TypeElement get(ProcessingEnvironment processingEnvironment, String typeFullName) {
		TypeElement result = find(processingEnvironment, typeFullName);
		if (result == null) {
			throw new IllegalArgumentException("No TypeElement resolved: typeFullName = " + typeFullName);
		}
		return result;
	}

	public static TypeElement get(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		TypeElement result = find(processingEnvironment, typeMirror);
		if (result == null) {
			throw new IllegalArgumentException("No TypeElement resolved: typeMirror = " + typeMirror);
		}
		return result;
	}

	/**
	 * @param processingEnvironment non-null
	 * @param typeElement           non-null
	 *
	 * @return non-null
	 *
	 * @since 0.2.8
	 */
	public static Stream<VariableElement> getAllFields(ProcessingEnvironment processingEnvironment, TypeElement typeElement) {
		return getTypeElements(processingEnvironment, typeElement)
				.flatMap(TypeElementUtils::getDeclaredFields);
	}

	/**
	 * @param processingEnvironment non-null
	 * @param typeElement           non-null
	 *
	 * @return non-null
	 *
	 * @since 0.5.0
	 */
	public static Stream<ExecutableElement> getAllMethodStream(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement) {
		return getTypeElements(processingEnvironment, typeElement).flatMap(TypeElementUtils::getDeclaredMethodStream);
	}

	/**
	 * @param processingEnvironment non-null
	 * @param typeElement           non-null
	 *
	 * @return non-null
	 *
	 * @since 0.5.0
	 */
	public static List<ExecutableElement> getAllMethods(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement) {
		return getAllMethodStream(processingEnvironment, typeElement).toList();
	}

	public static String getBinaryName(TypeElement typeElement) {
		Element enclosingElement = typeElement.getEnclosingElement();
		return enclosingElement.getKind() == ElementKind.PACKAGE
				? typeElement.getQualifiedName().toString()
				: ((TypeElement) enclosingElement).getQualifiedName() + "$" + typeElement.getSimpleName();
	}

	public static VariableElement getDeclaredField(TypeElement typeElement, String fieldSimpleName) {
		// TODO: Make better failure message.
		return Objects.requireNonNull(findDeclaredField(typeElement, fieldSimpleName));
	}

	public static Stream<VariableElement> getDeclaredFields(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.FIELD)
				.map(VariableElement.class::cast);
	}

	public static ExecutableElement getDeclaredMethod(ProcessingEnvironment processingEnvironment, TypeElement typeElement, MethodSimpleSignature methodSimpleSignature) {
		ExecutableElement result = findDeclaredMethod(processingEnvironment, typeElement, methodSimpleSignature);
		if (result == null) {
			throw new IllegalArgumentException("No DeclaredMethod found: " +
					"typeElement = " + typeElement + ", " +
					"methodSimpleSignature = " + methodSimpleSignature);
		}
		return result;
	}

	public static ExecutableElement getDeclaredMethod(
			ProcessingEnvironment processingEnvironment,
			MethodFullSignature methodFullSignature) {
		ExecutableElement result;
		{
			List<ExecutableElement> declaredMethods = getDeclaredMethods(get(processingEnvironment, methodFullSignature.getTypeFullSignature()), methodFullSignature.getName());
			//
			result = declaredMethods.stream()
					.filter(declaredMethod -> Objects.equals(
							MethodSignatureUtils.createFull(processingEnvironment, declaredMethod),
							methodFullSignature))
					.collect(CollectorUtils.findOnly());
		}
		return result;
	}

	public static ExecutableElement getDeclaredMethod(
			ProcessingEnvironment processingEnvironment,
			String methodFullSignature) {
		return getDeclaredMethod(processingEnvironment, MethodFullSignature.of(methodFullSignature));
	}

	public static ExecutableElement getDeclaredMethod(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			String methodSimpleSignature) {
		MethodFullSignature methodFullSignature = MethodSignatureUtils.createFull(typeElement, methodSimpleSignature);
		return getDeclaredMethodStream(typeElement, methodFullSignature.getName())
				.filter(declaredMethod -> Objects.equals(
						MethodSignatureUtils.createFull(processingEnvironment, declaredMethod),
						methodFullSignature))
				.collect(CollectorUtils.findOnly());
	}

	public static Stream<ExecutableElement> getDeclaredMethodStream(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.METHOD)
				.map(ExecutableElement.class::cast);
	}

	public static Stream<ExecutableElement> getDeclaredMethodStream(TypeElement typeElement, String methodSimpleName) {
		return getDeclaredMethodStream(typeElement)
				.filter(declaredMethod -> declaredMethod.getSimpleName().contentEquals(methodSimpleName));
	}

	public static List<ExecutableElement> getDeclaredMethods(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.METHOD)
				.map(ExecutableElement.class::cast)
				.collect(Collectors.toList());
	}

	public static List<ExecutableElement> getDeclaredMethods(TypeElement typeElement, String methodSimpleName) {
		return getDeclaredMethodStream(typeElement)
				.filter(declaredMethod -> declaredMethod.getSimpleName().contentEquals(methodSimpleName))
				.collect(Collectors.toList());
	}

	/**
	 * @param environment non-null
	 * @param mirror      non-null
	 *
	 * @return non-null
	 */
	public static TypeElement getElementOrFail(ProcessingEnvironment environment, TypeMirror mirror) {
		TypeElement result = (TypeElement) environment.getTypeUtils().asElement(mirror);
		if (result == null) {
			throw new IllegalArgumentException("No TypeElement found for TypeMirror: " + mirror);
		}
		return result;
	}

	public static String getLongName(TypeElement typeElement) {
		return !isTopLevel(typeElement)
				? getLongName(ElementUtils.requireType(typeElement.getEnclosingElement())) +
				"." +
				typeElement.getSimpleName()
				: typeElement.getSimpleName().toString();
	}

	public static Stream<TypeElement> getNestedTypeElementStream(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(ElementUtils::isType)
				.map(ElementUtils::requireType);
	}

	public static PackageElement getPackage(TypeElement typeElement) {
		return isTopLevel(typeElement)
				? ElementUtils.requirePackage(typeElement.getEnclosingElement())
				: getPackage(ElementUtils.requireType(typeElement.getEnclosingElement()));
	}

	public static String getPackageQualifiedName(TypeElement typeElement) {
		return getPackage(typeElement).getQualifiedName().toString();
	}

	public static String getSignature(TypeElement typeElement) {
		return getPackageQualifiedName(typeElement) + ":" + getLongName(typeElement);
	}

	/**
	 * Returns all {@link TypeElement TypeElements} for which <b>typeElement</b> instanceof is true.<br>
	 * This method is useful for member selection.
	 * <br>
	 * Note: the order is unspecified.
	 *
	 * @param processingEnvironment non-null
	 * @param typeElement           non-null
	 *
	 * @return non-empty
	 *
	 * @since 0.2.8
	 */
	public static Stream<TypeElement> getTypeElements(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement) {
		return TypicalJlmUtils.getDeclaredTypes(processingEnvironment, typeElement.asType())
				.map(declaredType -> get(processingEnvironment, declaredType));
	}

	public static boolean isAnnotationType(TypeElement typeElement) {
		return typeElement.getKind() == ElementKind.ANNOTATION_TYPE;
	}

	public static boolean isEffectivelyAbstract(TypeElement typeElement) {
		return typeElement.getKind().isInterface() || typeElement.getModifiers().contains(Modifier.ABSTRACT);
	}

	public static boolean isGeneric(TypeElement typeElement) {
		return !typeElement.getTypeParameters().isEmpty();
	}

	public static boolean isPlain(TypeElement typeElement) {
		return typeElement.getTypeParameters().isEmpty();
	}

	public static boolean isTopLevel(TypeElement typeElement) {
		return ElementUtils.isPackage(typeElement.getEnclosingElement());
	}

	public static TypeElement requireAnnotation(TypeElement argument) {
		if (argument == null || argument.getKind() != ElementKind.ANNOTATION_TYPE) {
			throw new IllegalArgumentException("Require annotation: " + argument);
		}
		return argument;
	}

	public static TypeElement requireGeneric(TypeElement argument) {
		if (argument == null || !isGeneric(argument)) {
			throw new IllegalArgumentException("Require generic: " + argument);
		}
		return argument;
	}

	public static TypeElement requirePlain(TypeElement argument) {
		if (argument == null || !isPlain(argument)) {
			throw new IllegalArgumentException("Require plain: " + argument);
		}
		return argument;
	}

	@Nullable
	public static Element selectMember(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			EntitySelector entitySelector) {
		Element result;
		{
			if (Objects.equals(entitySelector.getSimpleName(), "class")) {
				result = TypeElementUtils.get(processingEnvironment, Class.class);
			} else if (entitySelector.canBeMethod()) {
				result = selectMethod(processingEnvironment, typeElement, entitySelector.toMethodSelector());
			} else {
				// FIXME: Support inheritance
				result = typeElement.getEnclosedElements().stream()
						.filter(enclosedElement -> entitySelector.matches(processingEnvironment, enclosedElement))
						.collect(CollectorUtils.findOnly(true));
			}
		}
		return result;
	}

	// FIXME: compare tk.labyrinth.jaap.model.entity.selection.MethodSelector.matches
	@Nullable
	public static ExecutableElement selectMethod(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			MethodSelector methodSelector) {
		List<ExecutableElement> filteredByMatchCandidates = getTypeElements(processingEnvironment, typeElement)
				.flatMap(TypeElementUtils::getDeclaredMethodStream)
				.filter(executableElement -> MethodSelector.matches(
						processingEnvironment,
						methodSelector,
						executableElement))
				.toList();
		//
		// TODO: We want this logic to be smarter than just compare first arguments.
		List<ExecutableElement> filteredByParametersCandidates = filteredByMatchCandidates.size() > 1
				? filteredByMatchCandidates.stream()
				.map(element -> Pair.of(element, TypeMirrorUtils.evaluateType(element.getParameters().stream()
						.findFirst()
						.map(VariableElement::asType)
						.orElse(null))))
				.collect(Collectors.groupingBy(Pair::getRight))
				.entrySet()
				.stream()
				.min(Map.Entry.comparingByKey())
				.map(entry -> entry.getValue().stream().map(Pair::getLeft).toList())
				.orElseThrow()
				: filteredByMatchCandidates;
		//
		return filteredByParametersCandidates.stream()
				.min((first, second) -> compareByAssignability(
						processingEnvironment,
						ElementUtils.requireType(first.getEnclosingElement()),
						ElementUtils.requireType(second.getEnclosingElement())))
				.orElse(null);
	}
}
