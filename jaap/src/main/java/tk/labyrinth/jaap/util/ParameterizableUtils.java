package tk.labyrinth.jaap.util;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;

public class ParameterizableUtils {

	@Nullable
	public static TypeParameterElement findTypeParameter(Parameterizable parameterizable, int index) {
		return parameterizable.getTypeParameters().stream().skip(index).findFirst().orElse(null);
	}

	/**
	 * @param parameterizable non-null
	 * @param name            fit
	 * @param deep            whether to look up in enclosing elements
	 *
	 * @return nullable
	 */
	@Nullable
	public static TypeParameterElement findTypeParameter(
			Parameterizable parameterizable,
			String name,
			boolean deep) {
		TypeParameterElement result;
		{
			TypeParameterElement shallowResult = parameterizable.getTypeParameters().stream()
					.filter(typeParameter -> typeParameter.getSimpleName().contentEquals(name))
					.findFirst().orElse(null);
			if (shallowResult == null && deep && !parameterizable.getModifiers().contains(Modifier.STATIC)) {
				Element enclosingElement = parameterizable.getEnclosingElement();
				if (enclosingElement instanceof Parameterizable) {
					result = findTypeParameter((Parameterizable) enclosingElement, name, true);
				} else {
					result = null;
				}
			} else {
				result = shallowResult;
			}
		}
		return result;
	}

	@Nullable
	public static TypeParameterElement findTypeParameter(Parameterizable parameterizable, String name) {
		return findTypeParameter(parameterizable, name, false);
	}

	public static TypeParameterElement getTypeParameter(Parameterizable parameterizable, int index) {
		TypeParameterElement result = findTypeParameter(parameterizable, index);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"parameterizable = " + parameterizable + ", " +
					"index = " + index);
		}
		return result;
	}

	/**
	 * {@link Parameterizable} is extended by {@link ExecutableElement} and {@link TypeElement}.<br>
	 * {@link ExecutableElement} also represents initializers, which may not have type parameters.<br>
	 * This method returns <code>false</code> for such elements.<br>
	 *
	 * @param parameterizable nullable
	 *
	 * @return true if proper parameterizable, false otherwise
	 */
	public static boolean isProperParameterizable(Parameterizable parameterizable) {
		return parameterizable != null && !ElementUtils.isInitializer(parameterizable);
	}

	/**
	 * {@link Parameterizable} is extended by {@link ExecutableElement} and {@link TypeElement}.<br>
	 * {@link ExecutableElement} also represents initializers, which may not have type parameters.<br>
	 * This method throws {@link IllegalArgumentException} for such elements.<br>
	 *
	 * @param parameterizable non-null
	 *
	 * @return non-null
	 */
	public static Parameterizable requireProperParameterizable(Parameterizable parameterizable) {
		if (!isProperParameterizable(parameterizable)) {
			throw new IllegalArgumentException("Require proper parameterizable: " + parameterizable);
		}
		return parameterizable;
	}
}
