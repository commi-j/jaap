package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.core.model.JavaKeywords;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.annotation.Nullable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <a href="https://docs.oracle.com/javase/specs/jls/se15/html/jls-4.html#jls-4.1">https://docs.oracle.com/javase/specs/jls/se15/html/jls-4.html#jls-4.1</a>
 */
@Builder(toBuilder = true)
@Value
public class TypeDescription {
	// TODO: Ensure consistent field combinations

	public static final Pattern PATTERN = Pattern.compile("^" +
			"(?<mainSegment>.+?)" +
			"(\\sextends\\s(?<extendsSegment>.+?))?" +
			"(\\ssuper\\s(?<superSegment>.+?))?" +
			"(<(?<chevronSegment>.*)>)?" +
			"(?<bracketsSegment>(\\[])+)?" +
			"$");

	private static final Set<String> primitiveKeywords = JavaKeywords.primitives().stream()
			.map(JavaKeywords::lowercase)
			.collect(Collectors.toSet());

	@Nullable
	Integer arrayDepth;

	String fullName;

	/**
	 * super
	 */
	@Nullable
	TypeDescription lowerBound;

	/**
	 * Non-empty or null.
	 */
	@Nullable
	List<TypeDescription> parameters;

	/**
	 * extends
	 */
	@Nullable
	List<TypeDescription> upperBound;

	private Stream<TypeDescription> doBreakDown() {
		return StreamUtils.concat(
				Stream.of(TypeDescription.of(fullName)),
				lowerBound != null ? lowerBound.doBreakDown() : Stream.empty(),
				parameters != null ? parameters.stream().flatMap(TypeDescription::doBreakDown) : Stream.empty(),
				upperBound != null ? upperBound.stream().flatMap(TypeDescription::doBreakDown) : Stream.empty());
	}

	private boolean hasOnlyFullName() {
		return arrayDepth == null && lowerBound == null && parameters == null && upperBound == null;
	}

	private String toStringRepresentation(boolean excludeParameters, boolean simplifyVariableTypes) {
		StringBuilder stringBuilder = new StringBuilder();
		{
			String fullNameString;
			if (simplifyVariableTypes) {
				int variableSeparatorIndex = fullName.indexOf(SignatureSeparators.TYPE_PARAMETER);
				if (variableSeparatorIndex != -1) {
					fullNameString = fullName.substring(variableSeparatorIndex + 1);
				} else {
					fullNameString = fullName;
				}
			} else {
				fullNameString = fullName;
			}
			stringBuilder.append(fullNameString);
		}
		if (lowerBound != null) {
			stringBuilder
					.append(" super ")
					.append(lowerBound.toStringRepresentation(excludeParameters, simplifyVariableTypes));
		}
		if (!excludeParameters && parameters != null) {
			stringBuilder
					.append("<")
					.append(parameters.stream()
							.map(parameter -> parameter.toStringRepresentation(
									excludeParameters,
									simplifyVariableTypes))
							.collect(Collectors.joining(",")))
					.append(">");
		}
		if (upperBound != null) {
			stringBuilder
					.append(" extends ")
					.append(upperBound.stream()
							.map(parameter -> parameter.toStringRepresentation(
									excludeParameters,
									simplifyVariableTypes))
							.collect(Collectors.joining("&")));
		}
		if (arrayDepth != null) {
			stringBuilder.append(StringUtils.repeat("[]", arrayDepth));
		}
		return stringBuilder.toString();
	}

	public List<TypeDescription> breakDown() {
		return doBreakDown().toList();
	}

	public Pair<TypeSignature, String> destructureVariable() {
		requireVariable();
		//
		int indexOfSeparator = fullName.indexOf(SignatureSeparators.TYPE_PARAMETER);
		//
		return Pair.of(
				TypeSignature.ofValid(fullName.substring(0, indexOfSeparator)),
				fullName.substring(indexOfSeparator + 1));
	}

	public TypeDescription getArrayComponent() {
		if (arrayDepth == null) {
			throw new IllegalArgumentException("Require array: " + this);
		}
		// TODO: Optimize.
		return TypeDescription.of(toBuilder()
				.arrayDepth(arrayDepth == 1 ? null : arrayDepth - 1)
				.build()
				.toString());
	}

	public List<TypeDescription> getParametersOrEmpty() {
		return parameters != null ? parameters : List.of();
	}

	public List<TypeDescription> getParametersOrFail() {
		return Objects.requireNonNull(parameters, toString());
	}

	public String getQualifiedName() {
		return toString().replace(SignatureSeparators.TOP_LEVEL_TYPE, SignatureSeparators.PACKAGE_OR_TYPE);
	}

	// FIXME: This method does not work well for VariableType representation
	//  as we need extra information (bounds of TypeParameter) to properly resolve erasure.
	// TODO: Probably rename to getErasure.
	public TypeSignature getSignature() {
		TypeSignature result;
		{
			if (isWildcard()) {
				if (hasUpperBound()) {
					result = getUpperBound().get(0).getSignature();
				} else {
					result = TypeSignature.ofObject();
				}
			} else {
				result = TypeSignature.ofValid(toStringRepresentation(true, false));
			}
		}
		return result;
	}

	public String getSimpleString() {
		return toStringRepresentation(false, true);
	}

	public String getVariableName() {
		return fullName.substring(fullName.indexOf(SignatureSeparators.TYPE_PARAMETER) + 1);
	}

	public String getVariableParent() {
		return fullName.substring(0, fullName.indexOf(SignatureSeparators.TYPE_PARAMETER));
	}

	public boolean hasLowerBound() {
		return lowerBound != null;
	}

	public boolean hasParameters() {
		return parameters != null;
	}

	public boolean hasUpperBound() {
		return upperBound != null;
	}

	public boolean isArray() {
		return arrayDepth != null;
	}

	public boolean isDeclared() {
		return isReference() && !isArray() && !isVariable() && !isWildcard();
	}

	public boolean isParameterized() {
		return parameters != null;
	}

	public boolean isPlain() {
		// FIXME: Can not determine difference between plain and raw.
		return isDeclared() && !isParameterized();
	}

	public boolean isPrimitive() {
		return hasOnlyFullName() && primitiveKeywords.contains(fullName);
	}

	public boolean isRaw() {
		// FIXME: Can not determine difference between plain and raw.
		return isDeclared() && !isParameterized();
	}

	public boolean isReference() {
		return !isPrimitive() && !isVoid();
	}

	public boolean isUnboundedWildcard() {
		return isWildcard() && lowerBound == null && upperBound == null;
	}

	public boolean isVariable() {
		return fullName.contains(SignatureSeparators.TYPE_PARAMETER);
	}

	public boolean isVoid() {
		return hasOnlyFullName() && Objects.equals(fullName, JavaKeywords.VOID.lowercase());
	}

	public boolean isWildcard() {
		return Objects.equals(fullName, "?");
	}

	// TODO: Support Type too.
	public boolean matches(Class<?> javaClass) {
		return Objects.equals(getQualifiedName(), javaClass.getName());
	}

	public TypeDescription requireVariable() {
		if (!isVariable()) {
			throw new IllegalArgumentException("Require variable: %s".formatted(this));
		}
		//
		return this;
	}

	public String toLongForm() {
		String result;
		{
			String resultBeforeArray;
			{
				String longName = getLongName(getFullName());
				//
				if (hasParameters()) {
					resultBeforeArray = "%s<%s>".formatted(
							longName,
							getParametersOrFail().stream()
									.map(TypeDescription::toLongForm)
									.collect(Collectors.joining(",")));
				} else {
					resultBeforeArray = longName;
				}
			}
			result = "%s%s".formatted(
					resultBeforeArray,
					Optional.ofNullable(getArrayDepth())
							.map(arrayDepth -> StringUtils.repeat("[]", arrayDepth))
							.orElse(""));
		}
		return result;
	}

	@Override
	public String toString() {
		return toStringRepresentation(false, false);
	}

	// FIXME: Think if we need this method here.
	private static String getLongName(String fullName) {
		int indexOfColon = fullName.indexOf(':');
		return indexOfColon != -1 ? fullName.substring(indexOfColon + 1) : fullName;
	}

	@Nullable
	private static Integer resolveBracketsSegment(@Nullable String bracketsSegment) {
		return bracketsSegment != null ? bracketsSegment.length() / 2 : null;
	}

	@Nullable
	static List<TypeDescription> resolveList(@Nullable String value, String delimiter) {
		List<TypeDescription> result;
		if (value != null) {
			List<String> items = new ArrayList<>();
			//
			StringTokenizer stringTokenizer = new StringTokenizer(value, delimiter + "<>", true);
			//
			StringBuilder currentItem = new StringBuilder();
			int depth = 0;
			while (stringTokenizer.hasMoreTokens()) {
				String token = stringTokenizer.nextToken();
				switch (token) {
					case "<":
						currentItem.append("<");
						depth++;
						break;
					case ">":
						currentItem.append(">");
						depth--;
						break;
					default:
						if (depth == 0 && Objects.equals(token, delimiter)) {
							items.add(currentItem.toString().trim());
							currentItem = new StringBuilder();
						} else {
							currentItem.append(token);
						}
				}
			}
			items.add(currentItem.toString().trim());
			//
			result = items.stream()
					.map(TypeDescription::of)
					.collect(Collectors.toList());
		} else {
			result = null;
		}
		return result;
	}

	@Nullable
	static TypeDescription resolveString(@Nullable String value) {
		return value != null ? of(value) : null;
	}

	public static TypeDescription of(String typeDescriptionString) {
		TypeDescription result;
		{
			Matcher matcher = PATTERN.matcher(typeDescriptionString);
			if (matcher.matches()) {
				//
				// x
				String mainSegment = matcher.group("mainSegment");
				//
				// x extends y&z
				String extendsSegment = matcher.group("extendsSegment");
				//
				// x super y
				String superSegment = matcher.group("superSegment");
				//
				// x<y,z>
				String chevronSegment = matcher.group("chevronSegment");
				//
				// x[][]
				String bracketsSegment = matcher.group("bracketsSegment");
				//
				{
					int complexity = 0;
					if (extendsSegment != null) {
						complexity += 1;
					}
					if (superSegment != null) {
						complexity += 1;
					}
					if (chevronSegment != null || bracketsSegment != null) {
						complexity += 1;
					}
					if (complexity > 1) {
						throw new IllegalArgumentException("Overcomplicated: " + typeDescriptionString);
					}
				}
				//
				result = TypeDescription.builder()
						.arrayDepth(resolveBracketsSegment(bracketsSegment))
						.fullName(mainSegment)
						.lowerBound(resolveString(superSegment))
						.parameters(resolveList(chevronSegment, ","))
						.upperBound(resolveList(extendsSegment, "&"))
						.build();
			} else {
				throw new IllegalArgumentException("Does not match pattern: " + typeDescriptionString);
			}
		}
		return result;
	}

	public static TypeDescription of(Type javaType) {
		TypeDescription result;
		{
			if (javaType instanceof Class<?> javaClass) {
				result = ofNonParameterized(javaClass);
			} else if (javaType instanceof ParameterizedType parameterizedType) {
				result = new TypeDescription(
						null,
						of(parameterizedType.getRawType()).toString(),
						null,
						Stream.of(parameterizedType.getActualTypeArguments())
								.map(TypeDescription::of)
								.toList(),
						null);
			} else if (javaType instanceof TypeVariable<?> typeVariable) {
				result = new TypeDescription(
						null,
						"%s%s%s".formatted(
								ElementSignature.of(typeVariable.getGenericDeclaration()),
								SignatureSeparators.TYPE_PARAMETER,
								typeVariable.getName()),
						null,
						null,
						null);
			} else if (javaType instanceof WildcardType wildcardType) {
				result = new TypeDescription(
						null,
						"?",
						wildcardType.getLowerBounds().length != 0
								? Stream.of(wildcardType.getLowerBounds())
								.map(TypeDescription::of)
								.findFirst()
								.orElseThrow()
								: null,
						null,
						wildcardType.getUpperBounds().length != 0
								? Stream.of(wildcardType.getUpperBounds())
								.map(TypeDescription::of)
								.toList()
								: null);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static TypeDescription ofNonParameterized(Class<?> type) {
		// TODO: Ensure non-parameterized.
		//
		return of(ClassUtils.getSignature(type));
	}

	public static TypeDescription ofNull() {
		return of("null");
	}

	public static TypeDescription ofSimplyParameterized(Class<?> type, Class<?>... parameters) {
		return of(parameters.length > 0
				? "%s<%s>".formatted(
				ClassUtils.getSignature(type),
				Stream.of(parameters).map(ClassUtils::getSignature).collect(Collectors.joining(",")))
				: ClassUtils.getSignature(type));
	}

	public static TypeDescription ofVoid() {
		return of("void");
	}

	public static TypeDescription ofWildcard() {
		return of("?");
	}
}
