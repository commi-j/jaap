package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;
import tk.labyrinth.jaap.model.util.TypeElementHandleUtils;

import java.util.Objects;

@Value
public class DefaultRawTypeHandle implements RawTypeHandle {

	TypeElementHandle typeElementHandle;

	TypeHandleFactory typeHandleFactory;

	public DefaultRawTypeHandle(TypeElementHandle typeElementHandle, TypeHandleFactory typeHandleFactory) {
		this.typeElementHandle = TypeElementHandleUtils.requireGeneric(typeElementHandle);
		this.typeHandleFactory = Objects.requireNonNull(typeHandleFactory);
	}

	@Override
	public GenericTypeHandle asGenericType() {
		return new DefaultGenericTypeHandle(null, typeElementHandle, typeHandleFactory);
	}

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeElementHandle.getSignatureString())
				.build();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public TypeElementHandle toElement() {
		return typeElementHandle;
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
