package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.ElementHandle;

public interface IsElementHandle {

	ElementHandle asElement();
}
