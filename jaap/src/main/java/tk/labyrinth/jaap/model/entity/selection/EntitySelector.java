package tk.labyrinth.jaap.model.entity.selection;

import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import java.util.List;

@Value
public class EntitySelector {

	@NonNull
	EntitySelectionContext context;

	@NonNull
	String simpleName;

	private EntitySelector(@NonNull EntitySelectionContext context, @NonNull String simpleName) {
		this.context = context;
		this.simpleName = simpleName;
		//
		if (simpleName.contains(".")) {
			// TODO: Check for identifier pattern.
			throw new IllegalArgumentException("Require simple name: %s".formatted(simpleName));
		}
	}

	public EntitySelectorChain asChain() {
		return EntitySelectorChain.build(context, List.of(simpleName));
	}

	public boolean canBeMethod() {
		return context.canBeMethod();
	}

	public boolean canBePackage() {
		return context.canBePackage();
	}

	public boolean canBeType() {
		return context.canBeType();
	}

	public boolean canBeVariable() {
		return context.canBeVariable();
	}

	public boolean matches(ProcessingEnvironment processingEnvironment, Element element) {
		boolean result;
		if (element.getSimpleName().contentEquals(simpleName)) {
			result = switch (element.getKind()) {
				case CLASS, INTERFACE -> canBeType();
				case FIELD -> canBeVariable();
				case METHOD ->
					// First condition is cheap and ensures we are looking for Method.
					// Second one does full signature matching.
//						canBeMethod() && MethodSelector.matches(
//								processingEnvironment,
//								MethodSignatureUtils.createSimple(processingEnvironment, (ExecutableElement) element),
//								toMethodSelector());
					//
					// TODO: Clean up. Methods are not expected to be compared this way.
						throw new UnsupportedOperationException();
				case PACKAGE -> canBePackage();
				default -> false;
			};
		} else {
			result = false;
		}
		return result;
	}

	public MethodSelector toMethodSelector() {
		return MethodSelector.of(context.getMethodArgumentSignatures(), simpleName);
	}

	private static String extractSimpleName(IdentifierTree identifierTree) {
		return identifierTree.getName().toString();
	}

	public static EntitySelector build(EntitySelectionContext selectionContext, IdentifierTree identifierTree) {
		return build(selectionContext, extractSimpleName(identifierTree));
	}

	public static EntitySelector build(EntitySelectionContext selectionContext, String simpleName) {
		return new EntitySelector(selectionContext, simpleName);
	}

	public static EntitySelector createFrom(
			CompilationUnitContext compilationUnitContext,
			MemberSelectTree memberSelectTree) {
		return EntitySelector.build(
				EntitySelectionContext.createFrom(compilationUnitContext, memberSelectTree),
				memberSelectTree.getIdentifier().toString());
	}

	public static EntitySelector forMethod(String simpleName, List<MethodArgumentSignature> methodArgumentSignatures) {
		return build(EntitySelectionContext.forMethod(methodArgumentSignatures), simpleName);
	}

	public static EntitySelector forMethodInvocation(
			String simpleName,
			List<MethodArgumentSignature> methodArgumentSignatures) {
		return build(EntitySelectionContext.forMethodInvocation(methodArgumentSignatures), simpleName);
	}

	public static EntitySelector forTypeName(IdentifierTree identifierTree) {
		return forTypeName(extractSimpleName(identifierTree));
	}

	public static EntitySelector forTypeName(String simpleName) {
		return build(EntitySelectionContext.forTypeName(), simpleName);
	}

	public static EntitySelector forVariable(IdentifierTree identifierTree) {
		return forVariable(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariable(String simpleName) {
		return build(EntitySelectionContext.forVariable(), simpleName);
	}

	public static EntitySelector forVariableOrType(IdentifierTree identifierTree) {
		return forVariableOrType(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariableOrType(String simpleName) {
		return build(EntitySelectionContext.forVariableOrType(), simpleName);
	}

	public static EntitySelector forVariableType(IdentifierTree identifierTree) {
		return forVariableType(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariableType(String simpleName) {
		return build(EntitySelectionContext.forVariableType(), simpleName);
	}
}
