package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

@Value
public class DefaultWildcardTypeHandle implements WildcardTypeHandle {

	/**
	 * upper bound
	 */
	@Nullable
	List<TypeHandle> extendsBounds;

	/**
	 * lower bound
	 */
	@Nullable
	TypeHandle superBound;

	TypeHandleFactory typeHandleFactory;

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName("?")
				.lowerBound(superBound != null ? superBound.getDescription() : null)
				.upperBound(extendsBounds != null
						? extendsBounds.stream()
						.map(TypeHandle::getDescription)
						.collect(Collectors.toList())
						: null)
				.build();
	}
}
