package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Constructor or method or type type parameter.<br>
 * <br>
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * * - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface TypeParameterElementHandle extends
		HasParent<@NonNull ElementHandle>,
		HasSignature<String>,
		HasTopLevelTypeElement {

	Stream<TypeHandle> getBoundStream();

	default List<TypeHandle> getBounds() {
		return getBoundStream().collect(Collectors.toList());
	}

	String getName();

	@Override
	default TypeElementHandle getTopLevelTypeElement() {
		TypeElementHandle result;
		{
			ElementHandle parent = getParent();
			if (parent.isExecutableElement()) {
				result = parent.asExecutableElement().getTopLevelTypeElement();
			} else if (parent.isTypeElement()) {
				result = parent.asTypeElement().getTopLevelTypeElement();
			} else {
				throw new UnreachableStateException(ExceptionUtils.render(this));
			}
		}
		return result;
	}

	TypeHandle getType();
}
