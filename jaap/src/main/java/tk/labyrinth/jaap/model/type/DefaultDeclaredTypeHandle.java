package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

// FIXME: Rework into smth smarter.
@Value
public class DefaultDeclaredTypeHandle implements DeclaredTypeHandle {

	// TODO: Ensure same size as of parameters in type.
	@Nullable
	List<TypeHandle> parameters;

	// TODO: Ensure consistent with parameters.
	TypeElementHandle typeElementHandle;

	TypeHandleFactory typeHandleFactory;

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		return typeElementHandle.asAnnotationType();
	}

	@Override
	public GenericTypeHandle asGenericType() {
		if (!isGenericType()) {
			throw new IllegalStateException("Require generic: " + this);
		}
		return new DefaultGenericTypeHandle(parameters, typeElementHandle, typeHandleFactory);
	}

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		if (!isParameterizedType()) {
			throw new IllegalStateException("Require parameterized: " + this);
		}
		return new DefaultParameterizedTypeHandle(parameters, typeElementHandle, typeHandleFactory);
	}

	@Override
	public PlainTypeHandle asPlainType() {
		if (!isPlainType()) {
			throw new IllegalStateException("Require plain: " + this);
		}
		return new DefaultPlainTypeHandle(typeElementHandle, typeHandleFactory);
	}

	@Override
	public RawTypeHandle asRawType() {
		if (!isRawType()) {
			throw new IllegalStateException("Require raw: " + this);
		}
		return new DefaultRawTypeHandle(typeElementHandle, typeHandleFactory);
	}

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeElementHandle.getSignatureString())
				.parameters(parameters != null
						? parameters.stream()
						.map(TypeHandle::getDescription)
						.collect(Collectors.toList())
						: null)
				.build();
	}

	@Override
	public GenericContext getGenericContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isAnnotationType() {
		return typeElementHandle.isAnnotationType();
	}

	@Override
	public boolean isGenericType() {
		return typeElementHandle.hasTypeParameters();
	}

	@Override
	public boolean isParameterizedType() {
		return parameters != null;
	}

	@Override
	public boolean isPlainType() {
		return !typeElementHandle.hasTypeParameters();
	}

	@Override
	public boolean isRawType() {
		return typeElementHandle.hasTypeParameters() && parameters == null;
	}

	@Override
	public TypeElementHandle toElement() {
		return typeElementHandle;
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
