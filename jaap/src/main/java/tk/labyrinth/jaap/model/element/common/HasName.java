package tk.labyrinth.jaap.model.element.common;

public interface HasName {

	String getName();
}
