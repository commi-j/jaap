package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

@Value
public class DefaultGenericTypeHandle implements GenericTypeHandle {

	// TODO: Ensure same size as of parameters in type.
	@Nullable
	List<TypeHandle> parameters;

	// TODO: Check generic, not plain.
	TypeElementHandle typeElementHandle;

	TypeHandleFactory typeHandleFactory;

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		if (parameters == null) {
			throw new IllegalStateException("Require parameterized: " + this);
		}
		return new DefaultParameterizedTypeHandle(parameters, typeElementHandle, typeHandleFactory);
	}

	@Override
	public RawTypeHandle asRawType() {
		if (parameters != null) {
			throw new IllegalStateException("Require raw: " + this);
		}
		return new DefaultRawTypeHandle(typeElementHandle, typeHandleFactory);
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeElementHandle.getSignatureString())
				.parameters(parameters != null
						? parameters.stream()
						.map(TypeHandle::getDescription)
						.collect(Collectors.toList())
						: null)
				.build();
	}

	@Override
	public boolean isParameterizedType() {
		return parameters != null;
	}

	@Override
	public boolean isRawType() {
		return parameters == null;
	}
}
