package tk.labyrinth.jaap.model.factory;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.model.type.DefaultArrayTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultDeclaredTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultParameterizedTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultPlainTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultPrimitiveTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultRawTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultVariableTypeHandle;
import tk.labyrinth.jaap.model.type.DefaultWildcardTypeHandle;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public abstract class TypeHandleFactoryBase implements TypeHandleFactory {

	private final ElementHandleFactory elementHandleFactory;

	@Nullable
	@Override
	public TypeHandle find(String typeDescriptionString) {
		return find(TypeDescription.of(typeDescriptionString));
	}

	@Nullable
	@Override
	public TypeHandle find(TypeSignature typeSignature) {
		return find(typeSignature.toDescription());
	}

	@Nullable
	@Override
	public ArrayTypeHandle findArray(String arrayTypeDescriptionString) {
		return findArray(TypeDescription.of(arrayTypeDescriptionString));
	}

	@Nullable
	@Override
	public ArrayTypeHandle findArray(TypeDescription arrayTypeDescription) {
		TypeHandle typeHandle = find(arrayTypeDescription.getArrayComponent());
		return typeHandle != null ? new DefaultArrayTypeHandle(typeHandle, this) : null;
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(String declaredTypeDescriptionString) {
		return findDeclared(TypeDescription.of(declaredTypeDescriptionString));
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(TypeDescription declaredTypeDescription) {
		TypeElementHandle typeElementHandle = elementHandleFactory.findType(declaredTypeDescription.getFullName());
		List<TypeHandle> parameters;
		{
			List<TypeDescription> innerParameters = declaredTypeDescription.getParameters();
			parameters = innerParameters != null
					? innerParameters.stream()
					.map(this::find)
					.collect(Collectors.toList())
					: null;
		}
		//
		return typeElementHandle != null && (parameters == null || !parameters.contains(null))
				? new DefaultDeclaredTypeHandle(parameters, typeElementHandle, this)
				: null;
	}

	@Nullable
	@Override
	public ParameterizedTypeHandle findParameterized(String parameterizedTypeDescriptionString) {
		return findParameterized(TypeDescription.of(parameterizedTypeDescriptionString));
	}

	@Nullable
	@Override
	public ParameterizedTypeHandle findParameterized(TypeDescription parameterizedTypeDescription) {
		TypeElementHandle typeElementHandle = elementHandleFactory.findType(parameterizedTypeDescription.getFullName());
		List<TypeHandle> parameters = parameterizedTypeDescription.getParametersOrFail().stream()
				.map(this::find)
				.collect(Collectors.toList());
		//
		return typeElementHandle != null && !parameters.contains(null)
				? new DefaultParameterizedTypeHandle(parameters, typeElementHandle, this)
				: null;
	}

	@Nullable
	@Override
	public PlainTypeHandle findPlain(String plainTypeDescriptionString) {
		return findPlain(TypeDescription.of(plainTypeDescriptionString));
	}

	@Nullable
	@Override
	public PlainTypeHandle findPlain(TypeDescription plainTypeDescription) {
		// TODO: Require plain.
		//
		TypeElementHandle typeElementHandle = elementHandleFactory.findType(plainTypeDescription.getFullName());
		return typeElementHandle != null ? new DefaultPlainTypeHandle(typeElementHandle, this) : null;
	}

	@Nullable
	@Override
	public PrimitiveTypeHandle findPrimitive(String primitiveTypeDescriptionString) {
		return findPrimitive(TypeDescription.of(primitiveTypeDescriptionString));
	}

	@Nullable
	@Override
	public PrimitiveTypeHandle findPrimitive(TypeDescription primitiveTypeDescription) {
		// TODO: Require primitive.
		//
		return new DefaultPrimitiveTypeHandle(primitiveTypeDescription.getFullName(), this);
	}

	@Nullable
	@Override
	public RawTypeHandle findRaw(String rawTypeDescriptionString) {
		return findRaw(TypeDescription.of(rawTypeDescriptionString));
	}

	@Nullable
	@Override
	public RawTypeHandle findRaw(TypeDescription rawTypeDescription) {
		// TODO: Require raw.
		//
		TypeElementHandle typeElementHandle = elementHandleFactory.findType(rawTypeDescription.getFullName());
		return typeElementHandle != null ? new DefaultRawTypeHandle(typeElementHandle, this) : null;
	}

	@Nullable
	@Override
	public VariableTypeHandle findVariable(String variableTypeDescriptionString) {
		return findVariable(TypeDescription.of(variableTypeDescriptionString));
	}

	@Nullable
	@Override
	public VariableTypeHandle findVariable(TypeDescription variableTypeDescription) {
		// TODO: Require variable.
		//
		TypeParameterElementHandle typeParameterElementHandle = elementHandleFactory.findTypeParameter(
				variableTypeDescription.getFullName());
		return typeParameterElementHandle != null
				? new DefaultVariableTypeHandle(this, typeParameterElementHandle)
				: null;
	}

	@Nullable
	@Override
	public WildcardTypeHandle findWildcard(String wildcardTypeDescriptionString) {
		return findWildcard(TypeDescription.of(wildcardTypeDescriptionString));
	}

	@Nullable
	@Override
	public WildcardTypeHandle findWildcard(TypeDescription wildcardTypeDescription) {
		TypeHandle lowerBound;
		boolean lowerBoundValid;
		{
			if (wildcardTypeDescription.getLowerBound() != null) {
				lowerBound = find(wildcardTypeDescription.getLowerBound());
				lowerBoundValid = lowerBound != null;
			} else {
				lowerBound = null;
				lowerBoundValid = true;
			}
		}
		//
		List<TypeHandle> upperBound;
		boolean upperBoundValid;
		{
			if (wildcardTypeDescription.getUpperBound() != null) {
				upperBound = wildcardTypeDescription.getUpperBound().stream()
						.map(this::find)
						.collect(Collectors.toList());
				upperBoundValid = !upperBound.contains(null);
			} else {
				upperBound = null;
				upperBoundValid = true;
			}
		}
		//
		return lowerBoundValid && upperBoundValid
				? new DefaultWildcardTypeHandle(upperBound, lowerBound, this)
				: null;
	}

	@Override
	public TypeHandle get(TypeSignature typeSignature) {
		TypeHandle result = find(typeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: ordinaryTypeSignature = " + typeSignature);
		}
		return result;
	}

	@Override
	public TypeHandle get(String typeDescriptionString) {
		TypeHandle result = find(typeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescriptionString = " + typeDescriptionString);
		}
		return result;
	}

	@Override
	public TypeHandle get(TypeDescription typeDescription) {
		TypeHandle result = find(typeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescription = " + typeDescription);
		}
		return result;
	}

	@Override
	public ArrayTypeHandle getArray(String arrayTypeDescriptionString) {
		ArrayTypeHandle result = findArray(arrayTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"arrayTypeDescriptionString = " + arrayTypeDescriptionString);
		}
		return result;
	}

	@Override
	public ArrayTypeHandle getArray(TypeDescription arrayTypeDescription) {
		ArrayTypeHandle result = findArray(arrayTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"arrayTypeDescription = " + arrayTypeDescription);
		}
		return result;
	}

	@Override
	public DeclaredTypeHandle getDeclared(String declaredTypeDescriptionString) {
		DeclaredTypeHandle result = findDeclared(declaredTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"declaredTypeDescriptionString = " + declaredTypeDescriptionString);
		}
		return result;
	}

	@Override
	public DeclaredTypeHandle getDeclared(TypeDescription declaredTypeDescription) {
		DeclaredTypeHandle result = findDeclared(declaredTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"declaredTypeDescription = " + declaredTypeDescription);
		}
		return result;
	}

	@Override
	public ParameterizedTypeHandle getParameterized(String parameterizedTypeDescriptionString) {
		ParameterizedTypeHandle result = findParameterized(parameterizedTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"parameterizedTypeDescriptionString = " + parameterizedTypeDescriptionString);
		}
		return result;
	}

	@Override
	public ParameterizedTypeHandle getParameterized(TypeDescription parameterizedTypeDescription) {
		ParameterizedTypeHandle result = findParameterized(parameterizedTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"parameterizedTypeDescription = " + parameterizedTypeDescription);
		}
		return result;
	}

	@Override
	public PlainTypeHandle getPlain(String plainTypeDescriptionString) {
		PlainTypeHandle result = findPlain(plainTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: plainTypeDescriptionString = " + plainTypeDescriptionString);
		}
		return result;
	}

	@Override
	public PlainTypeHandle getPlain(TypeDescription plainTypeDescription) {
		PlainTypeHandle result = findPlain(plainTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: plainTypeDescription = " + plainTypeDescription);
		}
		return result;
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(String primitiveTypeDescriptionString) {
		PrimitiveTypeHandle result = findPrimitive(primitiveTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: primitiveTypeDescriptionString = " + primitiveTypeDescriptionString);
		}
		return result;
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(TypeDescription primitiveTypeDescription) {
		PrimitiveTypeHandle result = findPrimitive(primitiveTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: primitiveTypeDescription = " + primitiveTypeDescription);
		}
		return result;
	}

	@Override
	public RawTypeHandle getRaw(String rawTypeDescriptionString) {
		RawTypeHandle result = findRaw(rawTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: rawTypeDescriptionString = " + rawTypeDescriptionString);
		}
		return result;
	}

	@Override
	public RawTypeHandle getRaw(TypeDescription rawTypeDescription) {
		RawTypeHandle result = findRaw(rawTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: rawTypeDescription = " + rawTypeDescription);
		}
		return result;
	}

	@Override
	public VariableTypeHandle getVariable(String variableTypeDescriptionString) {
		VariableTypeHandle result = findVariable(variableTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"variableTypeDescriptionString = " + variableTypeDescriptionString);
		}
		return result;
	}

	@Override
	public VariableTypeHandle getVariable(TypeDescription variableTypeDescription) {
		VariableTypeHandle result = findVariable(variableTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: variableTypeDescription = " + variableTypeDescription);
		}
		return result;
	}

	@Override
	public WildcardTypeHandle getWildcard(String wildcardTypeDescriptionString) {
		WildcardTypeHandle result = findWildcard(wildcardTypeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"wildcardTypeDescriptionString = " + wildcardTypeDescriptionString);
		}
		return result;
	}

	@Override
	public WildcardTypeHandle getWildcard(TypeDescription wildcardTypeDescription) {
		WildcardTypeHandle result = findWildcard(wildcardTypeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: wildcardTypeDescription = " + wildcardTypeDescription);
		}
		return result;
	}
}
