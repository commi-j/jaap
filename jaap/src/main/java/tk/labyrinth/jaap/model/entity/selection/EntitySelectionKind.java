package tk.labyrinth.jaap.model.entity.selection;

// TODO: Support static imports & imports of fields & methods
public enum EntitySelectionKind {
	IMPORT_DECLARATION,
	METHOD,
	METHOD_INVOCATION,
	METHOD_INVOCATION_ARGUMENT,
	METHOD_INVOCATION_TARGET,
	PACKAGE,
	PACKAGE_DECLARATION,
	STAR_OR_TYPE_NAME,
	TYPE,
	TYPE_NAME,
	TYPE_NAME_OR_PACKAGE,
	TYPE_OR_PACKAGE,
	UNDEFINED,
	VARIABLE,
	VARIABLE_INITIALIZER,
	VARIABLE_OR_TYPE,
	VARIABLE_OR_TYPE_OR_PACKAGE,
	/**
	 * Type of variable in variable declaration, e.g.:<br>
	 * - VARIABLE_TYPE NAME;<br>
	 * - VARIABLE_TYPE NAME = EXPRESSION;<br>
	 */
	VARIABLE_TYPE,
}
