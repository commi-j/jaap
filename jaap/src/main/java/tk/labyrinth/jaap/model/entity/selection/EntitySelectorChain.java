package tk.labyrinth.jaap.model.entity.selection;

import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.util.MemberSelectTreeUtils;
import tk.labyrinth.jaap.langmodel.tree.util.MethodInvocationTreeUtils;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.stream.IntStream;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class EntitySelectorChain {

	@NonNull
	EntitySelectionContext context;

	@NonNull
	List<@NonNull String> simpleNames;

	public boolean canBeMethod() {
		return context.canBeMethod();
	}

	public boolean canBePackage() {
		return context.canBePackage();
	}

	public boolean canBeType() {
		return context.canBeType();
	}

	public boolean canBeVariable() {
		return context.canBeVariable();
	}

	public EntitySelectionContext getContext(int index) {
		return IntStream.range(0, simpleNames.size() - index - 1)
				//
				// null here because we don't use it, just need number of iterations.
				.mapToObj(internalIndex -> (EntitySelectionContext) null)
				.reduce(context, (nextContext, ignored) -> nextContext.getParent());
	}

	public int getLength() {
		return simpleNames.size();
	}

	public String getLongName() {
		return String.join(".", simpleNames);
	}

	public EntitySelector head() {
		return EntitySelector.build(getContext(0), simpleNames.get(0));
	}

	public EntitySelectorChain prepend(String simpleName) {
		// FIXME: Replace flatten with concat of E and Collection<E>.
		return new EntitySelectorChain(context, ListUtils.flatten(List.of(simpleName), simpleNames));
	}

	public Pair<EntitySelector, @Nullable EntitySelectorChain> split() {
		return Pair.of(
				head(),
				simpleNames.size() > 1
						? new EntitySelectorChain(context, simpleNames.subList(1, simpleNames.size()))
						: null);
	}

	private static List<String> extractSimpleNames(MemberSelectTree memberSelectTree) {
		return MemberSelectTreeUtils.getNameChain(memberSelectTree).toList();
	}

	private static List<String> extractSimpleNames(MethodInvocationTree methodInvocationTree) {
		return MethodInvocationTreeUtils.getNameChain(methodInvocationTree).toList();
	}

	public static EntitySelectorChain build(EntitySelectionContext context, List<String> simpleNames) {
		return new EntitySelectorChain(context, simpleNames);
	}

	public static EntitySelectorChain build(EntitySelectionContext context, MemberSelectTree memberSelectTree) {
		return build(context, extractSimpleNames(memberSelectTree));
	}

	public static EntitySelectorChain build(EntitySelectionContext context, MethodInvocationTree methodInvocationTree) {
		return build(context, extractSimpleNames(methodInvocationTree));
	}

	public static EntitySelectorChain forMethod(
			List<String> simpleNames,
			List<MethodArgumentSignature> methodArgumentSignatures) {
		return build(EntitySelectionContext.forMethod(methodArgumentSignatures), simpleNames);
	}

	public static EntitySelectorChain forMethod(
			MemberSelectTree memberSelectTree,
			List<MethodArgumentSignature> methodArgumentSignatures) {
		return build(EntitySelectionContext.forMethod(methodArgumentSignatures), extractSimpleNames(memberSelectTree));
	}

	public static EntitySelectorChain forMethod(
			MethodInvocationTree methodInvocationTree,
			List<MethodArgumentSignature> methodArgumentSignatures) {
		return build(EntitySelectionContext.forMethod(methodArgumentSignatures), extractSimpleNames(methodInvocationTree));
	}

	public static EntitySelectorChain forStarOrTypeInImport(List<String> simpleNames) {
		return build(EntitySelectionContext.forStarOrTypeName(), simpleNames);
	}

	public static EntitySelectorChain forType(List<String> simpleNames) {
		return build(EntitySelectionContext.forType(), simpleNames);
	}

	public static EntitySelectorChain forVariable(List<String> simpleNames) {
		return build(EntitySelectionContext.forVariable(), simpleNames);
	}

	public static EntitySelectorChain forVariableType(List<String> simpleNames) {
		return build(EntitySelectionContext.forVariableType(), simpleNames);
	}

	public static EntitySelectorChain forVariableType(MemberSelectTree memberSelectTree) {
		return forVariableType(extractSimpleNames(memberSelectTree));
	}
}
