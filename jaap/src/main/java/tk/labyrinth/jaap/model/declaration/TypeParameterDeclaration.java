package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class TypeParameterDeclaration {

	@Builder.Default
	List<TypeDescription> boundTypes = List.of();

	String name;
}
