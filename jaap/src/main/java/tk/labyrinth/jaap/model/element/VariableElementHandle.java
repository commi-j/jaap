package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.entity.mixin.HasSelectableMembers;

/**
 * Field of type or formal parameter of constructor or method.<br>
 * <br>
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * * - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface VariableElementHandle extends
		HasParent<@NonNull ElementHandle>,
		HasSelectableMembers,
		HasTopLevelTypeElement {

	@Override
	default TypeElementHandle getTopLevelTypeElement() {
		TypeElementHandle result;
		{
			ElementHandle parent = getParent();
			if (parent.isExecutableElement()) {
				result = parent.asExecutableElement().getTopLevelTypeElement();
			} else if (parent.isTypeElement()) {
				result = parent.asTypeElement().getTopLevelTypeElement();
			} else {
				throw new UnreachableStateException(ExceptionUtils.render(this));
			}
		}
		return result;
	}

	TypeHandle getType();
}
