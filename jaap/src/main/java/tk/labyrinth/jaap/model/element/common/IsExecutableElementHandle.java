package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.ExecutableElementHandle;

public interface IsExecutableElementHandle {

	ExecutableElementHandle asExecutableElement();
}
