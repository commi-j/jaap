package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.annotation.common.HasAnnotations;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.element.common.HasTypeParameters;
import tk.labyrinth.jaap.handle.element.util.MethodElementHandleUtils;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeAnnotationTypeHandle;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.element.common.IsElementHandle;
import tk.labyrinth.jaap.model.entity.mixin.HasSelectableMembers;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * * - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface TypeElementHandle extends
		HasAnnotations,
		// TODO: HasParent<PackageOrTypeElementHandle>
		HasParent<@Nullable ElementHandle>,
		HasSelectableMembers,
		HasSignature<TypeSignature>,
		HasTopLevelTypeElement,
		HasTypeParameters,
		IsElementHandle,
		MayBeAnnotationTypeHandle {

	@Nullable
	default MethodElementHandle findDeclaredMethodByName(String methodName) {
		return getDeclaredMethodStreamByName(methodName).collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	default MethodElementHandle findMethodByName(String methodName) {
		return getAllMethodStream()
				.filter(method -> Objects.equals(method.getName(), methodName))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	TypeElementHandle findSuperclass();

	default Stream<FieldElementHandle> getAllFieldStream() {
		return getAllTypeStream().flatMap(TypeElementHandle::getDeclaredFieldStream);
	}

	default List<FieldElementHandle> getAllFields() {
		return getAllFieldStream().collect(Collectors.toList());
	}

	default Stream<? extends MethodElementHandle> getAllMethodStream() {
		return getAllTypeStream().flatMap(TypeElementHandle::getDeclaredMethodStream);
	}

	default List<? extends MethodElementHandle> getAllMethods() {
		return getAllMethodStream().collect(Collectors.toList());
	}

	Stream<? extends TypeElementHandle> getAllTypeStream();

	default List<? extends TypeElementHandle> getAllTypes() {
		return getAllTypeStream().collect(Collectors.toList());
	}

	String getCanonicalName();

	Stream<ConstructorElementHandle> getConstructorStream();

	default List<ConstructorElementHandle> getConstructors() {
		return getConstructorStream().collect(Collectors.toList());
	}

	Stream<FieldElementHandle> getDeclaredFieldStream();

	default List<FieldElementHandle> getDeclaredFields() {
		return getDeclaredFieldStream().collect(Collectors.toList());
	}

	default MethodElementHandle getDeclaredMethodByName(String methodName) {
		MethodElementHandle result = findDeclaredMethodByName(methodName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"methodName = " + methodName + ", " +
					"this = " + this);
		}
		return result;
	}

	Stream<? extends MethodElementHandle> getDeclaredMethodStream();

	default Stream<? extends MethodElementHandle> getDeclaredMethodStreamByName(String methodName) {
		return getDeclaredMethodStream().filter(declaredMethod -> Objects.equals(declaredMethod.getName(), methodName));
	}

	default List<? extends MethodElementHandle> getDeclaredMethods() {
		return getDeclaredMethodStream().collect(Collectors.toList());
	}

	default List<? extends MethodElementHandle> getDeclaredMethodsByName(String methodName) {
		return getDeclaredMethodStreamByName(methodName).collect(Collectors.toList());
	}

	Stream<? extends TypeElementHandle> getDirectSupertypeStream();

	default List<? extends TypeElementHandle> getDirectSupertypes() {
		return getDirectSupertypeStream().collect(Collectors.toList());
	}

	FieldElementHandle getField(String fieldName);

	String getLongName();

	default MethodElementHandle getMethodByName(String methodName) {
		MethodElementHandle result = findMethodByName(methodName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"methodName = " + methodName + ", " +
					"this = " + this);
		}
		return result;
	}

	Stream<? extends TypeElementHandle> getNestedTypeStream();

	default List<? extends TypeElementHandle> getNestedTypes() {
		return getNestedTypeStream().collect(Collectors.toList());
	}

	default Stream<? extends MethodElementHandle> getNonOverridenMethodStream() {
		return MethodElementHandleUtils.filterNonOverriden(getAllMethodStream());
	}

	default Stream<? extends MethodElementHandle> getNonOverridenMethodStream(String methodName) {
		return getNonOverridenMethodStream().filter(method -> Objects.equals(method.getName(), methodName));
	}

	default List<? extends MethodElementHandle> getNonOverridenMethods() {
		return getNonOverridenMethodStream().collect(Collectors.toList());
	}

	default List<? extends MethodElementHandle> getNonOverridenMethods(String methodName) {
		return getNonOverridenMethodStream(methodName).collect(Collectors.toList());
	}

	PackageElementHandle getPackage();

	String getPackageQualifiedName();

	String getQualifiedName();

	String getSimpleName();

	default TypeElementHandle getSuperclass() {
		TypeElementHandle result = findSuperclass();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}

	@Override
	default TypeElementHandle getTopLevelTypeElement() {
		TypeElementHandle result;
		{
			ElementHandle parent = getParent();
			if (parent != null && parent.isTypeElement()) {
				result = parent.asTypeElement().getTopLevelTypeElement();
			} else {
				result = this;
			}
		}
		return result;
	}

	default Stream<TypeElementHandle> getTypeChain() {
		ElementHandle parent = getParent();
		return parent != null && parent.isTypeElement()
				? StreamUtils.concat(this, parent.asTypeElement().getTypeChain())
				: Stream.of(this);
	}

	/**
	 * {@link Object} is assignable from {@link String};<br>
	 * {@link String} is <b>not</b> assignable from {@link Object};<br>
	 * <br>
	 * {@link String} is assignable to {@link Object};<br>
	 * {@link Object} is <b>not</b> assignable to {@link String};<br>
	 *
	 * @param other non-null
	 *
	 * @return whether <b>this</b> is parent of <b>other</b>
	 *
	 * @see #isAssignableTo(TypeElementHandle)
	 */
	default boolean isAssignableFrom(TypeElementHandle other) {
		return other.isAssignableTo(this);
	}

	/**
	 * {@link String} is assignable to {@link Object};<br>
	 * {@link Object} is <b>not</b> assignable to {@link String};<br>
	 * <br>
	 * {@link Object} is assignable from {@link String};<br>
	 * {@link String} is <b>not</b> assignable from {@link Object};<br>
	 *
	 * @param other non-null
	 *
	 * @return whether <b>this</b> is child of <b>other</b>
	 *
	 * @see #isAssignableFrom(TypeElementHandle)
	 */
	default boolean isAssignableTo(TypeElementHandle other) {
		// TODO: Make sure it can not cause StackOverflow or rework into using #getAllSupertypes().
		return Objects.equals(this, other) ||
				getDirectSupertypes().stream().anyMatch(directSupertype -> directSupertype.isAssignableTo(other));
	}

	boolean isInterface();

	@Nullable
	MethodElementHandle selectMethodElement(MethodSelector methodSelector);

	@Nullable
	MethodElementHandle selectMethodElement(MethodSimpleSignature methodSimpleSignature);

	@Nullable
	MethodElementHandle selectMethodElement(String methodName, List<TypeHandle> argumentTypes);

	@Nullable
	default MethodElementHandle selectMethodElement(String methodName, Stream<TypeHandle> argumentTypes) {
		return selectMethodElement(methodName, argumentTypes.collect(Collectors.toList()));
	}

	@Nullable
	default MethodElementHandle selectMethodElement(String methodName, TypeHandle... argumentTypes) {
		return selectMethodElement(methodName, List.of(argumentTypes));
	}

	default MethodElementHandle selectMethodElementOrFail(MethodSimpleSignature methodSimpleSignature) {
		return Objects.requireNonNull(selectMethodElement(methodSimpleSignature));
	}

	default MethodElementHandle selectMethodElementOrFail(String methodName, List<TypeHandle> argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodName, argumentTypes));
	}

	default MethodElementHandle selectMethodElementOrFail(String methodName, Stream<TypeHandle> argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodName, argumentTypes));
	}

	default MethodElementHandle selectMethodElementOrFail(String methodName, TypeHandle... argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodName, argumentTypes));
	}

	DeclaredTypeHandle toType();
}
