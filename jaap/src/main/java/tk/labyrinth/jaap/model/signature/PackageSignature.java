package tk.labyrinth.jaap.model.signature;

import lombok.Value;

import java.util.List;

/**
 * Variants:<br>
 * - (unnamed);<br>
 * - foo;<br>
 * - foo.bar;<br>
 */
@Value(staticConstructor = "of")
public class PackageSignature {

	/**
	 * 0..n, 0 means unnamed package.
	 */
	List<String> packageNames;

	private PackageSignature(List<String> packageNames) {
		if (packageNames.stream().anyMatch(String::isEmpty)) {
			throw new IllegalArgumentException(String.format("Require no empty elements: %s", packageNames));
		}
		this.packageNames = packageNames;
	}

	public boolean isUnnamed() {
		return packageNames.isEmpty();
	}

	@Override
	public String toString() {
		return String.join(".", packageNames);
	}

	public static PackageSignature empty() {
		return of(List.of());
	}

	public static PackageSignature of(String packageSignatureString) {
		return !packageSignatureString.isEmpty()
				? of(List.of(packageSignatureString.split("\\.")))
				: empty();
	}

	public static PackageSignature of(Package javaPackage) {
		return of(javaPackage.getName());
	}
}
