package tk.labyrinth.jaap.model.factory;

import tk.labyrinth.jaap.model.element.MethodElementHandle;

import java.util.List;

public abstract class ElementHandleFactoryBase implements ElementHandleFactory {

	@Override
	public MethodElementHandle getMethod(Class<?> cl, String methodName, List<Class<?>> parameterClasses) {
		MethodElementHandle result = findMethod(cl, methodName, parameterClasses);
		if (result == null) {
			throw new IllegalArgumentException("Not found: cl = %s, methodName = %s, parameterClasses = %s".formatted(
					cl, methodName, parameterClasses));
		}
		return result;
	}

	@Override
	public MethodElementHandle getMethodByName(Class<?> cl, String methodName) {
		MethodElementHandle result = findMethodByName(cl, methodName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: cl = %s, methodName = %s".formatted(cl, methodName));
		}
		return result;
	}
}
