package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.annotation.common.HasAnnotations;
import tk.labyrinth.jaap.handle.element.common.HasAbstractModifier;
import tk.labyrinth.jaap.handle.element.common.HasDefaultModifier;
import tk.labyrinth.jaap.handle.element.common.HasFormalParameters;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.element.common.HasPublicModifier;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.element.common.HasStaticModifier;
import tk.labyrinth.jaap.handle.element.common.HasTypeParameters;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.element.common.IsElementHandle;
import tk.labyrinth.jaap.model.element.common.IsExecutableElementHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * * - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface MethodElementHandle extends
		HasAbstractModifier,
		HasAnnotations,
		HasDefaultModifier,
		HasFormalParameters,
		HasParent<@NonNull TypeElementHandle>,
		HasPublicModifier,
		HasSignature<String>,
		HasStaticModifier,
		HasTopLevelTypeElement,
		HasTypeParameters,
		IsElementHandle,
		IsExecutableElementHandle {

	default MethodDeclaration getDeclaration() {
		return MethodDeclaration.builder()
				.formalParameters(getFormalParameterStream()
						.map(FormalParameterElementHandle::getDeclaration)
						.collect(Collectors.toList()))
				.modifiers(getModifiers())
				.name(getName())
				.returnType(getReturnType().getDescription())
				.build();
	}

	default MethodFullSignature getFullSignature() {
		return getSimpleSignature().toFull(getParent().getSignatureString());
	}

	Stream<MethodModifier> getModifierStream();

	default List<MethodModifier> getModifiers() {
		return getModifierStream().collect(Collectors.toList());
	}

	String getName();

	TypeHandle getReturnType();

	/**
	 * Useful links:<br>
	 * - <a href="https://www.baeldung.com/java-method-signature-return-type#vararg-parameters">https://www.baeldung.com/java-method-signature-return-type#vararg-parameters</a><br>
	 *
	 * @return non-null
	 */
	default MethodSimpleSignature getSimpleSignature() {
		return MethodSimpleSignature.of(
				getName(),
				getFormalParameterStream().map(FormalParameterElementHandle::getTypeSignature));
	}
}
