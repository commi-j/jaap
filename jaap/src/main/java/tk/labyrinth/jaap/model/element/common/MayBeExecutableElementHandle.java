package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.ExecutableElementHandle;

public interface MayBeExecutableElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link ExecutableElementHandle}
	 */
	ExecutableElementHandle asExecutableElement();

	boolean isExecutableElement();
}
