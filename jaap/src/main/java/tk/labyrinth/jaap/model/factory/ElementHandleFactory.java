package tk.labyrinth.jaap.model.factory;

import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.secondary.ParameterizableElementHandle;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.List;

// TODO: Remove all default methods.
public interface ElementHandleFactory {

	@Nullable
	ElementHandle find(ElementSignature elementSignature);

	@Nullable
	ElementHandle find(String elementSignatureString);

	@Nullable
	FieldElementHandle findField(Class<?> type, String fieldName);

	@Nullable
	FieldElementHandle findField(String fieldFullName);

	@Nullable
	MethodElementHandle findMethod(Class<?> type, String methodSimpleSignatureString);

	@Nullable
	MethodElementHandle findMethod(Class<?> cl, String methodName, List<Class<?>> parameterClasses);

	@Nullable
	MethodElementHandle findMethod(MethodFullSignature methodFullSignature);

	@Nullable
	MethodElementHandle findMethod(String methodFullSignatureString);

	@Nullable
	MethodElementHandle findMethodByName(Class<?> cl, String methodName);

	@Nullable
	ParameterizableElementHandle findParameterizable(ElementSignature parameterizableElementSignature);

	ParameterizableElementHandle findParameterizable(String parameterizableElementSignatureString);

	@Nullable
	TypeElementHandle findType(String typeSignatureString);

	@Nullable
	TypeElementHandle findType(TypeSignature typeSignature);

	@Nullable
	TypeParameterElementHandle findTypeParameter(Class<?> type, String typeParameterName);

	@Nullable
	TypeParameterElementHandle findTypeParameter(String typeParameterSignature);

	ElementHandle get(Class<?> type);

	default ElementHandle get(String elementSignatureString) {
		ElementHandle result = find(elementSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: elementSignatureString = " + elementSignatureString);
		}
		return result;
	}

	default FieldElementHandle getField(Class<?> type, String fieldName) {
		FieldElementHandle result = findField(type, fieldName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type + ", fieldName = " + fieldName);
		}
		return result;
	}

	default FieldElementHandle getField(String fieldSignatureString) {
		FieldElementHandle result = findField(fieldSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fieldSignatureString = " + fieldSignatureString);
		}
		return result;
	}

	default MethodElementHandle getMethod(Class<?> type, String methodSimpleSignatureString) {
		MethodElementHandle result = findMethod(type, methodSimpleSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"type = " + type + ", " +
					"methodSimpleSignatureString = " + methodSimpleSignatureString);
		}
		return result;
	}

	MethodElementHandle getMethod(Class<?> cl, String methodName, List<Class<?>> parameterClasses);

	MethodElementHandle getMethod(Method method);

	default MethodElementHandle getMethod(String methodFullSignatureString) {
		MethodElementHandle result = findMethod(methodFullSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignatureString = " + methodFullSignatureString);
		}
		return result;
	}

	MethodElementHandle getMethodByName(Class<?> cl, String methodName);

	PackageElementHandle getPackage(ElementSignature packageElementSignature);

	PackageElementHandle getPackage(String packageSignatureString);

	PackageElementHandle getPackageOf(Class<?> childType);

	TypeElementHandle getType(Class<?> type);

	TypeElementHandle getType(String typeSignatureString);

	default TypeElementHandle getType(TypeSignature typeSignature) {
		TypeElementHandle result = findType(typeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeSignature = " + typeSignature);
		}
		return result;
	}

	TypeParameterElementHandle getTypeParameter(Class<?> type, String typeParameterName);

	default TypeParameterElementHandle getTypeParameter(String typeParameterSignature) {
		TypeParameterElementHandle result = findTypeParameter(typeParameterSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeParameterSignature = " + typeParameterSignature);
		}
		return result;
	}
}
