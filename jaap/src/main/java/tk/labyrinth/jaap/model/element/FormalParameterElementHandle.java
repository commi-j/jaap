package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.element.common.HasName;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.signature.TypeSignature;

/**
 * Constructor or method formal parameter.<br>
 * <br>
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * * - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface FormalParameterElementHandle extends
		HasName,
		HasParent<@NonNull ExecutableElementHandle>,
		HasSignature<String>,
		HasTopLevelTypeElement {

	default FormalParameterDeclaration getDeclaration() {
		return FormalParameterDeclaration.builder()
				.name(getName())
				.type(getType().getDescription())
				.build();
	}

	int getIndex();

	@Override
	default String getSignature() {
		return getParent().getSignatureString() + "#" + getIndex();
	}

	TypeHandle getType();

	default TypeSignature getTypeSignature() {
		return getType().getSignature();
	}
}
