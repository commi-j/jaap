package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.ConstructorElementHandle;

public interface MayBeConstructorElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link ConstructorElementHandle}
	 */
	ConstructorElementHandle asConstructorElement();

	boolean isConstructorElement();
}
