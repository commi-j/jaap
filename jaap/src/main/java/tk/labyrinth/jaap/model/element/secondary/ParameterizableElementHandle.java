package tk.labyrinth.jaap.model.element.secondary;

import tk.labyrinth.jaap.handle.element.common.HasTypeParameters;
import tk.labyrinth.jaap.model.element.common.MayBeConstructorElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeExecutableElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeMethodElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeTypeElementHandle;

import javax.lang.model.element.Parameterizable;

/**
 * @see Parameterizable
 */
public interface ParameterizableElementHandle extends
		HasTypeParameters,
		MayBeExecutableElementHandle,
		MayBeMethodElementHandle,
		MayBeConstructorElementHandle,
		MayBeTypeElementHandle {
	// empty
}
