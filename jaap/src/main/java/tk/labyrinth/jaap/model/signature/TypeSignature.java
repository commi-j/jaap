package tk.labyrinth.jaap.model.signature;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.core.model.JavaKeywords;
import tk.labyrinth.jaap.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This class represents signature to an erased type. It it essential in class and method lookups.<br>
 * <br>
 * <a href="https://docs.oracle.com/javase/specs/jls/se8/html/jls-4.html#jls-4.6">https://docs.oracle.com/javase/specs/jls/se8/html/jls-4.html#jls-4.6</a><br>
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class TypeSignature {

	public static final Map<String, TypeSignature> KEYWORD_TYPES = Stream
			.concat(
					JavaKeywords.primitives().stream(),
					Stream.of(JavaKeywords.VOID))
			.map(JavaKeywords::lowercase)
			.collect(Collectors.toMap(
					Function.identity(),
					javaKeyword -> new TypeSignature(0, null, List.of(javaKeyword))));

	// TODO: Check the cases of ..Type and p..Type
	public static final Pattern PATTERN = Pattern.compile("^" +
			"((?<packagesSegment>[\\w$.]+):)?" +
			"(?<typesSegment>[\\w$.]+)" +
			"(?<arraysSegment>(\\[])+)?" +
			"$");

	/**
	 * Non-negative.
	 */
	int arrayCount;

	/**
	 * Null for keywords (primitive or void), empty for unnamed.
	 */
	@Nullable
	PackageSignature packageSignature;

	/**
	 * 1..n
	 */
	List<String> typeSimpleNames;

	public String getBinaryName() {
		String longBinaryName = String.join("$", typeSimpleNames);
		//
		return packageSignature != null
				? packageSignature + SignatureSeparators.PACKAGE_OR_TYPE + longBinaryName
				: longBinaryName;
	}

	public ClassSignature getClassSignature() {
		ClassSignature result;
		{
			if (isArray()) {
				throw new NotImplementedException();
			} else if (isKeyword()) {
				result = ClassSignature.from(toString());
			} else if (isDeclaredOrVariable()) {
				result = ClassSignature.from(Stream
						.of(
								packageSignature != null ? packageSignature.toString() : "",
								String.join(SignatureSeparators.PACKAGE_OR_TYPE, typeSimpleNames))
						.filter(string -> !string.isEmpty())
						.collect(Collectors.joining(SignatureSeparators.TOP_LEVEL_TYPE)));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public String getLongName() {
		return String.join(SignatureSeparators.PACKAGE_OR_TYPE, typeSimpleNames);
	}

	public String getQualifiedName() {
		return toString().replace(SignatureSeparators.TOP_LEVEL_TYPE, SignatureSeparators.PACKAGE_OR_TYPE);
	}

	public String getSimpleName() {
		return typeSimpleNames.get(typeSimpleNames.size() - 1);
	}

	public boolean isArray() {
		return arrayCount > 0;
	}

	public boolean isDeclaredOrVariable() {
		// TODO: Check for wildcard?
		return isReference() && !isArray();
	}

	public boolean isKeyword() {
		return packageSignature == null;
	}

	public boolean isReference() {
		return packageSignature != null;
	}

	public TypeDescription toDescription() {
		return TypeDescription.of(toString());
	}

	public ElementSignature toElementSignature() {
		return ElementSignature.of(toString());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		//
		if (packageSignature != null && !packageSignature.isUnnamed()) {
			builder.append(packageSignature);
			builder.append(SignatureSeparators.TOP_LEVEL_TYPE);
		}
		//
		builder.append(StringUtils.join(typeSimpleNames, SignatureSeparators.PACKAGE_OR_TYPE));
		//
		IntStream.range(0, arrayCount).forEach(index -> builder.append("[]"));
		//
		return builder.toString();
	}

	/**
	 * Note: JLS allows class names to contain dollar character ($), which is a separator of nested classes in binary names.
	 * Since we don't expect to encounter dollar character in names on a regular basis, this method assumes any such characters
	 * to be class separators.
	 *
	 * @param binaryName fit
	 *
	 * @return non-null
	 *
	 * @see Class#getName()
	 */
	public static TypeSignature guessFromBinaryName(String binaryName) {
		TypeSignature result;
		{
			int borderIndex = binaryName.lastIndexOf('.');
			//
			String withColon = borderIndex != -1
					? binaryName.substring(0, borderIndex) + ":" + binaryName.substring(borderIndex + 1)
					: binaryName;
			//
			String withoutDollars = withColon.replace('$', '.');
			//
			result = ofValid(withoutDollars);
		}
		return result;
	}

	/**
	 * Guesses package-class border based on JLS expecting that class names start with capital letter.<br>
	 * Note:<br>
	 * - {@link Class#getCanonicalName()} returns qualified name;<br>
	 * - {@link Class#getName()} returns binary name and may not work properly in some cases;<br>
	 *
	 * @param qualifiedName non-null
	 *
	 * @return non-null
	 */
	public static TypeSignature guessFromQualifiedName(String qualifiedName) {
		TypeSignature result;
		{
			int borderIndex = qualifiedName.length() + 1;
			//
			while (borderIndex != -1) {
				int potentialBorderIndex = qualifiedName.lastIndexOf('.', borderIndex - 1);
				if (potentialBorderIndex != -1 &&
						!Character.isUpperCase(qualifiedName.charAt(potentialBorderIndex + 1))) {
					break;
				}
				borderIndex = potentialBorderIndex;
			}
			//
			result = ofValid(borderIndex != -1
					? qualifiedName.substring(0, borderIndex) + ":" + qualifiedName.substring(borderIndex + 1)
					: qualifiedName);
		}
		return result;
	}

	public static TypeSignature of(Class<?> javaType) {
		return ofValid(ClassUtils.getSignature(javaType));
	}

	public static TypeSignature ofObject() {
		return ofValid(ClassUtils.getSignature(Object.class));
	}

	public static TypeSignature ofValid(String typeSignatureString) {
		TypeSignature result;
		{
			TypeSignature keywordTypeSignature = KEYWORD_TYPES.get(typeSignatureString);
			if (keywordTypeSignature != null) {
				result = keywordTypeSignature;
			} else {
				Matcher matcher = PATTERN.matcher(typeSignatureString);
				//
				if (!matcher.matches()) {
					throw new IllegalArgumentException("False TypeSignature: " + typeSignatureString);
				}
				//
				String arraysSegment = matcher.group("arraysSegment");
				String packagesSegment = matcher.group("packagesSegment");
				String typesSegment = matcher.group("typesSegment");
				//
				PackageSignature packageSignature = packagesSegment != null
						? PackageSignature.of(packagesSegment)
						: PackageSignature.empty();
				List<String> types = List.of(StringUtils.splitPreserveAllTokens(
						typesSegment,
						SignatureSeparators.PACKAGE_OR_TYPE));
				int arrayCount = arraysSegment != null
						? arraysSegment.length() / 2
						: 0;
				//
				if (types.stream().anyMatch(String::isEmpty)) {
					throw new IllegalArgumentException("False TypeSignature: " + typeSignatureString);
				}
				//
				result = new TypeSignature(arrayCount, packageSignature, types);
			}
		}
		return result;
	}
}
