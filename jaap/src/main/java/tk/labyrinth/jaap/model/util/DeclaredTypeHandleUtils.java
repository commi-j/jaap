package tk.labyrinth.jaap.model.util;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;

public class DeclaredTypeHandleUtils {

	public static boolean isAnnotationType(DeclaredTypeHandle declaredTypeHandle) {
		return declaredTypeHandle != null && declaredTypeHandle.isAnnotationType();
	}

	public static AnnotationTypeHandle requireAnnotationType(DeclaredTypeHandle declaredTypeHandle) {
		return declaredTypeHandle.toElement().asAnnotationType();
	}
}
