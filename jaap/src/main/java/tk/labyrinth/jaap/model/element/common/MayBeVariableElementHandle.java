package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.VariableElementHandle;

public interface MayBeVariableElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link VariableElementHandle}
	 */
	VariableElementHandle asVariableElement();

	boolean isVariableElement();
}
