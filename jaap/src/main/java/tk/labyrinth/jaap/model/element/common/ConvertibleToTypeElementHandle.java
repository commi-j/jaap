package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

public interface ConvertibleToTypeElementHandle {

	TypeElementHandle toElement();
}
