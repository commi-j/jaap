package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

public abstract class ElementHandleBase implements ElementHandle {

	@Nullable
	@Override
	public TypeHandle findType() {
		TypeHandle result;
		{
			if (isExecutableElement()) {
				result = asExecutableElement().findType();
			} else if (isPackageElement()) {
				result = null;
			} else if (isTypeElement()) {
				result = asTypeElement().toType().asType();
			} else if (isTypeParameterElement()) {
				result = asTypeParameterElement().getType();
			} else if (isVariableElement()) {
				result = asVariableElement().getType();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
