package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.InitializerElementHandle;

public interface MayBeInitializerElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link InitializerElementHandle}
	 */
	InitializerElementHandle asInitializerElement();

	boolean isInitializerElement();
}
