package tk.labyrinth.jaap.model.signature;

import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Variants:<br>
 * - Class;<br>
 * - Class.Class;<br>
 * - package:Class;<br>
 * - package.package:Class;<br>
 * - package.package:Class.Class;<br>
 */
@Value(staticConstructor = "of")
public class ClassSignature {

	/**
	 * 1..n
	 */
	@NonNull
	List<String> classNames;

	@NonNull
	PackageSignature packageSignature;

	public String getBinaryName() {
		String joinedClassNames = String.join("$", classNames);
		//
		return !packageSignature.isUnnamed()
				? "%s.%s".formatted(packageSignature, joinedClassNames)
				: joinedClassNames;
	}

	public String getLongName() {
		return String.join(".", classNames);
	}

	public String getPackageName() {
		return packageSignature.toString();
	}

	public String getQualifiedName() {
		return !packageSignature.isUnnamed()
				? "%s.%s".formatted(packageSignature, getLongName())
				: getLongName();
	}

	public String getSimpleName() {
		return classNames.get(classNames.size() - 1);
	}

	public ClassSignature getTopLevelSignature() {
		return of(List.of(classNames.get(0)), packageSignature);
	}

	public boolean isTopLevel() {
		return classNames.size() == 1;
	}

	@Override
	public String toString() {
		return !packageSignature.isUnnamed()
				? packageSignature + ":" + String.join(".", classNames)
				: String.join(".", classNames);
	}

	// TODO: Use ClassUtils#getSignature
	public static ClassSignature from(String classSignatureString) {
		ClassSignature result;
		{
			int indexOfColon = classSignatureString.indexOf(':');
			//
			if (indexOfColon != -1) {
				result = of(
						List.of(classSignatureString.substring(indexOfColon + 1).split("\\.")),
						PackageSignature.of(classSignatureString.substring(0, indexOfColon)));
			} else {
				result = of(
						List.of(classSignatureString.split("\\.")),
						PackageSignature.empty());
			}
		}
		return result;
	}

	/**
	 * Guesses package-class border based on JLS expecting that class names start with capital letter.<br>
	 * Note:<br>
	 * - {@link Class#getCanonicalName()} returns qualified name;<br>
	 * - {@link Class#getName()} returns binary name and may not work properly in some cases;<br>
	 *
	 * @param qualifiedName non-null
	 *
	 * @return non-null
	 */
	public static ClassSignature guessFromQualifiedName(String qualifiedName) {
		ClassSignature result;
		{
			int borderIndex = qualifiedName.length() + 1;
			while (borderIndex != -1) {
				int potentialBorderIndex = qualifiedName.lastIndexOf('.', borderIndex - 1);
				if (potentialBorderIndex != -1 &&
						!Character.isUpperCase(qualifiedName.charAt(potentialBorderIndex + 1))) {
					break;
				}
				borderIndex = potentialBorderIndex;
			}
			result = of(
					List.of(qualifiedName.substring(borderIndex + 1).split("\\.")),
					PackageSignature.of(qualifiedName.substring(0, borderIndex)));
		}
		return result;
	}

	public static ClassSignature of(Class<?> javaClass) {
		return of(
				splitClasses(javaClass).map(Class::getSimpleName).collect(Collectors.toList()),
				javaClass.getPackage() != null
						? PackageSignature.of(javaClass.getPackage())
						: PackageSignature.empty());
	}

	public static Stream<Class<?>> splitClasses(Class<?> javaClass) {
		return javaClass.getEnclosingClass() != null
				? StreamUtils.concat(splitClasses(javaClass.getEnclosingClass()), javaClass)
				: Stream.of(javaClass);
	}
}
