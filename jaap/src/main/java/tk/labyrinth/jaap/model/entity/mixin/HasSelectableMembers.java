package tk.labyrinth.jaap.model.entity.mixin;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;

import javax.annotation.Nullable;

public interface HasSelectableMembers {

	@Nullable
	ElementHandle selectMember(EntitySelector selector);

	@Nullable
	default ElementHandle selectMember(EntitySelectorChain selectorChain) {
		ElementHandle result;
		{
			Pair<EntitySelector, EntitySelectorChain> headAndTail = selectorChain.split();
			EntitySelector head = headAndTail.getKey();
			EntitySelectorChain tail = headAndTail.getValue();
			//
			ElementHandle child = selectMember(head);
			if (child != null) {
				if (tail != null) {
					result = child.selectMember(tail);
				} else {
					result = child;
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
