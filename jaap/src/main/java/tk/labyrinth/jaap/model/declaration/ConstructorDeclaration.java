package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class ConstructorDeclaration {
	//
	// TODO: Throws?

	@Builder.Default
	List<FormalParameterDeclaration> formalParameters = List.of();

	@Builder.Default
	List<ConstructorModifier> modifiers = List.of();

	@Builder.Default
	List<TypeParameterDeclaration> typeParameters = List.of();
}
