package tk.labyrinth.jaap.model.util;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

public class TypeElementHandleUtils {

	public static boolean isGeneric(TypeElementHandle typeElementHandle) {
		return typeElementHandle != null && typeElementHandle.hasTypeParameters();
	}

	public static TypeElementHandle requireGeneric(TypeElementHandle typeElementHandle) {
		if (!isGeneric(typeElementHandle)) {
			throw new IllegalArgumentException("Require generic: " + typeElementHandle);
		}
		return typeElementHandle;
	}
}
