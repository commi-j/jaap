package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.FieldElementHandle;

public interface MayBeFieldElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link FieldElementHandle}
	 */
	FieldElementHandle asFieldElement();

	boolean isFieldElement();
}
