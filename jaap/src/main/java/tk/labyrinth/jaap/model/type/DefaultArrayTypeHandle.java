package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

@Value
public class DefaultArrayTypeHandle implements ArrayTypeHandle {

	TypeHandle componentType;

	TypeHandleFactory typeHandleFactory;

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeHandle getComponentType() {
		return componentType;
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.arrayDepth(1)
				.fullName(componentType.getDescription().toString())
				.build();
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
