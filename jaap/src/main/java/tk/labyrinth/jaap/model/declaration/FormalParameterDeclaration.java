package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class FormalParameterDeclaration {
	//
	// TODO: Annotations, Modifiers

	String name;

	TypeDescription type;
}
