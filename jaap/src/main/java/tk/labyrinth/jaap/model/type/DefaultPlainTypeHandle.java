package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

@Value
public class DefaultPlainTypeHandle implements PlainTypeHandle {

	// TODO: Check plain, not generic.
	TypeElementHandle typeElementHandle;

	TypeHandleFactory typeHandleFactory;

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeElementHandle.getSignatureString())
				.build();
	}

	@Override
	public GenericContext getGenericContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		throw new UnsupportedOperationException();
	}
}
