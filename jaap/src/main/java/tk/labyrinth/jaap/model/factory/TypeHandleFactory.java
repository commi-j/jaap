package tk.labyrinth.jaap.model.factory;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

// TODO: Remove all default methods.
public interface TypeHandleFactory {

	@Nullable
	TypeHandle find(String typeDescriptionString);

	@Nullable
	TypeHandle find(TypeDescription typeDescription);

	@Nullable
	TypeHandle find(TypeSignature typeSignature);

	@Nullable
	ArrayTypeHandle findArray(String arrayTypeDescriptionString);

	@Nullable
	ArrayTypeHandle findArray(TypeDescription arrayTypeDescription);

	@Nullable
	DeclaredTypeHandle findDeclared(String declaredTypeDescriptionString);

	@Nullable
	DeclaredTypeHandle findDeclared(TypeDescription declaredTypeDescription);

	@Nullable
	ParameterizedTypeHandle findParameterized(String parameterizedTypeDescriptionString);

	@Nullable
	ParameterizedTypeHandle findParameterized(TypeDescription parameterizedTypeDescription);

	@Nullable
	PlainTypeHandle findPlain(String plainTypeDescriptionString);

	@Nullable
	PlainTypeHandle findPlain(TypeDescription plainTypeDescription);

	@Nullable
	PrimitiveTypeHandle findPrimitive(String primitiveTypeDescriptionString);

	@Nullable
	PrimitiveTypeHandle findPrimitive(TypeDescription primitiveTypeDescription);

	@Nullable
	RawTypeHandle findRaw(String rawTypeDescriptionString);

	@Nullable
	RawTypeHandle findRaw(TypeDescription rawTypeDescription);

	@Nullable
	VariableTypeHandle findVariable(String variableTypeDescriptionString);

	@Nullable
	VariableTypeHandle findVariable(TypeDescription variableTypeDescription);

	@Nullable
	WildcardTypeHandle findWildcard(String wildcardTypeDescriptionString);

	@Nullable
	WildcardTypeHandle findWildcard(TypeDescription wildcardTypeDescription);

	default TypeHandle get(Class<?> type) {
		return get(GenericContext.empty(), type);
	}

	TypeHandle get(GenericContext genericContext, Class<?> type);

	TypeHandle get(TypeSignature typeSignature);

	TypeHandle get(String typeDescriptionString);

	TypeHandle get(TypeDescription typeDescription);

	ArrayTypeHandle getArray(String arrayTypeDescriptionString);

	ArrayTypeHandle getArray(TypeDescription arrayTypeDescription);

	DeclaredTypeHandle getDeclared(String declaredTypeDescriptionString);

	DeclaredTypeHandle getDeclared(TypeDescription declaredTypeDescription);

	ParameterizedTypeHandle getParameterized(String parameterizedTypeDescriptionString);

	ParameterizedTypeHandle getParameterized(TypeDescription parameterizedTypeDescription);

	ParameterizedTypeHandle getParameterized(GenericContext genericContext, Type type);

	PlainTypeHandle getPlain(String plainTypeDescriptionString);

	PlainTypeHandle getPlain(TypeDescription plainTypeDescription);

	PrimitiveTypeHandle getPrimitive(String primitiveTypeDescriptionString);

	PrimitiveTypeHandle getPrimitive(TypeDescription primitiveTypeDescription);

	RawTypeHandle getRaw(String rawTypeDescriptionString);

	RawTypeHandle getRaw(TypeDescription rawTypeDescription);

	VariableTypeHandle getVariable(String variableTypeDescriptionString);

	VariableTypeHandle getVariable(TypeDescription variableTypeDescription);

	WildcardTypeHandle getWildcard(String wildcardTypeDescriptionString);

	WildcardTypeHandle getWildcard(TypeDescription wildcardTypeDescription);
}
