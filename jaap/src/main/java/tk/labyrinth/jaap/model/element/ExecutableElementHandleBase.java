package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

public abstract class ExecutableElementHandleBase implements ExecutableElementHandle {

	@Nullable
	@Override
	public TypeHandle findType() {
		TypeHandle result;
		{
			if (isConstructorElement()) {
				result = asConstructorElement().getReturnType();
			} else if (isInitializerElement()) {
				result = null;
			} else if (isMethodElement()) {
				result = asMethodElement().getReturnType();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
