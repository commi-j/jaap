package tk.labyrinth.jaap.model.signature;

import lombok.Value;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class MethodSimpleSignature {

	public static final Pattern PATTERN = Pattern.compile("^(?<name>\\w+)(?<braces>\\((?<parameters>[\\w.:$\\[\\],]*)\\))$");

	String name;

	List<TypeSignature> parameters;

	private MethodSimpleSignature(String name, List<TypeSignature> parameters) {
		this.name = Objects.requireNonNull(name, "name");
		// FIXME: Ensure no null elements passed.
		this.parameters = Objects.requireNonNull(parameters, "parameters");
	}

	public MethodFullSignature toFull(String typeFullName) {
		return new MethodFullSignature(this, typeFullName);
	}

	public MethodFullSignature toFull(TypeSignature typeSignature) {
		return new MethodFullSignature(this, typeSignature.toString());
	}

	public String toLongString() {
		return "%s(%s)".formatted(
				name,
				parameters.stream()
						.map(TypeSignature::getLongName)
						.collect(Collectors.joining(",")));
	}

	@Override
	public String toString() {
		return String.format("%s(%s)",
				name,
				parameters.stream()
						.map(TypeSignature::toString)
						.collect(Collectors.joining(",")));
	}

	private static MethodSimpleSignature resolve(String value, Function<String, TypeSignature> typeResolutionFunction) {
		Matcher matcher = PATTERN.matcher(value);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("False MethodSimpleSignature: " + value);
		}
		List<TypeSignature> parameters;
		{
			String parametersGroup = matcher.group("parameters");
			if (!parametersGroup.isEmpty()) {
				parameters = Stream.of(parametersGroup.split(","))
						.map(typeResolutionFunction)
						.collect(Collectors.toList());
			} else {
				parameters = List.of();
			}
		}
		return of(matcher.group("name"), parameters);
	}

	public static MethodSimpleSignature guessFromQualifiedSignature(String qualifiedMethodSimpleSignatureString) {
		return resolve(qualifiedMethodSimpleSignatureString, TypeSignature::guessFromQualifiedName);
	}

	public static MethodSimpleSignature of(String methodSimpleSignatureString) {
		return resolve(methodSimpleSignatureString, TypeSignature::ofValid);
	}

	public static MethodSimpleSignature of(String simpleName, List<TypeSignature> parameters) {
		return new MethodSimpleSignature(simpleName, parameters);
	}

	public static MethodSimpleSignature of(String simpleName, Stream<TypeSignature> parameters) {
		return of(simpleName, ListUtils.fromNullable(parameters));
	}
}
