package tk.labyrinth.jaap.model.entity.selection;

import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;
import tk.labyrinth.misc4j2.java.lang.EnumUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Value
public class EntitySelectionContext {

	@NonNull
	EntitySelectionKind kind;

	/**
	 * Non-null for {@link EntitySelectionKind#METHOD} and {@link EntitySelectionKind#METHOD_INVOCATION},
	 * null for others.
	 */
	@Nullable
	List<@NonNull MethodArgumentSignature> methodArgumentSignatures;

	private EntitySelectionContext(
			@NonNull EntitySelectionKind kind,
			@Nullable List<MethodArgumentSignature> methodArgumentSignatures) {
		this.kind = kind;
		this.methodArgumentSignatures = methodArgumentSignatures;
		//
		if (EnumUtils.in(kind, EntitySelectionKind.METHOD, EntitySelectionKind.METHOD_INVOCATION)) {
			if (methodArgumentSignatures == null) {
				throw new IllegalArgumentException("methodArgumentSignatures must not be null: %s".formatted(kind));
			}
			if (methodArgumentSignatures.stream().anyMatch(Objects::isNull)) {
				throw new IllegalArgumentException("methodArgumentSignatures must not contain nulls: %s"
						.formatted(methodArgumentSignatures));
			}
		} else {
			if (methodArgumentSignatures != null) {
				throw new IllegalArgumentException("methodArgumentSignatures must be null: %s".formatted(kind));
			}
		}
	}

	public boolean canBeMethod() {
		return EnumUtils.in(kind,
				EntitySelectionKind.METHOD);
	}

	public boolean canBePackage() {
		return EnumUtils.in(kind,
				EntitySelectionKind.PACKAGE,
				EntitySelectionKind.TYPE_NAME_OR_PACKAGE,
				EntitySelectionKind.TYPE_OR_PACKAGE,
				EntitySelectionKind.VARIABLE_OR_TYPE_OR_PACKAGE);
	}

	public boolean canBeType() {
		return EnumUtils.in(kind,
				EntitySelectionKind.METHOD_INVOCATION_TARGET,
				EntitySelectionKind.STAR_OR_TYPE_NAME,
				EntitySelectionKind.TYPE,
				EntitySelectionKind.TYPE_NAME,
				EntitySelectionKind.TYPE_NAME_OR_PACKAGE,
				EntitySelectionKind.TYPE_OR_PACKAGE,
				EntitySelectionKind.VARIABLE_OR_TYPE,
				EntitySelectionKind.VARIABLE_OR_TYPE_OR_PACKAGE,
				EntitySelectionKind.VARIABLE_TYPE);
	}

	public boolean canBeVariable() {
		return EnumUtils.in(kind,
				EntitySelectionKind.METHOD_INVOCATION_ARGUMENT,
				EntitySelectionKind.METHOD_INVOCATION_TARGET,
				EntitySelectionKind.VARIABLE,
				EntitySelectionKind.VARIABLE_INITIALIZER,
				EntitySelectionKind.VARIABLE_OR_TYPE,
				EntitySelectionKind.VARIABLE_OR_TYPE_OR_PACKAGE);
	}

	public EntitySelectionContext getParent() {
		return switch (kind) {
			case IMPORT_DECLARATION -> EntitySelectionContext.forStarOrTypeName();
			case METHOD, TYPE, VARIABLE -> EntitySelectionContext.forVariableOrType();
			case METHOD_INVOCATION ->
					EntitySelectionContext.forMethod(Objects.requireNonNull(methodArgumentSignatures));
			case METHOD_INVOCATION_ARGUMENT, VARIABLE_INITIALIZER -> EntitySelectionContext.forVariable();
			case METHOD_INVOCATION_TARGET, TYPE_OR_PACKAGE, VARIABLE_OR_TYPE, VARIABLE_OR_TYPE_OR_PACKAGE ->
					EntitySelectionContext.forVariableOrTypeOrPackage();
			case PACKAGE, PACKAGE_DECLARATION -> EntitySelectionContext.forPackage();
			case STAR_OR_TYPE_NAME, TYPE_NAME, TYPE_NAME_OR_PACKAGE -> EntitySelectionContext.forTypeNameOrPackage();
			case UNDEFINED -> EntitySelectionContext.forUndefined();
			case VARIABLE_TYPE -> EntitySelectionContext.forTypeName();
		};
	}

	public static EntitySelectionContext createFrom(
			CompilationUnitContext compilationUnitContext,
			MemberSelectTree memberSelectTree) {
		EntitySelectionContext result;
		{
			Tree parent = compilationUnitContext.getParent(memberSelectTree);
			//
			if (parent instanceof MemberReferenceTree memberReferenceTree) {
				result = EntitySelectionContext.forType();
			} else if (parent instanceof VariableTree variableTree) {
				result = EntitySelectionContext.forVariable();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static EntitySelectionContext forImportDeclaration() {
		return new EntitySelectionContext(EntitySelectionKind.IMPORT_DECLARATION, null);
	}

	public static EntitySelectionContext forMethod(List<MethodArgumentSignature> methodArgumentSignatures) {
		return new EntitySelectionContext(EntitySelectionKind.METHOD, methodArgumentSignatures);
	}

	public static EntitySelectionContext forMethod(MethodArgumentSignature... methodArgumentSignatures) {
		return forMethod(Stream.of(methodArgumentSignatures));
	}

	public static EntitySelectionContext forMethod(Stream<MethodArgumentSignature> methodArgumentSignatures) {
		return forMethod(methodArgumentSignatures.toList());
	}

	public static EntitySelectionContext forMethodInvocation(List<MethodArgumentSignature> methodArgumentSignatures) {
		return new EntitySelectionContext(EntitySelectionKind.METHOD_INVOCATION, methodArgumentSignatures);
	}

	public static EntitySelectionContext forMethodInvocation(MethodArgumentSignature... methodArgumentSignatures) {
		return forMethodInvocation(Arrays.asList(methodArgumentSignatures));
	}

	public static EntitySelectionContext forMethodInvocation(Stream<MethodArgumentSignature> methodArgumentSignatures) {
		return forMethodInvocation(methodArgumentSignatures.toList());
	}

	public static EntitySelectionContext forMethodInvocationArgument() {
		return new EntitySelectionContext(EntitySelectionKind.METHOD_INVOCATION_ARGUMENT, null);
	}

	public static EntitySelectionContext forMethodInvocationTarget() {
		return new EntitySelectionContext(EntitySelectionKind.METHOD_INVOCATION_TARGET, null);
	}

	public static EntitySelectionContext forPackage() {
		return new EntitySelectionContext(EntitySelectionKind.PACKAGE, null);
	}

	public static EntitySelectionContext forPackageDeclaration() {
		return new EntitySelectionContext(EntitySelectionKind.PACKAGE_DECLARATION, null);
	}

	public static EntitySelectionContext forStarOrTypeName() {
		return new EntitySelectionContext(EntitySelectionKind.STAR_OR_TYPE_NAME, null);
	}

	public static EntitySelectionContext forType() {
		return new EntitySelectionContext(EntitySelectionKind.TYPE, null);
	}

	public static EntitySelectionContext forTypeName() {
		return new EntitySelectionContext(EntitySelectionKind.TYPE_NAME, null);
	}

	public static EntitySelectionContext forTypeNameOrPackage() {
		return new EntitySelectionContext(EntitySelectionKind.TYPE_NAME_OR_PACKAGE, null);
	}

	public static EntitySelectionContext forTypeOrPackage() {
		return new EntitySelectionContext(EntitySelectionKind.TYPE_OR_PACKAGE, null);
	}

	public static EntitySelectionContext forUndefined() {
		return new EntitySelectionContext(EntitySelectionKind.UNDEFINED, null);
	}

	public static EntitySelectionContext forVariable() {
		return new EntitySelectionContext(EntitySelectionKind.VARIABLE, null);
	}

	public static EntitySelectionContext forVariableInitializer() {
		return new EntitySelectionContext(EntitySelectionKind.VARIABLE_INITIALIZER, null);
	}

	public static EntitySelectionContext forVariableOrType() {
		return new EntitySelectionContext(EntitySelectionKind.VARIABLE_OR_TYPE, null);
	}

	public static EntitySelectionContext forVariableOrTypeOrPackage() {
		return new EntitySelectionContext(EntitySelectionKind.VARIABLE_OR_TYPE_OR_PACKAGE, null);
	}

	public static EntitySelectionContext forVariableType() {
		return new EntitySelectionContext(EntitySelectionKind.VARIABLE_TYPE, null);
	}
}
