package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

@Value
public class DefaultPrimitiveTypeHandle implements PrimitiveTypeHandle {

	String primitive;

	TypeHandleFactory typeHandleFactory;

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(primitive)
				.build();
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
