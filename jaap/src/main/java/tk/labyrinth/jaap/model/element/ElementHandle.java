package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.common.HasAnnotations;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.model.element.common.MayBeConstructorElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeExecutableElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeFieldElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeFormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeInitializerElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeMethodElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBePackageElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeTypeElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeTypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeVariableElementHandle;
import tk.labyrinth.jaap.model.entity.mixin.HasSelectableMembers;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * ElementHandle Hierarchy:<br>
 * * {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface ElementHandle extends
		HasAnnotations,
		//
		// FIXME: Remove this. ProcessingContext is part of langmodel API.
		HasProcessingContext,
		HasSelectableMembers,
		MayBeConstructorElementHandle,
		MayBeExecutableElementHandle,
		MayBeFieldElementHandle,
		MayBeFormalParameterElementHandle,
		MayBeInitializerElementHandle,
		MayBeMethodElementHandle,
		MayBePackageElementHandle,
		MayBeTypeElementHandle,
		MayBeTypeParameterElementHandle,
		MayBeVariableElementHandle {

	@Nullable
	@Override
	default AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return findDirectAnnotation(getProcessingContext().getAnnotationTypeHandle(annotationType));
	}

	/**
	 * Any Element except Initializer and Package has a Type.
	 *
	 * @return nullable
	 */
	@Nullable
	TypeHandle findType();

	@Override
	default List<AnnotationHandle> getDirectAnnotations() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Override
	default MergedAnnotationContext getMergedAnnotationContext(
			String annotationTypeSignature,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				getProcessingContext().getAnnotationTypeHandle(annotationTypeSignature),
				mergedAnnotationSpecification);
	}

	@Override
	default MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return new MergedAnnotationContext(annotationTypeHandle, this, mergedAnnotationSpecification);
	}

	@Override
	default MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	/**
	 * This one is required for member selection. Only packages, types, fields and methods
	 * participate in member selection, others are expected to throw exception.
	 *
	 * @return fit
	 */
	String getName();

	/**
	 * Can only be null for root PackageElement.
	 *
	 * @return nullable
	 */
	@Nullable
	ElementHandle getParent();

	String getSignatureString();
}
