package tk.labyrinth.jaap.model.methodlookup;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class LambdaSignature {

	/**
	 * Parameter types if declared. Most of the time types are implicit, so we should mostly rely on parameter count.
	 */
	List<@Nullable TypeDescription> parameters;

	/**
	 * For expression lambdas sometimes we may be sure it returns a value, like in case of '()->"foo"'
	 * - literal here has to be consumed;
	 */
	@Nullable
	Boolean requireConsumption;

	/**
	 * Result type, if it can be determined.
	 */
	@Nullable
	TypeDescription result;

	@Override
	public String toString() {
		return "%s->%s%s".formatted(
				parameters.size() != 1
						? "(%s)".formatted(parameters.stream()
						.map(String::valueOf)
						.collect(Collectors.joining(",")))
						: parameters.get(0),
				!Objects.equals(requireConsumption, Boolean.FALSE)
						? result != null ? result : TypeDescription.ofNonParameterized(Object.class)
						: "void",
				requireConsumption != null ? requireConsumption ? "!" : "" : "?");
	}
}
