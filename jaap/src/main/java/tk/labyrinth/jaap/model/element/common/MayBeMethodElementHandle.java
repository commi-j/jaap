package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.MethodElementHandle;

public interface MayBeMethodElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link MethodElementHandle}
	 */
	MethodElementHandle asMethodElement();

	boolean isMethodElement();
}
