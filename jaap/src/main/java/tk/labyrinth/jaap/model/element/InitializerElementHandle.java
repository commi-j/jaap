package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.element.common.IsElementHandle;
import tk.labyrinth.jaap.model.element.common.IsExecutableElementHandle;

/**
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * * - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface InitializerElementHandle extends
		HasParent<@NonNull TypeElementHandle>,
		HasTopLevelTypeElement,
		IsElementHandle,
		IsExecutableElementHandle {
	// empty
}
