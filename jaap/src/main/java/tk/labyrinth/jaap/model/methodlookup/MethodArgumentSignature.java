package tk.labyrinth.jaap.model.methodlookup;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

public interface MethodArgumentSignature {

	default String asInferredVariableName() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default LambdaSignature asLambdaSignature() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default TypeDescription asTypeDescription() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default boolean isInferredVariableName() {
		return false;
	}

	default boolean isLambdaSignature() {
		return false;
	}

	default boolean isTypeDescription() {
		return false;
	}

	static MethodArgumentSignature ofInferredVariableName(String inferredVariableName) {
		return new InferredVariableNameVariant(inferredVariableName);
	}

	static MethodArgumentSignature ofLambdaSignature(LambdaSignature lambdaSignature) {
		return new LambdaSignatureVariant(lambdaSignature);
	}

	static MethodArgumentSignature ofTypeDescription(TypeDescription typeDescription) {
		return new TypeDescriptionVariant(typeDescription);
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class LambdaSignatureVariant implements MethodArgumentSignature {

		@NonNull
		LambdaSignature lambdaSignature;

		@Override
		public LambdaSignature asLambdaSignature() {
			return lambdaSignature;
		}

		@Override
		public boolean isLambdaSignature() {
			return true;
		}

		@Override
		public String toString() {
			return lambdaSignature.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class TypeDescriptionVariant implements MethodArgumentSignature {

		@NonNull
		TypeDescription typeDescription;

		@Override
		public TypeDescription asTypeDescription() {
			return typeDescription;
		}

		@Override
		public boolean isTypeDescription() {
			return true;
		}

		@Override
		public String toString() {
			return typeDescription.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class InferredVariableNameVariant implements MethodArgumentSignature {

		@NonNull
		String inferredVariableName;

		@Override
		public String asInferredVariableName() {
			return inferredVariableName;
		}

		@Override
		public boolean isInferredVariableName() {
			return true;
		}

		@Override
		public String toString() {
			return inferredVariableName;
		}
	}
}
