package tk.labyrinth.jaap.model.text;

import lombok.Builder;
import lombok.Value;

import javax.annotation.Nullable;

@Builder
@Value
public class TypeName {

	/**
	 * Canonical name without package. Difference between this and simple name is nesting classes.<br>
	 * Examples:<br>
	 * - Map<br>
	 * - Map.Entry<br>
	 */
	String longName;

	@Nullable
	String packageName;

	public String getFullName() {
		return (packageName != null ? packageName + "." : "") + longName;
	}

	public String getSimpleName() {
		int indexOfLastDot = longName.lastIndexOf('.');
		return indexOfLastDot == -1 ? longName : longName.substring(indexOfLastDot + 1);
	}

	@Override
	public String toString() {
		return (packageName != null ? packageName + ":" : "") + longName;
	}

	public static TypeName of(Class<?> type) {
		String longName;
		{
			StringBuilder stringBuilder = new StringBuilder(type.getSimpleName());
			Class<?> enclosingClass = type;
			while ((enclosingClass = enclosingClass.getEnclosingClass()) != null) {
				stringBuilder.insert(0, enclosingClass.getSimpleName() + ".");
			}
			longName = stringBuilder.toString();
		}
		String packageName = type.getPackage() != null ? type.getPackage().getName() : null;
		return new TypeName(longName, packageName);
	}

	public static TypeName of(String typeNameString) {
		String longName;
		String packageName;
		{
			int indexOfSeparator = typeNameString.indexOf(":");
			if (indexOfSeparator != -1) {
				longName = typeNameString.substring(indexOfSeparator + 1);
				packageName = typeNameString.substring(0, indexOfSeparator);
			} else {
				longName = typeNameString;
				packageName = null;
			}
		}
		return new TypeName(longName, packageName);
	}

	public static TypeName ofVoid() {
		return of("void");
	}
}
