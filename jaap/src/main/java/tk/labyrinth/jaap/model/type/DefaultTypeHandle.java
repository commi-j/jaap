package tk.labyrinth.jaap.model.type;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.model.util.DeclaredTypeHandleUtils;

@Value
public class DefaultTypeHandle implements TypeHandle {

	@Getter(AccessLevel.NONE)
	TypeDescription typeDescription;

	TypeHandleFactory typeHandleFactory;

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ArrayTypeHandle asArrayType() {
		return typeHandleFactory.getArray(typeDescription);
	}

	@Override
	public DeclaredTypeHandle asDeclaredType() {
		return typeHandleFactory.getDeclared(typeDescription);
	}

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		return typeHandleFactory.getParameterized(typeDescription);
	}

	@Override
	public PlainTypeHandle asPlainType() {
		return typeHandleFactory.getPlain(typeDescription);
	}

	@Override
	public PrimitiveTypeHandle asPrimitiveType() {
		return typeHandleFactory.getPrimitive(typeDescription);
	}

	@Override
	public RawTypeHandle asRawType() {
		return typeHandleFactory.getRaw(typeDescription);
	}

	@Override
	public ReferenceTypeHandle asReferenceType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public VariableTypeHandle asVariableType() {
		return typeHandleFactory.getVariable(typeDescription);
	}

	@Override
	public WildcardTypeHandle asWildcardType() {
		return typeHandleFactory.getWildcard(typeDescription);
	}

	@Override
	public TypeDescription getDescription() {
		return typeDescription;
	}

	@Override
	public GenericContext getGenericContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeSignature getSignature() {
		return typeDescription.getSignature();
	}

	@Override
	public boolean isAnnotationType() {
		return DeclaredTypeHandleUtils.isAnnotationType(typeHandleFactory.findDeclared(typeDescription));
	}

	@Override
	public boolean isArrayType() {
		return typeDescription.isArray();
	}

	@Override
	public boolean isAssignableFrom(TypeHandle subtype) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isAssignableTo(TypeHandle supertype) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isDeclaredType() {
		return typeDescription.isDeclared();
	}

	@Override
	public boolean isParameterizedType() {
		return typeDescription.isParameterized();
	}

	@Override
	public boolean isPlainType() {
		return typeDescription.isPlain();
	}

	@Override
	public boolean isPrimitiveType() {
		return typeDescription.isPrimitive();
	}

	@Override
	public boolean isRawType() {
		return typeDescription.isRaw();
	}

	@Override
	public boolean isReferenceType() {
		return typeDescription.isReference();
	}

	@Override
	public boolean isVariableType() {
		return typeDescription.isVariable();
	}

	@Override
	public boolean isWildcardType() {
		return typeDescription.isWildcard();
	}

	@Override
	public TypeHandle resolve(GenericContext genericContext) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
