package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.PackageElementHandle;

public interface MayBePackageElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link PackageElementHandle}
	 */
	PackageElementHandle asPackageElement();

	boolean isPackageElement();
}
