package tk.labyrinth.jaap.model.element;

import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.model.element.common.IsElementHandle;
import tk.labyrinth.jaap.model.entity.mixin.HasSelectableMembers;

/**
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * - - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * * - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface PackageElementHandle extends
		HasSelectableMembers,
		HasSignature<String>,
		IsElementHandle {

	String getQualifiedName();

	@Override
	default String getSignature() {
		return getQualifiedName();
	}

	String getSimpleName();

	boolean isSynthetic();
}
