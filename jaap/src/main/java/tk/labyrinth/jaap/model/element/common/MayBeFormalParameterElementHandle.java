package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;

public interface MayBeFormalParameterElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link FormalParameterElementHandle}
	 */
	FormalParameterElementHandle asFormalParameterElement();

	boolean isFormalParameterElement();
}
