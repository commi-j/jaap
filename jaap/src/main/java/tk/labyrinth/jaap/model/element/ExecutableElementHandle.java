package tk.labyrinth.jaap.model.element;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.element.common.HasParent;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.model.element.common.IsElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeConstructorElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeInitializerElementHandle;
import tk.labyrinth.jaap.model.element.common.MayBeMethodElementHandle;

/**
 * ElementHandle Hierarchy:<br>
 * - {@link ElementHandle} (abstract)<br>
 * * - {@link ExecutableElementHandle} (a)<br>
 * - - - {@link ConstructorElementHandle} (concrete)<br>
 * - - - {@link InitializerElementHandle} (c)<br>
 * - - - {@link MethodElementHandle} (c)<br>
 * - - {@link PackageElementHandle} (c)<br>
 * - - {@link TypeElementHandle} (c)<br>
 * - - {@link TypeParameterElementHandle} (c)<br>
 * - - {@link VariableElementHandle} (a)<br>
 * - - - {@link FieldElementHandle} (c)<br>
 * - - - {@link FormalParameterElementHandle} (c)<br>
 */
public interface ExecutableElementHandle extends
		HasParent<@NonNull TypeElementHandle>,
		HasSignature<String>,
		HasTopLevelTypeElement,
		IsElementHandle,
		MayBeConstructorElementHandle,
		MayBeInitializerElementHandle,
		MayBeMethodElementHandle {

	/**
	 * Any ExecutableElement except Initializer has a Type.
	 *
	 * @return nullable
	 */
	@Nullable
	TypeHandle findType();

	@Override
	default TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}
}
