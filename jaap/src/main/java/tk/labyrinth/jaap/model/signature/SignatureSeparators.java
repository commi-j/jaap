package tk.labyrinth.jaap.model.signature;

import org.apache.commons.lang3.tuple.Pair;

/**
 * '.' - separator of packages and nested types;<br>
 * ':' - separator of package and top-level type (synthetic);<br>
 * '#' - separator of executables, variables and formal parameters (n);<br>
 * '@' - separator of annotations (n);<br>
 * '%' - signature of type parameters (synthetic);<br>
 */
// TODO: Test that these symbols are forbidden in names.
public class SignatureSeparators {

	public static final String ANNOTATION = "@";

	public static final String EXECUTABLE = "#";

	public static final String FORMAL_PARAMETER = "#";

	/**
	 * Not including top-level type separator.
	 */
	public static final String PACKAGE_OR_TYPE = ".";

	public static final String TOP_LEVEL_TYPE = ":";

	public static final String TYPE_PARAMETER = "%";

	public static final String VARIABLE = "#";

	public static Pair<String, String> split(String value, String separator) {
		Pair<String, String> result;
		{
			int index = value.indexOf(separator);
			if (index != -1) {
				result = Pair.of(value.substring(0, index), value.substring(index + separator.length()));
			} else {
				result = Pair.of(value, null);
			}
		}
		return result;
	}
}
