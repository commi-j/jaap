package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

public interface HasTopLevelTypeElement {

	/**
	 * All elements except package has top-level type element.
	 *
	 * @return non-null
	 */
	TypeElementHandle getTopLevelTypeElement();
}
