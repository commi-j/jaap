package tk.labyrinth.jaap.model.declaration;

public enum JavaMethodModifier implements MethodModifier {
	DEFAULT,
	FINAL,
	PRIVATE,
	PROTECTED,
	PUBLIC,
	STATIC
}
