package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

public interface MayBeTypeParameterElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link TypeParameterElementHandle}
	 */
	TypeParameterElementHandle asTypeParameterElement();

	boolean isTypeParameterElement();
}
