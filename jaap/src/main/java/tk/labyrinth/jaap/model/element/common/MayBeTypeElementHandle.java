package tk.labyrinth.jaap.model.element.common;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

public interface MayBeTypeElementHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException if not {@link TypeElementHandle}
	 */
	TypeElementHandle asTypeElement();

	boolean isTypeElement();
}
