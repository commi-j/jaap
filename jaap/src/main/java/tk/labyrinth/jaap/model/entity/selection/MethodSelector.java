package tk.labyrinth.jaap.model.entity.selection;

import com.google.common.collect.Streams;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.methodlookup.LambdaSignature;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.stream.Collectors;

@Value(staticConstructor = "of")
public class MethodSelector {

	List<MethodArgumentSignature> argumentSignatures;

	String name;

	public EntitySelector toEntitySelector() {
		return EntitySelector.forMethod(name, argumentSignatures);
	}

	@Override
	public String toString() {
		return "%s(%s)".formatted(
				name,
				argumentSignatures.stream()
						.map(String::valueOf)
						.collect(Collectors.joining(",")));
	}

	private static boolean matches(@Nullable Boolean requireConsumption, TypeMirror typeMirror) {
		boolean result;
		{
			if (requireConsumption != null) {
				result = requireConsumption != TypeMirrorUtils.isNoType(typeMirror);
			} else {
				// TODO: Support deep & smarter resolution.
				result = true;
			}
		}
		return result;
	}

	private static boolean matches(TypeDescription typeDescription, VariableElement variableElement) {
		boolean result;
		{
			if (typeDescription.isUnboundedWildcard()) {
				result = true;
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	// FIXME: find better name for method and its parameters.
	// FIXME: Deduplicate this one with tk.labyrinth.jaap.util.TypeElementUtils.selectMethod.
	public static boolean matches(
			ProcessingEnvironment processingEnvironment,
			MethodArgumentSignature argumentSignature,
			TypeMirror parameterType) {
		boolean result;
		{
			if (argumentSignature.isInferredVariableName()) {
				// FIXME: There should be a proper computation of variable type.
				//
				result = true;
			} else if (argumentSignature.isLambdaSignature()) {
				if (TypeMirrorUtils.isDeclaredType(parameterType)) {
					TypeElement parameterTypeElement = TypeElementUtils.get(processingEnvironment, parameterType);
					//
					ExecutableElement functionalInterfaceMethod = TypeElementUtils.findFunctionalInterfaceMethod(
							processingEnvironment,
							parameterTypeElement);
					//
					if (functionalInterfaceMethod != null) {
						LambdaSignature lambdaSignature = argumentSignature.asLambdaSignature();
						//
						if (lambdaSignature.getParameters().size() ==
								functionalInterfaceMethod.getParameters().size()) {
							//noinspection UnstableApiUsage
							boolean parametersMatches = Streams
									.zip(
											lambdaSignature.getParameters().stream(),
											functionalInterfaceMethod.getParameters().stream(),
											MethodSelector::matches)
									.allMatch(Boolean::booleanValue);
							//
							result = parametersMatches &&
									matches(
											lambdaSignature.getRequireConsumption(),
											functionalInterfaceMethod.getReturnType());
						} else {
							result = false;
						}
					} else {
						result = false;
					}
				} else {
					result = false;
				}
			} else if (argumentSignature.isTypeDescription()) {
				result = processingEnvironment.getTypeUtils().isAssignable(
						TypeMirrorUtils.resolve(
								processingEnvironment,
								argumentSignature.asTypeDescription().toString()),
						processingEnvironment.getTypeUtils().erasure(parameterType));
			} else {
				throw new UnsupportedOperationException();
			}
		}
		return result;
	}

	public static boolean matches(
			ProcessingEnvironment processingEnvironment,
			List<MethodArgumentSignature> argumentSignatures,
			List<TypeMirror> parameterTypes) {
		boolean result;
		{
			if (argumentSignatures.size() == parameterTypes.size()) {
				//noinspection UnstableApiUsage
				result = Streams
						.zip(argumentSignatures.stream(), parameterTypes.stream(), Pair::of)
						.allMatch(pair -> matches(processingEnvironment, pair.getLeft(), pair.getRight()));
			} else {
				// TODO: varargs?
				result = false;
			}
		}
		return result;
	}

	public static boolean matches(
			ProcessingEnvironment processingEnvironment,
			MethodSelector methodSelector,
			ExecutableElement executableElement) {
		boolean result;
		{
			if (executableElement.getSimpleName().contentEquals(methodSelector.getName())) {
				result = MethodSelector.matches(
						processingEnvironment,
						methodSelector.getArgumentSignatures(),
						executableElement.getParameters().stream()
								.map(VariableElement::asType)
								.toList());
			} else {
				result = false;
			}
		}
		return result;
	}

	public static MethodSelector parse(String methodSelectorString) {
		// TODO: Use own pattern.
		MethodSimpleSignature methodSimpleSignature = MethodSimpleSignature.of(methodSelectorString);
		//
		return of(
				methodSimpleSignature.getParameters().stream()
						.map(TypeSignature::toDescription)
						.map(MethodArgumentSignature::ofTypeDescription)
						.toList(),
				methodSimpleSignature.getName());
	}
}
