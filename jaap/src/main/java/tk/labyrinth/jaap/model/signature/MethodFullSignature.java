package tk.labyrinth.jaap.model.signature;

import lombok.Value;
import tk.labyrinth.jaap.misc4j.java.lang.reflect.ClassUtils;

import java.util.List;
import java.util.stream.Collectors;

@Value
public class MethodFullSignature {

	MethodSimpleSignature simpleSignature;

	// TODO: TypeFullSignature instead of String
	// In pure Java methods are only available on classes, but since we have plans to extend this behaviour
	// we may want to support other types as well.
	String typeFullSignature;

	public String getName() {
		return simpleSignature.getName();
	}

	public List<TypeSignature> getParameters() {
		return simpleSignature.getParameters();
	}

	// FIXME: Wrong method, we can't guarantee it to be canonical.
	public TypeSignature getTypeErasureSignature() {
		return TypeSignature.ofValid(typeFullSignature);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (typeFullSignature != null) {
			builder.append(typeFullSignature).append('#');
		}
		{
			builder.append(simpleSignature.toString());
		}
		return builder.toString();
	}

	public static MethodFullSignature of(
			ClassSignature classSignature,
			String methodName,
			List<TypeSignature> parameterTypeSignatures) {
		return of("%s#%s(%s)".formatted(
				classSignature,
				methodName,
				parameterTypeSignatures.stream()
						.map(TypeSignature::toString)
						.collect(Collectors.joining(","))));
	}

	public static MethodFullSignature of(String methodFullSignatureString) {
		MethodFullSignature result;
		{
			ElementSignature elementSignature = ElementSignature.of(methodFullSignatureString);
			if (elementSignature.matchesMethod()) {
				result = new MethodFullSignature(
						// FIXME: We want to report argument of this method if simple signature creation fails.
						MethodSimpleSignature.of(elementSignature.getContent()),
						elementSignature.getParentOrFail().toString());
				MethodSimpleSignature.of(elementSignature.getContent());
			} else {
				throw new IllegalArgumentException("False MethodFullSignature: " + methodFullSignatureString);
			}
		}
		return result;
	}

	public static MethodFullSignature of(
			String classSignatureString,
			String methodName,
			List<Class<?>> parameterClasses) {
		return of("%s#%s(%s)".formatted(
				classSignatureString,
				methodName,
				parameterClasses.stream()
						.map(ClassUtils::getSignature)
						.collect(Collectors.joining(","))));
	}
}
