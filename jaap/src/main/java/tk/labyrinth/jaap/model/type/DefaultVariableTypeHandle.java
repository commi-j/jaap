package tk.labyrinth.jaap.model.type;

import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

@Value
public class DefaultVariableTypeHandle implements VariableTypeHandle {

	@NonNull
	TypeHandleFactory typeHandleFactory;

	@NonNull
	TypeParameterElementHandle typeParameterElementHandle;

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeParameterElementHandle.getSignatureString())
				.build();
	}

	@Override
	public TypeParameterElementHandle toElement() {
		return typeParameterElementHandle;
	}
}
