package tk.labyrinth.jaap.model.type;

import lombok.Value;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class DefaultParameterizedTypeHandle implements ParameterizedTypeHandle {

	// TODO: Ensure same size as of parameters in type.
	List<TypeHandle> parameters;

	// TODO: Check generic, not plain.
	TypeElementHandle typeElementHandle;

	TypeHandleFactory typeHandleFactory;

	@Override
	public DeclaredTypeHandle asDeclaredType() {
		return new DefaultDeclaredTypeHandle(parameters, typeElementHandle, typeHandleFactory);
	}

	@Override
	public GenericTypeHandle asGenericType() {
		return new DefaultGenericTypeHandle(parameters, typeElementHandle, typeHandleFactory);
	}

	@Override
	public TypeHandle asType() {
		return typeHandleFactory.get(getDescription());
	}

	@Override
	public TypeDescription getDescription() {
		return TypeDescription.builder()
				.fullName(typeElementHandle.getSignatureString())
				.parameters(parameters.stream()
						.map(TypeHandle::getDescription)
						.collect(Collectors.toList()))
				.build();
	}

	@Override
	public GenericContext getGenericContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getParameterCount() {
		return parameters.size();
	}

	@Override
	public Stream<TypeHandle> getParameters() {
		return parameters.stream();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		throw new UnsupportedOperationException();
	}

	@Override
	public TypeElementHandle toElement() {
		return typeElementHandle;
	}

	@Override
	public RawTypeHandle toRawType() {
		return new DefaultRawTypeHandle(typeElementHandle, typeHandleFactory);
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
