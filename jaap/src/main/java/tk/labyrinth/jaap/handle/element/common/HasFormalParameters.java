package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface HasFormalParameters {

	default FormalParameterElementHandle getFormalParameter(int index) {
		return getFormalParameterStream().skip(index).findFirst().orElseThrow();
	}

	int getFormalParameterCount();

	Stream<FormalParameterElementHandle> getFormalParameterStream();

	default List<FormalParameterElementHandle> getFormalParameters() {
		return getFormalParameterStream().collect(Collectors.toList());
	}
}
