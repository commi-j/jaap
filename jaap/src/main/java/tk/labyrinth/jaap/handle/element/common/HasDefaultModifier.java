package tk.labyrinth.jaap.handle.element.common;

public interface HasDefaultModifier {

	/**
	 * @return <b>true</b> if this element has explicit <b>default</b> modifier, <b>false</b> otherwise.
	 */
	boolean hasExplicitDefaultModifier();

	/**
	 * @return <b>true</b> if this element has <b>default</b> modifier or implicitly <b>default</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitDefaultModifier()
	 * @see #isEffectivelyNonDefault()
	 */
	boolean isEffectivelyDefault();

	/**
	 * @return <b>true</b> if this element has no <b>default</b> modifier and not implicitly <b>default</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitDefaultModifier()
	 * @see #isEffectivelyDefault()
	 */
	boolean isEffectivelyNonDefault();
}
