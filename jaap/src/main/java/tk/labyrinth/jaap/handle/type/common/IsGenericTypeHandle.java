package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.GenericTypeHandle;

public interface IsGenericTypeHandle {

	GenericTypeHandle asGenericType();
}
