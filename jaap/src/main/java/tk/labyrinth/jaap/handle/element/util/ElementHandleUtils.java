package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Iterator;

public class ElementHandleUtils {

	@Nullable
	public static Element getMember(Element element, Iterator<String> simpleNames) {
		Element result;
		if (element instanceof TypeElement) {
			result = ElementLookupUtils.getMember((TypeElement) element, simpleNames);
		} else {
			throw new UnsupportedOperationException(ExceptionUtils.render(element));
		}
		return result;
	}
}
