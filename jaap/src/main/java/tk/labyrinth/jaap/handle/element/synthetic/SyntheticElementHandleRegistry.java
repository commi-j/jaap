package tk.labyrinth.jaap.handle.element.synthetic;

import lombok.Setter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.synthetic.element.SyntheticMethodElementHandle;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class SyntheticElementHandleRegistry {

	// TODO: Key should be TypeReference for better reliability.
	private final Map<String, Set<MethodDeclaration>> methodDeclarations = new HashMap<>();

	@Setter
	private ProcessingContext processingContext;

	@Nullable
	public MethodElementHandle findMethod(MethodFullSignature methodFullSignature) {
		return methodDeclarations.getOrDefault(
						methodFullSignature.getTypeErasureSignature().getQualifiedName(),
						Collections.emptySet())
				.stream()
				.filter(methodDeclaration -> Objects.equals(
						methodDeclaration.getSignature(),
						methodFullSignature.getSimpleSignature()))
				.map(methodDeclaration -> SyntheticMethodElementHandle.from(
						processingContext,
						methodFullSignature.getTypeErasureSignature(),
						methodDeclaration))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	public MethodElementHandle findMethod(TypeSignature typeSignature, MethodSelector methodSelector) {
		methodSelector.getArgumentSignatures().forEach(argumentSignature -> {
			if (!argumentSignature.isTypeDescription()) {
				throw new NotImplementedException();
			}
		});
		//
		return findMethod(MethodFullSignature.of(
				typeSignature.getClassSignature(),
				methodSelector.getName(),
				methodSelector.getArgumentSignatures().stream()
						.map(MethodArgumentSignature::asTypeDescription)
						.map(TypeDescription::getSignature)
						.toList()));
	}

	@Nullable
	public MethodElementHandle findMethod(String methodFullSignature) {
		return findMethod(MethodFullSignature.of(methodFullSignature));
	}

	public MethodElementHandle getMethod(String methodFullSignature) {
		MethodElementHandle result = findMethod(methodFullSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignature = " + methodFullSignature);
		}
		return result;
	}

	public Stream<MethodElementHandle> getMethodsOfType(TypeSignature typeSignature) {
		return methodDeclarations.getOrDefault(typeSignature.getQualifiedName(), Collections.emptySet())
				.stream().map(methodDeclaration -> SyntheticMethodElementHandle.from(
						processingContext, typeSignature, methodDeclaration));
	}

	public void registerMethodDeclaration(TypeSignature parentSignature, MethodDeclaration declaration) {
		methodDeclarations.computeIfAbsent(parentSignature.getQualifiedName(), key -> new HashSet<>()).add(declaration);
	}
}
