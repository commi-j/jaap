package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;

public interface MayBeParameterizedTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link ParameterizedTypeHandle}
	 */
	ParameterizedTypeHandle asParameterizedType();

	boolean isParameterizedType();
}
