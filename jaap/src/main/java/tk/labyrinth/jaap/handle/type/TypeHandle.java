package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.element.common.HasSignature;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.MayBeAnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeDeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBePlainTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBePrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeRawTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeVariableTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeWildcardTypeHandle;
import tk.labyrinth.jaap.model.signature.TypeSignature;

/**
 * TypeHandle Hierarchy:<br>
 * * {@link TypeHandle} (abstract)<br>
 * - - {@link PrimitiveTypeHandle} (concrete, primary)<br>
 * - - {@link ReferenceTypeHandle} (a)<br>
 * - - - {@link ArrayTypeHandle} (c, secondary)<br>
 * - - - {@link DeclaredTypeHandle} (a)<br>
 * - - - - {@link GenericTypeHandle} (a)<br>
 * - - - - - {@link ParameterizedTypeHandle} (c, s)<br>
 * - - - - - {@link RawTypeHandle} (c, p)<br>
 * - - - - {@link PlainTypeHandle} (c, p)<br>
 * - - - {@link VariableTypeHandle} (c, p)<br>
 * - - - {@link WildcardTypeHandle} (c, s)<br>
 * - - {@link VoidTypeHandle} (c, p)<br>
 */
public interface TypeHandle extends
		HasDescription,
		HasGenericContext,
		HasProcessingContext,
		HasSignature<TypeSignature>,
		HasTypeMirror,
		MayBeAnnotationTypeHandle,
		MayBeArrayTypeHandle,
		MayBeDeclaredTypeHandle,
		MayBeParameterizedTypeHandle,
		MayBePlainTypeHandle,
		MayBePrimitiveTypeHandle,
		MayBeRawTypeHandle,
		MayBeReferenceTypeHandle,
		MayBeVariableTypeHandle,
		MayBeWildcardTypeHandle {

	/**
	 * @param subtype non-null
	 *
	 * @return whether <b>this</b> is assignable from <b>subtype</b>
	 */
	boolean isAssignableFrom(TypeHandle subtype);

	/**
	 * @param supertype non-null
	 *
	 * @return whether <b>this</b> is assignable to <b>supertype</b>
	 */
	boolean isAssignableTo(TypeHandle supertype);

	TypeHandle resolve(GenericContext genericContext);
}
