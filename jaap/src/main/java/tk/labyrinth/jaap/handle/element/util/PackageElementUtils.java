package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.ElementSignature;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PackageElementUtils {

	@Nullable
	public static PackageElement find(
			ProcessingEnvironment processingEnvironment,
			ElementSignature packageElementSignature) {
		return find(processingEnvironment, packageElementSignature.toString());
	}

	@Nullable
	public static PackageElement find(ProcessingEnvironment processingEnvironment, String packageSignatureString) {
		// TODO: Make sure it fails if type with such name exists.
		return processingEnvironment.getElementUtils().getPackageElement(packageSignatureString);
	}

	@Nullable
	public static Element findMember(ProcessingEnvironment processingEnvironment, String memberFullName, EntitySelectionContext selectionContext) {
		Element result;
		{
			TypeElement typeElement;
			if (selectionContext.canBeType()) {
				typeElement = processingEnvironment.getElementUtils().getTypeElement(memberFullName);
			} else {
				typeElement = null;
			}
			//
			if (typeElement != null) {
				result = typeElement;
			} else if (selectionContext.canBePackage()) {
				result = processingEnvironment.getElementUtils().getPackageElement(memberFullName);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static Element findMember(ProcessingEnvironment processingEnvironment, String packageFullName, EntitySelector selector) {
		return findMember(processingEnvironment, packageFullName + "." + selector.getSimpleName(), selector.getContext());
	}

	public static String getSignature(PackageElement packageElement) {
		return packageElement.getQualifiedName().toString();
	}

	public static PackageElement resolve(ProcessingEnvironment processingEnvironment, Class<?> childType) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(childType, "childType");
		//
		return resolve(processingEnvironment, childType.getPackageName());
	}

	public static PackageElement resolve(ProcessingEnvironment processingEnvironment, String packageName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(packageName, "packageName");
		//
		return Objects.requireNonNull(processingEnvironment.getElementUtils().getPackageElement(packageName), packageName);
	}

	public static PackageElement resolve(ProcessingEnvironment processingEnvironment, Stream<String> packageNames) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(packageNames, "packageNames");
		//
		return resolve(processingEnvironment, packageNames.collect(Collectors.joining(".")));
	}
}
