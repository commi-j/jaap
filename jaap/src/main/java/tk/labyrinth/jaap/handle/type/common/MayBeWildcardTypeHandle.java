package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;

public interface MayBeWildcardTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link WildcardTypeHandle}
	 */
	WildcardTypeHandle asWildcardType();

	boolean isWildcardType();
}
