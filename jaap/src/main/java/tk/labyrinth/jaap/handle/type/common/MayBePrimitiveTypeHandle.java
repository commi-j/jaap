package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;

public interface MayBePrimitiveTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link PrimitiveTypeHandle}
	 */
	PrimitiveTypeHandle asPrimitiveType();

	boolean isPrimitiveType();
}
