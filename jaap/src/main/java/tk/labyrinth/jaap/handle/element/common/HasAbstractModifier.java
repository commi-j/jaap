package tk.labyrinth.jaap.handle.element.common;

public interface HasAbstractModifier {

	/**
	 * @return <b>true</b> if this element has explicit <b>abstract</b> modifier, <b>false</b> otherwise.
	 *
	 * @see #isEffectivelyAbstract()
	 * @see #isEffectivelyNonAbstract()
	 */
	boolean hasExplicitAbstractModifier();

	/**
	 * @return <b>true</b> if this element has <b>abstract</b> modifier or implicitly <b>abstract</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitAbstractModifier()
	 * @see #isEffectivelyNonAbstract()
	 */
	boolean isEffectivelyAbstract();

	/**
	 * @return <b>true</b> if this element has no <b>abstract</b> modifier and not implicitly <b>abstract</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitAbstractModifier()
	 * @see #isEffectivelyAbstract()
	 */
	boolean isEffectivelyNonAbstract();
}
