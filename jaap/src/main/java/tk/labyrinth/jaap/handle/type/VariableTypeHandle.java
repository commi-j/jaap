package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeParameterElementHandle;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

public interface VariableTypeHandle extends
		ConvertibleToTypeParameterElementHandle,
		HasDescription,
		IsTypeHandle {
	// empty
}
