package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.misc4j2.java.lang.EnumUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import java.util.Comparator;
import java.util.Iterator;

public class ElementLookupUtils {

	@Nullable
	public static Element getMember(TypeElement typeElement, Iterator<String> simpleNames) {
		Element result;
		{
			String simpleName = simpleNames.next();
			Element element = typeElement.getEnclosedElements().stream()
					.filter(innerElement -> innerElement.getSimpleName().contentEquals(simpleName))
					.filter(innerElement -> EnumUtils.in(innerElement.getKind(), ElementKind.FIELD, ElementKind.CLASS))
					.min(new FieldThenTypeComparator()).orElse(null);
			if (element != null) {
				if (simpleNames.hasNext()) {
					result = ElementHandleUtils.getMember(element, simpleNames);
				} else {
					result = element;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	private static class FieldThenTypeComparator implements Comparator<Element> {

		@Override
		public int compare(Element o1, Element o2) {
			int result;
			if (o1.getKind() == ElementKind.FIELD) {
				if (o2.getKind() == ElementKind.CLASS) {
					result = -1;
				} else {
					result = 0;
				}
			} else {
				if (o1.getKind() == ElementKind.CLASS) {
					if (o2.getKind() == ElementKind.FIELD) {
						result = 1;
					} else {
						result = 0;
					}
				} else {
					result = 0;
				}
			}
			return result;
		}
	}
}
