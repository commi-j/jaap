package tk.labyrinth.jaap.handle.base.mixin;

import javax.lang.model.type.TypeMirror;

@Deprecated
public interface HasTypeMirror {

	/**
	 * @return non-null
	 */
	@Deprecated
	default TypeMirror getTypeMirror() {
		throw new UnsupportedOperationException();
	}
}
