package tk.labyrinth.jaap.handle.element.common;

public interface HasStaticModifier {

	/**
	 * - Top-level type elements are implicitly static;<br>
	 * - Type elements nested in interfaces are implicitly static;<br>
	 * - Other nested type elements are implicitly non-static;<br>
	 * <br>
	 * - Methods are implicitly non-static;<br>
	 * <br>
	 * - Fields are implicitly non-static in classes and static in interfaces;<br>
	 *
	 * @return <b>true</b> if this element has explicit <b>static</b> modifier, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 */
	// FIXME: Looks like we can't get accurate value in annproc. Should think if we need this method at all.
	boolean hasExplicitStaticModifier();

	/**
	 * @return <b>true</b> if this element has no <b>static</b> modifier and not implicitly <b>static</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 * @see #isEffectivelyStatic()
	 */
	boolean isEffectivelyNonStatic();

	/**
	 * @return <b>true</b> if this element has <b>static</b> modifier or implicitly <b>static</b>, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 * @see #isEffectivelyNonStatic()
	 */
	boolean isEffectivelyStatic();
}
