package tk.labyrinth.jaap.handle.element.common;

public interface HasParent<P> {

	P getParent();
}
