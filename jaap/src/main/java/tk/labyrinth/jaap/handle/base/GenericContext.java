package tk.labyrinth.jaap.handle.base;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

import java.util.Map;

@Value
public class GenericContext {

	Map<TypeParameterElementHandle, TypeHandle> typeParameterMappings;

	public boolean isEmpty() {
		return typeParameterMappings.isEmpty();
	}

	public static GenericContext empty() {
		return new GenericContext(Map.of());
	}

	public static GenericContext withMappings(Map<TypeParameterElementHandle, TypeHandle> typeParameterMappings) {
		return new GenericContext(Map.copyOf(typeParameterMappings));
	}
}
