package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface HasTypeParameters {

	@Nullable
	default TypeParameterElementHandle findTypeParameter(int index) {
		return getTypeParameterStream().skip(index).findFirst().orElse(null);
	}

	@Nullable
	default TypeParameterElementHandle findTypeParameter(String name) {
		return getTypeParameterStream()
				.filter(typeParameter -> Objects.equals(typeParameter.getName(), name))
				.collect(CollectorUtils.findOnly(true));
	}

	default TypeParameterElementHandle getTypeParameter(int index) {
		TypeParameterElementHandle result = findTypeParameter(index);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"index = " + index + ", " +
					"this = " + this);
		}
		return result;
	}

	default TypeParameterElementHandle getTypeParameter(String name) {
		TypeParameterElementHandle result = findTypeParameter(name);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"name = " + name + ", " +
					"this = " + this);
		}
		return result;
	}

	default int getTypeParameterCount() {
		return (int) getTypeParameterStream().count();
	}

	Stream<TypeParameterElementHandle> getTypeParameterStream();

	default List<TypeParameterElementHandle> getTypeParameters() {
		return getTypeParameterStream().collect(Collectors.toList());
	}

	default boolean hasTypeParameters() {
		return getTypeParameterCount() > 0;
	}
}
