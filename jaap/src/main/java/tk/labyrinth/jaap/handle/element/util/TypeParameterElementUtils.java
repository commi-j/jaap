package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeVariable;

public class TypeParameterElementUtils {

	public static String getSignature(ProcessingEnvironment processingEnvironment, TypeParameterElement typeParameterElement) {
		return ElementUtils.getSignature(processingEnvironment, typeParameterElement.getGenericElement()) +
				SignatureSeparators.TYPE_PARAMETER +
				typeParameterElement.getSimpleName();
	}

	public static TypeVariable getTypeVariable(TypeParameterElement typeParameterElement) {
		return (TypeVariable) typeParameterElement.asType();
	}
}
