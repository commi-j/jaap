package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.TypeHandle;

public interface IsTypeHandle {

	TypeHandle asType();
}
