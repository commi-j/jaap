package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

public interface ConvertibleToTypeParameterElementHandle {

	TypeParameterElementHandle toElement();
}
