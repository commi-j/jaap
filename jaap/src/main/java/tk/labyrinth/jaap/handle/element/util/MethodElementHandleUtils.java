package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodElementHandleUtils {

	public static Stream<MethodElementHandle> filterNonOverriden(Stream<? extends MethodElementHandle> methodElementHandles) {
		Map<MethodSimpleSignature, List<MethodElementHandle>> groupedMethodElementHandles = methodElementHandles
				.collect(Collectors.groupingBy(MethodElementHandle::getSimpleSignature));
		return groupedMethodElementHandles.values().stream()
				.map(handles -> handles.stream().reduce((first, second) ->
						first.getParent().isAssignableTo(second.getParent()) ? first : second))
				.map(Optional::orElseThrow);
	}
}
