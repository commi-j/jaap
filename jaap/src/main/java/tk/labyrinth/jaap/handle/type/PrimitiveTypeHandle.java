package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

public interface PrimitiveTypeHandle extends
		HasDescription,
		IsTypeHandle {
	// empty
}
