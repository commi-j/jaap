package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.model.signature.SignatureSeparators;

/**
 * @param <S> Signature
 *
 * @see SignatureSeparators
 */
// TODO: All implementing classes must use signature objects instead of Strings.
public interface HasSignature<S> {

	S getSignature();

	default String getSignatureString() {
		return getSignature().toString();
	}
}
