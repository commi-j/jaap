package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.type.common.HasDescription;

/**
 * TypeHandle Hierarchy:<br>
 * - {@link TypeHandle} (abstract)<br>
 * - - {@link PrimitiveTypeHandle} (concrete)<br>
 * - - {@link ReferenceTypeHandle} (a)<br>
 * * - - {@link ArrayTypeHandle} (c)<br>
 * - - - {@link DeclaredTypeHandle} (a)<br>
 * - - - - {@link GenericTypeHandle} (a)<br>
 * - - - - - {@link ParameterizedTypeHandle} (c)<br>
 * - - - - - {@link RawTypeHandle} (c)<br>
 * - - - - {@link PlainTypeHandle} (c)<br>
 * - - - {@link VariableTypeHandle} (c)<br>
 * - - - {@link WildcardTypeHandle} (c)<br>
 * - - {@link VoidTypeHandle} (c)<br>
 * <br>
 * https://docs.oracle.com/javase/specs/jls/se7/html/jls-10.html<br>
 */
public interface ArrayTypeHandle extends
		HasDescription {

	TypeHandle asType();

	TypeHandle getComponentType();
}
