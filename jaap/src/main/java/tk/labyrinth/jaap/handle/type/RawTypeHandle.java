package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsGenericTypeHandle;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;
import tk.labyrinth.jaap.model.element.common.ConvertibleToTypeElementHandle;

public interface RawTypeHandle extends
		ConvertibleToTypeElementHandle,
		HasDescription,
		HasProcessingContext,
		HasTypeMirror,
		IsGenericTypeHandle,
		IsTypeHandle {
	// empty
}
