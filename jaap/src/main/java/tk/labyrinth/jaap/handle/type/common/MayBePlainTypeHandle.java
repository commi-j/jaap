package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.PlainTypeHandle;

public interface MayBePlainTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link PlainTypeHandle}
	 */
	PlainTypeHandle asPlainType();

	boolean isPlainType();
}
