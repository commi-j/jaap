package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeAnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeGenericTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBePlainTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeRawTypeHandle;
import tk.labyrinth.jaap.model.element.common.ConvertibleToTypeElementHandle;

/**
 * TypeHandle Hierarchy:<br>
 * - {@link TypeHandle} (abstract)<br>
 * - - {@link PrimitiveTypeHandle} (concrete)<br>
 * - - {@link ReferenceTypeHandle} (a)<br>
 * - - - {@link ArrayTypeHandle} (c)<br>
 * * - - {@link DeclaredTypeHandle} (a)<br>
 * - - - - {@link GenericTypeHandle} (a)<br>
 * - - - - - {@link ParameterizedTypeHandle} (c)<br>
 * - - - - - {@link RawTypeHandle} (c)<br>
 * - - - - {@link PlainTypeHandle} (c)<br>
 * - - - {@link VariableTypeHandle} (c)<br>
 * - - - {@link WildcardTypeHandle} (c)<br>
 * - - {@link VoidTypeHandle} (c)<br>
 * <br>
 */
public interface DeclaredTypeHandle extends
		ConvertibleToTypeElementHandle,
		HasDescription,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		IsTypeHandle,
		MayBeAnnotationTypeHandle,
		MayBeGenericTypeHandle,
		MayBeParameterizedTypeHandle,
		MayBePlainTypeHandle,
		MayBeRawTypeHandle {
	// empty
}
