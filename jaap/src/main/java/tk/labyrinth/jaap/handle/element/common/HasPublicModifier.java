package tk.labyrinth.jaap.handle.element.common;

public interface HasPublicModifier {

	/**
	 * @return <b>true</b> if this element has explicit public modifier, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitPublicModifier()
	 */
	// FIXME: Looks like we can't get accurate value in annproc. Should think if we need this method at all.
	boolean hasExplicitPublicModifier();

	/**
	 * @return <b>true</b> if this element has no public modifier and not implicitly public, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitPublicModifier()
	 * @see #isEffectivelyPublic()
	 */
	boolean isEffectivelyNonPublic();

	/**
	 * @return <b>true</b> if this element has public modifier or implicitly public, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitPublicModifier()
	 * @see #isEffectivelyNonPublic()
	 */
	boolean isEffectivelyPublic();
}
