package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

public interface PlainTypeHandle extends
		HasDescription,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		IsTypeHandle {
	// empty
}
