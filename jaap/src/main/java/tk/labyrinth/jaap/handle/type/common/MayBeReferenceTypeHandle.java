package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;

public interface MayBeReferenceTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link ReferenceTypeHandle}
	 */
	ReferenceTypeHandle asReferenceType();

	boolean isReferenceType();
}
