package tk.labyrinth.jaap.handle.element.util;

import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.langreflect.util.MethodUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.ParameterizableUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExecutableElementUtils {

	@Nullable
	public static ExecutableElement find(ProcessingEnvironment processingEnvironment, Class<?> type, MethodSimpleSignature simpleSignature) {
		return TypeElementUtils.findDeclaredMethod(processingEnvironment,
				TypeElementUtils.get(processingEnvironment, type), simpleSignature);
	}

	@Nullable
	public static ExecutableElement find(ProcessingEnvironment processingEnvironment, Executable executable) {
		ExecutableElement result;
		{
			TypeElement typeElement = TypeElementUtils.find(processingEnvironment, executable.getDeclaringClass());
			if (typeElement != null) {
				throw new NotImplementedException(ExceptionUtils.render(executable));
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static ExecutableElement findConstructor(ProcessingEnvironment processingEnvironment, TypeElement typeElement, List<TypeMirror> parameterTypes) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.CONSTRUCTOR)
				.map(ExecutableElement.class::cast)
				.filter(constructor -> Objects.equals(
						constructor.getParameters().stream()
								.map(parameter -> VariableElementUtils.getTypeMirrorErasure(processingEnvironment, parameter))
								.collect(Collectors.toList()),
						parameterTypes))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	public static ExecutableElement findConstructor(ProcessingEnvironment processingEnvironment, TypeElement typeElement, TypeMirror... parameterTypes) {
		return findConstructor(processingEnvironment, typeElement, List.of(parameterTypes));
	}

	@Nullable
	public static ExecutableElement findDeclaredMethodByName(
			ProcessingEnvironment processingEnvironment,
			Class<?> type,
			String methodName) {
		return TypeElementUtils.findDeclaredMethodByName(processingEnvironment, type, methodName);
	}

	@Nullable
	public static ExecutableElement findMethod(ProcessingEnvironment processingEnvironment, MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return TypeElementUtils.findDeclaredMethod(processingEnvironment, methodFullSignature);
	}

	@Nullable
	public static TypeElement findReturnTypeElement(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement) {
		return TypeElementUtils.find(processingEnvironment, executableElement.getReturnType());
	}

	@Nullable
	public static TypeParameterElement findTypeParameter(ExecutableElement executableElement, String name) {
		return ParameterizableUtils.findTypeParameter(executableElement, name);
	}

	public static ExecutableElement get(ProcessingEnvironment processingEnvironment, Class<?> type, MethodSimpleSignature methodSimpleSignature) {
		ExecutableElement result = find(processingEnvironment, type, methodSimpleSignature);
		if (result == null) {
			throw new IllegalArgumentException("No ExecutableElement found: " +
					"type = " + type + ", " +
					"methodSimpleSignature = " + methodSimpleSignature);
		}
		return result;
	}

	public static ExecutableElement get(ProcessingEnvironment processingEnvironment, Method method) {
		return TypeElementUtils.getDeclaredMethod(
				processingEnvironment,
				TypeElementUtils.get(processingEnvironment, method.getDeclaringClass()),
				MethodUtils.getSimpleSignature(method));
	}

	public static ExecutableElement getConstructor(ProcessingEnvironment processingEnvironment, TypeElement typeElement, List<TypeMirror> parameterTypes) {
		ExecutableElement result = findConstructor(processingEnvironment, typeElement, parameterTypes);
		if (result == null) {
			throw new IllegalArgumentException("No Constructor resolved: typeElement = " +
					typeElement + ", parameterTypes = " + parameterTypes);
		}
		return result;
	}

	public static ExecutableElement getConstructor(ProcessingEnvironment processingEnvironment, TypeElement typeElement, TypeMirror... parameterTypes) {
		return getConstructor(processingEnvironment, typeElement, List.of(parameterTypes));
	}

	public static Stream<ExecutableElement> getDeclaredInstanceInitializers(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.INSTANCE_INIT)
				.map(ExecutableElement.class::cast);
	}

	public static ExecutableElement getDeclaredMethodByName(
			ProcessingEnvironment processingEnvironment,
			Class<?> type,
			String methodName) {
		ExecutableElement result = findDeclaredMethodByName(processingEnvironment, type, methodName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"type = " + type + ", " +
					"methodName = " + methodName);
		}
		return result;
	}

	public static Stream<ExecutableElement> getDeclaredStaticInitializers(TypeElement typeElement) {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.STATIC_INIT)
				.map(ExecutableElement.class::cast);
	}

	public static TypeElement getReturnTypeElement(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement) {
		TypeElement result = findReturnTypeElement(processingEnvironment, executableElement);
		if (result == null) {
			throw new IllegalArgumentException("No return TypeElement found: executableElement = " + executableElement);
		}
		return result;
	}

	public static TypeMirror getReturnTypeMirrorErasure(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement) {
		return TypeMirrorUtils.erasure(processingEnvironment, executableElement);
	}

	// TODO: Make FormalParameters not write annotations.
	public static String getSignatureString(
			ProcessingEnvironment processingEnvironment,
			ExecutableElement executableElement) {
		return ElementUtils.getSignature(processingEnvironment, executableElement.getEnclosingElement()) + "#" +
				executableElement.getSimpleName() +
				executableElement.getParameters().stream()
						.map(parameter -> TypeMirrorUtils.getSignatureContributingString(
								TypeMirrorUtils.erasure(processingEnvironment, parameter.asType())))
						.collect(Collectors.joining(",", "(", ")"));
	}

	public static boolean isConstructor(ExecutableElement value) {
		return ElementUtils.isConstructor(value);
	}

	/**
	 * Note: only Methods can be abstract, Constructors and Initializers can not.
	 *
	 * @param executableElement non-null
	 *
	 * @return true if abstract, false otherwise
	 */
	public static boolean isEffectivelyAbstract(ExecutableElement executableElement) {
		return executableElement.getModifiers().contains(Modifier.ABSTRACT);
	}

	public static boolean isInitializer(ExecutableElement value) {
		return ElementUtils.isInitializer(value);
	}

	public static boolean isMethod(ExecutableElement value) {
		return ElementUtils.isMethod(value);
	}

	public static boolean isStatic(ExecutableElement executableElement) {
		return executableElement.getKind() == ElementKind.STATIC_INIT ||
				executableElement.getModifiers().contains(Modifier.STATIC);
	}

	public static ExecutableElement requireConstructor(ExecutableElement value) {
		return ElementUtils.requireConstructor(value);
	}

	public static ExecutableElement requireInitializer(ExecutableElement value) {
		return ElementUtils.requireInitializer(value);
	}

	public static ExecutableElement requireMethod(ExecutableElement value) {
		return ElementUtils.requireMethod(value);
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, Class<?> type, String methodSimpleSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(type, "type");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		//
		return resolve(processingEnvironment, MethodSignatureUtils.createFull(type, methodSimpleSignature));
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, Class<?> type, String methodSimpleName, List<TypeMirror> parameterTypes) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(type, "type");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return resolve(
				processingEnvironment,
				MethodSignatureUtils.createFull(
						TypeElementUtils.get(processingEnvironment, type),
						MethodSimpleSignature.of(
								methodSimpleName,
								parameterTypes.stream().map(TypeMirrorUtils::getSignature))));
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return TypeElementUtils.getDeclaredMethod(processingEnvironment, methodFullSignature);
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, String methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return resolve(processingEnvironment, MethodFullSignature.of(methodFullSignature));
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, String typeFullName, String methodSimpleSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeFullName, "typeFullName");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		//
		return resolve(
				processingEnvironment,
				MethodSignatureUtils.createFull(
						TypeElementUtils.get(processingEnvironment, typeFullName),
						methodSimpleSignature));
	}

	public static ExecutableElement resolve(ProcessingEnvironment processingEnvironment, TypeElement typeElement, String methodSimpleSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		//
		return TypeElementUtils.getDeclaredMethod(processingEnvironment, typeElement, methodSimpleSignature);
	}

	@Nullable
	public static Element selectMember(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement, EntitySelector selector) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(executableElement, "executableElement");
		Objects.requireNonNull(selector, "selector");
		//
		TypeElement returnTypeElement = findReturnTypeElement(processingEnvironment, executableElement);
		return returnTypeElement != null
				? TypeElementUtils.selectMember(processingEnvironment, returnTypeElement, selector)
				: null;
	}
}
