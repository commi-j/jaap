package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.langmodel.element.LangmodelElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandleBase;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.InitializerElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.VariableElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Value
public class LangmodelElementHandleImpl extends ElementHandleBase implements LangmodelElementHandle {

	Element element;

	LangmodelElementHandleFactory elementHandleFactory;

	public LangmodelElementHandleImpl(Element element, LangmodelElementHandleFactory elementHandleFactory) {
		this.element = Objects.requireNonNull(element);
		this.elementHandleFactory = Objects.requireNonNull(elementHandleFactory);
	}

	@Override
	public ConstructorElementHandle asConstructorElement() {
		return elementHandleFactory.getConstructor(element);
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		return elementHandleFactory.getExecutable(element);
	}

	@Override
	public FieldElementHandle asFieldElement() {
		return elementHandleFactory.getField(element);
	}

	@Override
	public FormalParameterElementHandle asFormalParameterElement() {
		return elementHandleFactory.getFormalParameter(element);
	}

	@Override
	public InitializerElementHandle asInitializerElement() {
		return elementHandleFactory.getInitializer(element);
	}

	@Override
	public MethodElementHandle asMethodElement() {
		return elementHandleFactory.getMethod(element);
	}

	@Override
	public PackageElementHandle asPackageElement() {
		return elementHandleFactory.getPackage(element);
	}

	@Override
	public TypeElementHandle asTypeElement() {
		return elementHandleFactory.getType(element);
	}

	@Override
	public TypeParameterElementHandle asTypeParameterElement() {
		return elementHandleFactory.getTypeParameter(element);
	}

	@Override
	public VariableElementHandle asVariableElement() {
		return elementHandleFactory.getVariable(element);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		return Objects.equals(toString(), o.toString());
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return element.getAnnotationMirrors().stream()
				.map(annotationMirror -> elementHandleFactory.getProcessingContext().getAnnotationHandle(
						element,
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public String getName() {
		return ElementUtils.getName(element);
	}

	@Nullable
	@Override
	public ElementHandle getParent() {
		Element enclosingElement = element.getEnclosingElement();
		return enclosingElement != null ? elementHandleFactory.get(enclosingElement) : null;
	}

	@Override
	public ProcessingContext getProcessingContext() {
		return elementHandleFactory.getProcessingContext();
	}

	@Override
	public String getSignatureString() {
		return ElementUtils.getSignature(elementHandleFactory.getProcessingEnvironment(), element);
	}

	@Override
	public int hashCode() {
		return Objects.hash(toString());
	}

	@Override
	public boolean isConstructorElement() {
		return ElementUtils.isConstructor(element);
	}

	@Override
	public boolean isExecutableElement() {
		return ElementUtils.isExecutable(element);
	}

	@Override
	public boolean isFieldElement() {
		return ElementUtils.isField(element);
	}

	@Override
	public boolean isFormalParameterElement() {
		return ElementUtils.isFormalParameter(element);
	}

	@Override
	public boolean isInitializerElement() {
		return ElementUtils.isInitializer(element);
	}

	@Override
	public boolean isMethodElement() {
		return ElementUtils.isMethod(element);
	}

	@Override
	public boolean isPackageElement() {
		return ElementUtils.isPackage(element);
	}

	@Override
	public boolean isTypeElement() {
		return ElementUtils.isType(element);
	}

	@Override
	public boolean isTypeParameterElement() {
		return ElementUtils.isTypeParameter(element);
	}

	@Override
	public boolean isVariableElement() {
		return ElementUtils.isVariable(element);
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		Element selectedMember = ElementUtils.selectMember(
				elementHandleFactory.getProcessingEnvironment(),
				element,
				selector);
		return selectedMember != null ? elementHandleFactory.get(selectedMember) : null;
	}

	@Override
	public String toString() {
		return getSignatureString();
	}
}
