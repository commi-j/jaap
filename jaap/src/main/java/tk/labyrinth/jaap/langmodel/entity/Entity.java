package tk.labyrinth.jaap.langmodel.entity;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

/**
 * foo.bar() - foo is IdentifierTree that may be:<br>
 * - LocalVariable - VariableTree;<br>
 * - MethodParameter - VariableElement (TODO: Need check);<br>
 * - LocalField - VariableTree;<br>
 * - InheritedField - VariableElement;<br>
 * - LocalType - ClassTree;<br>
 * - ImportedType - TypeElement;<br>
 * - PackageType - TypeElement;<br>
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface Entity {

	default Element asElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default ExecutableElement asExecutableElement() {
		return (ExecutableElement) asElement();
	}

	default ExpressionTree asExpressionTree() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default MethodInvocationTree asMethodInvocationTree() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default MethodTree asMethodTree() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default ThisEntity asThisEntity() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default Tree asTree() {
		Tree result;
		if (isExpressionTree()) {
			result = asExpressionTree();
		} else if (isMethodInvocationTree()) {
			result = asMethodInvocationTree();
		} else if (isMethodTree()) {
			result = asMethodTree();
		} else if (isVariableTree()) {
			result = asVariableTree();
		} else {
			throw new NotImplementedException(ExceptionUtils.render(this));
		}
		return result;
	}

	default TypeElement asTypeElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default VariableTree asVariableTree() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default boolean isElement() {
		return false;
	}

	default boolean isExecutableElement() {
		return isElement() && ElementUtils.isExecutable(asElement());
	}

	default boolean isExpressionTree() {
		return false;
	}

	default boolean isMethodInvocationTree() {
		return false;
	}

	default boolean isMethodTree() {
		return false;
	}

	default boolean isThisEntity() {
		return false;
	}

	default boolean isTree() {
		return isExpressionTree() || isMethodInvocationTree() || isMethodTree() || isVariableTree();
	}

	default boolean isTypeElement() {
		return isElement() && ElementUtils.isType(asElement());
	}

	default boolean isVariableTree() {
		return false;
	}

	static Entity ofElement(@NonNull Element element) {
		return new ElementVariant(element);
	}

	static Entity ofExpressionTree(ExpressionTree expressionTree) {
		return new ExpressionTreeVariant(expressionTree);
	}

	static Entity ofMethodInvocationTree(MethodInvocationTree methodInvocationTree) {
		return new MethodInvocationTreeVariant(methodInvocationTree);
	}

	static Entity ofMethodTree(MethodTree methodTree) {
		return new MethodTreeVariant(methodTree);
	}

	static Entity ofThisEntity(ThisEntity thisEntity) {
		return new ThisEntityVariant(thisEntity);
	}

	static Entity ofVariableTree(VariableTree variableTree) {
		return new VariableTreeVariant(variableTree);
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class ElementVariant implements Entity {

		@NonNull
		Element element;

		@Override
		public Element asElement() {
			return element;
		}

		@Override
		public TypeElement asTypeElement() {
			return ElementUtils.requireType(element);
		}

		@Override
		public boolean isElement() {
			return true;
		}

		@Override
		public String toString() {
			return element.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class ExpressionTreeVariant implements Entity {

		@NonNull
		ExpressionTree expressionTree;

		@Override
		public ExpressionTree asExpressionTree() {
			return expressionTree;
		}

		@Override
		public boolean isExpressionTree() {
			return true;
		}

		@Override
		public String toString() {
			return expressionTree.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class MethodInvocationTreeVariant implements Entity {

		@NonNull
		MethodInvocationTree methodInvocationTree;

		@Override
		public MethodInvocationTree asMethodInvocationTree() {
			return methodInvocationTree;
		}

		@Override
		public boolean isMethodInvocationTree() {
			return true;
		}

		@Override
		public String toString() {
			return methodInvocationTree.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class MethodTreeVariant implements Entity {

		@NonNull
		MethodTree methodTree;

		@Override
		public MethodTree asMethodTree() {
			return methodTree;
		}

		@Override
		public boolean isMethodTree() {
			return true;
		}

		@Override
		public String toString() {
			return methodTree.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class ThisEntityVariant implements Entity {

		@NonNull
		ThisEntity thisEntity;

		@Override
		public ThisEntity asThisEntity() {
			return thisEntity;
		}

		@Override
		public boolean isThisEntity() {
			return true;
		}

		@Override
		public String toString() {
			return thisEntity.toString();
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class VariableTreeVariant implements Entity {

		@NonNull
		VariableTree variableTree;

		@Override
		public VariableTree asVariableTree() {
			return variableTree;
		}

		@Override
		public boolean isVariableTree() {
			return true;
		}

		@Override
		public String toString() {
			return variableTree.toString();
		}
	}
}
