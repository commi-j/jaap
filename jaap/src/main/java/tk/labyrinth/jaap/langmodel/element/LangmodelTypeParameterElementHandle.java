package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

import javax.lang.model.element.TypeParameterElement;

public interface LangmodelTypeParameterElementHandle extends TypeParameterElementHandle {

	TypeParameterElement getTypeParameterElement();
}
