package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.MethodElementHandle;

import javax.lang.model.element.ExecutableElement;

public interface LangmodelMethodElementHandle extends MethodElementHandle {

	ExecutableElement getExecutableElement();
}
