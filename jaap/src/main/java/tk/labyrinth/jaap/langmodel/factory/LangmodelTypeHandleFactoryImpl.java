package tk.labyrinth.jaap.langmodel.factory;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextAware;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.impl.TypeHandleImpl;
import tk.labyrinth.jaap.langmodel.element.LangmodelTypeParameterElementHandle;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeVariableUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.ElementHandleFactory;
import tk.labyrinth.jaap.model.factory.TypeHandleFactoryBase;
import tk.labyrinth.jaap.model.type.DefaultTypeHandle;
import tk.labyrinth.jaap.util.PrimitiveTypeUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.reflect.Type;
import java.util.stream.Collectors;

public class LangmodelTypeHandleFactoryImpl extends TypeHandleFactoryBase implements
		LangmodelTypeHandleFactory,
		ProcessingContextAware {

	private final LangmodelTypeMirrorFactory typeMirrorFactory;

	private ProcessingContext processingContext;

	public LangmodelTypeHandleFactoryImpl(
			ElementHandleFactory elementHandleFactory,
			LangmodelTypeMirrorFactory typeMirrorFactory) {
		super(elementHandleFactory);
		this.typeMirrorFactory = typeMirrorFactory;
	}

	@Override
	public void acceptProcessingContext(ProcessingContext processingContext) {
		this.processingContext = processingContext;
	}

	@Nullable
	@Override
	public TypeHandle find(TypeDescription typeDescription) {
		TypeHandle result;
		if (typeDescription.isParameterized() || typeDescription.isWildcard()) {
			result = new DefaultTypeHandle(typeDescription, this);
		} else {
			TypeMirror typeMirror = typeMirrorFactory.find(typeDescription);
			result = typeMirror != null
					? new TypeHandleImpl(GenericContext.empty(), processingContext, typeMirror)
					: null;
		}
		return result;
	}

	@Override
	public TypeHandle get(GenericContext genericContext, Class<?> type) {
		return new TypeHandleImpl(genericContext, processingContext, typeMirrorFactory.get(type));
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeElement typeElement) {
		return get(genericContext, typeElement.asType());
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeMirror typeMirror) {
		TypeMirror resolvedTypeMirror;
		if (!genericContext.isEmpty()) {
			resolvedTypeMirror = TypeMirrorUtils.resolve(
					processingContext.getProcessingEnvironment(),
					genericContext.getTypeParameterMappings().entrySet().stream()
							.collect(Collectors.toMap(
									// FIXME: Cast from common model to langmodel model.
									entry -> ((LangmodelTypeParameterElementHandle) entry.getKey())
											.getTypeParameterElement(),
									entry -> entry.getValue().getTypeMirror())),
					typeMirror);
		} else {
			resolvedTypeMirror = typeMirror;
		}
		return new TypeHandleImpl(genericContext, processingContext, resolvedTypeMirror);
	}

	@Override
	public ArrayTypeHandle getArray(GenericContext genericContext, TypeMirror typeMirror) {
		return getArray(TypeMirrorUtils.toDescription(processingContext.getProcessingEnvironment(), typeMirror));
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, DeclaredType declaredType) {
		// TODO: Support GenericContext.
		return getDeclared(DeclaredTypeUtils.toDescription(processingContext.getProcessingEnvironment(), declaredType));
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeElement typeElement) {
		return getDeclared(genericContext, TypeMirrorUtils.requireDeclaredType(typeElement.asType()));
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeMirror typeMirror) {
		return getDeclared(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public GenericTypeHandle getGeneric(GenericContext genericContext, DeclaredType declaredType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public GenericTypeHandle getGeneric(GenericContext genericContext, TypeMirror typeMirror) {
		return getGeneric(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public ParameterizedTypeHandle getParameterized(
			GenericContext genericContext,
			DeclaredType declaredType) {
		// FIXME: GenericContext is actually ignored now.
		return getParameterized(DeclaredTypeUtils.toDescription(
				processingContext.getProcessingEnvironment(),
				declaredType));
	}

	@Override
	public ParameterizedTypeHandle getParameterized(GenericContext genericContext, Type type) {
		return getParameterized(
				genericContext,
				typeMirrorFactory.getDeclared(genericContext, type));
	}

	@Override
	public ParameterizedTypeHandle getParameterized(GenericContext genericContext, TypeMirror typeMirror) {
		return getParameterized(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public PlainTypeHandle getPlain(GenericContext genericContext, DeclaredType declaredType) {
		// TODO: Support genericContext.
		return getPlain(DeclaredTypeUtils.toDescription(processingContext.getProcessingEnvironment(), declaredType));
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(PrimitiveType primitiveType) {
		return getPrimitive(PrimitiveTypeUtils.toDescription(primitiveType));
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(TypeMirror typeMirror) {
		return getPrimitive(TypeMirrorUtils.requirePrimitive(typeMirror));
	}

	@Override
	public RawTypeHandle getRaw(DeclaredType declaredType) {
		return getRaw(DeclaredTypeUtils.toDescription(processingContext.getProcessingEnvironment(), declaredType));
	}

	@Override
	public RawTypeHandle getRaw(TypeMirror typeMirror) {
		return getRaw(TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public VariableTypeHandle getVariable(GenericContext genericContext, TypeMirror typeMirror) {
		return getVariable(genericContext, TypeMirrorUtils.requireTypeVariable(typeMirror));
	}

	@Override
	public VariableTypeHandle getVariable(GenericContext genericContext, TypeVariable typeVariable) {
		return getVariable(TypeVariableUtils.toDescription(processingContext.getProcessingEnvironment(), typeVariable));
	}
}
