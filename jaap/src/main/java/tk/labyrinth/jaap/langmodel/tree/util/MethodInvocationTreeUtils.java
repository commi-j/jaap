package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.entity.EntityUtils;
import tk.labyrinth.jaap.langmodel.entity.ThisEntity;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.methodlookup.LambdaSignature;
import tk.labyrinth.jaap.model.methodlookup.MethodArgumentSignature;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class MethodInvocationTreeUtils {

	@Nullable
	private static Boolean computeLambdaSignatureRequireConsumption(LambdaExpressionTree lambdaExpressionTree) {
		Boolean result;
		{
			result = switch (lambdaExpressionTree.getBodyKind()) {
				case EXPRESSION -> {
					Boolean requireConsumption;
					{
						Tree body = lambdaExpressionTree.getBody();
						//
						if (body instanceof ArrayAccessTree ||
								body instanceof IdentifierTree ||
								body instanceof LiteralTree ||
								body instanceof MemberSelectTree) {
							requireConsumption = true;
						} else if (body instanceof MethodInvocationTree) {
							// TODO: Determine from return type (void -> false, null otherwise).
							requireConsumption = null;
						} else {
							throw new NotImplementedException();
						}
					}
					yield requireConsumption;
				}
				case STATEMENT -> {
					throw new NotImplementedException();
				}
			};
		}
		return result;
	}

	private static LambdaSignature createLambdaSignature(
			CompilationUnitContext compilationUnitContext,
			LambdaExpressionTree lambdaExpressionTree) {
		return LambdaSignature.builder()
				.parameters(lambdaExpressionTree.getParameters().stream()
						.map(parameter -> parameter.getType() != null
								? TypeMirrorUtils.toDescription(
								compilationUnitContext.getProcessingEnvironment(),
								TreeUtils.findTypeMirror(
										compilationUnitContext,
										parameter.getType(),
										EntitySelectionContext.forVariableType()))
								: TypeDescription.ofWildcard())
						.collect(Collectors.toList()))
				.requireConsumption(computeLambdaSignatureRequireConsumption(lambdaExpressionTree))
				.build();
	}

	private static MethodArgumentSignature createMethodArgumentSignature(
			CompilationUnitContext compilationUnitContext,
			ExpressionTree expressionTree) {
		MethodArgumentSignature result;
		{
			if (expressionTree instanceof LambdaExpressionTree lambdaExpressionTree) {
				result = MethodArgumentSignature.ofLambdaSignature(createLambdaSignature(
						compilationUnitContext,
						lambdaExpressionTree));
			} else {
				Entity entity = ExpressionTreeUtils.findEntity(
						compilationUnitContext,
						expressionTree,
						EntitySelectionContext.forMethodInvocationArgument());
				//
				if (entity != null) {
					if (entity.isVariableTree()) {
						VariableTree variableTree = entity.asVariableTree();
						//
						if (VariableTreeUtils.isInferredLambdaParameter(compilationUnitContext, variableTree)) {
							result = MethodArgumentSignature.ofInferredVariableName(variableTree.getName().toString());
						} else {
							result = MethodArgumentSignature.ofTypeDescription(TypeMirrorUtils.toDescription(
									compilationUnitContext.getProcessingEnvironment(),
									VariableTreeUtils.findTypeMirror(compilationUnitContext, variableTree)));
						}
					} else {
						//
						result = MethodArgumentSignature.ofTypeDescription(TypeMirrorUtils.toDescription(
								compilationUnitContext.getProcessingEnvironment(),
								EntityUtils.getTypeMirror(
										compilationUnitContext,
										entity,
										EntitySelectionContext.forMethodInvocationArgument())));
					}
				} else {
					throw new NotImplementedException();
				}
			}
		}
		return result;
	}

	public static MethodSelector createMethodSelector(
			CompilationUnitContext compilationUnitContext,
			MethodInvocationTree methodInvocationTree) {
		return MethodSelector.of(
				methodInvocationTree.getArguments().stream()
						.map(argument -> createMethodArgumentSignature(compilationUnitContext, argument))
						.toList(),
				getName(methodInvocationTree));
	}

	@Nullable
	public static ExecutableElement findMethod(
			CompilationUnitContext compilationUnitContext,
			MethodInvocationTree methodInvocationTree) {
		ExecutableElement result;
		{
			MethodSelector methodSelector = createMethodSelector(compilationUnitContext, methodInvocationTree);
			//
			if (methodSelector != null) {
				if (hasExplicitTarget(methodInvocationTree)) {
					// foo.bar() - we know that bar() should be a member of foo.
					//
					TypeMirror targetTypeMirror = findTargetType(compilationUnitContext, methodInvocationTree);
					//
					if (targetTypeMirror != null) {
						result = TypeElementUtils.selectMethod(
								compilationUnitContext.getProcessingEnvironment(),
								TypeElementUtils.get(
										compilationUnitContext.getProcessingEnvironment(),
										targetTypeMirror),
								methodSelector);
					} else {
						result = null;
					}
				} else {
					// TODO: Support static import.
					// bar() - target could be any of nested type chain.
					//
					Entity entity = EntityUtils.findEntity(
							compilationUnitContext,
							methodInvocationTree,
							methodSelector.toEntitySelector());
					if (entity != null) {
						result = EntityUtils.getExecutableElement(entity);
					} else {
						result = null;
					}
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror findReturnType(
			CompilationUnitContext compilationUnitContext,
			MethodInvocationTree methodInvocationTree) {
		ExecutableElement method = findMethod(compilationUnitContext, methodInvocationTree);
		//
		return method != null
				? ExecutableElementUtils.getReturnTypeMirrorErasure(
				compilationUnitContext.getProcessingEnvironment(),
				method)
				: null;
	}

	@Nullable
	public static Entity findTargetEntity(
			CompilationUnitContext compilationUnitContext,
			MethodInvocationTree methodInvocationTree) {
		Entity result;
		{
			ExpressionTree targetExpressionTree = findTargetExpressionTree(methodInvocationTree);
			//
			if (targetExpressionTree != null) {
				// Invocation on target: foo.bar().
				//
				result = ExpressionTreeUtils.findEntity(
						compilationUnitContext,
						targetExpressionTree,
						EntitySelectionContext.forMethodInvocationTarget());
			} else {
				// Invocation in local context: bar().
				//
				ExecutableElement method = findMethod(compilationUnitContext, methodInvocationTree);
				//
				if (method != null) {
					TypeElement scopeTypeElement = compilationUnitContext.findScopeTypeElement(methodInvocationTree);
					if (ExecutableElementUtils.isStatic(method)) {
						result = Entity.ofElement(scopeTypeElement);
					} else {
						result = Entity.ofThisEntity(ThisEntity.of(scopeTypeElement));
					}
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	public static ExpressionTree findTargetExpressionTree(MethodInvocationTree methodInvocationTree) {
		ExpressionTree result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = null;
			} else if (methodSelect instanceof MemberSelectTree) {
				result = ((MemberSelectTree) methodSelect).getExpression();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodSelect));
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTargetType(
			CompilationUnitContext compilationUnitContext,
			MethodInvocationTree methodInvocationTree) {
		TypeMirror result;
		{
			Entity targetEntity = findTargetEntity(compilationUnitContext, methodInvocationTree);
			//
			if (targetEntity != null) {
				result = EntityUtils.findTypeMirror(
						compilationUnitContext,
						targetEntity,
						EntitySelectionContext.forVariableOrType());
			} else {
				result = null;
			}
		}
		return result;
	}

	public static Entity getEntity(MethodInvocationTree methodInvocationTree) {
		return Entity.ofExpressionTree(methodInvocationTree);
	}

	// TODO: Cover with tests.
	public static String getName(MethodInvocationTree methodInvocationTree) {
		String result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree identifierTree) {
				result = identifierTree.getName().toString();
			} else if (methodSelect instanceof MemberSelectTree memberSelectTree) {
				result = memberSelectTree.getIdentifier().toString();
			} else {
				throw new UnsupportedOperationException(ExceptionUtils.render(methodInvocationTree));
			}
		}
		return result;
	}

	// TODO: Cover with tests.
	public static Stream<String> getNameChain(MethodInvocationTree methodInvocationTree) {
		Stream<String> result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree identifierTree) {
				result = IdentifierTreeUtils.getNameChain(identifierTree);
			} else if (methodSelect instanceof MemberSelectTree memberSelectTree) {
				result = MemberSelectTreeUtils.getNameChain(memberSelectTree);
			} else {
				throw new UnsupportedOperationException(ExceptionUtils.render(methodInvocationTree));
			}
		}
		return result;
	}

	// TODO: Cover with tests.
	public static boolean hasExplicitTarget(MethodInvocationTree methodInvocationTree) {
		return !(methodInvocationTree.getMethodSelect() instanceof IdentifierTree);
	}
}
