package tk.labyrinth.jaap.langmodel.factory;

import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Method;

public interface LangmodelElementFactory {

	VariableElement findDeclaredField(ElementSignature fieldSignature);

	VariableElement findDeclaredField(String fieldSignatureString);

	@Nullable
	Element findElement(ElementSignature elementSignature);

	@Nullable
	default Element findElement(String elementSignatureString) {
		return findElement(ElementSignature.of(elementSignatureString));
	}

	@Nullable
	ExecutableElement findMethod(Class<?> type, String methodSimpleSignatureString);

	@Nullable
	ExecutableElement findMethod(MethodFullSignature methodFullSignature);

	@Nullable
	default ExecutableElement findMethod(String methodFullSignature) {
		return findMethod(MethodFullSignature.of(methodFullSignature));
	}

	@Nullable
	PackageElement findPackage(ElementSignature packageElementSignature);

	@Nullable
	PackageElement findPackage(String packageSignatureString);

	@Nullable
	Element findPackageOrType(ElementSignature packageOrTypeSignature);

	@Nullable
	Parameterizable findParameterizable(ElementSignature parameterizableSignature);

	@Nullable
	Parameterizable findParameterizable(String parameterizableSignature);

	@Nullable
	default TypeElement findType(String typeSignatureString) {
		return findType(TypeSignature.ofValid(typeSignatureString));
	}

	@Nullable
	TypeElement findType(TypeSignature typeSignature);

	@Nullable
	TypeParameterElement findTypeParameter(String fullTypeParameterElementSignature);

	Element get(Class<?> type);

	ExecutableElement getMethod(Method method);

	PackageElement getPackage(Element element);

	PackageElement getPackageOf(Class<?> childType);

	TypeElement getType(Class<?> type);

	default TypeElement getType(String typeSignatureString) {
		TypeElement result = findType(typeSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeSignatureString = " + typeSignatureString);
		}
		return result;
	}

	TypeElement getType(TypeMirror typeMirror);

	default TypeParameterElement getTypeParameter(String fullTypeParameterElementSignature) {
		TypeParameterElement result = findTypeParameter(fullTypeParameterElementSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeParameterElementSignature = " + fullTypeParameterElementSignature);
		}
		return result;
	}
}
