package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.langmodel.element.LangmodelInitializerElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;

import javax.lang.model.element.ExecutableElement;
import java.util.Objects;

@Value
public class LangmodelInitializerElementHandleImpl implements LangmodelInitializerElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	ExecutableElement executableElement;

	public LangmodelInitializerElementHandleImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			ExecutableElement executableElement) {
		this.elementHandleFactory = Objects.requireNonNull(elementHandleFactory);
		this.executableElement = ExecutableElementUtils.requireInitializer(executableElement);
	}

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(executableElement);
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		return elementHandleFactory.getExecutable(executableElement);
	}

	@NonNull
	@Override
	public TypeElementHandle getParent() {
		return elementHandleFactory.getType(executableElement.getEnclosingElement());
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}
}
