package tk.labyrinth.jaap.langmodel.factory;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.InitializerElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.VariableElementHandle;
import tk.labyrinth.jaap.model.factory.ElementHandleFactory;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

public interface LangmodelElementHandleFactory extends ElementHandleFactory {

	ElementHandle get(Element element);

	ConstructorElementHandle getConstructor(ExecutableElement executableElement);

	ConstructorElementHandle getConstructor(Element element);

	ExecutableElementHandle getExecutable(Element element);

	ExecutableElementHandle getExecutable(ExecutableElement executableElement);

	FieldElementHandle getField(Element element);

	FieldElementHandle getField(VariableElement variableElement);

	FormalParameterElementHandle getFormalParameter(Element element);

	FormalParameterElementHandle getFormalParameter(VariableElement variableElement);

	InitializerElementHandle getInitializer(Element element);

	InitializerElementHandle getInitializer(ExecutableElement executableElement);

	MethodElementHandle getMethod(Element element);

	MethodElementHandle getMethod(ExecutableElement executableElement);

	PackageElementHandle getPackage(Element element);

	PackageElementHandle getPackage(PackageElement packageElement);

	@Deprecated
	ProcessingContext getProcessingContext();

	ProcessingEnvironment getProcessingEnvironment();

	TypeElementHandle getType(Class<?> type);

	TypeElementHandle getType(String typeSignatureString);

	TypeElementHandle getType(Element element);

	TypeElementHandle getType(TypeElement typeElement);

	TypeElementHandle getType(TypeMirror typeMirror);

	TypeParameterElementHandle getTypeParameter(Element element);

	TypeParameterElementHandle getTypeParameter(TypeParameterElement typeParameterElement);

	VariableElementHandle getVariable(Element element);

	VariableElementHandle getVariable(VariableElement variableElement);
}
