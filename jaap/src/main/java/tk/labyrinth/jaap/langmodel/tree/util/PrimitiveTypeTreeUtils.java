package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.PrimitiveTypeTree;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;

public class PrimitiveTypeTreeUtils {

	public static TypeMirror getTypeMirror(
			CompilationUnitContext compilationUnitContext,
			PrimitiveTypeTree primitiveTypeTree) {
		return getTypeMirror(compilationUnitContext.getProcessingEnvironment(), primitiveTypeTree);
	}

	public static TypeMirror getTypeMirror(
			ProcessingEnvironment processingEnvironment,
			PrimitiveTypeTree primitiveTypeTree) {
		return processingEnvironment.getTypeUtils().getPrimitiveType(primitiveTypeTree.getPrimitiveTypeKind());
	}
}
