package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;

import javax.lang.model.element.VariableElement;

public interface LangmodelFormalParameterElementHandle extends FormalParameterElementHandle {

	VariableElement getVariableElement();
}
