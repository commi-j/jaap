package tk.labyrinth.jaap.langmodel.type.synthetic;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.TypeVisitor;
import java.lang.annotation.Annotation;
import java.util.List;

// FIXME: Decide if we need this one.
@Deprecated
@RequiredArgsConstructor
public class SyntheticTypeVariable implements TypeVariable {

	private final TypeDescription typeDescription;

	@Override
	public <R, P> R accept(TypeVisitor<R, P> v, P p) {
		throw new NotImplementedException();
	}

	@Override
	public Element asElement() {
		throw new NotImplementedException();
	}

	@Override
	public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
		throw new NotImplementedException();
	}

	@Override
	public List<? extends AnnotationMirror> getAnnotationMirrors() {
		throw new NotImplementedException();
	}

	@Override
	public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
		throw new NotImplementedException();
	}

	@Override
	public TypeKind getKind() {
		return TypeKind.TYPEVAR;
	}

	@Override
	public TypeMirror getLowerBound() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror getUpperBound() {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
