package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ParenthesizedTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.lang.model.type.TypeMirror;

public class ParenthesizedTreeUtils {

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			ParenthesizedTree parenthesizedTree) {
		return ExpressionTreeUtils.findTypeMirror(
				compilationUnitContext,
				parenthesizedTree.getExpression(),
				EntitySelectionContext.forVariableOrType());
	}

	public static Entity getEntity(ParenthesizedTree parenthesizedTree) {
		return Entity.ofExpressionTree(parenthesizedTree);
	}
}
