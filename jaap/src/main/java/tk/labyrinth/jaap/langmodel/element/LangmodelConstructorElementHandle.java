package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.ConstructorElementHandle;

import javax.lang.model.element.ExecutableElement;

public interface LangmodelConstructorElementHandle extends ConstructorElementHandle {

	ExecutableElement getExecutableElement();
}
