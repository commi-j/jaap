package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayTypeTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.lang.model.type.ArrayType;
import javax.lang.model.type.TypeMirror;

public class ArrayTypeTreeUtils {

	@Nullable
	public static ArrayType findArrayType(CompilationUnitContext compilationUnitContext, ArrayTypeTree arrayTypeTree) {
		TypeMirror componentTypeMirror = TreeUtils.findTypeMirror(
				compilationUnitContext,
				arrayTypeTree.getType(),
				EntitySelectionContext.forTypeName());
		return componentTypeMirror != null
				? compilationUnitContext.getProcessingEnvironment().getTypeUtils().getArrayType(componentTypeMirror)
				: null;
	}
}
