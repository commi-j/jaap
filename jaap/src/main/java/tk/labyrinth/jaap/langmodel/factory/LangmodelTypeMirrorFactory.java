package tk.labyrinth.jaap.langmodel.factory;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import java.lang.reflect.Type;

public interface LangmodelTypeMirrorFactory {

	@Nullable
	TypeMirror find(String fullTypeSignature);

	@Nullable
	TypeMirror find(TypeDescription typeDescription);

	@Nullable
	TypeMirror find(TypeSignature typeSignature);

	@Nullable
	TypeVariable findVariable(TypeDescription typeDescription);

	@Nullable
	WildcardType findWildcard(TypeDescription typeDescription);

	default TypeMirror get(TypeSignature typeSignature) {
		TypeMirror result = find(typeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeSignature = " + typeSignature);
		}
		return result;
	}

	default TypeMirror get(Class<?> type) {
		return get(GenericContext.empty(), type);
	}

	TypeMirror get(GenericContext genericContext, Type type);

	default TypeMirror get(String fullTypeSignature) {
		TypeMirror result = find(fullTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeSignature = " + fullTypeSignature);
		}
		return result;
	}

	TypeMirror get(TypeDescription typeDescription);

	DeclaredType getDeclared(GenericContext genericContext, Type type);

	default TypeVariable getVariable(TypeDescription typeDescription) {
		TypeVariable result = findVariable(typeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescription = " + typeDescription);
		}
		return result;
	}
}
