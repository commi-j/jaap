package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.PackageElementHandle;

public interface LangmodelPackageElementHandle extends PackageElementHandle {
	// empty
}
