package tk.labyrinth.jaap.langmodel.element.secondary;

import lombok.Value;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.secondary.ParameterizableElementHandle;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.ParameterizableUtils;

import javax.lang.model.element.Parameterizable;
import java.util.stream.Stream;

@Value
public class LangmodelParameterizedElementHandleImpl implements ParameterizableElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	Parameterizable parameterizable;

	public LangmodelParameterizedElementHandleImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			Parameterizable parameterizable) {
		this.elementHandleFactory = elementHandleFactory;
		this.parameterizable = ParameterizableUtils.requireProperParameterizable(parameterizable);
	}

	@Override
	public ConstructorElementHandle asConstructorElement() {
		return elementHandleFactory.getConstructor(parameterizable);
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		return elementHandleFactory.getExecutable(parameterizable);
	}

	@Override
	public MethodElementHandle asMethodElement() {
		return elementHandleFactory.getMethod(parameterizable);
	}

	@Override
	public TypeElementHandle asTypeElement() {
		return elementHandleFactory.getType(parameterizable);
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		return parameterizable.getTypeParameters().stream().map(elementHandleFactory::getTypeParameter);
	}

	@Override
	public boolean isConstructorElement() {
		return ElementUtils.isConstructor(parameterizable);
	}

	@Override
	public boolean isExecutableElement() {
		return ElementUtils.isExecutable(parameterizable);
	}

	@Override
	public boolean isMethodElement() {
		return ElementUtils.isMethod(parameterizable);
	}

	@Override
	public boolean isTypeElement() {
		return ElementUtils.isType(parameterizable);
	}
}
