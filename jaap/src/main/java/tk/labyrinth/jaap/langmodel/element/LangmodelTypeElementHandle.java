package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.TypeElementHandle;

import javax.lang.model.element.TypeElement;

public interface LangmodelTypeElementHandle extends TypeElementHandle {

	TypeElement getTypeElement();
}
