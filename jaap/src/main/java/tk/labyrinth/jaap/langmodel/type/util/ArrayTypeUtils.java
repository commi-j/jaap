package tk.labyrinth.jaap.langmodel.type.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.ArrayType;

public class ArrayTypeUtils {

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, ArrayType arrayType) {
		return TypeDescription.builder()
				.arrayDepth(1)
				.fullName(TypeMirrorUtils.toDescription(
						processingEnvironment,
						arrayType.getComponentType()).toString())
				.build();
	}
}
