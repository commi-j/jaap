package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.ExecutableElementHandle;

import javax.lang.model.element.ExecutableElement;

public interface LangmodelExecutableElementHandle extends ExecutableElementHandle {

	ExecutableElement getExecutableElement();
}
