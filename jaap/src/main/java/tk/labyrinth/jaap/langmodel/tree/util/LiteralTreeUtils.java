package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.LiteralTree;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

public class LiteralTreeUtils {

	public static Entity getEntity(LiteralTree literalTree) {
		return Entity.ofExpressionTree(literalTree);
	}

	public static TypeMirror getTypeMirror(CompilationUnitContext compilationUnitContext, LiteralTree literalTree) {
		return getTypeMirror(compilationUnitContext.getProcessingEnvironment(), literalTree);
	}

	public static TypeMirror getTypeMirror(ProcessingEnvironment processingEnvironment, LiteralTree literalTree) {
		TypeMirror result;
		{
			Object value = literalTree.getValue();
			if (value == null) {
				result = processingEnvironment.getTypeUtils().getNullType();
			} else if (value instanceof Boolean) {
				result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
			} else if (value instanceof Integer) {
				result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT);
			} else if (value instanceof Long) {
				result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG);
			} else if (value instanceof String) {
				result = processingEnvironment.getElementUtils().getTypeElement(String.class.getName()).asType();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(literalTree));
			}
		}
		return result;
	}
}
