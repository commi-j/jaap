package tk.labyrinth.jaap.langmodel.factory;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.model.factory.TypeHandleFactory;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

public interface LangmodelTypeHandleFactory extends TypeHandleFactory {

	TypeHandle get(GenericContext genericContext, TypeElement typeElement);

	TypeHandle get(GenericContext genericContext, TypeMirror typeMirror);

	ArrayTypeHandle getArray(GenericContext genericContext, TypeMirror typeMirror);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, DeclaredType declaredType);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeElement typeElement);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeMirror typeMirror);

	GenericTypeHandle getGeneric(GenericContext genericContext, DeclaredType declaredType);

	GenericTypeHandle getGeneric(GenericContext genericContext, TypeMirror typeMirror);

	ParameterizedTypeHandle getParameterized(GenericContext genericContext, DeclaredType declaredType);

	ParameterizedTypeHandle getParameterized(GenericContext genericContext, TypeMirror typeMirror);

	PlainTypeHandle getPlain(GenericContext genericContext, DeclaredType declaredType);

	PrimitiveTypeHandle getPrimitive(PrimitiveType primitiveType);

	PrimitiveTypeHandle getPrimitive(TypeMirror typeMirror);

	RawTypeHandle getRaw(DeclaredType declaredType);

	RawTypeHandle getRaw(TypeMirror typeMirror);

	VariableTypeHandle getVariable(GenericContext genericContext, TypeMirror typeMirror);

	VariableTypeHandle getVariable(GenericContext genericContext, TypeVariable typeVariable);
}
