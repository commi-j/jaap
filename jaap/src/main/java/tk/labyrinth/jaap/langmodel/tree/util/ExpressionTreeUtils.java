package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.TypeCastTree;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

public class ExpressionTreeUtils {

	@Nullable
	public static Entity findEntity(
			CompilationUnitContext compilationUnitContext,
			ExpressionTree expressionTree,
			EntitySelectionContext entitySelectionContext) {
		Entity result;
		{
			if (expressionTree instanceof IdentifierTree identifierTree) {
				result = IdentifierTreeUtils.findEntity(compilationUnitContext, identifierTree, entitySelectionContext);
			} else if (expressionTree instanceof LiteralTree literalTree) {
				result = LiteralTreeUtils.getEntity(literalTree);
			} else if (expressionTree instanceof MemberSelectTree memberSelectTree) {
				result = MemberSelectTreeUtils.findEntity(
						compilationUnitContext,
						memberSelectTree,
						entitySelectionContext);
			} else if (expressionTree instanceof MethodInvocationTree methodInvocationTree) {
				result = MethodInvocationTreeUtils.getEntity(methodInvocationTree);
			} else if (expressionTree instanceof NewClassTree newClassTree) {
				result = NewClassTreeUtils.getEntity(newClassTree);
			} else if (expressionTree instanceof ParameterizedTypeTree) {
				throw new NotImplementedException(ExceptionUtils.render(expressionTree));
			} else if (expressionTree instanceof ParenthesizedTree parenthesizedTree) {
				result = ParenthesizedTreeUtils.getEntity(parenthesizedTree);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(expressionTree));
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			ExpressionTree expressionTree,
			EntitySelectionContext entitySelectionContext) {
		TypeMirror result;
		{
			if (expressionTree instanceof ArrayAccessTree arrayAccessTree) {
				result = ArrayAccessTreeUtils.findTypeMirror(compilationUnitContext, arrayAccessTree);
			} else if (expressionTree instanceof IdentifierTree identifierTree) {
				result = IdentifierTreeUtils.findTypeMirror(
						compilationUnitContext,
						identifierTree,
						entitySelectionContext);
			} else if (expressionTree instanceof LambdaExpressionTree lambdaExpressionTree) {
				result = LambdaExpressionTreeUtils.findTypeMirror(compilationUnitContext, lambdaExpressionTree);
			} else if (expressionTree instanceof LiteralTree literalTree) {
				result = LiteralTreeUtils.getTypeMirror(compilationUnitContext, literalTree);
			} else if (expressionTree instanceof MemberSelectTree memberSelectTree) {
				result = MemberSelectTreeUtils.findTypeMirror(
						compilationUnitContext,
						memberSelectTree,
						entitySelectionContext);
			} else if (expressionTree instanceof MethodInvocationTree methodInvocationTree) {
				result = MethodInvocationTreeUtils.findReturnType(compilationUnitContext, methodInvocationTree);
			} else if (expressionTree instanceof NewClassTree newClassTree) {
				result = NewClassTreeUtils.findTypeMirror(compilationUnitContext, newClassTree);
			} else if (expressionTree instanceof ParameterizedTypeTree parameterizedTypeTree) {
				result = ParameterizedTypeTreeUtils.findTypeMirror(compilationUnitContext, parameterizedTypeTree);
			} else if (expressionTree instanceof ParenthesizedTree parenthesizedTree) {
				result = ParenthesizedTreeUtils.findTypeMirror(compilationUnitContext, parenthesizedTree);
			} else if (expressionTree instanceof PrimitiveTypeTree primitiveTypeTree) {
				result = PrimitiveTypeTreeUtils.getTypeMirror(compilationUnitContext, primitiveTypeTree);
			} else if (expressionTree instanceof TypeCastTree typeCastTree) {
				result = TreeUtils.findTypeMirror(
						compilationUnitContext,
						typeCastTree.getType(),
						entitySelectionContext);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
