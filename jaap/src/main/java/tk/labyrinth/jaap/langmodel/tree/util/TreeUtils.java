package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.PackageTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class TreeUtils {

	private static final boolean detectNulls = true;

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			Tree tree,
			EntitySelectionContext entitySelectionContext) {
		TypeMirror result;
		{
			if (tree instanceof ExpressionTree expressionTree) {
				result = ExpressionTreeUtils.findTypeMirror(
						compilationUnitContext,
						expressionTree,
						entitySelectionContext);
			} else if (tree instanceof VariableTree variableTree) {
				result = VariableTreeUtils.findTypeMirror(compilationUnitContext, variableTree);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	// TODO: Ensure order of children for all cases.
	public static Stream<? extends Tree> streamChildren(Tree tree) {
		Stream<? extends Tree> children;
		{
			if (tree instanceof AnnotationTree annotationTree) {
				children = Stream.concat(
						Stream.of(annotationTree.getAnnotationType()),
						annotationTree.getArguments().stream());
			} else if (tree instanceof ArrayAccessTree arrayAccessTree) {
				children = Stream.of(arrayAccessTree.getExpression());
			} else if (tree instanceof ArrayTypeTree arrayTypeTree) {
				children = Stream.of(arrayTypeTree.getType());
			} else if (tree instanceof AssignmentTree assignmentTree) {
				children = Stream.of(assignmentTree.getVariable(), assignmentTree.getExpression());
			} else if (tree instanceof BinaryTree binaryTree) {
				children = Stream.of(binaryTree.getLeftOperand(), binaryTree.getRightOperand());
			} else if (tree instanceof BlockTree blockTree) {
				children = blockTree.getStatements().stream();
			} else if (tree instanceof CatchTree catchTree) {
				children = Stream.of(catchTree.getParameter(), catchTree.getBlock());
			} else if (tree instanceof ClassTree classTree) {
				children = classTree.getMembers().stream();
			} else if (tree instanceof CompilationUnitTree compilationUnitTree) {
				children = StreamUtils.concat(
						Optional.ofNullable(compilationUnitTree.getModule()).stream(),
						Optional.ofNullable(compilationUnitTree.getPackage()).stream(),
						compilationUnitTree.getImports().stream(),
						compilationUnitTree.getTypeDecls().stream());
			} else if (tree instanceof ConditionalExpressionTree conditionalExpressionTree) {
				children = Stream.of(
						conditionalExpressionTree.getCondition(),
						conditionalExpressionTree.getTrueExpression(),
						conditionalExpressionTree.getFalseExpression());
			} else if (tree instanceof ExpressionStatementTree expressionStatementTree) {
				children = Stream.of(expressionStatementTree.getExpression());
			} else if (tree instanceof IdentifierTree) {
				children = Stream.empty();
			} else if (tree instanceof IfTree ifTree) {
				children = Stream.concat(
						Stream.of(ifTree.getCondition(), ifTree.getThenStatement()),
						Optional.ofNullable(ifTree.getElseStatement()).stream());
			} else if (tree instanceof ImportTree importTree) {
				children = Stream.of(importTree.getQualifiedIdentifier());
			} else if (tree instanceof InstanceOfTree instanceOfTree) {
				children = Stream.concat(
						Stream.of(instanceOfTree.getExpression(), instanceOfTree.getType()),
						Optional.ofNullable(instanceOfTree.getPattern()).stream());
			} else if (tree instanceof LambdaExpressionTree lambdaExpressionTree) {
				children = Stream.concat(
						lambdaExpressionTree.getParameters().stream(),
						Stream.of(lambdaExpressionTree.getBody()));
			} else if (tree instanceof LiteralTree) {
				children = Stream.empty();
			} else if (tree instanceof MemberReferenceTree memberReferenceTree) {
				children = Stream.concat(
						Optional.ofNullable(memberReferenceTree.getTypeArguments())
								.map(List::stream)
								.orElse(Stream.empty()),
						Stream.of(memberReferenceTree.getQualifierExpression()));
			} else if (tree instanceof MemberSelectTree memberSelectTree) {
				children = Stream.of(memberSelectTree.getExpression());
			} else if (tree instanceof MethodInvocationTree methodInvocationTree) {
				children = StreamUtils.concat(
						methodInvocationTree.getTypeArguments().stream(),
						Stream.of(methodInvocationTree.getMethodSelect()),
						methodInvocationTree.getArguments().stream());
			} else if (tree instanceof MethodTree methodTree) {
				children = StreamUtils.concat(
						Stream.of(methodTree.getModifiers()),
						methodTree.getTypeParameters().stream(),
						Optional.ofNullable(methodTree.getReturnType()).stream(),
						methodTree.getParameters().stream(),
						methodTree.getThrows().stream(),
						Optional.ofNullable(methodTree.getBody()).stream(),
						Optional.ofNullable(methodTree.getDefaultValue()).stream(),
						//
						// TODO: Unclear what this is.
						Optional.ofNullable(methodTree.getReceiverParameter()).stream());
			} else if (tree instanceof ModifiersTree modifiersTree) {
				children = modifiersTree.getAnnotations().stream();
			} else if (tree instanceof NewArrayTree newArrayTree) {
				children = StreamUtils.concat(
						newArrayTree.getAnnotations().stream(),
						Optional.ofNullable(newArrayTree.getType()).stream(),
						newArrayTree.getDimensions().stream(),
						newArrayTree.getInitializers().stream());
			} else if (tree instanceof NewClassTree newClassTree) {
				children = StreamUtils.concat(
						Optional.ofNullable(newClassTree.getEnclosingExpression()).stream(),
						newClassTree.getTypeArguments().stream(),
						Stream.of(newClassTree.getIdentifier()),
						newClassTree.getArguments().stream(),
						Optional.ofNullable(newClassTree.getClassBody()).stream());
			} else if (tree instanceof PackageTree packageTree) {
				children = Stream.concat( // FIXME: StreamUtils#append.
						packageTree.getAnnotations().stream(),
						Stream.of(packageTree.getPackageName()));
			} else if (tree instanceof ParameterizedTypeTree parameterizedTypeTree) {
				children = Stream.concat(
						Stream.of(parameterizedTypeTree.getType()),
						parameterizedTypeTree.getTypeArguments().stream());
			} else if (tree instanceof ParenthesizedTree parenthesizedTree) {
				children = Stream.of(parenthesizedTree.getExpression());
			} else if (tree instanceof PrimitiveTypeTree) {
				children = Stream.empty();
			} else if (tree instanceof ReturnTree returnTree) {
				children = Stream.of(returnTree.getExpression());
			} else if (tree instanceof ThrowTree throwTree) {
				children = Stream.of(throwTree.getExpression());
			} else if (tree instanceof TryTree tryTree) {
				children = StreamUtils.concat(
						tryTree.getResources().stream(),
						Stream.of(tryTree.getBlock()),
						tryTree.getCatches().stream(),
						Optional.ofNullable(tryTree.getFinallyBlock()).stream());
			} else if (tree instanceof TypeCastTree typeCastTree) {
				children = Stream.of(typeCastTree.getType(), typeCastTree.getExpression());
			} else if (tree instanceof TypeParameterTree typeParameterTree) {
				children = Stream.concat(
						typeParameterTree.getAnnotations().stream(),
						typeParameterTree.getBounds().stream());
			} else if (tree instanceof UnaryTree unaryTree) {
				children = Stream.of(unaryTree.getExpression());
			} else if (tree instanceof VariableTree variableTree) {
				children = StreamUtils.concat(
						Stream.of(variableTree.getModifiers()),
						Optional.ofNullable(variableTree.getType()).stream(),
						Optional.ofNullable(variableTree.getNameExpression()).stream(),
						Optional.ofNullable(variableTree.getInitializer()).stream());
			} else if (tree instanceof WhileLoopTree whileLoopTree) {
				children = Stream.of(whileLoopTree.getCondition(), whileLoopTree.getStatement());
			} else if (tree instanceof WildcardTree wildcardTree) {
				children = Optional.ofNullable(wildcardTree.getBound()).stream();
			} else {
				throw new NotImplementedException();
			}
		}
		Stream<? extends Tree> result;
		{
			if (detectNulls) {
				List<Tree> trees = children.collect(Collectors.toList());
				if (trees.contains(null)) {
					throw new IllegalArgumentException();
				}
				result = trees.stream();
			} else {
				result = children;
			}
		}
		return result;
	}

	public static Stream<? extends Tree> streamTree(Tree tree) {
		return StreamUtils.concat(tree, streamChildren(tree).flatMap(TreeUtils::streamTree));
	}

	public static <T extends Tree> Stream<T> streamTree(Tree tree, Class<T> filterClass) {
		return streamTree(tree, filterClass::isInstance).map(filterClass::cast);
	}

	public static Stream<Tree> streamTree(Tree tree, Predicate<Tree> predicate) {
		return Stream.concat(Stream.of(tree), streamChildren(tree).flatMap(child -> streamTree(child, predicate)))
				.filter(predicate);
	}
}
