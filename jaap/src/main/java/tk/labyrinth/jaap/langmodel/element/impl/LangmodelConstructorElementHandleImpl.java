package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelConstructorElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

import javax.lang.model.element.ExecutableElement;
import java.util.Objects;
import java.util.stream.Stream;

@Value
public class LangmodelConstructorElementHandleImpl implements LangmodelConstructorElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	ExecutableElement executableElement;

	LangmodelTypeHandleFactory typeHandleFactory;

	public LangmodelConstructorElementHandleImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			ExecutableElement executableElement,
			LangmodelTypeHandleFactory typeHandleFactory) {
		this.elementHandleFactory = Objects.requireNonNull(elementHandleFactory);
		this.executableElement = ExecutableElementUtils.requireConstructor(executableElement);
		this.typeHandleFactory = Objects.requireNonNull(typeHandleFactory);
	}

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(executableElement);
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		return elementHandleFactory.getExecutable(executableElement);
	}

	@Override
	public int getFormalParameterCount() {
		return executableElement.getParameters().size();
	}

	@Override
	public Stream<FormalParameterElementHandle> getFormalParameterStream() {
		return executableElement.getParameters().stream().map(elementHandleFactory::getFormalParameter);
	}

	@NonNull
	@Override
	public TypeElementHandle getParent() {
		return elementHandleFactory.getType(executableElement.getEnclosingElement());
	}

	@Override
	public TypeHandle getReturnType() {
		return typeHandleFactory.get(GenericContext.empty(), executableElement.getReturnType());
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		return executableElement.getTypeParameters().stream().map(elementHandleFactory::getTypeParameter);
	}
}
