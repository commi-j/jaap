package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.IdentifierTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.entity.EntityUtils;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;

import javax.lang.model.type.TypeMirror;
import java.util.stream.Stream;

public class IdentifierTreeUtils {

	@Nullable
	public static Entity findEntity(
			CompilationUnitContext compilationUnitContext,
			IdentifierTree identifierTree,
			EntitySelectionContext entitySelectionContext) {
		return EntityUtils.findEntity(
				compilationUnitContext,
				identifierTree,
				EntitySelector.build(entitySelectionContext, identifierTree));
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			IdentifierTree identifierTree,
			EntitySelectionContext entitySelectionContext) {
		TypeMirror result;
		{
			Entity entity = findEntity(compilationUnitContext, identifierTree, entitySelectionContext);
			//
			result = entity != null
					? EntityUtils.findTypeMirror(compilationUnitContext, entity, entitySelectionContext)
					: null;
		}
		return result;
	}

	// TODO: Cover with tests.
	public static String getName(IdentifierTree identifierTree) {
		return identifierTree.getName().toString();
	}

	// TODO: Cover with tests.
	public static Stream<String> getNameChain(IdentifierTree identifierTree) {
		return Stream.of(getName(identifierTree));
	}
}
