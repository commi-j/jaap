package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.InitializerElementHandle;

import javax.lang.model.element.ExecutableElement;

public interface LangmodelInitializerElementHandle extends InitializerElementHandle {

	ExecutableElement getExecutableElement();
}
