package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.NewClassTree;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

public class NewClassTreeUtils {

	@Nullable
	public static TypeMirror findTypeMirror(CompilationUnitContext compilationUnitContext, NewClassTree newClassTree) {
		TypeMirror result;
		if (newClassTree.getClassBody() != null) {
			// TODO: Anonymous classes.
			throw new NotImplementedException(ExceptionUtils.render(newClassTree));
		} else {
			result = ExpressionTreeUtils.findTypeMirror(
					compilationUnitContext,
					newClassTree.getIdentifier(),
					EntitySelectionContext.forType());
		}
		return result;
	}

	public static Entity getEntity(NewClassTree newClassTree) {
		return Entity.ofExpressionTree(newClassTree);
	}
}
