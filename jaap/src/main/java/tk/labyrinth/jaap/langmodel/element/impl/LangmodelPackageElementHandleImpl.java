package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import tk.labyrinth.jaap.handle.element.util.PackageElementUtils;
import tk.labyrinth.jaap.langmodel.element.LangmodelPackageElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;

@Value
public class LangmodelPackageElementHandleImpl implements LangmodelPackageElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	PackageElement packageElement;

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(packageElement);
	}

	@Override
	public String getQualifiedName() {
		return packageElement.getQualifiedName().toString();
	}

	@Override
	public String getSimpleName() {
		return packageElement.getSimpleName().toString();
	}

	@Override
	public boolean isSynthetic() {
		return false;
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		Element member = PackageElementUtils.findMember(
				elementHandleFactory.getProcessingEnvironment(),
				getQualifiedName(),
				selector);
		return member != null ? elementHandleFactory.get(member) : null;
	}

	@Override
	public String toString() {
		return getQualifiedName();
	}
}
