package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelFormalParameterElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.lang.model.element.VariableElement;

@Value
public class LangmodelFormalParameterElementHandleImpl implements LangmodelFormalParameterElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	LangmodelTypeHandleFactory typeHandleFactory;

	VariableElement variableElement;

	@Override
	public int getIndex() {
		return ElementUtils.requireExecutable(variableElement.getEnclosingElement()).getParameters()
				.indexOf(variableElement);
	}

	@Override
	public String getName() {
		return variableElement.getSimpleName().toString();
	}

	@Override
	public @NonNull ExecutableElementHandle getParent() {
		return elementHandleFactory.getExecutable(variableElement.getEnclosingElement());
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public TypeHandle getType() {
		return typeHandleFactory.get(GenericContext.empty(), variableElement.asType());
	}

	@Override
	public String toString() {
		return getSignatureString();
	}
}
