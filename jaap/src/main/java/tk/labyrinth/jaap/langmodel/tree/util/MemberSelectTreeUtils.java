package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.entity.Entity;
import tk.labyrinth.jaap.langmodel.entity.EntityUtils;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Encountered in:<br>
 * - Import Declaration;<br>
 * - MethodInvocation Arguments;<br>
 * - MethodInvocation MethodSelections;<br>
 * - Package Declaration;<br>
 * - Variable Initializers;<br>
 * - Variable Types;<br>
 */
// TODO: Cover with tests.
public class MemberSelectTreeUtils {

	/**
	 * Splits provided {@link MemberSelectTree} into self-sustained {@link ExpressionTree} and sequence of simple names.
	 *
	 * @param memberSelectTree non-null
	 *
	 * @return non-null with nullable left and non-empty right
	 */
	public static Pair<@Nullable ExpressionTree, List<String>> deconstruct(MemberSelectTree memberSelectTree) {
		ExpressionTree resultExpressionTree;
		List<String> resultSimpleNames = new ArrayList<>();
		{
			ExpressionTree reducedTree = ObjectUtils.<ExpressionTree>reduce(
					memberSelectTree,
					MemberSelectTree.class::isInstance,
					expressionTree -> {
						MemberSelectTree tree = (MemberSelectTree) expressionTree;
						resultSimpleNames.add(0, tree.getIdentifier().toString());
						return tree.getExpression();
					});
			//
			// MemberSelect is depleted at this point.
			if (reducedTree instanceof IdentifierTree) {
				// Identifier.
				//
				resultSimpleNames.add(0, ((IdentifierTree) reducedTree).getName().toString());
				resultExpressionTree = null;
			} else {
				// Any except Identifier or MemberSelect.
				//
				resultExpressionTree = reducedTree;
			}
		}
		return Pair.of(resultExpressionTree, resultSimpleNames);
	}

	@Nullable
	public static Element findElement(
			CompilationUnitContext compilationUnitContext,
			MemberSelectTree memberSelectTree,
			EntitySelectionContext entitySelectionContext) {
		Entity entity = findEntity(compilationUnitContext, memberSelectTree, entitySelectionContext);
		//
		return entity != null && entity.isElement() ? entity.asElement() : null;
	}

	@Nullable
	public static Entity findEntity(
			CompilationUnitContext compilationUnitContext,
			MemberSelectTree memberSelectTree,
			EntitySelectionContext entitySelectionContext) {
		Entity result;
		{
			Pair<ExpressionTree, List<String>> targetAndSimpleNames = deconstruct(memberSelectTree);
			ExpressionTree targetTree = targetAndSimpleNames.getLeft();
			List<String> simpleNames = targetAndSimpleNames.getRight();
			//
			if (targetTree != null) {
				TypeMirror targetTypeMirror = ExpressionTreeUtils.findTypeMirror(
						compilationUnitContext,
						targetTree,
						EntitySelectionContext.forVariableOrType()); // TODO: Check if this context is right.
				if (targetTypeMirror != null) {
					TypeElement targetTypeElement = TypeElementUtils.find(
							compilationUnitContext.getProcessingEnvironment(),
							targetTypeMirror);
					if (targetTypeElement != null) {
						Element element = ElementUtils.navigate(
								compilationUnitContext.getProcessingEnvironment(),
								targetTypeElement,
								EntitySelectorChain.build(entitySelectionContext, simpleNames));
						//
						result = element != null ? Entity.ofElement(element) : null;
					} else {
						result = null;
					}
				} else {
					result = null;
				}
			} else {
				result = EntityUtils.findEntity(
						compilationUnitContext,
						memberSelectTree,
						EntitySelectorChain.build(entitySelectionContext, simpleNames));
			}
			return result;
		}
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			MemberSelectTree memberSelectTree,
			EntitySelectionContext entitySelectionContext) {
		Entity entity = findEntity(compilationUnitContext, memberSelectTree, entitySelectionContext);
		//
		return entity != null
				? EntityUtils.findTypeMirror(compilationUnitContext, entity, entitySelectionContext)
				: null;
	}

	// TODO: Cover with tests.
	public static Stream<String> getNameChain(MemberSelectTree memberSelectTree) {
		Stream<String> previousSimpleNames;
		{
			ExpressionTree expression = memberSelectTree.getExpression();
			if (expression instanceof IdentifierTree identifierTree) {
				previousSimpleNames = IdentifierTreeUtils.getNameChain(identifierTree);
			} else if (expression instanceof MemberSelectTree memberSelectTreeExpression) {
				previousSimpleNames = getNameChain(memberSelectTreeExpression);
			} else if (expression instanceof MethodInvocationTree) {
				previousSimpleNames = Stream.empty();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(memberSelectTree));
			}
		}
		return StreamUtils.concat(previousSimpleNames, memberSelectTree.getIdentifier().toString());
	}
}
