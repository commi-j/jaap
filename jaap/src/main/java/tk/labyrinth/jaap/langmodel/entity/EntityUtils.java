package tk.labyrinth.jaap.langmodel.entity;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.tree.util.ClassTreeUtils;
import tk.labyrinth.jaap.langmodel.tree.util.MethodTreeUtils;
import tk.labyrinth.jaap.langmodel.tree.util.TreeUtils;
import tk.labyrinth.jaap.langmodel.tree.util.VariableTreeUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class EntityUtils {

	@Nullable
	private static Entity doFindEnclosedType(ClassTree classTree, String simpleName) {
		Entity result;
		{
			ClassTree classTreeMember = classTree.getMembers().stream()
					.filter(ClassTree.class::isInstance)
					.map(ClassTree.class::cast)
					.filter(member -> member.getSimpleName().contentEquals(simpleName))
					.collect(CollectorUtils.findOnly(true));
			if (classTreeMember != null) {
				result = Entity.ofElement(ClassTreeUtils.getTypeElement(classTreeMember));
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	private static Entity doFindField(ClassTree classTree, String simpleName) {
		Entity result;
		{
			VariableTree variableTreeMember = classTree.getMembers().stream()
					.filter(VariableTree.class::isInstance)
					.map(VariableTree.class::cast)
					.filter(member -> member.getName().contentEquals(simpleName))
					.collect(CollectorUtils.findOnly(true));
			if (variableTreeMember != null) {
				result = Entity.ofVariableTree(variableTreeMember);
			} else {
				// TODO: inherited & enclosed fields
				VariableElement variableElement = TypeElementUtils.findDeclaredField(
						ClassTreeUtils.getTypeElement(classTree),
						simpleName);
				result = variableElement != null ? Entity.ofElement(variableElement) : null;
			}
		}
		return result;
	}

	@Nullable
	private static Entity doFindMethod(
			ProcessingEnvironment processingEnvironment,
			ClassTree classTree,
			EntitySelector entitySelector) {
		Entity result;
		{
			MethodSelector methodSelector = entitySelector.toMethodSelector();
			//
			List<ExecutableElement> foundMethods = ClassTreeUtils.getHierarchy(processingEnvironment, classTree)
					.map(ClassTree::getMembers)
					.flatMap(List::stream)
					.filter(MethodTree.class::isInstance)
					.map(MethodTree.class::cast)
					.map(MethodTreeUtils::getExecutableElement)
					.filter(executableElement -> MethodSelector.matches(processingEnvironment, methodSelector, executableElement))
					.toList();
			//
			if (!foundMethods.isEmpty()) {
				if (foundMethods.size() > 1) {
					result = Entity.ofElement(selectMostSpecificMethod(foundMethods, methodSelector));
				} else {
					result = Entity.ofElement(foundMethods.get(0));
				}
			} else {
				// TODO: inherited & enclosed methods
				//VariableElement variableElement = TypeElementUtils.findDeclaredField(typeElement, simpleName);
				//result = variableElement != null ? Entity.ofVariableElement(variableElement) : null;
				result = null;
			}
		}
		return result;
	}

	@Nullable
	private static TypeElement doFindTypeElement(
			ProcessingEnvironment processingEnvironment,
			CompilationUnitTree compilationUnitTree,
			String simpleName) {
		TypeElement result;
		{
			TypeElement foundTypeElement;
			{
				Name simpleNameAsName = processingEnvironment.getElementUtils().getName(simpleName);
				TypeElement lookupTypeElement = SymbolTableUtils.lookup(compilationUnitTree, simpleNameAsName);
				if (lookupTypeElement != null) {
					// FIXME: "java.lang" must goes after package lookup.
					foundTypeElement = lookupTypeElement;
				} else {
					String packageName = compilationUnitTree.getPackageName().toString();
					//
					foundTypeElement = TypeElementUtils.find(
							processingEnvironment,
							"%s$%s".formatted(packageName, simpleName));
				}
			}
			result = foundTypeElement;
		}
		return result;
	}

	@Nullable
	private static Entity lookupInBlock(BlockTree blockTree, EntitySelector selector) {
		Entity result;
		{
			if (selector.canBeVariable()) {
				VariableTree variableTree = blockTree.getStatements().stream()
						.filter(VariableTree.class::isInstance)
						.map(VariableTree.class::cast)
						.filter(internalVariableTree -> internalVariableTree.getName().contentEquals(
								selector.getSimpleName()))
						.collect(CollectorUtils.findOnly(true));
				result = variableTree != null ? Entity.ofVariableTree(variableTree) : null;
			} else {
				// TODO: Local classes?
				result = null;
			}
		}
		return result;
	}

	@Nullable
	private static Entity lookupInClass(
			ProcessingEnvironment processingEnvironment,
			ClassTree classTree,
			EntitySelector selector) {
		Entity result;
		{
			if (selector.getSimpleName().equals("this")) {
				// Special case of 'this'.
				// TODO: Check if the scope supports 'this' context.
				result = Entity.ofThisEntity(ThisEntity.of(ClassTreeUtils.getTypeElement(classTree)));
			} else if (selector.canBeMethod()) {
				// When we are looking for method we are not looking for anything else.
				result = doFindMethod(processingEnvironment, classTree, selector);
			} else {
				// Variable or enclosed type or this type.
				// TODO: Probably all types that does not require import, i.e. from the same package.
				Entity variableTreeEntity;
				//
				if (selector.canBeVariable()) {
					variableTreeEntity = doFindField(classTree, selector.getSimpleName());
				} else {
					variableTreeEntity = null;
				}
				//
				if (variableTreeEntity != null) {
					result = variableTreeEntity;
				} else if (selector.canBeType()) {
					// TODO: Enclosed types
					result = doFindEnclosedType(classTree, selector.getSimpleName());
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	private static Entity lookupInCompilationUnit(
			ProcessingEnvironment processingEnvironment,
			CompilationUnitTree compilationUnitTree,
			EntitySelector selector) {
		Entity result;
		{
			Element element;
			{
				TypeElement typeElement;
				if (selector.canBeType()) {
					typeElement = doFindTypeElement(
							processingEnvironment,
							compilationUnitTree,
							selector.getSimpleName());
				} else {
					typeElement = null;
				}
				if (typeElement != null) {
					element = typeElement;
				} else if (selector.canBePackage()) {
					// FIXME: Will fail here as Entity does not support packages.
					element = processingEnvironment.getElementUtils().getPackageElement(selector.getSimpleName());
				} else {
					element = null;
				}
			}
			result = element != null ? Entity.ofElement(element) : null;
		}
		return result;
	}

	@Nullable
	private static Entity lookupInLambdaExpression(
			LambdaExpressionTree lambdaExpressionTree,
			EntitySelector entitySelector) {
		Entity result;
		{
			if (entitySelector.canBeVariable()) {
				VariableTree variableTree = lambdaExpressionTree.getParameters().stream()
						.filter(parameter -> parameter.getName().contentEquals(entitySelector.getSimpleName()))
						.findFirst()
						.orElse(null);
				//
				result = variableTree != null ? Entity.ofVariableTree(variableTree) : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	private static Entity lookupInMethod(MethodTree methodTree, EntitySelector selector) {
		Entity result;
		{
			if (selector.canBeVariable()) {
				VariableTree variableTree = methodTree.getParameters().stream()
						.filter(parameter -> parameter.getName().contentEquals(selector.getSimpleName()))
						.collect(CollectorUtils.findOnly(true));
				result = variableTree != null ? Entity.ofVariableTree(variableTree) : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static Element findElement(
			CompilationUnitContext compilationUnitContext,
			Tree scope,
			EntitySelectorChain selectorChain) {
		Element result;
		{
			Entity entity = findEntity(compilationUnitContext, scope, selectorChain);
			//
			result = entity != null && entity.isElement()
					? entity.asElement()
					: null;
		}
		return result;
	}

	@Nullable
	public static Entity findEntity(
			CompilationUnitContext compilationUnitContext,
			Tree scope,
			EntitySelectorChain selectorChain) {
		Entity result;
		{
			Pair<EntitySelector, EntitySelectorChain> headAndTail = selectorChain.split();
			EntitySelector head = headAndTail.getLeft();
			EntitySelectorChain tail = headAndTail.getRight();
			//
			Entity headEntity = findEntity(compilationUnitContext, scope, head);
			if (headEntity != null) {
				if (tail != null) {
					Element headElement;
					if (headEntity.isElement()) {
						headElement = headEntity.asElement();
					} else if (headEntity.isVariableTree()) {
						headElement = VariableTreeUtils.findElement(headEntity.asVariableTree());
					} else {
						throw new UnreachableStateException(ExceptionUtils.render(selectorChain));
					}
					if (headElement != null) {
						Element tailElement = ElementUtils.navigate(
								compilationUnitContext.getProcessingEnvironment(),
								headElement,
								tail);
						result = tailElement != null ? Entity.ofElement(tailElement) : null;
					} else {
						result = null;
					}
				} else {
					result = headEntity;
				}
			} else {
				if (tail != null) {
					Element element = compilationUnitContext.findElement(scope);
					result = element != null ? Entity.ofElement(element) : null;
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	public static Entity findEntity(
			CompilationUnitContext compilationUnitContext,
			Tree scope,
			EntitySelector selector) {
		return compilationUnitContext.getTreeStream(scope)
				.map(currentTree -> {
					Entity entity;
					{
						if (currentTree instanceof BlockTree blockTree) {
							entity = lookupInBlock(blockTree, selector);
						} else if (currentTree instanceof ClassTree classTree) {
							entity = lookupInClass(
									compilationUnitContext.getProcessingEnvironment(),
									classTree,
									selector);
						} else if (currentTree instanceof CompilationUnitTree compilationUnitTree) {
							entity = lookupInCompilationUnit(
									compilationUnitContext.getProcessingEnvironment(),
									compilationUnitTree,
									selector);
						} else if (currentTree instanceof LambdaExpressionTree lambdaExpressionTree) {
							entity = lookupInLambdaExpression(lambdaExpressionTree, selector);
						} else if (currentTree instanceof MethodTree methodTree) {
							entity = lookupInMethod(methodTree, selector);
						} else {
							entity = null;
						}
					}
					return entity;
				})
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			Tree scope,
			EntitySelectorChain selectorChain) {
		TypeMirror result;
		{
			Entity entity = findEntity(compilationUnitContext, scope, selectorChain);
			//
			result = entity != null
					? findTypeMirror(compilationUnitContext, entity, selectorChain.getContext())
					: null;
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			Entity entity,
			EntitySelectionContext entitySelectionContext) {
		TypeMirror result;
		{
			if (entity.isElement()) {
				result = TypeMirrorUtils.erasure(compilationUnitContext.getProcessingEnvironment(), entity.asElement());
			} else if (entity.isThisEntity()) {
				result = TypeMirrorUtils.erasure(
						compilationUnitContext.getProcessingEnvironment(),
						entity.asThisEntity().getTypeElement());
			} else if (entity.isTree()) {
				result = TreeUtils.findTypeMirror(compilationUnitContext, entity.asTree(), entitySelectionContext);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(entity));
			}
		}
		return result;
	}

	public static ExecutableElement getExecutableElement(Entity entity) {
		ExecutableElement result;
		{
			if (entity.isExecutableElement()) {
				result = entity.asExecutableElement();
			} else if (entity.isMethodTree()) {
				result = MethodTreeUtils.getExecutableElement(entity.asMethodTree());
			} else {
				throw new UnsupportedOperationException(ExceptionUtils.render(entity));
			}
		}
		return result;
	}

	public static TypeMirror getTypeMirror(
			CompilationUnitContext compilationUnitContext,
			Entity entity,
			EntitySelectionContext entitySelectionContext) {
		TypeMirror result = findTypeMirror(compilationUnitContext, entity, entitySelectionContext);
		{
			if (result == null) {
				throw new IllegalArgumentException(
						"Not found: compilationUnitContext = %s, entity = %s, entitySelectionContext = %s"
								.formatted(compilationUnitContext, entity, entitySelectionContext));
			}
		}
		return result;
	}

	public static ExecutableElement selectMostSpecificMethod(
			List<ExecutableElement> executableElements,
			MethodSelector methodSelector) {
		ExecutableElement result;
		{
			List<TypeMirror> argumentTypes = methodSelector.getArgumentSignatures().stream()
					.map(argumentSignature -> {
						TypeMirror argumentType;
						{
							if (argumentSignature.isInferredVariableName()) {
								argumentType = null; // TODO
							} else if (argumentSignature.isLambdaSignature()) {
								throw new NotImplementedException();
							} else if (argumentSignature.isTypeDescription()) {
								throw new NotImplementedException();
							} else {
								throw new UnreachableStateException();
							}
						}
						return argumentType;
					})
					.toList();
			//
			result = null; // TODO
		}
		return result;
	}
}
