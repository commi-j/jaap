package tk.labyrinth.jaap.langmodel.util;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.enhanced.element.EnhancedTypeElementHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelFieldElementHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelFormalParameterElementHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelMethodElementHandle;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelFieldElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelTypeElementHandleImpl;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

public class LangmodelUtils {

	private static LangmodelTypeElementHandleImpl extractTypeElementHandle(TypeElementHandle typeElementHandle) {
		LangmodelTypeElementHandleImpl result;
		{
			TypeElementHandle effectiveHandle;
			if (typeElementHandle instanceof EnhancedTypeElementHandle) {
				effectiveHandle = ((EnhancedTypeElementHandle) typeElementHandle).getTypeElementHandle();
			} else {
				effectiveHandle = typeElementHandle;
			}
			result = (LangmodelTypeElementHandleImpl) effectiveHandle;
		}
		return result;
	}

	public static Element extractElement(ElementHandle elementHandle) {
		return ((LangmodelElementHandleImpl) elementHandle).getElement();
	}

	public static ExecutableElement extractExecutableElement(MethodElementHandle methodElementHandle) {
		return ((LangmodelMethodElementHandle) methodElementHandle).getExecutableElement();
	}

	@Deprecated
	public static ProcessingContext extractProcessingContext(FieldElementHandle fieldElementHandle) {
		return ((LangmodelFieldElementHandleImpl) fieldElementHandle).getElementHandleFactory()
				.getProcessingContext();
	}

	@Deprecated
	public static ProcessingContext extractProcessingContext(TypeElementHandle typeElementHandle) {
		return extractTypeElementHandle(typeElementHandle).getElementHandleFactory()
				.getProcessingContext();
	}

	public static ProcessingEnvironment extractProcessingEnvironment(FieldElementHandle fieldElementHandle) {
		return ((LangmodelFieldElementHandleImpl) fieldElementHandle).getElementHandleFactory()
				.getProcessingEnvironment();
	}

	public static ProcessingEnvironment extractProcessingEnvironment(TypeElementHandle typeElementHandle) {
		return extractTypeElementHandle(typeElementHandle).getElementHandleFactory()
				.getProcessingEnvironment();
	}

	public static TypeElement extractTypeElement(TypeElementHandle typeElementHandle) {
		return extractTypeElementHandle(typeElementHandle).getTypeElement();
	}

	public static VariableElement extractVariableElement(FieldElementHandle fieldElementHandle) {
		return ((LangmodelFieldElementHandle) fieldElementHandle).getVariableElement();
	}

	public static VariableElement extractVariableElement(FormalParameterElementHandle formalParameterElementHandle) {
		return ((LangmodelFormalParameterElementHandle) formalParameterElementHandle).getVariableElement();
	}
}
