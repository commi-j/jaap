package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelMethodElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;

import javax.annotation.Nullable;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class LangmodelMethodElementHandleImpl implements LangmodelMethodElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	ExecutableElement executableElement;

	LangmodelTypeHandleFactory typeHandleFactory;

	public LangmodelMethodElementHandleImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			ExecutableElement executableElement,
			LangmodelTypeHandleFactory typeHandleFactory) {
		this.elementHandleFactory = Objects.requireNonNull(elementHandleFactory);
		this.executableElement = ExecutableElementUtils.requireMethod(executableElement);
		this.typeHandleFactory = Objects.requireNonNull(typeHandleFactory);
	}

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(executableElement);
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		return elementHandleFactory.getExecutable(executableElement);
	}

	@Nullable
	@Override
	public AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return findDirectAnnotation(elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return executableElement.getAnnotationMirrors().stream()
				.map(annotationMirror -> elementHandleFactory.getProcessingContext().getAnnotationHandle(
						executableElement,
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public int getFormalParameterCount() {
		return executableElement.getParameters().size();
	}

	@Override
	public Stream<FormalParameterElementHandle> getFormalParameterStream() {
		return executableElement.getParameters().stream().map(elementHandleFactory::getFormalParameter);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return new MergedAnnotationContext(annotationTypeHandle, asElement(), mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			String annotationTypeSignature,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationTypeSignature),
				mergedAnnotationSpecification);
	}

	@Override
	public Stream<MethodModifier> getModifierStream() {
		return executableElement.getModifiers().stream().map(modifier -> JavaMethodModifier.valueOf(modifier.name()));
	}

	@Override
	public String getName() {
		return executableElement.getSimpleName().toString();
	}

	@Override
	public TypeElementHandle getParent() {
		return elementHandleFactory.getType(executableElement.getEnclosingElement());
	}

	@Override
	public TypeHandle getReturnType() {
		return typeHandleFactory.get(GenericContext.empty(), executableElement.getReturnType());
	}

	@Override
	public String getSignature() {
		return ExecutableElementUtils.getSignatureString(
				elementHandleFactory.getProcessingEnvironment(),
				executableElement);
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		return executableElement.getTypeParameters().stream().map(elementHandleFactory::getTypeParameter);
	}

	@Override
	public boolean hasExplicitAbstractModifier() {
		return executableElement.getModifiers().contains(Modifier.ABSTRACT);
	}

	@Override
	public boolean hasExplicitDefaultModifier() {
		return executableElement.getModifiers().contains(Modifier.DEFAULT);
	}

	@Override
	public boolean hasExplicitPublicModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyAbstract() {
		return !getParent().isInterface()
				? hasExplicitAbstractModifier()
				: isEffectivelyNonStatic() && isEffectivelyNonDefault();
	}

	@Override
	public boolean isEffectivelyDefault() {
		return executableElement.getModifiers().contains(Modifier.DEFAULT);
	}

	@Override
	public boolean isEffectivelyNonAbstract() {
		return !isEffectivelyAbstract();
	}

	@Override
	public boolean isEffectivelyNonDefault() {
		return !isEffectivelyDefault();
	}

	@Override
	public boolean isEffectivelyNonPublic() {
		return !isEffectivelyPublic();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		return !isEffectivelyStatic();
	}

	@Override
	public boolean isEffectivelyPublic() {
		return executableElement.getModifiers().contains(Modifier.PUBLIC);
	}

	@Override
	public boolean isEffectivelyStatic() {
		return getExecutableElement().getModifiers().contains(Modifier.STATIC);
	}

	@Override
	public String toString() {
		return getSignature();
	}
}
