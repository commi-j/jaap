package tk.labyrinth.jaap.langmodel.factory;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.element.util.MethodSignatureUtils;
import tk.labyrinth.jaap.handle.element.util.PackageElementUtils;
import tk.labyrinth.jaap.handle.element.util.VariableElementUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Method;
import java.util.Objects;

@RequiredArgsConstructor
public class LangmodelElementFactoryImpl implements LangmodelElementFactory {

	private final ProcessingEnvironment processingEnvironment;

	@Override
	public VariableElement findDeclaredField(ElementSignature fieldSignature) {
		return VariableElementUtils.findDeclaredField(processingEnvironment, fieldSignature);
	}

	@Override
	public VariableElement findDeclaredField(String fieldSignatureString) {
		return findDeclaredField(ElementSignature.of(fieldSignatureString));
	}

	@Nullable
	@Override
	public Element findElement(ElementSignature elementSignature) {
		Objects.requireNonNull(elementSignature, "elementSignature");
		//
		Element result;
		if (elementSignature.matchesField()) {
			result = findDeclaredField(elementSignature);
		} else if (elementSignature.matchesMethod()) {
			// TODO: Do not use #toString().
			result = findMethod(elementSignature.toString());
		} else if (elementSignature.matchesPackageOrType()) {
			result = findPackageOrType(elementSignature);
		} else {
			throw new NotImplementedException(elementSignature.toString());
		}
		return result;
	}

	@Nullable
	@Override
	public ExecutableElement findMethod(Class<?> type, String methodSimpleSignatureString) {
		return findMethod(MethodSignatureUtils.createFull(type, methodSimpleSignatureString));
	}

	@Nullable
	@Override
	public ExecutableElement findMethod(MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		// FIXME: Make it not fail on non-existing methods.
		return ExecutableElementUtils.findMethod(processingEnvironment, methodFullSignature);
	}

	@Nullable
	@Override
	public PackageElement findPackage(ElementSignature packageElementSignature) {
		return PackageElementUtils.find(processingEnvironment, packageElementSignature);
	}

	@Nullable
	@Override
	public PackageElement findPackage(String packageSignatureString) {
		return PackageElementUtils.find(processingEnvironment, packageSignatureString);
	}

	@Nullable
	@Override
	public Element findPackageOrType(ElementSignature packageOrTypeSignature) {
		Element result;
		{
			Element found;
			if (packageOrTypeSignature.matchesType()) {
				// FIXME: Do not use #toString().
				found = findType(packageOrTypeSignature.toString());
			} else {
				found = null;
			}
			if (found != null) {
				result = found;
			} else if (packageOrTypeSignature.matchesPackage()) {
				result = findPackage(packageOrTypeSignature);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Parameterizable findParameterizable(ElementSignature parameterizableSignature) {
		// FIXME:
		//  1. Must use "matchesExecutable";
		//  2. Must invoke methods with signatures, not strings.
		return parameterizableSignature.matchesMethod()
				? findMethod(parameterizableSignature.toString())
				: findType(parameterizableSignature.toString());
	}

	@Nullable
	@Override
	public Parameterizable findParameterizable(String parameterizableSignature) {
		return findParameterizable(ElementSignature.of(parameterizableSignature));
	}

	@Nullable
	@Override
	public TypeElement findType(TypeSignature typeSignature) {
		return TypeElementUtils.find(processingEnvironment, typeSignature.toString());
	}

	@Nullable
	@Override
	public TypeParameterElement findTypeParameter(String fullTypeParameterElementSignature) {
		Objects.requireNonNull(fullTypeParameterElementSignature, "fullTypeParameterElementSignature");
		//
		TypeParameterElement result;
		{
			// Left - ParameterizedSignature
			// Right - Name
			Pair<String, String> split = SignatureSeparators.split(
					fullTypeParameterElementSignature,
					SignatureSeparators.TYPE_PARAMETER);
			//
			if (split.getRight() != null) {
				String typeParameterName = split.getRight();
				Parameterizable parent = findParameterizable(split.getLeft());
				//
				if (parent != null) {
					result = parent.getTypeParameters().stream()
							.filter(typeParameter -> typeParameter.getSimpleName().contentEquals(typeParameterName))
							.findFirst()
							.orElse(null);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public Element get(Class<?> type) {
		// FIXME: Use ElementUtils.
		return TypeElementUtils.get(processingEnvironment, type);
	}

	@Override
	public ExecutableElement getMethod(Method method) {
		return ExecutableElementUtils.get(processingEnvironment, method);
	}

	@Override
	public PackageElement getPackage(Element element) {
		return ElementUtils.requirePackage(element);
	}

	@Override
	public PackageElement getPackageOf(Class<?> childType) {
		return PackageElementUtils.resolve(processingEnvironment, childType);
	}

	@Override
	public TypeElement getType(Class<?> type) {
		return TypeElementUtils.get(processingEnvironment, type);
	}

	@Override
	public TypeElement getType(TypeMirror typeMirror) {
		return TypeElementUtils.get(processingEnvironment, typeMirror);
	}
}
