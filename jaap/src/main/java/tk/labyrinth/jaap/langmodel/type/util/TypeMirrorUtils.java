package tk.labyrinth.jaap.langmodel.type.util;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.type.tool.ParameterResolvingTypeVisitor;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.ParameterizableUtils;
import tk.labyrinth.jaap.util.PrimitiveTypeUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.ReferenceType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeMirrorUtils {

	/**
	 * Primitives and void.
	 */
	private static final Map<String, Function<ProcessingEnvironment, TypeMirror>> keywordTypeMirrorResolvers;

	static {
		Map<String, Function<ProcessingEnvironment, TypeMirror>> map = Stream.of(TypeKind.values())
				.filter(TypeKind::isPrimitive)
				.collect(Collectors.toMap(
						typeKind -> typeKind.toString().toLowerCase(),
						typeKind -> processingEnvironment -> processingEnvironment.getTypeUtils()
								.getPrimitiveType(typeKind)));
		map.put(
				TypeKind.VOID.toString().toLowerCase(),
				processingEnvironment -> processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID));
		keywordTypeMirrorResolvers = Map.copyOf(map);
	}

	/**
	 * Returns:<br>
	 * -1 if first is assignable to second;<br>
	 * 0 if first is the same as second;<br>
	 * 1 if second is assignable to first;<br>
	 * Throws exception if incompatible.<br>
	 *
	 * @param processingEnvironment non-null
	 * @param first                 non-null
	 * @param second                non-null
	 *
	 * @return -1, 0 or 1
	 *
	 * @throws IllegalArgumentException if incompatible
	 */
	public static int compareByAssignability(
			ProcessingEnvironment processingEnvironment,
			TypeMirror first,
			TypeMirror second) {
		int firstValue = processingEnvironment.getTypeUtils().isAssignable(first, second) ? -1 : 0;
		int secondValue = processingEnvironment.getTypeUtils().isAssignable(second, first) ? 1 : 0;
		//
		if (firstValue == 0 && secondValue == 0) {
			throw new IllegalArgumentException("Incompatible: first = %s, second = %s".formatted(first, second));
		}
		//
		return firstValue + secondValue;
	}

	public static int compareConsideringPrimitives(@Nullable TypeMirror first, @Nullable TypeMirror second) {
		return evaluateType(first) - evaluateType(second);
	}

	public static int computeDistance(TypeMirror first, TypeMirror second) {
		// int -> int = 0
		// int -> Integer = 1
		// int -> Number = -1
		// Integer -> int = 1
		// Integer -> Integer = 0
		// Integer -> Number = ?
		// Integer -> Comparable<Integer>, Constable, ConstantDesc = ?
		// Integer -> Serializable = ?
		// Integer -> Object = ?
		//
		// Integer -> int or Number?
		//
		throw new NotImplementedException();
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, Element element) {
		TypeMirror result;
		if (element instanceof ExecutableElement) {
			result = erasure(processingEnvironment, (ExecutableElement) element);
		} else if (element instanceof TypeElement) {
			result = erasure(processingEnvironment, (TypeElement) element);
		} else if (element instanceof VariableElement) {
			result = erasure(processingEnvironment, (VariableElement) element);
		} else {
			throw new IllegalArgumentException(ExceptionUtils.render(element));
		}
		return result;
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement) {
		// FIXME: It will probably provide false type for constructors.
		//
		// We convert returnType to String and then resolve it to obtain canonical version of this TypeMirror.
		// See tk.labyrinth.javax.lang.model.element.ExecutableElementTest#testGetReturnType() for issue details.
		return resolve(processingEnvironment, processingEnvironment.getTypeUtils()
				.erasure(executableElement.getReturnType()).toString());
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, TypeElement typeElement) {
		return processingEnvironment.getTypeUtils().erasure(typeElement.asType());
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return processingEnvironment.getTypeUtils().erasure(typeMirror);
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, VariableElement variableElement) {
		return processingEnvironment.getTypeUtils().erasure(variableElement.asType());
	}

	public static List<TypeMirror> erasures(ProcessingEnvironment processingEnvironment, List<TypeMirror> typeMirrors) {
		return typeMirrors.stream()
				.map(typeMirror -> erasure(processingEnvironment, typeMirror))
				.collect(Collectors.toList());
	}

	public static int evaluateType(@Nullable TypeMirror value) {
		return value != null
				? switch (value.getKind()) {
			case BOOLEAN -> 0;
			case BYTE -> 1;
			case CHAR -> 3;
			case DOUBLE -> 9;
			case INT -> 4;
			case FLOAT -> 5;
			case LONG -> 8;
			case SHORT -> 2;
			case DECLARED -> 16;
			default -> throw new UnsupportedOperationException(ExceptionUtils.render(value));
		}
				: 17;
	}

	@Nullable
	public static TypeMirror find(ProcessingEnvironment processingEnvironment, Class<?> type) {
		// TODO: Add nullability.
		return resolve(processingEnvironment, type);
	}

	@Nullable
	public static TypeMirror find(ProcessingEnvironment processingEnvironment, Type type) {
		TypeMirror result;
		if (type instanceof Class) {
			result = find(processingEnvironment, (Class<?>) type);
		} else if (type instanceof ParameterizedType) {
			// TODO: Preserve parameters.
			result = find(processingEnvironment, (Class<?>) ((ParameterizedType) type).getRawType());
		} else if (type instanceof TypeVariable) {
			TypeVariable<?> typeVariable = (TypeVariable<?>) type;
			//
			Element element = ElementUtils.find(processingEnvironment, typeVariable.getGenericDeclaration());
			if (element != null) {
				TypeParameterElement typeParameter = ElementUtils.findTypeParameter(element, typeVariable.getName());
				if (typeParameter != null) {
					result = typeParameter.asType();
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	/**
	 * @param processingEnvironment non-null
	 * @param typeFullName          non-null
	 *
	 * @return non-null for primitive or void, null otherwise
	 */
	@Nullable
	public static TypeMirror findKeywordType(ProcessingEnvironment processingEnvironment, String typeFullName) {
		return keywordTypeMirrorResolvers.getOrDefault(typeFullName, innerProcessingEnvironment -> null)
				.apply(processingEnvironment);
	}

	public static TypeMirror fromDeclaration(
			ProcessingEnvironment processingEnvironment,
			Parameterizable context,
			TypeDescription typeDescription) {
		TypeMirror result;
		if (typeDescription.getArrayDepth() != null) {
			// Array
			//
			throw new NotImplementedException();
		} else if (typeDescription.getParameters() != null) {
			// Parameterized
			//
			result = processingEnvironment.getTypeUtils().getDeclaredType(
					TypeElementUtils.get(processingEnvironment, typeDescription.getFullName()),
					typeDescription.getParameters().stream()
							.map(parameter -> fromDeclaration(processingEnvironment, context, parameter))
							.toArray(TypeMirror[]::new));
		} else if (typeDescription.getLowerBound() != null || typeDescription.getUpperBound() != null) {
			// Variable or Wildcard
			//
			throw new NotImplementedException();
		} else {
			// Plain or Primitive or Raw or Variable or Wildcard
			//
			String typeFullName = typeDescription.getFullName();
			//
			if (typeFullName.contains(".")) {
				// Plain or Raw
				//
				TypeElement typeElement = TypeElementUtils.get(processingEnvironment, typeFullName);
				if (typeElement.getTypeParameters().isEmpty()) {
					// Plain
					//
					result = typeElement.asType();
				} else {
					// Raw
					//
					result = erasure(processingEnvironment, typeElement);
				}
			} else {
				// Primitive or Variable or Wildcard
				//
				if (Objects.equals(typeFullName, "?")) {
					// Wildcard
					//
					result = processingEnvironment.getTypeUtils().getWildcardType(null, null);
				} else {
					// Primitive or Variable
					//
					TypeMirror keywordType = findKeywordType(processingEnvironment, typeFullName);
					if (keywordType != null) {
						// Primitive
						//
						result = keywordType;
					} else {
						// Variable
						//
						TypeParameterElement typeParameterElement = ParameterizableUtils
								.findTypeParameter(context, typeFullName, true);
						if (typeParameterElement != null) {
							result = typeParameterElement.asType();
						} else {
							throw new IllegalArgumentException("Unresolvable: " + typeDescription);
						}
					}
				}
			}
		}
		return result;
	}

	public static TypeMirror get(ProcessingEnvironment processingEnvironment, Class<?> type) {
		TypeMirror result = find(processingEnvironment, type);
		if (result == null) {
			throw new IllegalArgumentException("No TypeMirror resolved: type = " + type);
		}
		return result;
	}

	public static TypeMirror get(ProcessingEnvironment processingEnvironment, Type type) {
		TypeMirror result = find(processingEnvironment, type);
		if (result == null) {
			throw new IllegalArgumentException("No TypeMirror resolved: type = " + type);
		}
		return result;
	}

	/**
	 * Returns a Stream of distinct TypeMirrors which are either same as or superclass of provided TypeMirror.<br>
	 * This method returns Classes only.<br>
	 *
	 * @param environment non-null
	 * @param mirror      non-null
	 *
	 * @return non-empty, contains at least provided TypeMirror
	 *
	 * @throws IllegalArgumentException in case provided typeMirror is Interface
	 * @see #getTypeHierarchy(ProcessingEnvironment, TypeMirror)
	 */
	public static Stream<TypeMirror> getClassHierarchy(ProcessingEnvironment environment, TypeMirror mirror) {
		if (environment.getTypeUtils().asElement(mirror).getKind() == ElementKind.INTERFACE) {
			throw new IllegalArgumentException("Class or Enum required: " + mirror);
		}
		List<TypeMirror> result = new ArrayList<>();
		{
			TypeMirror targetTypeMirror = mirror;
			while (targetTypeMirror != null) {
				result.add(targetTypeMirror);
				targetTypeMirror = environment.getTypeUtils().directSupertypes(targetTypeMirror).stream()
						.findFirst().orElse(null);
			}
		}
		return result.stream();
	}

	public static TypeSignature getSignature(TypeMirror typeMirror) {
		TypeSignature result;
		{
			if (TypeMirrorUtils.isIntersectionType(typeMirror)) {
				// Should go before DeclaredType because implementing class is also Declared one.
				result = getSignature(TypeMirrorUtils.requireIntersectionType(typeMirror).getBounds().get(0));
			} else if (TypeMirrorUtils.isDeclaredType(typeMirror)) {
				// FIXME: Use ElementUtils#getSignature when we will have method without ProcEnv.
				result = TypeSignature.ofValid(TypeElementUtils.getSignature(ElementUtils.requireType(
						TypeMirrorUtils.requireDeclaredType(typeMirror).asElement())));
			} else if (TypeMirrorUtils.isPrimitiveType(typeMirror)) {
				// Erasure of primitives does not remove annotations, so we need to remove them here.
				result = TypeSignature.ofValid(typeMirror.getKind().toString().toLowerCase());
			} else if (TypeMirrorUtils.isTypeVariable(typeMirror)) {
				result = getSignature(TypeMirrorUtils.requireTypeVariable(typeMirror).getUpperBound());
				//
			} else {
				// TODO: Arrays?
				result = TypeSignature.ofValid(typeMirror.toString());
			}
		}
		return result;
	}

	// FIXME: Do erasure inside.
	@Deprecated // See getErasureSignature
	public static String getSignatureContributingString(TypeMirror erasedTypeMirror) {
		String result;
		if (TypeMirrorUtils.isDeclaredType(erasedTypeMirror)) {
			// FIXME: Use ElementUtils#getSignature when we will have method without ProcEnv.
			result = TypeElementUtils.getSignature(ElementUtils.requireType(
					TypeMirrorUtils.requireDeclaredType(erasedTypeMirror).asElement()));
		} else if (TypeMirrorUtils.isPrimitiveType(erasedTypeMirror)) {
			// Erasure of primitives does not remove annotations, so we need to remove them here.
			result = erasedTypeMirror.getKind().toString().toLowerCase();
		} else {
			// TODO: Arrays?
			result = erasedTypeMirror.toString();
		}
		return result;
	}

	/**
	 * Returns a Stream of distinct TypeMirrors which are either same as or supertype of provided TypeMirror.<br>
	 * This method returns both Classes and Interfaces.<br>
	 *
	 * @param environment non-null
	 * @param mirror      non-null
	 *
	 * @return non-empty, contains at least provided TypeMirror
	 *
	 * @see #getClassHierarchy(ProcessingEnvironment, TypeMirror)
	 */
	public static Stream<TypeMirror> getTypeHierarchy(ProcessingEnvironment environment, TypeMirror mirror) {
		List<TypeMirror> result = new ArrayList<>();
		result.add(mirror);
		int index = 0;
		while (result.size() > index) {
			environment.getTypeUtils().directSupertypes(result.get(index)).forEach(directSupertype -> {
				if (result.stream().noneMatch(resultTypeMirror ->
						environment.getTypeUtils().isSameType(resultTypeMirror, directSupertype))) {
					result.add(directSupertype);
				}
			});
			index++;
		}
		return result.stream();
	}

	public static boolean isArrayType(TypeMirror typeMirror) {
		return typeMirror instanceof ArrayType;
	}

	public static boolean isAssignable(ProcessingEnvironment processingEnvironment, TypeMirror first, TypeMirror second) {
		return processingEnvironment.getTypeUtils().isAssignable(first, second);
	}

	public static boolean isDeclaredType(TypeMirror typeMirror) {
		return typeMirror instanceof DeclaredType;
	}

	public static boolean isErasure(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return typeMirror == erasure(processingEnvironment, typeMirror);
	}

	public static boolean isInterface(ProcessingEnvironment environment, TypeMirror typeMirror) {
		return environment.getTypeUtils().asElement(typeMirror).getKind() == ElementKind.INTERFACE;
	}

	public static boolean isIntersectionType(TypeMirror typeMirror) {
		return typeMirror instanceof IntersectionType;
	}

	public static boolean isNoType(TypeMirror typeMirror) {
		return typeMirror instanceof NoType;
	}

	public static boolean isPrimitiveType(TypeMirror typeMirror) {
		return typeMirror instanceof PrimitiveType;
	}

	public static boolean isReferenceType(TypeMirror typeMirror) {
		return typeMirror instanceof ReferenceType;
	}

	public static boolean isTypeVariable(TypeMirror typeMirror) {
		// FIXME: Solve import conflict with java.lang.reflect.TypeVariable.
		return typeMirror instanceof javax.lang.model.type.TypeVariable;
	}

	public static boolean isWildcardType(TypeMirror typeMirror) {
		return typeMirror instanceof WildcardType;
	}

	public static ArrayType requireArrayType(TypeMirror typeMirror) {
		if (!isArrayType(typeMirror)) {
			throw new IllegalArgumentException("Require ArrayType: " + typeMirror);
		}
		return (ArrayType) typeMirror;
	}

	public static DeclaredType requireDeclaredType(TypeMirror typeMirror) {
		if (!isDeclaredType(typeMirror)) {
			throw new IllegalArgumentException("Require DeclaredType: " + typeMirror);
		}
		return (DeclaredType) typeMirror;
	}

	public static IntersectionType requireIntersectionType(TypeMirror typeMirror) {
		if (!isIntersectionType(typeMirror)) {
			throw new IllegalArgumentException("Require IntersectionType: " + typeMirror);
		}
		return (IntersectionType) typeMirror;
	}

	public static NoType requireNoType(TypeMirror value) {
		if (!isNoType(value)) {
			throw new IllegalArgumentException("Require noType: " + value);
		}
		return (NoType) value;
	}

	public static TypeMirror requireNonPrimitive(TypeMirror argument) {
		if (argument == null || argument.getKind().isPrimitive()) {
			throw new IllegalArgumentException("Require non-primitive: " + argument);
		}
		return argument;
	}

	public static PrimitiveType requirePrimitive(TypeMirror typeMirror) {
		if (!isPrimitiveType(typeMirror)) {
			throw new IllegalArgumentException("Require PrimitiveType: " + typeMirror);
		}
		return (PrimitiveType) typeMirror;
	}

	/**
	 * Tests whether first is a subtype of second.
	 *
	 * @param environment non-null
	 * @param first       non-null
	 * @param second      non-null
	 *
	 * @return first
	 */
	public static TypeMirror requireSubtype(ProcessingEnvironment environment, TypeMirror first, TypeMirror second) {
		if (first == null || second == null || !environment.getTypeUtils().isSubtype(first, second)) {
			throw new IllegalArgumentException("Require subtype: first = " + first + ", second = " + second);
		}
		return first;
	}

	public static javax.lang.model.type.TypeVariable requireTypeVariable(TypeMirror typeMirror) {
		// FIXME: Solve qualified name conflict with java.lang.reflect.TypeVariable.
		if (!isTypeVariable(typeMirror)) {
			throw new IllegalArgumentException("Require TypeVariable: " + typeMirror);
		}
		return (javax.lang.model.type.TypeVariable) typeMirror;
	}

	public static WildcardType requireWildcardType(TypeMirror typeMirror) {
		if (!isWildcardType(typeMirror)) {
			throw new IllegalArgumentException("Require WildcardType: " + typeMirror);
		}
		return (WildcardType) typeMirror;
	}

	public static TypeMirror resolve(
			ProcessingEnvironment processingEnvironment,
			Map<TypeParameterElement, TypeMirror> typeParameterMappings,
			TypeMirror typeMirror) {
		return new ParameterResolvingTypeVisitor().visit(
				typeMirror,
				new ParameterResolvingTypeVisitor.Context(
						processingEnvironment,
						typeParameterMappings));
	}

	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, Class<?> type) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(type, "type");
		//
		return resolve(processingEnvironment, type.getCanonicalName(), false);
	}

	@Deprecated
	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, String typeFullName) {
		return resolve(processingEnvironment, typeFullName, true);
	}

	@Deprecated
	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, String typeFullName, boolean erase) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeFullName, "typeFullName");
		//
		TypeMirror result;
		if (typeFullName.endsWith("[]")) {
			result = processingEnvironment.getTypeUtils().getArrayType(resolve(processingEnvironment,
					typeFullName.substring(0, typeFullName.length() - 2)));
		} else {
			switch (typeFullName) {
				case "boolean":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
					break;
				case "byte":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BYTE);
					break;
				case "char":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.CHAR);
					break;
				case "double":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.DOUBLE);
					break;
				case "float":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT);
					break;
				case "int":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT);
					break;
				case "long":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG);
					break;
				case "short":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.SHORT);
					break;
				case "void":
					result = processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID);
					break;
				default:
					TypeElement typeElement = TypeElementUtils.get(processingEnvironment, typeFullName);
					result = erase
							? erasure(processingEnvironment, typeElement)
							: typeElement.asType();
			}
		}
		return result;
	}

	public static TypeMirror resolveUpperBound(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return switch (typeMirror.getKind()) {
			case ARRAY, BOOLEAN, BYTE, CHAR, DECLARED, DOUBLE, FLOAT, INT, LONG, SHORT -> typeMirror;
			case NULL -> get(processingEnvironment, Object.class);
			case NONE, PACKAGE, VOID -> throw new IllegalArgumentException(ExceptionUtils.render(typeMirror));
			case WILDCARD -> {
				TypeMirror extendsBound = requireWildcardType(typeMirror).getExtendsBound();
				//
				yield extendsBound != null
						? resolveUpperBound(processingEnvironment, extendsBound)
						: get(processingEnvironment, Object.class);
			}
			default -> throw new NotImplementedException(ExceptionUtils.render(typeMirror));
		};
	}

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		if (isIntersectionType(typeMirror)) {
			throw new IllegalArgumentException("IntersectionType can not be rendered as single TypeDescription: " + typeMirror);
		}
		return toDescriptionConsideringIntersections(processingEnvironment, typeMirror).get(0);
	}

	public static List<TypeDescription> toDescriptionConsideringIntersections(
			ProcessingEnvironment processingEnvironment,
			TypeMirror typeMirror) {
		List<TypeDescription> result;
		{
			if (typeMirror instanceof ArrayType arrayType) {
				result = List.of(ArrayTypeUtils.toDescription(processingEnvironment, arrayType));
			} else if (typeMirror instanceof DeclaredType declaredType) {
				result = List.of(DeclaredTypeUtils.toDescription(processingEnvironment, declaredType));
			} else if (typeMirror instanceof IntersectionType intersectionType) {
				result = IntersectionTypeUtils.toDescriptions(processingEnvironment, intersectionType);
			} else if (typeMirror instanceof NoType) {
				result = List.of(TypeDescription.ofVoid());
			} else if (typeMirror instanceof NullType) {
				result = List.of(TypeDescription.ofNull());
			} else if (typeMirror instanceof PrimitiveType primitiveType) {
				result = List.of(PrimitiveTypeUtils.toDescription(primitiveType));
			} else if (typeMirror instanceof javax.lang.model.type.TypeVariable typeVariable) {
				result = List.of(TypeVariableUtils.toDescription(processingEnvironment, typeVariable));
			} else if (typeMirror instanceof WildcardType wildcardType) {
				result = List.of(WildcardTypeUtils.toDescription(processingEnvironment, wildcardType));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
