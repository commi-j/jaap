package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.langmodel.element.LangmodelExecutableElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandleBase;
import tk.labyrinth.jaap.model.element.InitializerElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;

import javax.lang.model.element.ExecutableElement;
import java.util.Objects;

@Value
public class LangmodelExecutableElementHandleImpl extends ExecutableElementHandleBase implements
		LangmodelExecutableElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	ExecutableElement executableElement;

	public LangmodelExecutableElementHandleImpl(
			LangmodelElementHandleFactory elementHandleFactory,
			ExecutableElement executableElement) {
		this.elementHandleFactory = Objects.requireNonNull(elementHandleFactory);
		this.executableElement = Objects.requireNonNull(executableElement);
	}

	@Override
	public ConstructorElementHandle asConstructorElement() {
		return elementHandleFactory.getConstructor(executableElement);
	}

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(executableElement);
	}

	@Override
	public InitializerElementHandle asInitializerElement() {
		return elementHandleFactory.getInitializer(executableElement);
	}

	@Override
	public MethodElementHandle asMethodElement() {
		return elementHandleFactory.getMethod(executableElement);
	}

	@NonNull
	@Override
	public TypeElementHandle getParent() {
		return elementHandleFactory.getType(executableElement.getEnclosingElement());
	}

	@Override
	public String getSignature() {
		return ExecutableElementUtils.getSignatureString(
				elementHandleFactory.getProcessingEnvironment(),
				executableElement);
	}

	@Override
	public boolean isConstructorElement() {
		return ExecutableElementUtils.isConstructor(executableElement);
	}

	@Override
	public boolean isInitializerElement() {
		return ExecutableElementUtils.isInitializer(executableElement);
	}

	@Override
	public boolean isMethodElement() {
		return ExecutableElementUtils.isMethod(executableElement);
	}
}
