package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.FieldElementHandle;

import javax.lang.model.element.VariableElement;

public interface LangmodelFieldElementHandle extends FieldElementHandle {

	VariableElement getVariableElement();
}
