package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayAccessTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.lang.model.type.TypeMirror;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ArrayAccessTreeUtils {

	// TODO: Tests.
	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			ArrayAccessTree arrayAccessTree) {
		TypeMirror typeMirror = ExpressionTreeUtils.findTypeMirror(
				compilationUnitContext,
				arrayAccessTree.getExpression(),
				EntitySelectionContext.forVariableType()); // TODO: Ensure this is proper kind.
		//
		return typeMirror != null ? TypeMirrorUtils.requireArrayType(typeMirror).getComponentType() : null;
	}
}
