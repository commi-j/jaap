package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.ElementHandle;

import javax.lang.model.element.Element;

public interface LangmodelElementHandle extends ElementHandle {

	Element getElement();
}
