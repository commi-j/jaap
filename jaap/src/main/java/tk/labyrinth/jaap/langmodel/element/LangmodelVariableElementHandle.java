package tk.labyrinth.jaap.langmodel.element;

import tk.labyrinth.jaap.model.element.VariableElementHandle;

import javax.lang.model.element.VariableElement;

public interface LangmodelVariableElementHandle extends VariableElementHandle {

	VariableElement getVariableElement();
}
