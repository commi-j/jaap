package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelFieldElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.VariableElementHandle;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;

import javax.annotation.Nullable;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;

@Value
public class LangmodelFieldElementHandleImpl implements LangmodelFieldElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	LangmodelTypeHandleFactory typeHandleFactory;

	VariableElement variableElement;

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(variableElement);
	}

	@Override
	public VariableElementHandle asVariableElement() {
		return elementHandleFactory.getVariable(variableElement);
	}

	@Nullable
	@Override
	public AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return findDirectAnnotation(elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return variableElement.getAnnotationMirrors().stream()
				.map(annotationMirror -> elementHandleFactory.getProcessingContext().getAnnotationHandle(
						variableElement,
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return new MergedAnnotationContext(annotationTypeHandle, asElement(), mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			String annotationTypeSignature,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationTypeSignature),
				mergedAnnotationSpecification);
	}

	@Override
	public String getName() {
		return variableElement.getSimpleName().toString();
	}

	@Override
	public @NonNull TypeElementHandle getParent() {
		return elementHandleFactory.getType(variableElement.getEnclosingElement());
	}

	@Override
	public String getSignature() {
		return getParent().getSignatureString() + SignatureSeparators.VARIABLE + getName();
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public TypeHandle getType() {
		return typeHandleFactory.get(GenericContext.empty(), variableElement.asType());
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		return !isEffectivelyStatic();
	}

	@Override
	public boolean isEffectivelyStatic() {
		return getVariableElement().getModifiers().contains(Modifier.STATIC);
	}

	@Override
	public String toString() {
		return getSignatureString();
	}
}
