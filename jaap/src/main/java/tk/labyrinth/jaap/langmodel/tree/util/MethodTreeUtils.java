package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.MethodTree;
import com.sun.tools.javac.tree.JCTree;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;

public class MethodTreeUtils {

	public static ExecutableElement getExecutableElement(MethodTree methodTree) {
		return ((JCTree.JCMethodDecl) methodTree).sym;
	}

	// TODO: Make complex lookup (class/package/hierarchy)
	public static boolean hasAnnotation(MethodTree methodTree, String annotationName) {
		return methodTree.getModifiers().getAnnotations().stream()
				.map(AnnotationTree::getAnnotationType)
				.map(JCTree.JCIdent.class::cast)
				.anyMatch(jcIdent -> jcIdent.sym.getQualifiedName().contentEquals(annotationName));
	}

	public static boolean isNotStatic(MethodTree methodTree) {
		return !isStatic(methodTree);
	}

	public static boolean isStatic(MethodTree methodTree) {
		return methodTree.getModifiers().getFlags().contains(Modifier.STATIC);
	}
}
