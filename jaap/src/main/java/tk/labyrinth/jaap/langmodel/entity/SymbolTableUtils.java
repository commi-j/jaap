package tk.labyrinth.jaap.langmodel.entity;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.jaap.core.moduleaccess.ModuleAccessManager;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import java.util.List;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class SymbolTableUtils {

	static {
		ModuleAccessManager.addExportsToAllUnnamed();
	}

	private static Symbol.ClassSymbol doLookup(Scope scope, com.sun.tools.javac.util.Name name) {
		Symbol.ClassSymbol result;
		{
			List<Symbol.ClassSymbol> symbols = StreamUtils.from(scope.getSymbolsByName(name).iterator())
					.map(Symbol.ClassSymbol.class::cast)
					.toList();
			if (symbols.size() == 1) {
				result = symbols.get(0);
			} else if (symbols.isEmpty()) {
				result = null;
			} else {
				throw new UnreachableStateException(ExceptionUtils.render(symbols));
			}
		}
		return result;
	}

	public static TypeElement lookup(CompilationUnitTree compilationUnitTree, Name name) {
		JCTree.JCCompilationUnit jcCompilationUnit = (JCTree.JCCompilationUnit) compilationUnitTree;
		com.sun.tools.javac.util.Name jcName = (com.sun.tools.javac.util.Name) name;
		//
		TypeElement result;
		{
			TypeElement topLevelResult = doLookup(jcCompilationUnit.toplevelScope, jcName);
			if (topLevelResult != null) {
				result = topLevelResult;
			} else {
				TypeElement namedImportResult = doLookup(jcCompilationUnit.namedImportScope, jcName);
				if (namedImportResult != null) {
					result = namedImportResult;
				} else {
					TypeElement starImportResult = doLookup(jcCompilationUnit.starImportScope, jcName);
					// TODO: Insert same package over java.lang
					if (starImportResult != null) {
						result = starImportResult;
					} else {
						result = null;
					}
				}
			}
		}
		return result;
	}
}
