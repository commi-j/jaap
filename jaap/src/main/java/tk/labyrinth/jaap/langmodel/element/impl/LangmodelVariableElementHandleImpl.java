package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.util.TypeHandleUtils;
import tk.labyrinth.jaap.langmodel.element.LangmodelVariableElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;

import javax.annotation.Nullable;
import javax.lang.model.element.VariableElement;

@Value
public class LangmodelVariableElementHandleImpl implements LangmodelVariableElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	LangmodelTypeHandleFactory typeHandleFactory;

	VariableElement variableElement;

	@Override
	public @NonNull ElementHandle getParent() {
		return elementHandleFactory.get(variableElement.getEnclosingElement());
	}

	@Override
	public TypeHandle getType() {
		return typeHandleFactory.get(GenericContext.empty(), variableElement.asType());
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		return TypeHandleUtils.selectMember(getType(), selector);
	}
}
