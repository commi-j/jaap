package tk.labyrinth.jaap.langmodel.entity;

import lombok.Value;

import javax.lang.model.element.TypeElement;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Value(staticConstructor = "of")
public class ThisEntity {

	TypeElement typeElement;

	@Override
	public String toString() {
		return typeElement.toString() + ".this";
	}
}
