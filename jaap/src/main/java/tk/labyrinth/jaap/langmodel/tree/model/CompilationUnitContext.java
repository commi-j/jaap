package tk.labyrinth.jaap.langmodel.tree.model;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.util.ClassTreeUtils;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import java.util.Objects;
import java.util.stream.Stream;

@Value(staticConstructor = "of")
public class CompilationUnitContext {

	@NonNull
	CompilationUnitTree compilationUnitTree;

	@NonNull
	ProcessingEnvironment processingEnvironment;

	@Nullable
	public Element findElement(Tree tree) {
		Element result;
		{
			Element element = getTrees().getElement(getTreePath(tree));
			//
			if (element instanceof PackageElement packageElement) {
				// There is a specific issue that com.sun.source.util.Trees returns Element for Package without Classes
				// while javax.lang.model.util.Elements returns null. Code below makes it to behave consistently.
				//
				result = processingEnvironment.getElementUtils().getPackageElement(packageElement.getQualifiedName());
			} else {
				result = element;
			}
		}
		return result;
	}

	@Nullable
	public Tree findParent(Tree tree) {
		return tree != compilationUnitTree ? getTreePath(tree).getParentPath().getLeaf() : null;
	}

	// TODO: Rename to ancestor or smth like this.
	@Nullable
	public <T extends Tree> T findParentOfType(Class<T> parentClass, Tree tree) {
		return getTreeStream(tree)
				.skip(1)
				.filter(parentClass::isInstance)
				.map(parentClass::cast)
				.findFirst()
				.orElse(null);
	}

	@Nullable
	public TypeElement findScopeTypeElement(Tree tree) {
		return getTreeStream(tree)
				.filter(ClassTree.class::isInstance)
				.map(ClassTree.class::cast)
				.map(ClassTreeUtils::getTypeElement)
				.findFirst()
				.orElse(null);
	}

	public Tree getParent(Tree tree) {
		Tree result = findParent(tree);
		if (result == null) {
			throw new IllegalArgumentException("Not found: tree = %s, this = %s".formatted(tree, this));
		}
		return result;
	}

	public TypeElement getScopeTypeElement(Tree tree) {
		TypeElement result = findScopeTypeElement(tree);
		if (result == null) {
			throw new IllegalArgumentException("Not found: tree = %s, this = %s".formatted(tree, this));
		}
		return result;
	}

	public TreePath getTreePath(Tree tree) {
		return Objects.requireNonNull(getTrees().getPath(compilationUnitTree, tree));
	}

	public Stream<Tree> getTreeStream(Tree tree) {
		return StreamUtils.from(getTreePath(tree));
	}

	public Trees getTrees() {
		return Trees.instance(processingEnvironment);
	}

	@Override
	public String toString() {
		return compilationUnitTree.getSourceFile().toString();
	}
}
