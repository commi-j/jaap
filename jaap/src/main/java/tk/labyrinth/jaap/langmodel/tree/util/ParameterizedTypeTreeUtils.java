package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ParameterizedTypeTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class ParameterizedTypeTreeUtils {

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			ParameterizedTypeTree parameterizedTypeTree) {
		TypeMirror result;
		{
			// FIXME: There may be an option of obtaining TypeElement, but we downgrade it to Mirror
			//  and then Upgrade again. Need to fix with direct usage of Element.
			TypeMirror type = TreeUtils.findTypeMirror(
					compilationUnitContext,
					parameterizedTypeTree.getType(),
					EntitySelectionContext.forType());
			TypeElement element = TypeElementUtils.get(compilationUnitContext.getProcessingEnvironment(), type);
			// FIXME: Replace with DeclaredTypeUtils.resolve (need to create it before).
			result = compilationUnitContext.getProcessingEnvironment().getTypeUtils().getDeclaredType(
					element,
					parameterizedTypeTree.getTypeArguments().stream()
							.map(typeArgument -> TreeUtils.findTypeMirror(
									compilationUnitContext,
									typeArgument,
									EntitySelectionContext.forType()))
							.toArray(TypeMirror[]::new));
		}
		return result;
	}
}
