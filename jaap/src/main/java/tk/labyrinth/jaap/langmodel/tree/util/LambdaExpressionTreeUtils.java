package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

/**
 * LambdaExpression parent is either MethodInvocation or VariableAssignment.
 *
 * @author Commitman
 * @version 1.0.0
 */
public class LambdaExpressionTreeUtils {

	@Nullable
	public static TypeMirror findBodyTypeMirror(
			CompilationUnitContext compilationUnitContext,
			LambdaExpressionTree lambdaExpressionTree) {
		TypeMirror result;
		{
			result = switch (lambdaExpressionTree.getBodyKind()) {
				case EXPRESSION -> TreeUtils.findTypeMirror(
						compilationUnitContext,
						lambdaExpressionTree.getBody(),
						//
						// TODO: It's not always VARIABLE, but should work fine. But we want to make it precise for cases like MethodInv.
						EntitySelectionContext.forVariable());
				case STATEMENT -> throw new NotImplementedException();
			};
		}
		return result;
	}

	@Nullable
	public static VariableTree findDeclaration(LambdaExpressionTree lambdaExpressionTree, Name name) {
		throw new NotImplementedException();
	}

	@Nullable
	public static TypeMirror findParameterType(
			CompilationUnitContext compilationUnitContext,
			VariableTree variableTree) {
		TypeMirror result;
		{
			LambdaExpressionTree lambdaExpressionTree = (LambdaExpressionTree) compilationUnitContext
					.getParent(variableTree);
			//
			Tree parentTree = compilationUnitContext.getParent(lambdaExpressionTree);
			//
			if (parentTree instanceof MethodInvocationTree methodInvocationTree) {
				ExecutableElement executableElement = MethodInvocationTreeUtils.findMethod(
						compilationUnitContext,
						methodInvocationTree);
				//
				if (executableElement != null) {
					VariableElement variableElement = executableElement.getParameters()
							.get(methodInvocationTree.getArguments().indexOf(lambdaExpressionTree));
					//
					result = TypeMirrorUtils.requireDeclaredType(variableElement.asType()).getTypeArguments()
							.get(lambdaExpressionTree.getParameters().indexOf(variableTree));
				} else {
					throw new NotImplementedException();
				}
			} else if (parentTree instanceof VariableTree parentVariableTree) {
				TypeMirror parentTypeMirror = TreeUtils.findTypeMirror(
						compilationUnitContext,
						parentVariableTree.getType(),
						EntitySelectionContext.forType());
				//
				if (TypeMirrorUtils.isDeclaredType(parentTypeMirror)) {
					result = TypeMirrorUtils.requireDeclaredType(parentTypeMirror).getTypeArguments()
							.get(lambdaExpressionTree.getParameters().indexOf(variableTree));
				} else {
					result = null;
				}
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(
			CompilationUnitContext compilationUnitContext,
			LambdaExpressionTree lambdaExpressionTree) {
		// TODO: In general it's impossible to determine Lambda type without knowing invoking method.
		return null;
	}
}
