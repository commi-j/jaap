package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.tree.JCTree;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.core.moduleaccess.ModuleAccessManager;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

/**
 * VariableTrees could be:<br>
 * - Fields (parent instanceof {@link ClassTree});<br>
 * - Method parameters (parent instanceof {@link MethodTree});<br>
 * - Lambda parameters (parent instanceof {@link LambdaExpressionTree});<br>
 * - Local variables (parent instanceof {@link BlockTree});<br>
 *
 * @author Commitman
 * @version 1.0.0
 */
public class VariableTreeUtils {

	static {
		ModuleAccessManager.addExportsToAllUnnamed();
	}

	@Nullable
	public static VariableElement findElement(VariableTree variableTree) {
		return ((JCTree.JCVariableDecl) variableTree).sym;
	}

	@Nullable
	public static TypeMirror findTypeMirror(CompilationUnitContext compilationUnitContext, VariableTree variableTree) {
		TypeMirror result;
		{
			VariableElement variableElement = findElement(variableTree);
			if (variableElement != null) {
				result = TypeMirrorUtils.erasure(compilationUnitContext.getProcessingEnvironment(), variableElement);
			} else {
				if (variableTree.getType() != null) {
					result = TreeUtils.findTypeMirror(
							compilationUnitContext,
							variableTree.getType(),
							EntitySelectionContext.forType());
				} else {
					// Variables without types are lambda parameters.
					//
					result = LambdaExpressionTreeUtils.findParameterType(compilationUnitContext, variableTree);
				}
			}
		}
		return result;
	}

	public static VariableElement getElement(VariableTree variableTree) {
		return Objects.requireNonNull(findElement(variableTree), "variableTree = " + variableTree);
	}

	public static boolean isField(CompilationUnitContext compilationUnitContext, VariableTree variableTree) {
		return compilationUnitContext.getParent(variableTree) instanceof ClassTree;
	}

	public static boolean isInferredLambdaParameter(
			CompilationUnitContext compilationUnitContext,
			VariableTree variableTree) {
		return isLambdaParameter(compilationUnitContext, variableTree) && variableTree.getType() == null;
	}

	public static boolean isLambdaParameter(CompilationUnitContext compilationUnitContext, VariableTree variableTree) {
		return compilationUnitContext.getParent(variableTree) instanceof LambdaExpressionTree;
	}

	public static VariableTree requireField(CompilationUnitContext compilationUnitContext, VariableTree variableTree) {
		if (!isField(compilationUnitContext, variableTree)) {
			throw new IllegalArgumentException("Require field: %s".formatted(variableTree));
		}
		return variableTree;
	}
}
