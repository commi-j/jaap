package tk.labyrinth.jaap.langmodel.factory;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextAware;
import tk.labyrinth.jaap.handle.element.util.VariableElementUtils;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelConstructorElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelExecutableElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelFieldElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelFormalParameterElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelInitializerElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelMethodElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelPackageElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelTypeElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelTypeParameterElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.impl.LangmodelVariableElementHandleImpl;
import tk.labyrinth.jaap.langmodel.element.secondary.LangmodelParameterizedElementHandleImpl;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.InitializerElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.VariableElementHandle;
import tk.labyrinth.jaap.model.element.secondary.ParameterizableElementHandle;
import tk.labyrinth.jaap.model.factory.ElementHandleFactoryBase;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.synthetic.element.SyntheticElementHandle;
import tk.labyrinth.jaap.synthetic.element.SyntheticPackageElementHandle;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class LangmodelElementHandleFactoryImpl extends ElementHandleFactoryBase implements
		LangmodelElementHandleFactory,
		ProcessingContextAware {

	private final LangmodelElementFactory elementFactory;

	private final LangmodelTypeMirrorFactory typeMirrorFactory;

	@Getter
	private ProcessingContext processingContext;

	@Override
	public void acceptProcessingContext(ProcessingContext processingContext) {
		this.processingContext = processingContext;
	}

	@Nullable
	@Override
	public ElementHandle find(ElementSignature elementSignature) {
		Objects.requireNonNull(elementSignature, "elementSignature");
		//
		ElementHandle result;
		{
			Element element = elementFactory.findElement(elementSignature);
			if (element != null) {
				result = get(element);
			} else {
				if (elementSignature.matchesPackage()) {
					result = new SyntheticElementHandle(this, elementSignature);
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	@Override
	public ElementHandle find(String elementSignatureString) {
		return find(ElementSignature.of(elementSignatureString));
	}

	@Override
	public FieldElementHandle findField(Class<?> type, String fieldName) {
		// FIXME: Make it not fail on null.
		TypeElementHandle typeElementHandle = getType(type);
		return getField(VariableElementUtils.resolveField(
				processingContext.getProcessingEnvironment(), type, fieldName));
	}

	@Override
	public FieldElementHandle findField(String fieldFullName) {
		// FIXME: Make it not fail on null.
		return getField(VariableElementUtils.resolveField(
				processingContext.getProcessingEnvironment(), fieldFullName));
	}

	@Nullable
	@Override
	public MethodElementHandle findMethod(Class<?> type, String methodSimpleSignatureString) {
		ExecutableElement executableElement = elementFactory.findMethod(type, methodSimpleSignatureString);
		return executableElement != null ? getMethod(executableElement) : null;
	}

	@Nullable
	@Override
	public MethodElementHandle findMethod(Class<?> cl, String methodName, List<Class<?>> parameterClasses) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public MethodElementHandle findMethod(MethodFullSignature methodFullSignature) {
		ExecutableElement executableElement = elementFactory.findMethod(methodFullSignature);
		return executableElement != null ? getMethod(executableElement) : null;
	}

	@Nullable
	@Override
	public MethodElementHandle findMethod(String methodFullSignatureString) {
		return findMethod(MethodFullSignature.of(methodFullSignatureString));
	}

	@Nullable
	@Override
	public MethodElementHandle findMethodByName(Class<?> cl, String methodName) {
		List<Method> methods = Stream.of(cl.getDeclaredMethods())
				.filter(declaredMethod -> Objects.equals(declaredMethod.getName(), methodName))
				.toList();
		//
		if (methods.size() > 1) {
			throw new IllegalArgumentException();
		}
		//
		return !methods.isEmpty() ? getMethod(methods.get(0)) : null;
	}

	@Nullable
	@Override
	public ParameterizableElementHandle findParameterizable(ElementSignature parameterizableElementSignature) {
		Parameterizable parameterizable = elementFactory.findParameterizable(parameterizableElementSignature);
		return parameterizable != null
				? new LangmodelParameterizedElementHandleImpl(this, parameterizable)
				: null;
	}

	@Override
	public ParameterizableElementHandle findParameterizable(String parameterizableElementSignatureString) {
		return findParameterizable(ElementSignature.of(parameterizableElementSignatureString));
	}

	@Nullable
	@Override
	public TypeElementHandle findType(String typeSignatureString) {
		return findType(TypeSignature.ofValid(typeSignatureString));
	}

	@Nullable
	@Override
	public TypeElementHandle findType(TypeSignature typeSignature) {
		Objects.requireNonNull(typeSignature, "typeSignature");
		//
		TypeElement typeElement = elementFactory.findType(typeSignature);
		return typeElement != null ? getType(typeElement) : null;
	}

	@Nullable
	@Override
	public TypeParameterElementHandle findTypeParameter(Class<?> type, String typeParameterName) {
		return getType(type).findTypeParameter(typeParameterName);
	}

	@Nullable
	@Override
	public TypeParameterElementHandle findTypeParameter(String typeParameterSignature) {
		TypeParameterElementHandle result;
		{
			Pair<String, String> split = SignatureSeparators.split(
					typeParameterSignature,
					SignatureSeparators.TYPE_PARAMETER);
			if (split.getRight() != null) {
				String typeParameterName = split.getRight();
				ParameterizableElementHandle parent = findParameterizable(split.getLeft());
				//
				if (parent != null) {
					result = parent.getTypeParameters().stream()
							.filter(typeParameter -> typeParameter.getName().contentEquals(typeParameterName))
							.findFirst()
							.orElse(null);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public ElementHandle get(Class<?> type) {
		return get(elementFactory.get(type));
	}

	@Override
	public ElementHandle get(Element element) {
		return new LangmodelElementHandleImpl(element, this);
	}

	@Override
	public ConstructorElementHandle getConstructor(Element element) {
		return getConstructor(ElementUtils.requireExecutable(element));
	}

	@Override
	public ConstructorElementHandle getConstructor(ExecutableElement executableElement) {
		return new LangmodelConstructorElementHandleImpl(
				this,
				executableElement,
				processingContext.getTypeHandleFactory());
	}

	@Override
	public ExecutableElementHandle getExecutable(Element element) {
		return getExecutable(ElementUtils.requireExecutable(element));
	}

	@Override
	public ExecutableElementHandle getExecutable(ExecutableElement executableElement) {
		return new LangmodelExecutableElementHandleImpl(this, executableElement);
	}

	@Override
	public FieldElementHandle getField(Element element) {
		return getField(ElementUtils.requireVariable(element));
	}

	@Override
	public FieldElementHandle getField(VariableElement variableElement) {
		return new LangmodelFieldElementHandleImpl(
				processingContext.getElementHandleFactory(),
				processingContext.getTypeHandleFactory(),
				variableElement);
	}

	@Override
	public FormalParameterElementHandle getFormalParameter(Element element) {
		return getFormalParameter(ElementUtils.requireFormalParameter(element));
	}

	@Override
	public FormalParameterElementHandle getFormalParameter(VariableElement variableElement) {
		return new LangmodelFormalParameterElementHandleImpl(
				processingContext.getElementHandleFactory(),
				processingContext.getTypeHandleFactory(),
				variableElement);
	}

	@Override
	public InitializerElementHandle getInitializer(Element element) {
		return getInitializer(ElementUtils.requireExecutable(element));
	}

	@Override
	public InitializerElementHandle getInitializer(ExecutableElement executableElement) {
		return new LangmodelInitializerElementHandleImpl(this, executableElement);
	}

	@Override
	public MethodElementHandle getMethod(Element element) {
		return getMethod(ElementUtils.requireExecutable(element));
	}

	@Override
	public MethodElementHandle getMethod(ExecutableElement executableElement) {
		return new LangmodelMethodElementHandleImpl(
				this,
				executableElement,
				processingContext.getTypeHandleFactory());
	}

	@Override
	public MethodElementHandle getMethod(Method method) {
		return getMethod(elementFactory.getMethod(method));
	}

	@Override
	public PackageElementHandle getPackage(Element element) {
		return getPackage(elementFactory.getPackage(element));
	}

	@Override
	public PackageElementHandle getPackage(PackageElement packageElement) {
		return new LangmodelPackageElementHandleImpl(this, packageElement);
	}

	@Override
	public PackageElementHandle getPackage(ElementSignature packageElementSignature) {
		PackageElementHandle result;
		{
			PackageElement packageElement = elementFactory.findPackage(packageElementSignature);
			if (packageElement != null) {
				result = getPackage(packageElement);
			} else {
				result = new SyntheticPackageElementHandle(this, packageElementSignature.toString());
			}
		}
		return result;
	}

	@Override
	public PackageElementHandle getPackage(String packageSignatureString) {
		return getPackage(ElementSignature.of(packageSignatureString));
	}

	@Override
	public PackageElementHandle getPackageOf(Class<?> childType) {
		return getPackage(elementFactory.getPackageOf(childType));
	}

	@Override
	public ProcessingEnvironment getProcessingEnvironment() {
		return processingContext.getProcessingEnvironment();
	}

	@Override
	public TypeElementHandle getType(Class<?> type) {
		return getType(elementFactory.getType(type));
	}

	@Override
	public TypeElementHandle getType(Element element) {
		return getType(ElementUtils.requireType(element));
	}

	@Override
	public TypeElementHandle getType(String typeSignatureString) {
		return getType(elementFactory.getType(typeSignatureString));
	}

	@Override
	public TypeElementHandle getType(TypeElement typeElement) {
		return new LangmodelTypeElementHandleImpl(
				this,
				typeElement,
				processingContext.getTypeHandleFactory());
	}

	@Override
	public TypeElementHandle getType(TypeMirror typeMirror) {
		return getType(TypeElementUtils.get(processingContext.getProcessingEnvironment(), typeMirror));
	}

	@Override
	public TypeParameterElementHandle getTypeParameter(Element element) {
		return getTypeParameter(ElementUtils.requireTypeParameter(element));
	}

	@Override
	public TypeParameterElementHandle getTypeParameter(Class<?> type, String typeParameterName) {
		TypeParameterElementHandle result = findTypeParameter(type, typeParameterName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"type = " + type + ", " +
					"typeParameterName = " + typeParameterName);
		}
		return result;
	}

	@Override
	public TypeParameterElementHandle getTypeParameter(TypeParameterElement typeParameterElement) {
		return new LangmodelTypeParameterElementHandleImpl(
				processingContext.getElementHandleFactory(),
				processingContext.getTypeHandleFactory(),
				typeParameterElement);
	}

	@Override
	public VariableElementHandle getVariable(Element element) {
		return getVariable(ElementUtils.requireVariable(element));
	}

	@Override
	public VariableElementHandle getVariable(VariableElement variableElement) {
		return new LangmodelVariableElementHandleImpl(
				processingContext.getElementHandleFactory(),
				processingContext.getTypeHandleFactory(),
				variableElement);
	}
}
