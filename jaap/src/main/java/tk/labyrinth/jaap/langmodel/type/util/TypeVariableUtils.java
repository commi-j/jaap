package tk.labyrinth.jaap.langmodel.type.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.handle.element.util.TypeParameterElementUtils;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeVariable;

public class TypeVariableUtils {

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, TypeVariable typeVariable) {
		return TypeDescription.builder()
				.fullName(TypeParameterElementUtils.getSignature(
						processingEnvironment,
						ElementUtils.requireTypeParameter(typeVariable.asElement())))
				.build();
	}
}
