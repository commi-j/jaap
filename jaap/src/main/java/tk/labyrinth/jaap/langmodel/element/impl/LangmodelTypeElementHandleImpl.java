package tk.labyrinth.jaap.langmodel.element.impl;

import com.google.common.collect.Streams;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelTypeElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.langmodel.util.LangmodelUtils;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.MethodSelector;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class LangmodelTypeElementHandleImpl implements LangmodelTypeElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	TypeElement typeElement;

	LangmodelTypeHandleFactory typeHandleFactory;

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		return elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(typeElement.asType());
	}

	@Override
	public ElementHandle asElement() {
		return elementHandleFactory.get(typeElement);
	}

	@Nullable
	@Override
	public AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return findDirectAnnotation(elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType));
	}

	@Nullable
	@Override
	public TypeElementHandle findSuperclass() {
		TypeMirror superclass = typeElement.getSuperclass();
		return superclass != null ? elementHandleFactory.getType(superclass) : null;
	}

	@Override
	public Stream<? extends TypeElementHandle> getAllTypeStream() {
		Queue<TypeElementHandle> unprocessedTypeElements = new LinkedList<>();
		{
			// Instead of referring to this we should always refer to handle obtained from factory.
			// This is for cases like enhanced type where this does not represent accurate type.
			// This problem is quite similar to the one occurring in Spring self references behind proxy.
			unprocessedTypeElements.add(elementHandleFactory.getType(typeElement));
		}
		Set<TypeElementHandle> processedTypeElements = new HashSet<>();
		//
		List<TypeElementHandle> result = new ArrayList<>();
		while (!unprocessedTypeElements.isEmpty()) {
			TypeElementHandle typeElement = unprocessedTypeElements.remove();
			if (processedTypeElements.add(typeElement)) {
				result.add(typeElement);
				unprocessedTypeElements.addAll(typeElement.getDirectSupertypes());
			}
		}
		if (isInterface()) {
			TypeElementHandle objectTypeElement = elementHandleFactory.getType(Object.class);
			result.add(objectTypeElement);
		}
		return result.stream();
	}

	@Override
	public String getCanonicalName() {
		return typeElement.getQualifiedName().toString();
	}

	@Override
	public Stream<ConstructorElementHandle> getConstructorStream() {
		return typeElement.getEnclosedElements().stream()
				.filter(ElementUtils::isConstructor)
				.map(elementHandleFactory::getConstructor);
	}

	@Override
	public Stream<FieldElementHandle> getDeclaredFieldStream() {
		return TypeElementUtils.getDeclaredFields(typeElement).map(elementHandleFactory::getField);
	}

	@Override
	public Stream<? extends MethodElementHandle> getDeclaredMethodStream() {
		return typeElement.getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getKind() == ElementKind.METHOD)
				.map(ExecutableElement.class::cast)
				.map(elementHandleFactory::getMethod);
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return typeElement.getAnnotationMirrors().stream()
				.map(annotationMirror -> elementHandleFactory.getProcessingContext().getAnnotationHandle(
						typeElement,
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public Stream<? extends TypeElementHandle> getDirectSupertypeStream() {
		return elementHandleFactory.getProcessingEnvironment().getTypeUtils()
				.directSupertypes(typeElement.asType()).stream()
				.map(elementHandleFactory::getType);
	}

	@Override
	public FieldElementHandle getField(String fieldName) {
		// TODO: Support name shadowing (multiple fields may be yielded).
		return getAllFieldStream()
				.filter(field -> Objects.equals(field.getName(), fieldName))
				.collect(CollectorUtils.findOnly());
	}

	@Override
	public String getLongName() {
		return TypeElementUtils.getLongName(typeElement);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return new MergedAnnotationContext(annotationTypeHandle, asElement(), mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(
			String annotationTypeSignature,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				elementHandleFactory.getProcessingContext().getAnnotationTypeHandle(annotationTypeSignature),
				mergedAnnotationSpecification);
	}

	@Override
	public Stream<? extends TypeElementHandle> getNestedTypeStream() {
		return TypeElementUtils.getNestedTypeElementStream(typeElement).map(elementHandleFactory::getType);
	}

	@Override
	public PackageElementHandle getPackage() {
		return elementHandleFactory.getPackage(TypeElementUtils.getPackage(typeElement));
	}

	@Override
	public String getPackageQualifiedName() {
		return TypeElementUtils.getPackageQualifiedName(typeElement);
	}

	@Nullable
	@Override
	public ElementHandle getParent() {
		Element enclosingElement = typeElement.getEnclosingElement();
		return enclosingElement != null ? elementHandleFactory.get(enclosingElement) : null;
	}

	@Override
	public String getQualifiedName() {
		return typeElement.getQualifiedName().toString();
	}

	@Override
	public TypeSignature getSignature() {
		return TypeSignature.ofValid(TypeElementUtils.getSignature(typeElement));
	}

	@Override
	public String getSimpleName() {
		return typeElement.getSimpleName().toString();
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		return typeElement.getTypeParameters().stream().map(elementHandleFactory::getTypeParameter);
	}

	@Override
	public boolean isAnnotationType() {
		return TypeElementUtils.isAnnotationType(typeElement);
	}

	@Override
	public boolean isInterface() {
		return typeElement.getKind() == ElementKind.INTERFACE;
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		Element element = TypeElementUtils.selectMember(
				elementHandleFactory.getProcessingEnvironment(),
				typeElement,
				selector);
		//
		return element != null ? elementHandleFactory.get(element) : null;
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(MethodSelector methodSelector) {
		ExecutableElement executableElement = TypeElementUtils.selectMethod(
				elementHandleFactory.getProcessingEnvironment(),
				typeElement,
				methodSelector);
		//
		return executableElement != null ? elementHandleFactory.getMethod(executableElement) : null;
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(MethodSimpleSignature methodSimpleSignature) {
		return selectMethodElement(
				methodSimpleSignature.getName(),
				// FIXME: Use #getParameterString().
				methodSimpleSignature.getParameters().stream().map(typeHandleFactory::get));
	}

	@Nullable
	@Override
	public MethodElementHandle selectMethodElement(String methodName, List<TypeHandle> argumentTypes) {
		return getNonOverridenMethodStream(methodName)
				.filter(method -> {
					boolean result;
					{
						List<FormalParameterElementHandle> formalParameters = method.getFormalParameters();
						// FIXME: Support vararg.
						if (argumentTypes.size() == formalParameters.size()) {
							// Filtering out methods with parameters unassignable from provided arguments.
							//
							//noinspection UnstableApiUsage
							result = Streams.zip(argumentTypes.stream(), formalParameters.stream(), Pair::of)
									.allMatch(pair -> elementHandleFactory.getProcessingEnvironment().getTypeUtils().isAssignable(
											pair.getKey().getTypeMirror(), pair.getValue().getType().getTypeMirror()));
						} else {
							result = false;
						}
					}
					return result;
				}).min(Comparator
						.comparing(
								Function.identity(),
								LangmodelTypeElementHandleImpl::compareMethods)
						.thenComparing(comparatorOfMethodParameters()))
				.orElse(null);
	}

	@Override
	public String toString() {
		return getSignatureString();
	}

	@Override
	public DeclaredTypeHandle toType() {
		return typeHandleFactory.getDeclared(GenericContext.empty(), typeElement);
	}

	@Deprecated
	@SuppressWarnings("DeprecatedIsStillUsed")
	private static Comparator<MethodElementHandle> comparatorOfMethodParameters() {
		return Comparator.comparing(
				method -> method.getFormalParameterStream().map(LangmodelUtils::extractVariableElement)
						.map(VariableElement::asType)
						.findFirst().orElseThrow(() -> new IllegalArgumentException("Methods with no parameters must not be compared here")),
				TypeMirrorUtils::compareConsideringPrimitives);
	}

	@Deprecated
	@SuppressWarnings("DeprecatedIsStillUsed")
	private static int compareMethods(MethodElementHandle first, MethodElementHandle second) {
		int result;
		{
			TypeElementHandle firstParent = first.getParent();
			TypeElementHandle secondParent = second.getParent();
			if (!Objects.equals(firstParent, secondParent)) {
				// TODO: Replace with single comparation.
				if (firstParent.isAssignableTo(secondParent)) {
					result = -1;
				} else if (firstParent.isAssignableFrom(secondParent)) {
					result = 1;
				} else {
					result = 0;
				}
			} else {
				result = 0;
			}
		}
		return result;
//		Comparator.<MethodElementHandle>comparing(method -> 0).<MethodElementHandle>thenComparing(
//				method -> method.getParameters()
//						.map(ParameterElementHandle::getVariableElement)
//						.map(VariableElement::asType)
//						.findFirst().orElse(null),
//				TypeMirrorUtils::compareConsideringPrimitives
//		)
	}
}
