package tk.labyrinth.jaap.langmodel.factory;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.element.util.TypeParameterElementUtils;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@EqualsAndHashCode
@RequiredArgsConstructor
public class LangmodelTypeMirrorFactoryImpl implements LangmodelTypeMirrorFactory {

	private final LangmodelElementFactory elementFactory;

	private final ProcessingEnvironment processingEnvironment;

	@Nullable
	@Override
	public TypeMirror find(String fullTypeSignature) {
		Objects.requireNonNull(fullTypeSignature, "fullTypeSignature");
		//
		TypeMirror result;
		if (fullTypeSignature.endsWith("[]")) {
			TypeMirror subtype = find(fullTypeSignature.substring(0, fullTypeSignature.length() - 2));
			if (subtype != null) {
				result = processingEnvironment.getTypeUtils().getArrayType(subtype);
			} else {
				result = null;
			}
		} else {
			switch (fullTypeSignature) {
				case "boolean":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
					break;
				case "byte":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BYTE);
					break;
				case "char":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.CHAR);
					break;
				case "double":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.DOUBLE);
					break;
				case "float":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT);
					break;
				case "int":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT);
					break;
				case "long":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG);
					break;
				case "short":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.SHORT);
					break;
				case "void":
					result = processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID);
					break;
				default:
					TypeElement typeElement = elementFactory.findType(fullTypeSignature);
					if (typeElement != null) {
						result = TypeMirrorUtils.erasure(processingEnvironment, typeElement);
					} else {
						result = null;
					}
			}
		}
		return result;
	}

	@Nullable
	@Override
	public TypeMirror find(TypeDescription typeDescription) {
		TypeMirror result;
		if (typeDescription.getArrayDepth() != null) {
			// Array
			//
			TypeMirror typeMirror = find(typeDescription.toBuilder()
					.arrayDepth(typeDescription.getArrayDepth() == 1 ? null : typeDescription.getArrayDepth() - 1)
					.build());
			result = typeMirror != null
					? processingEnvironment.getTypeUtils().getArrayType(typeMirror)
					: null;
		} else if (typeDescription.getParameters() != null) {
			// Parameterized
			//
			List<TypeMirror> parameters = typeDescription.getParameters().stream()
					.map(this::find)
					.collect(Collectors.toList());
			if (!parameters.contains(null)) {
				result = processingEnvironment.getTypeUtils().getDeclaredType(
						TypeElementUtils.get(processingEnvironment, typeDescription.getFullName()),
						parameters.toArray(TypeMirror[]::new));
			} else {
				result = null;
			}
		} else if (typeDescription.getLowerBound() != null || typeDescription.getUpperBound() != null) {
			// Variable or Wildcard (with bounds)
			//
			throw new NotImplementedException();
		} else {
			// Plain or Primitive or Raw or Variable or Wildcard
			//
			String typeFullName = typeDescription.getFullName();
			//
			if (typeFullName.contains(SignatureSeparators.TYPE_PARAMETER)) {
				// Variable (without bounds)
				//
				result = findVariable(typeDescription);
			} else if (typeFullName.contains(SignatureSeparators.PACKAGE_OR_TYPE)) {
				// Plain or Raw
				//
				TypeElement typeElement = TypeElementUtils.get(processingEnvironment, typeFullName);
				if (typeElement.getTypeParameters().isEmpty()) {
					// Plain
					//
					result = typeElement.asType();
				} else {
					// Raw
					//
					result = TypeMirrorUtils.erasure(processingEnvironment, typeElement);
				}
			} else {
				// Primitive or Wildcard
				//
				if (Objects.equals(typeFullName, "?")) {
					// Wildcard (without bounds)
					//
					result = findWildcard(typeDescription);
				} else {
					// Primitive
					//
					TypeMirror keywordType = TypeMirrorUtils.findKeywordType(processingEnvironment, typeFullName);
					if (keywordType != null) {
						// Primitive
						//
						result = keywordType;
					} else {
						// Unknown one-segment entry
						//  FIXME: May be a class within default package.
						//
						result = null;
					}
				}
			}
		}
		return result;
	}

	@Nullable
	@Override
	public TypeMirror find(TypeSignature typeSignature) {
		Objects.requireNonNull(typeSignature, "typeSignature");
		//
		return find(typeSignature.toString());
	}

	@Nullable
	@Override
	public TypeVariable findVariable(TypeDescription typeDescription) {
		TypeVariable result;
		{
			TypeParameterElement typeParameterElement = elementFactory.findTypeParameter(typeDescription.getFullName());
			//
			if (typeParameterElement != null) {
				result = TypeParameterElementUtils.getTypeVariable(typeParameterElement);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public WildcardType findWildcard(TypeDescription typeDescription) {
		// TODO: requireWildcard.
		//
		// TODO: super, extends.
		return processingEnvironment.getTypeUtils().getWildcardType(null, null);
	}

	@Override
	public TypeMirror get(GenericContext genericContext, Type type) {
		// TODO: Move code from TMUtils to this class.
		return TypeMirrorUtils.get(processingEnvironment, type);
	}

	@Override
	public TypeMirror get(TypeDescription typeDescription) {
		TypeMirror result = find(typeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescription = " + typeDescription);
		}
		return result;
	}

	@Override
	public DeclaredType getDeclared(GenericContext genericContext, Type type) {
		return DeclaredTypeUtils.requireParameterized(TypeMirrorUtils.requireDeclaredType(get(genericContext, type)));
	}
}
