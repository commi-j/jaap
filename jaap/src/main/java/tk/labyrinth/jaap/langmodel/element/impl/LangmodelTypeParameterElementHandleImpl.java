package tk.labyrinth.jaap.langmodel.element.impl;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.element.util.TypeParameterElementUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.element.LangmodelTypeParameterElementHandle;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementHandleFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeHandleFactory;
import tk.labyrinth.jaap.model.element.ElementHandle;

import javax.lang.model.element.TypeParameterElement;
import java.util.stream.Stream;

@Value
public class LangmodelTypeParameterElementHandleImpl implements LangmodelTypeParameterElementHandle {

	LangmodelElementHandleFactory elementHandleFactory;

	LangmodelTypeHandleFactory typeHandleFactory;

	TypeParameterElement typeParameterElement;

	@Override
	public Stream<TypeHandle> getBoundStream() {
		return typeParameterElement.getBounds().stream()
				.map(bound -> typeHandleFactory.get(GenericContext.empty(), bound));
	}

	@Override
	public String getName() {
		return typeParameterElement.getSimpleName().toString();
	}

	@Override
	public @NonNull ElementHandle getParent() {
		return elementHandleFactory.get(typeParameterElement.getEnclosingElement());
	}

	@Override
	public String getSignature() {
		return TypeParameterElementUtils.getSignature(
				elementHandleFactory.getProcessingEnvironment(),
				typeParameterElement);
	}

	@Override
	public TypeHandle getType() {
		return typeHandleFactory.get(GenericContext.empty(), typeParameterElement.asType());
	}

	@Override
	public String toString() {
		return getSignatureString();
	}
}
