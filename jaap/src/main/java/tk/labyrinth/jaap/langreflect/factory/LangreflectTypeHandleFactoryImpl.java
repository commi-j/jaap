package tk.labyrinth.jaap.langreflect.factory;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.factory.ElementHandleFactory;
import tk.labyrinth.jaap.model.factory.TypeHandleFactoryBase;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

// TODO: Make use of it.
public class LangreflectTypeHandleFactoryImpl extends TypeHandleFactoryBase implements LangreflectTypeHandleFactory {

	public LangreflectTypeHandleFactoryImpl(ElementHandleFactory elementHandleFactory) {
		super(elementHandleFactory);
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public TypeHandle find(TypeSignature typeSignature) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public TypeHandle find(TypeDescription typeDescription) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle get(GenericContext genericContext, Class<?> type) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ParameterizedTypeHandle getParameterized(GenericContext genericContext, Type type) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
