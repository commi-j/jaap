package tk.labyrinth.jaap.langreflect.util;

import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.lang.reflect.Method;
import java.util.stream.Stream;

public class MethodUtils {

	public static MethodFullSignature getFullSignature(Method method) {
		return getSimpleSignature(method).toFull(TypeSignature.of(method.getDeclaringClass()).toString());
	}

	public static MethodSimpleSignature getSimpleSignature(Method method) {
		return MethodSimpleSignature.of(
				method.getName(),
				Stream.of(method.getParameters()).map(ParameterUtils::getSignature));
	}
}
