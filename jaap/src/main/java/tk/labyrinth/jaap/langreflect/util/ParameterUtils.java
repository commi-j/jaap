package tk.labyrinth.jaap.langreflect.util;

import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.lang.reflect.Parameter;

public class ParameterUtils {

	public static TypeSignature getSignature(Parameter parameter) {
		return TypeSignature.of(parameter.getType());
	}
}
