package tk.labyrinth.jaap.synthetic.element;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Stream;

@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SyntheticMethodElementHandle implements MethodElementHandle {

	private final MethodDeclaration declaration;

	private final TypeSignature parentSignature;

	@EqualsAndHashCode.Exclude
	@Getter
	private final ProcessingContext processingContext;

	@Override
	public ElementHandle asElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public int getFormalParameterCount() {
		return declaration.getFormalParameters().size();
	}

	@Override
	public Stream<FormalParameterElementHandle> getFormalParameterStream() {
		return declaration.getFormalParameters().stream().map(parameter ->
				SyntheticFormalParameterElementHandle.from(this, parameter));
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(AnnotationTypeHandle annotationTypeHandle, MergedAnnotationSpecification mergedAnnotationSpecification) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(Class<? extends Annotation> annotationType, MergedAnnotationSpecification mergedAnnotationSpecification) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public MergedAnnotationContext getMergedAnnotationContext(String annotationTypeSignature, MergedAnnotationSpecification mergedAnnotationSpecification) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<MethodModifier> getModifierStream() {
		return declaration.getModifiers().stream();
	}

	@Override
	public String getName() {
		return declaration.getName();
	}

	@Nonnull
	@Override
	public TypeElementHandle getParent() {
		return processingContext.getTypeElementHandle(parentSignature);
	}

	@Override
	public TypeHandle getReturnType() {
		return processingContext.getTypeHandle(declaration.getReturnType());
	}

	@Override
	public String getSignature() {
		return parentSignature + SignatureSeparators.EXECUTABLE + declaration.getSignature().toString();
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public Stream<TypeParameterElementHandle> getTypeParameterStream() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitAbstractModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitDefaultModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitPublicModifier() {
		return declaration.getModifiers().contains(JavaMethodModifier.PUBLIC);
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		return declaration.getModifiers().contains(JavaMethodModifier.STATIC);
	}

	@Override
	public boolean isEffectivelyAbstract() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyDefault() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonAbstract() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonDefault() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		// FIXME: Varargs
		return getFullSignature().toString();
	}

	public static SyntheticMethodElementHandle from(
			ProcessingContext processingContext,
			TypeSignature parentSignature,
			MethodDeclaration declaration) {
		return new SyntheticMethodElementHandle(declaration, parentSignature, processingContext);
	}
}
