package tk.labyrinth.jaap.synthetic.element;

import lombok.Value;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.ConstructorElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.ElementHandleBase;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.InitializerElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.element.VariableElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.factory.ElementHandleFactory;
import tk.labyrinth.jaap.model.signature.ElementSignature;

import javax.annotation.Nullable;

/**
 * Currently only supports packages.
 */
@Value
public class SyntheticElementHandle extends ElementHandleBase implements ElementHandle {

	ElementHandleFactory elementHandleFactory;

	ElementSignature elementSignature;

	@Override
	public ConstructorElementHandle asConstructorElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ExecutableElementHandle asExecutableElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FieldElementHandle asFieldElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FormalParameterElementHandle asFormalParameterElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public InitializerElementHandle asInitializerElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public MethodElementHandle asMethodElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PackageElementHandle asPackageElement() {
		return elementHandleFactory.getPackage(elementSignature);
	}

	@Override
	public TypeElementHandle asTypeElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeParameterElementHandle asTypeParameterElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public VariableElementHandle asVariableElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getName() {
		return getElementSignature().getContent();
	}

	@Nullable
	@Override
	public ElementHandle getParent() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getSignatureString() {
		return elementSignature.toString();
	}

	@Override
	public boolean isConstructorElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isExecutableElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isFieldElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isFormalParameterElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isInitializerElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isMethodElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isPackageElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isTypeElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isTypeParameterElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isVariableElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		return getSignatureString();
	}
}
