package tk.labyrinth.jaap.synthetic.element;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.element.ExecutableElementHandle;
import tk.labyrinth.jaap.model.element.FormalParameterElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;

import java.util.List;

@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SyntheticFormalParameterElementHandle implements FormalParameterElementHandle {

	private final FormalParameterDeclaration declaration;

	private final SyntheticMethodElementHandle parent;

	@Override
	public int getIndex() {
		// FIXME: Should not override.
		List<FormalParameterElementHandle> formalParameters = parent.getFormalParameters();
		return formalParameters.indexOf(this);
	}

	@Override
	public String getName() {
		return declaration.getName();
	}

	@Override
	public @NonNull ExecutableElementHandle getParent() {
		return parent.asExecutableElement();
	}

	@Override
	public String getSignature() {
		return parent.getSignatureString() + "#" + getIndex();
	}

	@Override
	public TypeElementHandle getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	public TypeHandle getType() {
		return parent.getProcessingContext().getTypeHandle(declaration.getType());
	}

	@Override
	public String toString() {
		return getSignatureString();
	}

	public static SyntheticFormalParameterElementHandle from(SyntheticMethodElementHandle parent, FormalParameterDeclaration declaration) {
		return new SyntheticFormalParameterElementHandle(declaration, parent);
	}
}
