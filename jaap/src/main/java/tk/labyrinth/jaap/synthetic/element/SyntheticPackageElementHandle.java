package tk.labyrinth.jaap.synthetic.element;

import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.factory.ElementHandleFactory;

import javax.annotation.Nullable;

@Value
public class SyntheticPackageElementHandle implements PackageElementHandle {

	ElementHandleFactory elementHandleFactory;

	String packageQualifiedName;

	@Override
	public ElementHandle asElement() {
		// FIXME: Problem is that EHF must know we're requesting synth package.
		throw new NotImplementedException();
//		return elementHandleFactory.get();
	}

	@Override
	public String getQualifiedName() {
		return packageQualifiedName;
	}

	@Override
	public String getSimpleName() {
		int lastIndexOfDot = packageQualifiedName.lastIndexOf('.');
		return lastIndexOfDot != -1 ? packageQualifiedName.substring(lastIndexOfDot + 1) : packageQualifiedName;
	}

	@Override
	public boolean isSynthetic() {
		return true;
	}

	@Nullable
	@Override
	public ElementHandle selectMember(EntitySelector selector) {
		ElementHandle result;
		if (selector.canBePackage() || selector.canBeType()) {
			result = elementHandleFactory.find(getSignatureString() + "." + selector.getSimpleName());
		} else {
			throw new IllegalArgumentException("Wrong Selector: " +
					"selector = " + selector + ", " +
					"this = " + this);
		}
		return result;
	}
}
