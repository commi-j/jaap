package tk.labyrinth.jaap.typical.jlm;

import lombok.Value;
import tk.labyrinth.jaap.typical.core.TypeDescriptor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Stream;

@Value
public class TypeMirrorTypeDescriptor implements TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor> {

	ProcessingEnvironment processingEnvironment;

	TypeMirror typeMirror;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		TypeMirrorTypeDescriptor other = (TypeMirrorTypeDescriptor) obj;
		return processingEnvironment.getTypeUtils().isSameType(typeMirror, other.typeMirror);
	}

	@Override
	public Stream<TypeMirrorTypeDescriptor> getDeclaredTypes() {
		return InternalJlmUtils.getDeclaredTypes(processingEnvironment, typeMirror).stream()
				.map(declaredType -> new TypeMirrorTypeDescriptor(processingEnvironment, declaredType));
	}

	@Override
	public TypeMirror getType() {
		return typeMirror;
	}

	@Override
	public int hashCode() {
		return typeMirror.toString().hashCode();
	}

	@Override
	public String toString() {
		return typeMirror.toString();
	}
}
