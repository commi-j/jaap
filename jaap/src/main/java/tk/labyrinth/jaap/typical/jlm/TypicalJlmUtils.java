package tk.labyrinth.jaap.typical.jlm;

import tk.labyrinth.jaap.typical.core.TypicalCoreUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Stream;

public class TypicalJlmUtils {

	public static Stream<TypeMirror> getDeclaredTypes(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return TypicalCoreUtils.getDeclaredTypes(new TypeMirrorTypeDescriptor(processingEnvironment, typeMirror))
				.map(TypeMirrorTypeDescriptor::getType);
	}
}
