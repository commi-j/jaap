package tk.labyrinth.jaap.typical.jlr;

import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.Nullable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InternalJlrUtils {

	private static Stream<Class<?>> doGetDeclaredTypes(Class<?> type) {
		Class<?> superclass = type.getSuperclass();
		return StreamUtils.concat(
				Stream.of(type),
				superclass != null ? doGetDeclaredTypes(superclass) : Stream.empty(),
				Stream.of(type.getInterfaces()).flatMap(InternalJlrUtils::doGetDeclaredTypes));
	}

	private static Stream<Class<?>> doGetDeclaredTypes(ParameterizedType parameterizedType) {
		return doGetDeclaredTypes((Class<?>) parameterizedType.getRawType());
	}

	private static Stream<Class<?>> doGetDeclaredTypes(Type type) {
		Stream<Class<?>> result;
		if (type instanceof Class) {
			result = doGetDeclaredTypes((Class<?>) type);
		} else if (type instanceof ParameterizedType) {
			result = doGetDeclaredTypes((ParameterizedType) type);
		} else if (type instanceof TypeVariable) {
			result = doGetDeclaredTypes((TypeVariable<?>) type);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	private static Stream<Class<?>> doGetDeclaredTypes(TypeVariable<?> typeVariable) {
		return Stream.of(typeVariable.getBounds()).flatMap(InternalJlrUtils::doGetDeclaredTypes);
	}

	@Nullable
	private static Class<?> findSuperclass(Class<?> type) {
		Class<?> result;
		if (type.isInterface()) {
			if (type.isAnnotation()) {
				result = null;
			} else {
				result = Object.class;
			}
		} else {
			result = type.getSuperclass();
		}
		return result;
	}

	@Nullable
	public static Class<?> findClass(Type type) {
		Class<?> result;
		if (type instanceof Class) {
			result = (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			result = (Class<?>) ((ParameterizedType) type).getRawType();
		} else if (type instanceof TypeVariable<?>) {
			result = null;
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	/**
	 * Cases:<br>
	 * - {@link Class} - <b>type</b>.{@link Class#getSuperclass() getSuperclass()}*;<br>
	 * - {@link ParameterizedType} - <b>type</b>.{@link ParameterizedType#getRawType() getRawType()}.{@link Class#getSuperclass() getSuperclass()}*;<br>
	 * <br>
	 * * If class is interface (but not annotation) it's superclass is considered to be {@link Object};<br>
	 *
	 * @param type non-null
	 *
	 * @return nullable
	 */
	@Nullable
	public static Class<?> findSuperclass(Type type) {
		Objects.requireNonNull(type, "type");
		//
		Class<?> result;
		if (type instanceof Class) {
			result = findSuperclass(getClass(type));
		} else if (type instanceof ParameterizedType) {
			result = findSuperclass(getClass(type));
		} else if (type instanceof TypeVariable<?>) {
			TypeVariable<?> typeVariable = (TypeVariable<?>) type;
			Type[] bounds = typeVariable.getBounds();
			if (bounds.length > 0) {
				Type firstBoundType = bounds[0];
				Class<?> firstBoundClass = getClass(firstBoundType);
			} else {
				result = Object.class;
			}
			throw new NotImplementedException(ExceptionUtils.render(type));
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	public static Class<?> getClass(Type type) {
		Class<?> result = findClass(type);
		if (result == null) {
			throw new IllegalArgumentException("No class found for type: " + type);
		}
		return result;
	}

	public static Set<Class<?>> getDeclaredTypes(Type type) {
		Set<Class<?>> result = doGetDeclaredTypes(type).collect(Collectors.toSet());
		if (isReferenceType(type)) {
			result.add(Object.class);
		}
		return result;
	}

	public static boolean isReferenceType(Type type) {
		boolean result;
		{
			Class<?> typeClass = findClass(type);
			if (typeClass != null) {
				// Array, Declared -> true
				// Primitive, Void -> false
				result = !(typeClass.isPrimitive() || typeClass == void.class);
			} else {
				// Variable
				result = true;
			}
		}
		return result;
	}
}
