package tk.labyrinth.misc4j.lang.struct;

import lombok.Value;

@Value
public class ParameterMapping<E, D extends TypeDescriptor<E, D>> {

	TypeDescriptor<E, D> from;

	TypeDescriptor<E, D> to;
}
