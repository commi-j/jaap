package tk.labyrinth.misc4j.lang.struct;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HierarchyUtils {

	public static <E, D extends TypeDescriptor<E, D>> TypeDescriptorChain<E, D> getChain(TypeDescriptor<E, D> subtype, TypeDescriptor<E, D> supertype) {
		if (!subtype.getRawDescriptor().isSubtype(supertype.getRawDescriptor())) {
			throw new IllegalArgumentException("Wrong relationship: subtype = " + subtype + ", supertype = " + supertype);
		}
		List<TypeDescriptorChain<E, D>> chains = new ArrayList<>();
		chains.add(TypeDescriptorChain.of(subtype));
		//
		TypeDescriptorChain<E, D> result = null;
		while (result == null) {
			if (chains.isEmpty()) {
				throw new IllegalStateException("Inconsistent hierarchy: subtype = " + subtype + ", supertype = " + supertype);
			}
			TypeDescriptorChain<E, D> currentChain = chains.remove(0);
			TypeDescriptor<E, D> currentDescriptor = currentChain.getLast();
			if (Objects.equals(currentDescriptor.getRawDescriptor(), supertype.getRawDescriptor())) {
				result = currentChain;
			} else {
				{
					TypeDescriptor<E, D> currentSuperclass = currentDescriptor.getSuperclass();
					if (currentSuperclass != null) {
						chains.add(currentChain.append(currentSuperclass));
					}
				}
				// TODO: Optimize to not add interfaces which were already added.
				currentDescriptor.getDirectSuperinterfaces().forEach(currentDirectSuperinterface ->
						chains.add(currentChain.append(currentDirectSuperinterface)));
			}
		}
		return result;
	}
}
