package tk.labyrinth.misc4j.lang.model;

import lombok.Value;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j.lang.struct.TypeDescriptor;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Stream;

@Value
public class TypeMirrorTypeDescriptor implements TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor> {

	ProcessingEnvironment environment;

	TypeMirror element;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		TypeMirrorTypeDescriptor other = (TypeMirrorTypeDescriptor) obj;
		return environment.getTypeUtils().isSameType(element, other.element);
	}

	@Override
	public TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor> getDefaultDescriptor() {
		return new TypeMirrorTypeDescriptor(environment, environment.getTypeUtils().asElement(element).asType());
	}

	@Override
	public Stream<TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor>> getDirectSuperinterfaces() {
		return TypeElementUtils.get(environment, element).getInterfaces().stream()
				.map(typeMirror -> new TypeMirrorTypeDescriptor(environment, typeMirror));
	}

	@Override
	public Stream<TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor>> getParameters() {
		Stream<TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor>> result;
		{
			if (element.getKind() == TypeKind.DECLARED) {
				result = ((DeclaredType) element).getTypeArguments().stream()
						.map(typeMirror -> new TypeMirrorTypeDescriptor(environment, typeMirror));
			} else {
				result = Stream.empty();
			}
		}
		return result;
	}

	@Override
	public TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor> getRawDescriptor() {
		return new TypeMirrorTypeDescriptor(environment, environment.getTypeUtils().erasure(element));
	}

	@Nullable
	@Override
	public TypeDescriptor<TypeMirror, TypeMirrorTypeDescriptor> getSuperclass() {
		TypeMirror typeMirror = TypeElementUtils.get(environment, element).getSuperclass();
		return typeMirror.getKind() != TypeKind.NONE ? new TypeMirrorTypeDescriptor(environment, typeMirror) : null;
	}

	@Override
	public int hashCode() {
		// It must be same for Descriptors with same TypeMirrors. May be there is a smarter way.
		return 0;
	}

	@Override
	public boolean isSubtype(TypeDescriptor<TypeMirror, ?> descriptor) {
		return environment.getTypeUtils().isSubtype(element, descriptor.getElement());
	}

	@Override
	public boolean isVariable() {
		return element.getKind() == TypeKind.TYPEVAR;
	}

	@Override
	public String toString() {
		return element.toString();
	}
}
