package tk.labyrinth.misc4j.lang.model;

import tk.labyrinth.misc4j.lang.struct.TypeDescriptor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.stream.Stream;

public class HierarchyUtils {

	public static Stream<TypeElement> getChain(ProcessingEnvironment environment, TypeElement subtype, TypeElement supertype) {
		return tk.labyrinth.misc4j.lang.struct.HierarchyUtils.getChain(
				new TypeMirrorTypeDescriptor(environment, subtype.asType()),
				new TypeMirrorTypeDescriptor(environment, supertype.asType())
		).getDescriptors().map(TypeDescriptor::getElement)
				.map(typeMirror -> environment.getTypeUtils().asElement(typeMirror))
				.map(TypeElement.class::cast);
	}
}
