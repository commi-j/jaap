package tk.labyrinth.misc4j.lang.struct;

import lombok.Value;

@Value
public class ParameterDescriptor<E, D extends TypeDescriptor<E, D>> {

	TypeDescriptor<E, D> parameter;

	TypeDescriptor<E, D> type;
}
