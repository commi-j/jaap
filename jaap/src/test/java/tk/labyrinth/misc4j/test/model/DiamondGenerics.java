package tk.labyrinth.misc4j.test.model;

import java.util.Map;

public class DiamondGenerics {

	public interface ABMap<A, B> extends Map<A, B> {
		// empty
	}

	public interface DeepStringLongMap extends KLongMap<String>, ABMap<String, Long>, StringVMap<Long> {
		// empty
	}

	public interface KLongMap<T> extends ABMap<T, Long> {
		// empty
	}

	public interface StringLongMap extends KLongMap<String>, StringVMap<Long> {
		// empty
	}

	public interface StringVMap<T> extends ABMap<String, T> {
		// empty
	}
}
