package tk.labyrinth.jaap.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.List;

@ExtendWithJaap
class ElementUtilsTest {

	@Test
	void testNavigate(ProcessingEnvironment processingEnvironment) {
		{
			// System.out.checkError();
			//
			Assertions.assertEquals(
					ExecutableElementUtils.resolve(processingEnvironment, PrintStream.class, "checkError()"),
					ElementUtils.navigate(
							processingEnvironment,
							TypeElementUtils.get(processingEnvironment, System.class),
							EntitySelectorChain.forMethod(List.of("out", "checkError"), List.of())));
		}
		{
			// BigDecimal.ZERO.ONE.signum()
			//
			Assertions.assertEquals(
					ExecutableElementUtils.resolve(processingEnvironment, BigDecimal.class, "signum()"),
					ElementUtils.navigate(
							processingEnvironment,
							TypeElementUtils.get(processingEnvironment, BigDecimal.class),
							EntitySelectorChain.forMethod(List.of("ZERO", "ONE", "signum"), List.of())));
		}
	}
}
