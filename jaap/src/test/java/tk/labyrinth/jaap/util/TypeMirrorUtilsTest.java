package tk.labyrinth.jaap.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.handle.element.util.VariableElementUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.model.generic.UnionTypeParameter;
import tk.labyrinth.jaap.test.model.ComplexParameters;
import tk.labyrinth.jaap.test.model.FieldSignatures;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ExtendWithJaap
class TypeMirrorUtilsTest {

	@Test
	void testCompareByAssignability(ProcessingEnvironment processingEnvironment) {
		TypeMirror doubleTypeMirror = TypeMirrorUtils.get(processingEnvironment, Double.class);
		TypeMirror integerTypeMirror = TypeMirrorUtils.get(processingEnvironment, Integer.class);
		TypeMirror numberTypeMirror = TypeMirrorUtils.get(processingEnvironment, Number.class);
		//
		Assertions.assertEquals(
				-1,
				TypeMirrorUtils.compareByAssignability(processingEnvironment, integerTypeMirror, numberTypeMirror));
		Assertions.assertEquals(
				0,
				TypeMirrorUtils.compareByAssignability(processingEnvironment, integerTypeMirror, integerTypeMirror));
		Assertions.assertEquals(
				1,
				TypeMirrorUtils.compareByAssignability(processingEnvironment, numberTypeMirror, integerTypeMirror));
		ContribAssertions.assertThrows(
				() -> TypeMirrorUtils.compareByAssignability(processingEnvironment, integerTypeMirror, doubleTypeMirror),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Incompatible: first = java.lang.Integer, second = java.lang.Double",
						fault.toString()));
	}

	// TODO
	@Test
	void testComputeDistance(ProcessingEnvironment processingEnvironment) {
		TypeMirror intTypeMirror = TypeMirrorUtils.get(processingEnvironment, int.class);
		TypeMirror integerTypeMirror = TypeMirrorUtils.get(processingEnvironment, Integer.class);
		TypeMirror numberTypeMirror = TypeMirrorUtils.get(processingEnvironment, Number.class);
		TypeMirror objectTypeMirror = TypeMirrorUtils.get(processingEnvironment, Object.class);
	}

	@Test
	void testErasureWithExecutableElement(ProcessingEnvironment processingEnvironment) {
		ExecutableElement executableElement = ExecutableElementUtils.resolve(processingEnvironment, List.class, "iterator()");
		//
		Assertions.assertEquals("java.util.Iterator<E>", executableElement.getReturnType().toString());
		Assertions.assertEquals("java.util.Iterator", TypeMirrorUtils.erasure(
				processingEnvironment, executableElement).toString());
	}

	@Test
	void testErasureWithTypeElement(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, List.class);
		//
		Assertions.assertEquals("java.util.List<E>", typeElement.asType().toString());
		Assertions.assertEquals("java.util.List", TypeMirrorUtils.erasure(
				processingEnvironment, typeElement).toString());
	}

	@Test
	void testErasureWithTypeMirror(ProcessingEnvironment processingEnvironment) {
		TypeMirror typeMirror = TypeElementUtils.get(processingEnvironment, List.class).asType();
		//
		Assertions.assertEquals("java.util.List<E>", typeMirror.toString());
		Assertions.assertEquals("java.util.List", TypeMirrorUtils.erasure(
				processingEnvironment, typeMirror).toString());
	}

	@Test
	void testGetWithClass(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("java.util.List<E>",
				TypeMirrorUtils.get(processingEnvironment, List.class).toString());
	}

	@Test
	void testGetWithType(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("java.util.List<E>",
				TypeMirrorUtils.get(processingEnvironment, (Type) List.class).toString());
		Assertions.assertEquals("java.util.AbstractList<E>",
				TypeMirrorUtils.get(processingEnvironment, ArrayList.class.getGenericSuperclass()).toString());
		{
			TypeVariable typeVariable = (TypeVariable) TypeMirrorUtils.get(processingEnvironment,
					UnionTypeParameter.class.getTypeParameters()[1]);
			Assertions.assertEquals("U", typeVariable.toString());
			Assertions.assertEquals(TypeElementUtils.get(processingEnvironment, UnionTypeParameter.class),
					typeVariable.asElement().getEnclosingElement());
			Assertions.assertEquals("java.lang.Object&java.util.List<E>&java.util.RandomAccess",
					typeVariable.getUpperBound().toString());
			Assertions.assertEquals("<nulltype>", typeVariable.getLowerBound().toString());
		}
	}

	@Test
	void testIsAssignable(ProcessingEnvironment processingEnvironment) {
		{
			Assertions.assertTrue(TypeMirrorUtils.isAssignable(processingEnvironment,
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT),
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG)
			));
			Assertions.assertFalse(TypeMirrorUtils.isAssignable(processingEnvironment,
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG),
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT)
			));
		}
		{
			Assertions.assertTrue(TypeMirrorUtils.isAssignable(processingEnvironment,
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT),
					processingEnvironment.getElementUtils().getTypeElement(Object.class.getCanonicalName()).asType()
			));
			Assertions.assertFalse(TypeMirrorUtils.isAssignable(processingEnvironment,
					processingEnvironment.getElementUtils().getTypeElement(Object.class.getCanonicalName()).asType(),
					processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT)
			));
		}
	}

	@Test
	void testIsErasure(ProcessingEnvironment processingEnvironment) {
		TypeMirror typeMirror = TypeElementUtils.get(processingEnvironment, List.class).asType();
		//
		Assertions.assertFalse(TypeMirrorUtils.isErasure(processingEnvironment,
				typeMirror));
		Assertions.assertTrue(TypeMirrorUtils.isErasure(processingEnvironment,
				processingEnvironment.getTypeUtils().erasure(typeMirror)));
		Assertions.assertTrue(TypeMirrorUtils.isErasure(processingEnvironment,
				TypeMirrorUtils.erasure(processingEnvironment, typeMirror)));
	}

	@Test
	void testResolveUpperBoundWithSuperWildcard(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"java.lang.Object",
				String.valueOf(TypeMirrorUtils.resolveUpperBound(
						processingEnvironment,
						TypeMirrorUtils
								.requireDeclaredType(ExecutableElementUtils
										.getDeclaredMethodByName(processingEnvironment, Optional.class, "map")
										.getParameters()
										.get(0)
										.asType())
								.getTypeArguments()
								.get(0))));
	}

	@Test
	void testResolveWithClass(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"int",
				TypeMirrorUtils.resolve(processingEnvironment, int.class).toString());
		Assertions.assertEquals(
				"java.util.List<E>",
				TypeMirrorUtils.resolve(processingEnvironment, List.class).toString());
		Assertions.assertEquals(
				"java.util.Map.Entry<K,V>",
				TypeMirrorUtils.resolve(processingEnvironment, Map.Entry.class).toString());
		Assertions.assertEquals(
				"void",
				TypeMirrorUtils.resolve(processingEnvironment, void.class).toString());
	}

	@Test
	void testResolveWithString(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("int", TypeMirrorUtils.resolve(
				processingEnvironment, "int").toString());
		Assertions.assertEquals("java.util.List", TypeMirrorUtils.resolve(
				processingEnvironment, "java.util.List").toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeMirrorUtils.resolve(
				processingEnvironment, "java.util.Map.Entry").toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeMirrorUtils.resolve(
				processingEnvironment, "java.util.Map$Entry").toString());
		Assertions.assertEquals("void", TypeMirrorUtils.resolve(
				processingEnvironment, "void").toString());
	}

	@Test
	void testResolveWithTypeParameterMappings(ProcessingEnvironment processingEnvironment) {
		ExecutableElement executableElement = ExecutableElementUtils.getDeclaredMethodByName(
				processingEnvironment,
				ComplexParameters.class,
				"map");
		//
		Assertions.assertEquals(
				"java.util:Map<java.lang:Integer,? extends java.util:List<java.lang:Integer>>",
				TypeMirrorUtils
						.toDescription(
								processingEnvironment,
								TypeMirrorUtils.resolve(
										processingEnvironment,
										Map.of(
												ParameterizableUtils.getTypeParameter(executableElement, 0),
												TypeMirrorUtils.get(processingEnvironment, Integer.class)),
										executableElement.getParameters().get(0).asType()))
						.toString());
	}

	@Test
	void testToDescription(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"java.util:Optional%T",
				TypeMirrorUtils.toDescription(
						processingEnvironment,
						ExecutableElementUtils.resolve(
								processingEnvironment,
								Optional.class,
								"get()").getReturnType()).toString());
		Assertions.assertEquals(
				"java.util:Optional<java.util:Optional#empty()%T>",
				TypeMirrorUtils
						.toDescription(
								processingEnvironment,
								ExecutableElementUtils.resolve(
												processingEnvironment,
												Optional.class,
												"empty()")
										.getReturnType())
						.toString());
		Assertions.assertEquals(
				"java.util.function:Function<? super java.util:Optional%T,? extends java.util:Optional#map(java.util.function:Function)%U>",
				TypeMirrorUtils
						.toDescription(
								processingEnvironment,
								ExecutableElementUtils.resolve(
												processingEnvironment,
												Optional.class,
												"map(java.util.function:Function)")
										.getParameters().get(0).asType())
						.toString());
	}

	@CompilationTarget(sourceTypes = FieldSignatures.class)
	@Test
	void testToDescriptionOfTypeWithTypeUseAnnotation(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"long",
				TypeMirrorUtils
						.toDescription(
								processingEnvironment,
								VariableElementUtils
										.resolveField(
												processingEnvironment,
												FieldSignatures.class,
												"l")
										.asType())
						.toString());
		Assertions.assertEquals(
				"java.lang:Object",
				TypeMirrorUtils
						.toDescription(
								processingEnvironment,
								VariableElementUtils
										.resolveField(
												processingEnvironment,
												FieldSignatures.class,
												"obj")
										.asType())
						.toString());
	}
}
