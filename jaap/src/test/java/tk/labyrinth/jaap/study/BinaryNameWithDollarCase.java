package tk.labyrinth.jaap.study;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.CompilationException;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

public class BinaryNameWithDollarCase {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void test() {
		ContribAssertions.assertThrows(
				() -> testCompiler.run(CompilationTarget.ofSourceResources(
						"j2a/binarynamewithdollar/ClassWith$InName.java",
						"j2a/binarynamewithdollar/ClassWith.java")),
				fault -> {
					Assertions.assertEquals(CompilationException.class, fault.getClass());
					List<String> output = ((CompilationException) fault).getOutput();
					//
					Assertions.assertEquals(4, output.size());
					//
					// Full line is: ${baseDir}/jaap/target/test-classes/j2a/binarynamewithdollar/ClassWith.java:5: error: duplicate class: j2a.binarynamewithdollar.ClassWith.InName
					// We don't check the ${baseDir} part.
					ContribAssertions.assertEndsWith(
							"/jaap/target/test-classes/j2a/binarynamewithdollar/ClassWith.java:5: error: duplicate class: j2a.binarynamewithdollar.ClassWith.InName",
							output.get(0).replace("\\", "/"));
					//
					Assertions.assertEquals("\tpublic static class InName {", output.get(1));
					Assertions.assertEquals("\t              ^", output.get(2));
					Assertions.assertEquals("1 error", output.get(3));
				});
	}
}
