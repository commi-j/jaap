package tk.labyrinth.jaap.enhanced;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.element.synthetic.SyntheticElementHandleRegistry;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.test.model.declaration.TestDeclarations;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ExtendWithJaap
class EnhancedProcessingTest {

	// TODO: This should support signatures without colons.
	//
	@Disabled("FIXME: Member selection is now working now")
	@Test
	void testBigDecimalGreaterThan(ProcessingEnvironment processingEnvironment) {
		SyntheticElementHandleRegistry syntheticElementHandleRegistry = new SyntheticElementHandleRegistry();
		syntheticElementHandleRegistry.registerMethodDeclaration(
				TypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		//
		ProcessingContext processingContext = EnhancedProcessing.createContext(
				processingEnvironment,
				syntheticElementHandleRegistry);
		//
		TypeElementHandle bigDecimalHandle = processingContext.getTypeElementHandle(BigDecimal.class);
		{
			MethodElementHandle greaterThanHandle = processingContext.getMethodElementHandle(
					"java.math:BigDecimal#greaterThan(java.math:BigDecimal)");
			Assertions.assertNotNull(greaterThanHandle);
			Assertions.assertEquals(bigDecimalHandle, greaterThanHandle.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanHandle.getReturnType());
		}
		{
			List<? extends MethodElementHandle> declaredMethods = bigDecimalHandle.getDeclaredMethods();
			MethodElementHandle greaterThanHandle = declaredMethods.stream()
					.filter(declaredMethod -> Objects.equals(declaredMethod.getName(), "greaterThan"))
					.findAny().orElse(null);
			Assertions.assertNotNull(greaterThanHandle);
			Assertions.assertEquals(bigDecimalHandle, greaterThanHandle.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanHandle.getReturnType());
		}
		{
			List<MethodElementHandle> allMethods = bigDecimalHandle.getAllMethodStream().collect(Collectors.toList());
			MethodElementHandle greaterThanHandle = allMethods.stream()
					.filter(methodHandle -> Objects.equals(methodHandle.getName(), "greaterThan"))
					.findAny().orElse(null);
			Assertions.assertNotNull(greaterThanHandle);
			Assertions.assertEquals(bigDecimalHandle, greaterThanHandle.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanHandle.getReturnType());
		}
		{
			MethodElementHandle greaterThanHandle = bigDecimalHandle.selectMethodElement(
					"greaterThan",
					processingContext.getTypeHandle(BigDecimal.class));
			Assertions.assertNotNull(greaterThanHandle);
			Assertions.assertEquals(bigDecimalHandle, greaterThanHandle.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanHandle.getReturnType());
		}
	}
}
