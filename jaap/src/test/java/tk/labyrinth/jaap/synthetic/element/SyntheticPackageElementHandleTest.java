package tk.labyrinth.jaap.synthetic.element;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.math.BigDecimal;
import java.util.List;

@ExtendWithJaap
class SyntheticPackageElementHandleTest {

	@Disabled("FIXME: Need to find solution as standard annproc model does not support synth packages and we don't want to add such dependency.")
	@Test
	void testSelectMember(ProcessingContext processingContext) {
		{
			// import java.math.BigDecimal;
			//
			PackageElementHandle packageElement = processingContext.getPackageElementHandle("java");
			Assertions.assertTrue(packageElement instanceof SyntheticPackageElementHandle);
			Assertions.assertEquals(
					processingContext.getElementHandle(BigDecimal.class),
					packageElement.selectMember(EntitySelectorChain.forStarOrTypeInImport(
							List.of("math", "BigDecimal"))));
		}
	}
}