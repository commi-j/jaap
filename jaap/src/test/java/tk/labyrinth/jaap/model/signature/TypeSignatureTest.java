package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.jaap.testing.model.AllTypes;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class TypeSignatureTest {

	@Test
	void testComplexCaseBreakdown() {
		TypeSignature typeSignature = TypeSignature.ofValid("d$ll$r:N$tN$st$dT$p$.N$st$dT$p$[][]");
		Assertions.assertEquals(PackageSignature.of("d$ll$r"), typeSignature.getPackageSignature());
		Assertions.assertEquals(List.of("N$tN$st$dT$p$", "N$st$dT$p$"), typeSignature.getTypeSimpleNames());
		Assertions.assertEquals(2, typeSignature.getArrayCount());
	}

	// TODO
	@Disabled
	@Test
	void testGetBinaryName() {
//		Assertions.assertEquals("java.util.Map$Entry", TypeErasureSignature.of(Map.Entry.class).getBinaryName());
	}

	@Test
	void testGetClassSignatureWithShort() {
		Assertions.assertEquals(
				ClassSignature.of(short.class),
				TypeSignature.of(short.class).getClassSignature());
	}

	@Test
	void testGetSimpleName() {
		Assertions.assertEquals(
				"Object",
				TypeSignature.of(Object.class).getSimpleName());
		Assertions.assertEquals(
				"int",
				TypeSignature.of(int.class).getSimpleName());
		Assertions.assertEquals(
				"Entry",
				TypeSignature.of(Map.Entry.class).getSimpleName());
	}

	@Test
	void testGuessFromBinaryName() {
		Assertions.assertEquals(
				"java.lang:Object",
				TypeSignature.guessFromBinaryName(Object.class.getName()).toString());
		Assertions.assertEquals(
				"int",
				TypeSignature.guessFromBinaryName(int.class.getName()).toString());
		Assertions.assertEquals(
				"java.util:Map.Entry",
				TypeSignature.guessFromBinaryName(Map.Entry.class.getName()).toString());
	}

	@Test
	void testNamesWithDollarsCases() {
		{
			TypeSignature typeSignature = TypeSignature.ofValid("d$ll$r:N$tN$st$dT$p$");
			Assertions.assertEquals(PackageSignature.of("d$ll$r"), typeSignature.getPackageSignature());
			Assertions.assertEquals(List.of("N$tN$st$dT$p$"), typeSignature.getTypeSimpleNames());
		}
		{
			TypeSignature typeSignature = TypeSignature.ofValid("$");
			Assertions.assertNotNull(typeSignature.getPackageSignature());
			Assertions.assertEquals(List.of(), typeSignature.getPackageSignature().getPackageNames());
			Assertions.assertEquals(List.of("$"), typeSignature.getTypeSimpleNames());
		}
	}

	@Test
	void testOf() throws ClassNotFoundException {
		{
			// Comparing with String.
			{
				Assertions.assertEquals("java.util:Map", TypeSignature.of(Map.class).toString());
				Assertions.assertEquals("java.util:Map.Entry", TypeSignature.of(Map.Entry.class).toString());
			}
			{
				Assertions.assertEquals("java.lang:Object[]", TypeSignature.of(Object[].class).toString());
				Assertions.assertEquals("int[][]", TypeSignature.of(int[][].class).toString());
			}
		}
		{
			// Checking structure.
			{
				TypeSignature typeSignature = TypeSignature.of(int.class);
				//
				Assertions.assertEquals("int", typeSignature.toString());
				Assertions.assertNull(typeSignature.getPackageSignature());
			}
			{
				TypeSignature typeSignature = TypeSignature.of(Object.class);
				//
				Assertions.assertEquals("java.lang:Object", typeSignature.toString());
				Assertions.assertNotNull(typeSignature.getPackageSignature());
				Assertions.assertFalse(typeSignature.getPackageSignature().isUnnamed());
			}
			{
				TypeSignature typeSignature = TypeSignature.of(Class.forName("PackagelessClass"));
				//
				Assertions.assertEquals("PackagelessClass", typeSignature.toString());
				Assertions.assertNotNull(typeSignature.getPackageSignature());
				Assertions.assertTrue(typeSignature.getPackageSignature().isUnnamed());
			}
		}
	}

	@Test
	void testOfValid() {
		Assertions.assertEquals(
				List.of(
						"java.lang:Integer",
						"int",
						"java.util:List",
						"java.util:Map.Entry",
						"java.lang:Object",
						"int[]",
						"void"),
				AllTypes.getAsClasses().stream()
						.map(javaClass -> TypeSignature.ofValid(ClassUtils.getSignature(javaClass)))
						.map(TypeSignature::toString)
						.collect(Collectors.toList()));
	}

	@Test
	void testToString() {
		{
			// Name only
			Assertions.assertEquals("type", TypeSignature.ofValid("type").toString());
			Assertions.assertEquals("Type", TypeSignature.ofValid("Type").toString());
			Assertions.assertEquals("TYPE", TypeSignature.ofValid("TYPE").toString());
		}
		{
			// Dots and name
			Assertions.assertEquals("j2a.Type", TypeSignature.ofValid("j2a.Type").toString());
			Assertions.assertEquals("j2a.l2g.Type", TypeSignature.ofValid("j2a.l2g.Type").toString());
		}
	}
}