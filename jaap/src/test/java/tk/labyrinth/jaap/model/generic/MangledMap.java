package tk.labyrinth.jaap.model.generic;

import java.util.Map;

/**
 * Map with K and V parameters swapped.
 *
 * @param <V> Map's K
 * @param <K> Map's V
 */
public interface MangledMap<V, K> extends Map<V, K> {
	// empty
}
