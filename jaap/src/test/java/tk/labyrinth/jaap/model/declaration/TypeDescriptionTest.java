package tk.labyrinth.jaap.model.declaration;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

class TypeDescriptionTest {

	@Test
	void ofSimplyParameterized() {
		Assertions.assertEquals(
				"java.util:List",
				TypeDescription.ofSimplyParameterized(List.class).toString());
		Assertions.assertEquals(
				"java.util:List<java.lang:String>",
				TypeDescription.ofSimplyParameterized(List.class, String.class).toString());
		Assertions.assertEquals(
				"java.util:Map<java.lang:String,java.lang:Long>",
				TypeDescription.ofSimplyParameterized(Map.class, String.class, Long.class).toString());
	}

	@Test
	void testBreakDown() {
		Assertions.assertEquals(
				Stream.of("Map", "?", "Object", "Set", "?")
						.map(TypeDescription::of)
						.toList(),
				TypeDescription.of("Map<? extends Object,Set<?>>").breakDown());
	}

	@Test
	void testDestructureVariable() {
		Assertions.assertEquals(
				TypeDescription.of(Optional.class.getTypeParameters()[0]).destructureVariable(),
				Pair.of(TypeSignature.of(Optional.class), "T"));
	}

	@Test
	void testGetSignature() {
		Assertions.assertEquals(
				"int[]",
				TypeDescription.of("int[]").getSignature().toString());
		Assertions.assertEquals(
				"Map",
				TypeDescription.of("Map<? extends Object,Set<?>>").getSignature().toString());
		Assertions.assertEquals(
				"java.lang:Object",
				TypeDescription.ofWildcard().getSignature().toString());
		Assertions.assertEquals(
				"java.lang:String",
				TypeDescription
						.of(TypeUtils.wildcardType()
								.withUpperBounds(String.class)
								.build())
						.getSignature()
						.toString());
	}

	@Test
	void testGetSimpleString() {
		Assertions.assertEquals(
				"T",
				TypeDescription.of("java.util.stream:Stream%T").getSimpleString());
		Assertions.assertEquals(
				"List<T>",
				TypeDescription.of("List<java.util.stream:Stream%T>").getSimpleString());
		Assertions.assertEquals(
				"? extends T",
				TypeDescription.of("? extends java.util.stream:Stream%T").getSimpleString());
	}

	@Test
	void testOfJavaType() {
		Assertions.assertEquals(
				"java.util:List<java.lang:String>",
				TypeDescription
						.of(TypeUtils.parameterize(List.class, String.class))
						.toString());
		Assertions.assertEquals(
				"?",
				TypeDescription
						.of(TypeUtils.wildcardType().build())
						.toString());
		Assertions.assertEquals(
				"? extends java.lang:String",
				TypeDescription
						.of(TypeUtils.wildcardType()
								.withUpperBounds(String.class)
								.build())
						.toString());
	}

	@Test
	void testOfJavaTypeVariable() throws NoSuchFieldException {
		Assertions.assertEquals(
				"java.util:Optional%T",
				TypeDescription.of(Optional.class.getDeclaredField("value").getGenericType()).toString());
	}

	@Test
	void testOfString() {
		{
			TypeDescription typeDescription = TypeDescription.of("T");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("T", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertNull(typeDescription.getParameters());
			Assertions.assertNull(typeDescription.getUpperBound());
		}
		{
			TypeDescription typeDescription = TypeDescription.of("T extends U & V");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("T", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertNull(typeDescription.getParameters());
			Assertions.assertEquals(
					List.of(
							TypeDescription.of("U"),
							TypeDescription.of("V")),
					typeDescription.getUpperBound());
		}
		{
			TypeDescription typeDescription = TypeDescription.of("Map<? extends Object,Set<?>>");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("Map", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertEquals(
					List.of(
							TypeDescription.of("? extends Object"),
							TypeDescription.of("Set<?>")),
					typeDescription.getParameters());
			Assertions.assertNull(typeDescription.getUpperBound());
		}
	}

	@Test
	void testToLongForm() {
		Assertions.assertEquals(
				"List<String>",
				TypeDescription.of("java.lang:List<java.lang:String>").toLongForm());
	}
}