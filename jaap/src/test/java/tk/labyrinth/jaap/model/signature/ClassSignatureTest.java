package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Map;

class ClassSignatureTest {

	@Test
	void testFromClassWithUnnamedPackage() {
		Assertions.assertEquals(
				"ClassWithUnnamedPackage",
				ClassSignature.from("ClassWithUnnamedPackage").toString());
	}

	@MethodSource("getClassesForTests")
	@ParameterizedTest
	void testGetBinaryName(Class<?> javaClass) {
		Assertions.assertEquals(
				javaClass.getName(),
				ClassSignature.of(javaClass).getBinaryName());
	}

	@MethodSource("getClassesForTests")
	@ParameterizedTest
	void testGetQualifiedName(Class<?> javaClass) {
		Assertions.assertEquals(
				javaClass.getCanonicalName(),
				ClassSignature.of(javaClass).getQualifiedName());
	}

	@Test
	void testGuessFromQualifiedName() {
		Assertions.assertEquals(
				"java.util:Map",
				ClassSignature.guessFromQualifiedName(Map.class.getCanonicalName()).toString());
		Assertions.assertEquals(
				"java.util:Map.Entry",
				ClassSignature.guessFromQualifiedName(Map.Entry.class.getCanonicalName()).toString());
	}

	private static List<Class<?>> getClassesForTests() {
		return List.of(
				Integer.class,
				int.class,
				Map.class,
				Map.Entry.class);
	}
}