package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ElementSignatureTest {

	@Test
	void testAnnotationOnMethod() {
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:toString()/annotation:java.lang.Override",
				ElementSignature.of("java.util:Optional#toString()@java.lang.Override").toTechnicalString());
	}

	@Test
	void testAnnotationOnType() {
		Assertions.assertEquals(
				"package:org/package:junit/package:jupiter/package:api/type:Test/annotation:java.lang.annotation:Documented",
				ElementSignature.of("org.junit.jupiter.api:Test@java.lang.annotation:Documented").toTechnicalString());
	}

	@Test
	void testAppendAnnotationWithClass() {
		Assertions.assertEquals(
				"package:java/package:util/type:Object/field:field/annotation:java.lang:Deprecated",
				ElementSignature.of("java.util:Object#field").appendAnnotation(Deprecated.class)
						.toTechnicalString());
		Assertions.assertEquals(
				"package:java/package:util/type:Object/field:field/annotation:java.lang:Override",
				ElementSignature.of("java.util:Object#field").appendAnnotation(Override.class)
						.toTechnicalString());
	}

	@Test
	void testAppendAnnotationWithString() {
		Assertions.assertEquals(
				"package:java/package:util/type:Object/field:field/annotation:java.lang:Override",
				ElementSignature.of("java.util:Object#field").appendAnnotation("java.lang:Override")
						.toTechnicalString());
	}

	@Test
	void testBreakDownForConstructorFormalParameter() {
	}

	@Test
	void testField() {
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/field:EMPTY",
				ElementSignature.of("java.util:Optional#EMPTY").toTechnicalString());
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/field:value",
				ElementSignature.of("java.util:Optional#value").toTechnicalString());
	}

	@Test
	void testFormalParameterOfConstructor() {
		Assertions.assertEquals(
				"package:java/package:util/type:HashMap/constructor:(int,float)/formalParameter:initialCapacity",
				ElementSignature.of("java.util:HashMap(int,float)#initialCapacity").toTechnicalString());
	}

	@Test
	void testFormalParameterOfMethod() {
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:of(java.lang.Object)/formalParameter:0",
				ElementSignature.of("java.util:Optional#of(java.lang.Object)#0").toTechnicalString());
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:ifPresentOrElse(java.util.function.Consumer,java.lang.Runnable)/formalParameter:0",
				ElementSignature.of("java.util:Optional#ifPresentOrElse(java.util.function.Consumer,java.lang.Runnable)#0")
						.toTechnicalString());
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:ifPresentOrElse(java.util.function.Consumer,java.lang.Runnable)/formalParameter:1",
				ElementSignature.of("java.util:Optional#ifPresentOrElse(java.util.function.Consumer,java.lang.Runnable)#1")
						.toTechnicalString());
	}

	@Test
	void testMatchesMethod() {
		// FIXME: Now we think ElSign does not support simple signatures. Think about it later.
//		Assertions.assertTrue(ElementSignature2.of("getBytes()").matchesMethod());
		Assertions.assertTrue(ElementSignature.of("String#getBytes()").matchesMethod());
		Assertions.assertTrue(ElementSignature.of("java.lang.String#getBytes()").matchesMethod());
		Assertions.assertTrue(ElementSignature.of("java.lang.String#getBytes(java.nio.charset.Charset)").matchesMethod());
		//
		Assertions.assertFalse(ElementSignature.of("java.lang.String#getBytes").matchesMethod());
	}

	@Test
	void testMethod() {
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:get()",
				ElementSignature.of("java.util:Optional#get()").toTechnicalString());
		Assertions.assertEquals(
				"package:java/package:util/type:Optional/method:of(java.lang.Object)",
				ElementSignature.of("java.util:Optional#of(java.lang.Object)").toTechnicalString());
	}

	@Test
	void testOf() {
		Assertions.assertEquals(
				"package:foo/package:bar/type:TestClass/annotation:foo.bar:Annotation",
				ElementSignature.of("foo.bar:TestClass@foo.bar:Annotation").toTechnicalString());
		Assertions.assertEquals(
				"package:foo/package:bar/type:TestClass/field:field/annotation:foo.bar:Annotation",
				ElementSignature.of("foo.bar:TestClass#field@foo.bar:Annotation").toTechnicalString());
		//
		// FIXME: Those copied from previous version of signature but need revision.
//		{
//			ElementSignature2 value = ElementSignature2.of("field");
//			Assertions.assertEquals("field", value.getNameBeforeSharp());
//			Assertions.assertFalse(value.hasBraces());
//			Assertions.assertNull(value.getParameters());
//		}
//		{
//			ElementSignature2 value = ElementSignature2.of("method()");
//			Assertions.assertEquals("method", value.getNameBeforeSharp());
//			Assertions.assertTrue(value.hasBraces());
//			Assertions.assertEquals(List.of(), value.getParameters());
//		}
//		{
//			ElementSignature2 value = ElementSignature2.of("java.util:Map.Entry");
//			Assertions.assertEquals("java.util:Map.Entry", value.toString());
//		}
//		{
//			ElementSignature2 value = ElementSignature2.of("java.lang.String#getBytes(java.nio.charset.Charset)");
//			Assertions.assertEquals("java.lang", value.getDotPath());
//			Assertions.assertEquals("String", value.getNameBeforeSharp());
//			Assertions.assertEquals("getBytes", value.getNameAfterSharp());
//			Assertions.assertTrue(value.hasBraces());
//			Assertions.assertEquals(List.of("java.nio.charset.Charset"), value.getParameters());
//		}
//		{
//			ElementSignature2 value = ElementSignature2.of("java.lang.String#getBytes(byte[],int,byte)");
//			Assertions.assertEquals("java.lang", value.getDotPath());
//			Assertions.assertEquals("String", value.getNameBeforeSharp());
//			Assertions.assertEquals("getBytes", value.getNameAfterSharp());
//			Assertions.assertTrue(value.hasBraces());
//			Assertions.assertEquals(List.of("byte[]", "int", "byte"), value.getParameters());
//		}
//		{
//			ContribAssertions.assertThrows(() -> ElementSignature2.of(""), fault ->
//					Assertions.assertEquals("java.lang.IllegalArgumentException: False ElementSignature2: ", fault.toString()));
//			ContribAssertions.assertThrows(() -> ElementSignature2.of("()"), fault ->
//					Assertions.assertEquals("java.lang.IllegalArgumentException: False ElementSignature2: ()", fault.toString()));
//		}
	}

	@Test
	void testPackage() {
		Assertions.assertEquals(
				"packageOrType:java",
				ElementSignature.of("java").toTechnicalString());
		//
		Assertions.assertEquals(
				"packageOrType:java/packageOrType:util",
				ElementSignature.of("java.util").toTechnicalString());
	}

	@Test
	void testToLongString() {
		Assertions.assertEquals(
				"TestClass@Annotation",
				ElementSignature
						.of("foo.bar:TestClass@foo.bar:Annotation")
						.toLongString());
		Assertions.assertEquals(
				"TestClass#field@Annotation",
				ElementSignature
						.of("foo.bar:TestClass#field@foo.bar:Annotation")
						.toLongString());
		Assertions.assertEquals(
				"TestClass()@Annotation",
				ElementSignature
						.of("foo.bar:TestClass()@foo.bar:Annotation")
						.toLongString());
		Assertions.assertEquals(
				"TestClass()#parameter@Annotation",
				ElementSignature
						.of("foo.bar:TestClass()#parameter@foo.bar:Annotation")
						.toLongString());
		Assertions.assertEquals(
				"Object#equals(Object)",
				ElementSignature
						.of("java.lang:Object#equals(java.lang:Object)")
						.toLongString());
	}

	@Test
	void testToString() {
		{
			String value = "java.lang:String#getBytes()";
			Assertions.assertEquals(value, ElementSignature.of(value).toString());
		}
		{
			String value = "java.lang:String#getBytes(java.nio.charset:Charset)";
			Assertions.assertEquals(value, ElementSignature.of(value).toString());
		}
		{
			String value = "java.lang:String#getBytes(byte[],int,byte)";
			Assertions.assertEquals(value, ElementSignature.of(value).toString());
		}
	}

	@Test
	void testToStringOfAnnotationOnFormalParameter() {
		// TODO: Find annotated parameter in java core.
		Assertions.assertEquals(
				"one:Two#three(java.lang.Object)#0@java.lang.Override",
				ElementSignature.of("one:Two#three(java.lang.Object)#0@java.lang.Override").toString());
	}

	@Test
	void testToTechnicalStringOfAnnotationOnFormalParameter() {
		// TODO: Find annotated parameter in java core.
		Assertions.assertEquals(
				"package:one/type:Two/method:three(java.lang.Object)/formalParameter:0/annotation:java.lang.Override",
				ElementSignature.of("one:Two#three(java.lang.Object)#0@java.lang.Override").toTechnicalString());
	}

	@Test
	void testType() {
		Assertions.assertEquals(
				"package:java/package:util/type:Map",
				ElementSignature.of("java.util:Map").toTechnicalString());
		Assertions.assertEquals(
				"packageOrType:java/packageOrType:util/packageOrType:Map",
				ElementSignature.of("java.util.Map").toTechnicalString());
		//
		Assertions.assertEquals(
				"package:java/package:util/type:Map/type:Entry",
				ElementSignature.of("java.util:Map.Entry").toTechnicalString());
		Assertions.assertEquals(
				"packageOrType:java/packageOrType:util/packageOrType:Map/packageOrType:Entry",
				ElementSignature.of("java.util.Map.Entry").toTechnicalString());
	}
}