package tk.labyrinth.jaap.model.entity.selection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class EntitySelectorChainTest {

	@Test
	void testHead() {
		Assertions.assertEquals(
				EntitySelector.build(EntitySelectionContext.forVariable(), "a"),
				EntitySelectorChain.forVariable(List.of("a")).head());
		Assertions.assertEquals(
				EntitySelector.build(EntitySelectionContext.forVariableOrType(), "a"),
				EntitySelectorChain.forVariable(List.of("a", "b")).head());
		Assertions.assertEquals(
				EntitySelector.build(EntitySelectionContext.forVariableOrTypeOrPackage(), "a"),
				EntitySelectorChain.forVariable(List.of("a", "b", "c")).head());
		Assertions.assertEquals(
				EntitySelector.build(EntitySelectionContext.forVariableOrTypeOrPackage(), "a"),
				EntitySelectorChain.forVariable(List.of("a", "b", "c", "d")).head());
	}

	@Test
	void testPrepend() {
		Assertions.assertEquals(
				EntitySelectorChain.forVariable(List.of("z", "a", "b", "c")),
				EntitySelectorChain.forVariable(List.of("a", "b", "c")).prepend("z"));
	}
}
