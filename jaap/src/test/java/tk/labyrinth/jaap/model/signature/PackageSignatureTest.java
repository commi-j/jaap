package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PackageSignatureTest {

	@Test
	void testOfPackage() throws ClassNotFoundException {
		Assertions.assertEquals(
				"java.lang",
				PackageSignature.of(Object.class.getPackage()).toString());
		Assertions.assertEquals(
				"",
				PackageSignature.of(Class.forName("PackagelessClass").getPackage()).toString());
	}
}