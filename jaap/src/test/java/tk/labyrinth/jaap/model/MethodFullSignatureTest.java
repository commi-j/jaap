package tk.labyrinth.jaap.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

class MethodFullSignatureTest {

	@Test
	void testOf() {
		{
			MethodFullSignature value = MethodFullSignature.of("java.lang:Object#wait()");
			Assertions.assertEquals("java.lang:Object", value.getTypeFullSignature());
			Assertions.assertEquals("wait", value.getName());
			Assertions.assertEquals(List.of(), value.getParameters());
		}
		{
			MethodFullSignature value = MethodFullSignature.of("java.lang:Object#wait(long)");
			Assertions.assertEquals("java.lang:Object", value.getTypeFullSignature());
			Assertions.assertEquals("wait", value.getName());
			Assertions.assertEquals(List.of(TypeSignature.ofValid("long")), value.getParameters());
		}
		{
			MethodFullSignature value = MethodFullSignature.of("java.lang:Object#wait(long,int)");
			Assertions.assertEquals("java.lang:Object", value.getTypeFullSignature());
			Assertions.assertEquals("wait", value.getName());
			Assertions.assertEquals(
					List.of(TypeSignature.ofValid("long"), TypeSignature.ofValid("int")),
					value.getParameters());
		}
		{
			ContribAssertions.assertThrows(() -> MethodFullSignature.of("wait()"), fault -> {
				Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
				Assertions.assertEquals("False MethodFullSignature: wait()", fault.getMessage());
			});
		}
		{
			// Name with number, but not first symbol.
			MethodFullSignature value = MethodFullSignature.of("tk.labyrinth.jaap.model:MethodSignatureStringTest.Declarations#get0()");
			Assertions.assertEquals("tk.labyrinth.jaap.model:MethodSignatureStringTest.Declarations", value.getTypeFullSignature());
			Assertions.assertEquals("get0", value.getName());
			Assertions.assertEquals(List.of(), value.getParameters());
		}
		{
			// Name with number as first symbol.
			// FIXME: It must fail as Java does not permit number as first symbol.
			MethodFullSignature value = MethodFullSignature.of("java.lang:Object#2String()");
			Assertions.assertEquals("java.lang:Object", value.getTypeFullSignature());
			Assertions.assertEquals("2String", value.getName());
			Assertions.assertEquals(List.of(), value.getParameters());
		}
	}
}
