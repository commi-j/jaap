package tk.labyrinth.jaap.model.declaration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.test.model.declaration.TestDeclarations;

class MethodDeclarationTest {

	@Test
	void testGetSignature() {
		Assertions.assertEquals(
				"greaterThan(java.util.function.Function)",
				TestDeclarations.mapInObjects().getSignature().toString());
	}
}