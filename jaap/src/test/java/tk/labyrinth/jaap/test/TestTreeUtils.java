package tk.labyrinth.jaap.test;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.langmodel.tree.util.CompilationUnitTreeUtils;
import tk.labyrinth.jaap.langmodel.tree.util.TreeUtils;

import java.util.List;
import java.util.stream.Stream;

public class TestTreeUtils {

	public static Stream<CompilationUnitTree> getRootTreeStream(AnnotationProcessingRound annotationProcessingRound) {
		return CompilationUnitTreeUtils.streamFrom(annotationProcessingRound);
	}

	public static List<CompilationUnitTree> getRootTrees(AnnotationProcessingRound annotationProcessingRound) {
		return getRootTreeStream(annotationProcessingRound).toList();
	}

	public static <T extends Tree> List<? extends Pair<CompilationUnitContext, ? extends T>> getTreesOfClass(
			AnnotationProcessingRound annotationProcessingRound,
			Class<? extends T> treeClass) {
		return getRootTreeStream(annotationProcessingRound)
				.flatMap(compilationUnitTree -> {
					CompilationUnitContext compilationUnitContext = CompilationUnitContext.of(
							compilationUnitTree,
							annotationProcessingRound.getProcessingEnvironment());
					//
					return TreeUtils
							.streamTree(compilationUnitTree, treeClass)
							.map(tree -> Pair.of(compilationUnitContext, tree));
				})
				.toList();
	}

	public static <T extends Tree> List<? extends Pair<CompilationUnitContext, ? extends T>> getTreesOfClassWithinStaticInitializers(
			AnnotationProcessingRound annotationProcessingRound,
			Class<? extends T> treeClass) {
		return getRootTreeStream(annotationProcessingRound)
				.flatMap(compilationUnitTree -> {
					CompilationUnitContext compilationUnitContext = CompilationUnitContext.of(
							compilationUnitTree,
							annotationProcessingRound.getProcessingEnvironment());
					//
					return TreeUtils
							.streamTree(
									compilationUnitTree,
									tree -> tree instanceof BlockTree blockTree && blockTree.isStatic())
							.flatMap(tree -> TreeUtils.streamTree(tree, treeClass))
							.map(tree -> Pair.of(compilationUnitContext, tree));
				})
				.toList();
	}
}
