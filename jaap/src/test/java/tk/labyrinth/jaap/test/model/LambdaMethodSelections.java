package tk.labyrinth.jaap.test.model;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaMethodSelections {

	static {
		foo(() -> System.out);
		foo(() -> System.out.println());
		foo(o -> o);
	}

	static void foo(Runnable runnable) {
		// no-op
	}

	static void foo(Consumer<Object> consumer) {
		// no-op
	}

	static void foo(Function<Object, Object> function) {
		// no-op
	}

	static void foo(Supplier<Object> supplier) {
		// no-op
	}
}
