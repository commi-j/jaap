package tk.labyrinth.jaap.test.model.methodinvocation;

public class MethodInvocationWithThisTarget {

	{
		this.thisMethod();
	}

	void thisMethod() {
		// no-op
	}
}
