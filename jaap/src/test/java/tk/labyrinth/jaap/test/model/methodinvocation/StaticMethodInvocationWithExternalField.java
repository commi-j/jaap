package tk.labyrinth.jaap.test.model.methodinvocation;

import java.math.BigDecimal;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class StaticMethodInvocationWithExternalField {

	static {
		String.valueOf(BigDecimal.ZERO);
	}
}
