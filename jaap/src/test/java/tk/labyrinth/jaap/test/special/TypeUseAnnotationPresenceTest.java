package tk.labyrinth.jaap.test.special;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.test.model.FieldSignatures;
import tk.labyrinth.jaap.test.model.MethodSignatures;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
public class TypeUseAnnotationPresenceTest {

	@Test
	void testTypeUseAnnotationAbsentIfElementIsNotInCompilationTarget(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.lang.Object",
				processingContext
						.getFieldElementHandle(
								FieldSignatures.class,
								"obj")
						.getType()
						.getTypeMirror()
						.toString());
		Assertions.assertEquals(
				"java.lang.Object",
				processingContext
						.getMethodElementHandleByName(
								MethodSignatures.class,
								"withObjectFormalParameterWithTypeUseAnnotation")
						.getFormalParameter(0)
						.getType()
						.getTypeMirror()
						.toString());
	}

	@CompilationTarget(sourceTypes = {
			FieldSignatures.class,
			MethodSignatures.class,
	})
	@Test
	void testTypeUseAnnotationPresentIfElementIsInCompilationTarget(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"@org.checkerframework.checker.fenum.qual.Fenum(\"\") java.lang.Object",
				processingContext
						.getFieldElementHandle(
								FieldSignatures.class,
								"obj")
						.getType()
						.getTypeMirror()
						.toString());
		Assertions.assertEquals(
				"@org.checkerframework.checker.fenum.qual.Fenum(\"\") java.lang.Object",
				processingContext
						.getMethodElementHandleByName(
								MethodSignatures.class,
								"withObjectFormalParameterWithTypeUseAnnotation")
						.getFormalParameter(0)
						.getType()
						.getTypeMirror()
						.toString());
	}
}
