package tk.labyrinth.jaap.test.model.methodinvocation;

import java.util.function.Consumer;

public class MethodInvocationsWithinLambda {

	static {
		Runnable runnable = () -> foo();
		Consumer<Integer> integerConsumer = i -> foo();
		Consumer<Long> longConsumer = l -> foo(l);
	}

	public static void foo() {
		// no-op
	}

	public static void foo(int i) {
		// no-op
	}

	public static void foo(long l) {
		// no-op
	}
}
