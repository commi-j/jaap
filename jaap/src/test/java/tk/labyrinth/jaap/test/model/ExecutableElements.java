package tk.labyrinth.jaap.test.model;

@SuppressWarnings({"ClassInitializerMayBeStatic", "unused"})
public class ExecutableElements {

	static {
		System.out.println("Static Initializer 0");
	}

	static {
		System.out.println("Static Initializer 1");
	}

	{
		System.out.println("Instance Initializer 0");
	}

	{
		System.out.println("Instance Initializer 1");
	}

	{
		System.out.println("Instance Initializer 2");
	}

	public ExecutableElements() {
		System.out.println("Explicit NoArgs Constructor");
	}

	public ExecutableElements(Object arg0) {
		System.out.println("Explicit OneArg Constructor");
	}

	void instanceMethod() {
		// no-op
	}

	static void staticMethod() {
		// no-op
	}
}
