package tk.labyrinth.jaap.test.model.methodinvocation;

import java.util.Optional;
import java.util.function.Supplier;

@SuppressWarnings("Convert2MethodRef")
public class MethodInvocationsWithSupplierLambdas {

	static {
		Optional<String> stringOptional = Optional.of("foo");
		stringOptional.orElseGet(() -> getBar());
	}

	public static void foo(Supplier<Integer> sn) {
	}

	public static String getBar() {
		return "bar";
	}
}
