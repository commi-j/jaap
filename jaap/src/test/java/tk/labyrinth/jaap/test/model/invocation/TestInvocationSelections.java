package tk.labyrinth.jaap.test.model.invocation;

import java.io.Serializable;

public class TestInvocationSelections {

	static {
		// Integer, Number, Object;
		// Comparable<Integer>, Constable, ConstantDesc;
		// Serializable;
		Integer integer0 = 12;
		Integer integer1 = 12;
		{
			// Method with Number is picked.
			intOrNumber(integer0);
		}
		{
			// Method with Object is picked.
			intOrObject(integer0);
		}
		{
			// Method with Object is picked.
			numberOrSerializable(integer0);
		}
		{
			// Method with Numbers is picked.
			intAndIntegerOrNumberAndNumber(integer0, integer1);
		}
		{
			// intAndNumberOrNumberAndInt(integer0, integer1);
		}
	}

	public static String comparableOrSerializable(Comparable comparable) {
		return "Comparable";
	}

	public static String comparableOrSerializable(Serializable serializable) {
		return "Serializable";
	}

	public static String intAndIntegerOrNumberAndNumber(int i, Integer integer) {
		return "int and Integer";
	}

	public static String intAndIntegerOrNumberAndNumber(Number number0, Number number1) {
		return "Number and Number";
	}

	public static String intAndNumberOrNumberAndInt(int i, Number number) {
		return "int and Number";
	}

	public static String intAndNumberOrNumberAndInt(Number number, int i) {
		return "Number and int";
	}

	public static String intOrNumber(int i) {
		return "int";
	}

	public static String intOrNumber(Number number) {
		return "Number";
	}

	public static String intOrObject(int i) {
		return "int";
	}

	public static String intOrObject(Object object) {
		return "Object";
	}

	public static String numberOrSerializable(Number number) {
		return "Number";
	}

	public static String numberOrSerializable(Serializable serializable) {
		return "Serializable";
	}
}
