package tk.labyrinth.jaap.test;

@SuppressWarnings("unused")
@TestAnnotation
public class Dummy {

	@TestAnnotation(string = "custom")
	private Object custom;

	@TestMetaAnnotation
	private Object meta;

	public static class StaticNested {
		// empty
	}
}
