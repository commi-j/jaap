package tk.labyrinth.jaap.test.model.methodinvocation;

@SuppressWarnings("UnusedReturnValue")
public class StaticMethodInvocations {

	static {
		testMethod();
		StaticMethodInvocations.testMethod();
		tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocations.testMethod();
	}

	static Integer testMethod() {
		throw new UnsupportedOperationException();
	}
}
