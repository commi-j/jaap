package tk.labyrinth.jaap.test.model;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("JavaReflectionMemberAccess")
public class ArrayAccesses {

	static List<String> stringList;

	static {
		try {
			Type type = ((ParameterizedType) ArrayAccesses.class.getField("stringList").getGenericType())
					.getActualTypeArguments()[0];
			Optional.of(ArrayAccesses.class.getField("stringList").getGenericType())
					.map(fieldGenericType -> asParameterizedType(fieldGenericType).getActualTypeArguments()[0]);
		} catch (NoSuchFieldException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static ParameterizedType asParameterizedType(Type type) {
		return (ParameterizedType) type;
	}
}
