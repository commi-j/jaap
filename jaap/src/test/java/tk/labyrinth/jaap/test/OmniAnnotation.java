package tk.labyrinth.jaap.test;

import java.lang.annotation.Annotation;

public @interface OmniAnnotation {

	Class<? extends Annotation>[] annotationTypes() default {};

	SuppressWarnings[] annotations() default {};

	String string() default "default";

	String stringWithoutDefault();

	String[] strings() default {};

	Class<?>[] types() default {};
}
