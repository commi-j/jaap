package tk.labyrinth.jaap.langmodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.handle.element.util.TypeParameterElementUtils;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactory;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactoryImpl;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class LangmodelElementFactoryImplTest {

	@Test
	void testGetTypeParameterWithString(ProcessingEnvironment processingEnvironment) {
		LangmodelElementFactory elementFactory = new LangmodelElementFactoryImpl(processingEnvironment);
		//
		Assertions.assertEquals(
				"java.util:Optional%T",
				TypeParameterElementUtils.getSignature(
						processingEnvironment,
						elementFactory.getTypeParameter("java.util:Optional%T")));
		Assertions.assertEquals(
				"java.util:Map%K",
				TypeParameterElementUtils.getSignature(
						processingEnvironment,
						elementFactory.getTypeParameter("java.util:Map%K")));
		Assertions.assertEquals(
				"java.util:Optional#map(java.util.function:Function)%U",
				TypeParameterElementUtils.getSignature(
						processingEnvironment,
						elementFactory.getTypeParameter(
								"java.util:Optional#map(java.util.function:Function)%U")));
		{
			// TODO: Test for constructor.
		}
		{
			// TODO: Support qualified, not only canonical signatures.
//			Assertions.assertEquals(
//					"java.util:Map.Entry%V",
//					TypeParameterElementUtils.getSignature(
//							processingEnvironment,
//							elementFactory.getTypeParameter("java.util:HashMap.Entry%V")));
		}
	}

	@Test
	void testGetTypeWithString(ProcessingEnvironment processingEnvironment) {
		LangmodelElementFactory elementFactory = new LangmodelElementFactoryImpl(processingEnvironment);
		//
		Assertions.assertEquals(
				"java.util:Map.Entry",
				TypeElementUtils.getSignature(elementFactory.getType("java.util:Map.Entry")));
		{
			// TODO: Currently not supported.
//		Assertions.assertEquals(
//				"java.util:Map.Entry",
//				TypeElementUtils.getSignature(elementFactory.getType("java.util:HashMap.Entry")));
		}
	}
}