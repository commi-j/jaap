package tk.labyrinth.jaap.langmodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.langmodel.factory.LangmodelElementFactoryImpl;
import tk.labyrinth.jaap.langmodel.factory.LangmodelTypeMirrorFactoryImpl;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class LangmodelTypeMirrorFactoryImplTest {

	@Test
	void testFindVariable(ProcessingEnvironment processingEnvironment) {
		LangmodelTypeMirrorFactoryImpl typeMirrorFactory = new LangmodelTypeMirrorFactoryImpl(
				new LangmodelElementFactoryImpl(processingEnvironment),
				processingEnvironment);
		//
		Assertions.assertEquals(
				"K",
				typeMirrorFactory.getVariable(TypeDescription.builder()
						.fullName("java.util.Map%K")
						.build()).toString());
		Assertions.assertNull(
				typeMirrorFactory.findVariable(
						TypeDescription.builder()
								.fullName("java.util.Map%T")
								.build()));
	}

	@Test
	void testFindWithParameterizedTypeDescription(ProcessingEnvironment processingEnvironment) {
		LangmodelTypeMirrorFactoryImpl typeMirrorFactory = new LangmodelTypeMirrorFactoryImpl(
				new LangmodelElementFactoryImpl(processingEnvironment),
				processingEnvironment);
		//
		Assertions.assertEquals(
				"T",
				String.valueOf(typeMirrorFactory.find(TypeDescription.of("java.util:Optional%T"))));
		Assertions.assertNull(
				typeMirrorFactory.find(TypeDescription.of("java.util:Optional%U")));
		//
		Assertions.assertEquals(
				"U",
				String.valueOf(typeMirrorFactory.find(TypeDescription.of("java.util:Optional#map(java.util.function:Function)%U"))));
		Assertions.assertNull(
				typeMirrorFactory.find(TypeDescription.of("java.util:Optional#map(java.util.function:Function)%T")));
		//
		Assertions.assertNull(
				typeMirrorFactory.find(TypeDescription.of("java.lang:Object#nonexistent()%T")));
	}
}