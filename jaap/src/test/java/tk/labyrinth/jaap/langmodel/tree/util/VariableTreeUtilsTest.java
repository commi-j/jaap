package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.VariableTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.element.util.VariableElementUtils;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.variable.Variables;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.Arrays;
import java.util.List;

@ExtendWithJaap
class VariableTreeUtilsTest {

	@CompilationTarget(sourceTypes = Variables.class)
	@Test
	void testFindElement(
			AnnotationProcessingRound annotationProcessingRound,
			ProcessingEnvironment processingEnvironment) {
		List<? extends Pair<CompilationUnitContext, ? extends VariableTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, VariableTree.class);
		//
		Assertions.assertEquals(
				Arrays.asList(
						VariableElementUtils.resolveField(processingEnvironment, Variables.class, "field"),
						VariableElementUtils.resolveParameter(
								processingEnvironment,
								Variables.class,
								"method(java.lang:Number)",
								0),
						null,
						null),
				ListUtils.mapNonnull(pairs, pair -> VariableTreeUtils.findElement(pair.getRight())));
	}

	@CompilationTarget(sourceTypes = Variables.class)
	@Test
	void testFindTypeMirror(
			AnnotationProcessingRound annotationProcessingRound,
			ProcessingEnvironment processingEnvironment) {
		List<? extends Pair<CompilationUnitContext, ? extends VariableTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, VariableTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"java.math.BigDecimal",
						"java.lang.Number",
						"java.math.BigInteger",
						"java.io.Serializable"),
				pairs.stream()
						.map(pair -> VariableTreeUtils.findTypeMirror(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}
}
