package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayTypeTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.ArrayTypes;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class ArrayTypeTreeUtilsTest {

	@CompilationTarget(sourceTypes = ArrayTypes.class)
	@Test
	void testFindArrayType(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends ArrayTypeTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, ArrayTypeTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"int[]",
						"java.lang.Integer[]"),
				pairs.stream()
						.map(pair -> ArrayTypeTreeUtils.findArrayType(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}
}