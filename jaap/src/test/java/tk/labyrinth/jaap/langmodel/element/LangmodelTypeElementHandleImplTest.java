package tk.labyrinth.jaap.langmodel.element;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.testing.model.MemberAmbiguity;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;
import java.util.Map;

@ExtendWithJaap
class LangmodelTypeElementHandleImplTest {

	@Disabled
	@Test
	void testGetAllMethods(ProcessingContext context) {
		// TODO: Make it check it finds methods in supers.
//		Assertions.assertEquals(167, context.getTypeElementHandle(BigDecimal.class).getAllMethods().count());
//		Assertions.assertEquals(18, context.getTypeElementHandle(Number.class).getAllMethods().count());
//		Assertions.assertEquals(13, context.getTypeElementHandle(Comparable.class).getAllMethods().count());
//		Assertions.assertEquals(12, context.getTypeElementHandle(Object.class).getAllMethods().count());
	}

	@Test
	void testGetAllMethodsAndGetDeclaredMethods(ProcessingContext processingContext) {
		// FIXME: Replace with 2 proper tests.
		Assertions.assertEquals(
				processingContext.getTypeElementHandle(Object.class).getDeclaredMethods().size(),
				processingContext.getTypeElementHandle(Object.class).getAllMethodStream().count());
		Assertions.assertTrue(
				processingContext.getTypeElementHandle(Comparable.class).getDeclaredMethods().size() <
						processingContext.getTypeElementHandle(Comparable.class).getAllMethodStream().count());
		Assertions.assertTrue(
				processingContext.getTypeElementHandle(Number.class).getDeclaredMethods().size() <
						processingContext.getTypeElementHandle(Number.class).getAllMethodStream().count());
	}

	@Test
	void testGetDeclaredMethods(ProcessingContext context) {
		// FIXME: Number may be different for different versions of Java.
//		Assertions.assertEquals(148, context.getTypeElementHandle(BigDecimal.class).getDeclaredMethods().count());
//		Assertions.assertEquals(6, context.getTypeElementHandle(Number.class).getDeclaredMethods().count());
//		Assertions.assertEquals(1, context.getTypeElementHandle(Comparable.class).getDeclaredMethods().count());
//		Assertions.assertEquals(12, context.getTypeElementHandle(Object.class).getDeclaredMethods().count());
	}

	@Test
	void testGetTypeChain(ProcessingContext processingContext) {
		ContribAssertions.assertEquals(List.of(
				processingContext.getTypeElementHandle(Map.class)
		), processingContext.getTypeElementHandle(Map.class).getTypeChain());
		ContribAssertions.assertEquals(List.of(
				processingContext.getTypeElementHandle(Map.Entry.class),
				processingContext.getTypeElementHandle(Map.class)
		), processingContext.getTypeElementHandle(Map.Entry.class).getTypeChain());
	}

	@Test
	void testSelectMemberWithChain(ProcessingContext processingContext) {
		{
			TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(MemberAmbiguity.class);
			ElementHandle memberHandle = typeElementHandle.selectMember(EntitySelectorChain.forVariable(
					List.of("Member")));
			//
			Assertions.assertNotNull(memberHandle);
			Assertions.assertTrue(memberHandle.isFieldElement());
			Assertions.assertEquals("Member", memberHandle.getName());
		}
		{
			TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(
					MemberAmbiguity.WithNestedType.class);
			ElementHandle memberHandle = typeElementHandle.selectMember(EntitySelectorChain.forType(List.of("Member")));
			//
			Assertions.assertNotNull(memberHandle);
			Assertions.assertTrue(memberHandle.isTypeElement());
			Assertions.assertEquals("Member", memberHandle.getName());
		}
	}

	@Test
	void testSelectMemberWithSelector(ProcessingContext processingContext) {
		{
			TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(MemberAmbiguity.class);
			ElementHandle memberHandle = typeElementHandle.selectMember(
					EntitySelector.build(EntitySelectionContext.forVariable(), "Member"));
			Assertions.assertNotNull(memberHandle);
			Assertions.assertTrue(memberHandle.isFieldElement());
			Assertions.assertEquals("Member", memberHandle.getName());
		}
		{
			TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(MemberAmbiguity.WithNestedType.class);
			ElementHandle memberHandle = typeElementHandle.selectMember(
					EntitySelector.build(EntitySelectionContext.forType(), "Member"));
			Assertions.assertNotNull(memberHandle);
			Assertions.assertTrue(memberHandle.isTypeElement());
			Assertions.assertEquals("Member", memberHandle.getName());
		}
	}

	@Disabled("FIXME: Member selection is now working now")
	@Test
	void testSelectMethodElementOrFail(ProcessingContext processingContext) {
		{
			// Choosing between java.lang.String#length() and java.lang.CharSequence#length().
			//
			Assertions.assertEquals(
					processingContext.getTypeElementHandle(String.class),
					processingContext.getTypeElementHandle(String.class)
							.selectMethodElementOrFail(MethodSimpleSignature.of("length()"))
							.getParent());
		}
	}

	@Disabled("FIXME: Member selection is now working now")
	@Test
	void testSelectMethodElementWithMethodSimpleSignature(ProcessingContext processingContext) {
		TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(String.class);
		{
			MethodElementHandle methodHandle = typeElementHandle.selectMethodElement(
					MethodSimpleSignature.of("valueOf(int)"));
			Assertions.assertNotNull(methodHandle);
			Assertions.assertEquals("java.lang:String#valueOf(int)", methodHandle.toString());
		}
		{
			MethodElementHandle methodHandle = typeElementHandle.selectMethodElement(
					MethodSimpleSignature.of("valueOf(java.lang.Object)"));
			Assertions.assertNotNull(methodHandle);
			Assertions.assertEquals("java.lang:String#valueOf(java.lang:Object)", methodHandle.toString());
		}
	}

	@Disabled("FIXME: Member selection is now working now")
	@Test
	void testSelectMethodElementWithStringAndTypeHandleArray(ProcessingContext processingContext) {
		TypeElementHandle typeElementHandle = processingContext.getTypeElementHandle(String.class);
		{
			MethodElementHandle methodHandle = typeElementHandle.selectMethodElement(
					"valueOf",
					processingContext.getTypeHandle(int.class));
			Assertions.assertNotNull(methodHandle);
			Assertions.assertEquals("java.lang:String#valueOf(int)", methodHandle.toString());
		}
		{
			MethodElementHandle methodHandle = typeElementHandle.selectMethodElement(
					"valueOf",
					processingContext.getTypeHandle(Object.class));
			Assertions.assertNotNull(methodHandle);
			Assertions.assertEquals("java.lang:String#valueOf(java.lang:Object)", methodHandle.toString());
		}
	}
}
