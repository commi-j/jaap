package tk.labyrinth.jaap.langmodel.tree.model;

import com.sun.source.tree.MemberSelectTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class CompilationUnitContextTest {

	@CompilationTarget(sourceTypes = CompilationUnitContextTest.class)
	@Test
	void testFindElement(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MemberSelectTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MemberSelectTree.class)
				.stream()
				.limit(2)
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.langmodel.tree.model",
						"null"),
				pairs.stream()
						.map(pair -> pair.getLeft().findElement(pair.getRight()))
						.map(String::valueOf)
						.toList());
		//
		// com.sun.source.util.Trees returns Element for Package without Classes
		// while javax.lang.model.util.Elements returns null. Ensuring this behaviour here.
		Pair<CompilationUnitContext, ? extends MemberSelectTree> pair = pairs.get(1);
		Assertions.assertNotNull(pair.getLeft().getTrees().getElement(pair.getLeft().getTreePath(pair.getRight())));
		Assertions.assertNull(pair.getLeft().getProcessingEnvironment().getElementUtils().getPackageElement(
				pair.getRight().toString()));
	}
}