package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.MethodInvocationTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.LambdaMethodSelections;
import tk.labyrinth.jaap.test.model.methodinvocation.ChainedMethodInvocations;
import tk.labyrinth.jaap.test.model.methodinvocation.MethodInvocationWithThisTarget;
import tk.labyrinth.jaap.test.model.methodinvocation.MethodInvocationsWithSupplierLambdas;
import tk.labyrinth.jaap.test.model.methodinvocation.MethodInvocationsWithinLambda;
import tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocationWithExternalField;
import tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocations;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.io.Console;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ExtendWithJaap
class MethodInvocationTreeUtilsTest {
//	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
//	@Test
//	void testBuildMethodSelectionContext(
//			AnnotationProcessingRound annotationProcessingRound,
//			ProcessingEnvironment processingEnvironment) {
//		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
//				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
//				.stream()
//				.filter(pair -> Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
//				.toList();
//		//
//		Assertions.assertEquals(
//				List.of(EntitySelectionContext.forMethod(TypeMirrorUtils.toDescription(
//						processingEnvironment,
//						TypeMirrorUtils.resolve(processingEnvironment, BigDecimal.class)))),
//				ListUtils.mapNonnull(pairs, tree -> MethodInvocationTreeUtils.buildMethodSelectionContext(
//						node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
//	}
//
//	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
//	@Test
//	void testFindArgumentTypes(AnnotationProcessingRound round) {
//		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
//				.filter(node -> !node.getMethodName().contentEquals("super"))
//				.toList();
//		//
//		Assertions.assertEquals(
//				List.of(List.of("java.math.BigDecimal")),
//				nodes.stream()
//						.map(node -> MethodInvocationTreeUtils.findArgumentTypes(
//								node.getNodeContext().getTreeContext(),
//								node.getMethodInvocationTree()))
//						.map(argumentTypes -> argumentTypes.stream().map(String::valueOf).toList())
//						.toList());
//	}

	@CompilationTarget(sourceTypes = LambdaMethodSelections.class)
	@Test
	void testCreateMethodSelectorWithLambdas(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "foo"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"foo(()->java.lang:Object!)",
						"foo(()->java.lang:Object?)",
						"foo(?->java.lang:Object!)"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.createMethodSelector(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@CompilationTarget(sourceTypes = ChainedMethodInvocations.class)
	@Test
	void testFindMethod(
			AnnotationProcessingRound annotationProcessingRound,
			ProcessingEnvironment processingEnvironment) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						ExecutableElementUtils.resolve(processingEnvironment, Object.class, "toString()"),
						ExecutableElementUtils.resolve(processingEnvironment, Console.class, "reader()"),
						ExecutableElementUtils.resolve(processingEnvironment, System.class, "console()")),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findMethod(pair.getLeft(), pair.getRight()))
						.collect(Collectors.toList()));
	}

	@CompilationTarget(sourceTypes = MethodInvocationsWithSupplierLambdas.class)
	@Test
	void testFindMethodWithLambda(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"<T>of(T)",
						"orElseGet(java.util.function.Supplier<? extends T>)",
						"getBar()"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findMethod(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@Disabled // TODO: tk.labyrinth.jaap.langmodel.entity.EntityUtils.selectMostSpecificMethod
	@CompilationTarget(sourceTypes = MethodInvocationsWithinLambda.class)
	@Test
	void testFindMethodWithinLambda(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"",
						"",
						""),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findMethod(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
	@Test
	void testFindReturnType(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"java.lang.String"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findReturnType(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@CompilationTarget(sourceTypes = MethodInvocationWithThisTarget.class)
	@Test
	void testFindTargetEntityWithThisTarget(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test.model.methodinvocation.MethodInvocationWithThisTarget.this"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findTargetEntity(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocations.class)
	@Test
	void testFindTargetType(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class)
				.stream()
				.filter(pair -> !Objects.equals(MethodInvocationTreeUtils.getName(pair.getRight()), "super"))
				.toList();
		//
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocations",
						"tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocations",
						"tk.labyrinth.jaap.test.model.methodinvocation.StaticMethodInvocations"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.findTargetType(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocations.class)
	@Test
	void testGetName(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MethodInvocationTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, MethodInvocationTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"super",
						"testMethod",
						"testMethod",
						"testMethod"),
				pairs.stream()
						.map(pair -> MethodInvocationTreeUtils.getName(pair.getRight()))
						.toList());
	}
}
