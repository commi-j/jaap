package tk.labyrinth.jaap.langmodel.element;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.test.RepeatableAnnotation;
import tk.labyrinth.jaap.test.model.ComplexParameters;
import tk.labyrinth.jaap.test.model.MethodParameters;
import tk.labyrinth.jaap.test.model.VarargMethodsAndInheritance;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

@ExtendWithJaap
class LangmodelMethodElementHandleImplTest {

	@Test
	void testGetFormalParameter(ProcessingContext processingContext) {
		{
			MethodElementHandle handle = processingContext.getMethodElementHandle(
					"java.lang:Object#wait(long,int)");
			//
			Assertions.assertEquals(
					"java.lang:Object#wait(long,int)#0",
					handle.getFormalParameter(0).toString());
			Assertions.assertEquals(
					"java.lang:Object#wait(long,int)#1",
					handle.getFormalParameter(1).toString());
		}
	}

	@Test
	void testGetFormalParameters(ProcessingContext processingContext) {
		Assertions.assertEquals(
				List.of(
						"java.lang:Object#wait(long,int)#0",
						"java.lang:Object#wait(long,int)#1"),
				ListUtils.mapToString(
						processingContext
								.getMethodElementHandle("java.lang:Object#wait(long,int)")
								.getFormalParameters()));
	}

	@Test
	void testGetFullSignature(ProcessingContext context) {
		TypeElementHandle handle = context.getTypeElementHandle(Object.class);
		//
		Assertions.assertEquals(
				"java.lang:Object#getClass()",
				handle.getDeclaredMethodByName("getClass").getFullSignature().toString());
		Assertions.assertEquals(
				"java.lang:Object#hashCode()",
				handle.getDeclaredMethodByName("hashCode").getFullSignature().toString());
		//
		ContribAssertions.assertEquals(
				List.of(
						"java.lang:Object#wait()",
						"java.lang:Object#wait(long)",
						"java.lang:Object#wait(long,int)"),
				handle.getDeclaredMethodStreamByName("wait")
						.map(MethodElementHandle::getFullSignature)
						.map(MethodFullSignature::toString)
						.sorted());
	}

	@Test
	void testGetMergedAnnotationGetRelevantAnnotations(ProcessingContext processingContext) {
		TypeElementHandle myInterfaceHandle = processingContext.getTypeElementHandle(
				RepeatableAnnotation.MyInterface.class);
		AnnotationTypeHandle repeatableAnnotationHandle = processingContext.getAnnotationTypeHandle(
				RepeatableAnnotation.class);
		{
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=0)",
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=1)"),
					myInterfaceHandle.getDeclaredMethodByName("multiple")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=2)"),
					myInterfaceHandle.getDeclaredMethodByName("repeated")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=3)"),
					myInterfaceHandle.getDeclaredMethodByName("single")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
		}
		{
			ContribAssertions.assertEquals(
					List.of(),
					myInterfaceHandle.getDeclaredMethodByName("nothing")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream());
		}
	}

	@Test
	void testGetReturnType(ProcessingContext context) {
		Assertions.assertEquals("java.lang.Class<?>", context.getMethodElementHandle(
				"java.lang:Object#getClass()").getReturnType().toString());
		Assertions.assertEquals("int", context.getMethodElementHandle(
				"java.lang:Object#hashCode()").getReturnType().toString());
		Assertions.assertEquals("void", context.getMethodElementHandle(
				"java.lang:Object#notify()").getReturnType().toString());
	}

	@Test
	void testGetSignature(ProcessingContext context) {
		Assertions.assertEquals(
				"java.lang:Object#wait()",
				context.getMethodElementHandle("java.lang:Object#wait()").getSignature());
		Assertions.assertEquals(
				"java.lang:Object#wait(long)",
				context.getMethodElementHandle("java.lang:Object#wait(long)").getSignature());
		Assertions.assertEquals(
				"java.lang:Object#wait(long,int)",
				context.getMethodElementHandle("java.lang:Object#wait(long,int)").getSignature());
		Assertions.assertEquals(
				"java.util:Optional#of(java.lang:Object)",
				context.getMethodElementHandleByName("java.util:Optional#of").getSignature());
	}

	@Test
	void testGetSimpleSignature(ProcessingContext context) {
		{
			TypeElementHandle typeHandle = context.getTypeElementHandle(Object.class);
			//
			Assertions.assertEquals(
					"getClass()",
					typeHandle.getDeclaredMethodByName("getClass").getSimpleSignature().toString());
			Assertions.assertEquals(
					"hashCode()",
					typeHandle.getDeclaredMethodByName("hashCode").getSimpleSignature().toString());
			//
			ContribAssertions.assertEquals(
					List.of(
							"wait()",
							"wait(long)",
							"wait(long,int)"),
					typeHandle.getDeclaredMethodStreamByName("wait")
							.map(MethodElementHandle::getSimpleSignature)
							.map(MethodSimpleSignature::toString)
							.sorted());
		}
		{
			TypeElementHandle typeHandle = context.getTypeElementHandle(MethodParameters.class);
			//
			Assertions.assertEquals(
					"primitive(int)",
					typeHandle.getDeclaredMethodByName("primitive").getSimpleSignature().toString());
			Assertions.assertEquals(
					"object(java.lang:Object)",
					typeHandle.getDeclaredMethodByName("object").getSimpleSignature().toString());
			Assertions.assertEquals(
					"integerList(java.util:List)",
					typeHandle.getDeclaredMethodByName("integerList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"rawtypeList(java.util:List)",
					typeHandle.getDeclaredMethodByName("rawtypeList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"simpleVariableType(java.lang:Object)",
					typeHandle.getDeclaredMethodByName("simpleVariableType").getSimpleSignature().toString());
			Assertions.assertEquals(
					"string(java.lang:String)",
					typeHandle.getDeclaredMethodByName("string").getSimpleSignature().toString());
			Assertions.assertEquals(
					"unboundedWildcardList(java.util:List)",
					typeHandle.getDeclaredMethodByName("unboundedWildcardList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"extendsVariableType(java.lang:Number)",
					typeHandle.getDeclaredMethodByName("extendsVariableType").getSimpleSignature().toString());
		}
		{
			TypeElementHandle typeHandle = context.getTypeElementHandle(ComplexParameters.class);
			//
			Assertions.assertEquals(
					"comparable(java.lang:Comparable)",
					typeHandle.getDeclaredMethodByName("comparable").getSimpleSignature().toString());
			Assertions.assertEquals(
					"map(java.util:Map)",
					typeHandle.getDeclaredMethodByName("map").getSimpleSignature().toString());
			Assertions.assertEquals(
					"put(tk.labyrinth.jaap.test.model:ComplexParameters,java.util:List)",
					typeHandle.getDeclaredMethodByName("put").getSimpleSignature().toString());
		}
	}

	@Test
	void testGetSimpleSignatureWithArrayAndVarargParameters(ProcessingContext context) {
		MethodElementHandle arrayMethodElementHandle = context.getMethodElementHandleByName(VarargMethodsAndInheritance.Parent.class, "array");
		MethodElementHandle varargMethodElementHandle = context.getMethodElementHandleByName(VarargMethodsAndInheritance.Parent.class, "vararg");
		//
		{
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#array(int[])",
					arrayMethodElementHandle.toString());
			Assertions.assertEquals(
					"array(int[])",
					arrayMethodElementHandle.getSimpleSignature().toString());
		}
		{
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])",
					varargMethodElementHandle.toString());
			Assertions.assertEquals(
					"vararg(int[])",
					varargMethodElementHandle.getSimpleSignature().toString());
		}
	}

	@Test
	void testHasMergedAnnotation(ProcessingContext processingContext) {
		TypeElementHandle myInterfaceHandle = processingContext.getTypeElementHandle(
				RepeatableAnnotation.MyInterface.class);
		AnnotationTypeHandle repeatableAnnotationHandle = processingContext.getAnnotationTypeHandle(
				RepeatableAnnotation.class);
		{
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethodByName("multiple").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethodByName("repeated").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethodByName("single").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
		}
		{
			Assertions.assertFalse(
					myInterfaceHandle.getDeclaredMethodByName("nothing").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
		}
	}

	@Test
	void testsOfHasTypeParameters(ProcessingContext context) {
		{
			// No TypeParameters
			//
			MethodElementHandle compareMethod = context.getMethodElementHandleByName(
					"java.util:Comparator#compare");
			//
			ContribAssertions.assertEquals(
					List.of(),
					compareMethod.getTypeParameterStream().map(TypeParameterElementHandle::getSignatureString));
			Assertions.assertNull(compareMethod.findTypeParameter("T"));
			Assertions.assertNull(compareMethod.findTypeParameter(1));
		}
		{
			// Two TypeParameters
			//
			MethodElementHandle comparingOneMethod = context.getMethodElementHandle(
					"java.util:Comparator#comparing(java.util.function:Function)");
			//
			ContribAssertions.assertEquals(
					List.of(
							"java.util:Comparator#comparing(java.util.function:Function)%T",
							"java.util:Comparator#comparing(java.util.function:Function)%U"),
					comparingOneMethod.getTypeParameterStream().map(TypeParameterElementHandle::getSignatureString));
			Assertions.assertEquals("java.util:Comparator#comparing(java.util.function:Function)%T",
					comparingOneMethod.getTypeParameter("T").getSignatureString());
			Assertions.assertEquals("java.util:Comparator#comparing(java.util.function:Function)%U",
					comparingOneMethod.getTypeParameter(1).getSignatureString());
		}
	}
}
