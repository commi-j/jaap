package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.ArrayAccessTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.ArrayAccesses;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class ArrayAccessTreeUtilsTest {

	@CompilationTarget(sourceTypes = ArrayAccesses.class)
	@Test
	void testFindType(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends ArrayAccessTree>> pairs = TestTreeUtils
				.getTreesOfClassWithinStaticInitializers(annotationProcessingRound, ArrayAccessTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"java.lang.reflect.Type",
						"java.lang.reflect.Type"),
				pairs.stream()
						.map(pair -> ArrayAccessTreeUtils.findTypeMirror(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}
}