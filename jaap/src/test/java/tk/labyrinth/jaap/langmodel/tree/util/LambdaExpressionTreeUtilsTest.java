package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.LambdaExpressionTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.LambdaMethodSelections;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class LambdaExpressionTreeUtilsTest {

	@CompilationTarget(sourceTypes = LambdaMethodSelections.class)
	@Test
	void testFindBodyTypeMirror(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends LambdaExpressionTree>> pairs = TestTreeUtils
				.getTreesOfClass(annotationProcessingRound, LambdaExpressionTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"java.io.PrintStream",
						"void",
						"java.lang.Object"),
				pairs.stream()
						.map(pair -> LambdaExpressionTreeUtils.findBodyTypeMirror(pair.getLeft(), pair.getRight()))
						.map(String::valueOf)
						.toList());
	}
}