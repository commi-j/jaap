package tk.labyrinth.jaap.langmodel.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class LangmodelTypeHandleFactoryImplTest {

	@Test
	void testFindWithTypeDescription(ProcessingContext processingContext) {
		// TODO: Inject TypeHandleFactory.
		LangmodelTypeHandleFactory typeHandleFactory = processingContext.getTypeHandleFactory();
		//
		Assertions.assertEquals(
				"int",
				String.valueOf(typeHandleFactory.find(TypeDescription.of("int"))));
		//
		Assertions.assertNull(
				typeHandleFactory.find(TypeDescription.of("nonexistent")));
	}
}