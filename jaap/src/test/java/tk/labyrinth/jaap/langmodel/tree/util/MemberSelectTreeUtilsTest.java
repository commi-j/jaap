package tk.labyrinth.jaap.langmodel.tree.util;

import com.sun.source.tree.MemberSelectTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.langmodel.tree.model.CompilationUnitContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.test.TestTreeUtils;
import tk.labyrinth.jaap.test.model.memberselect.MemberSelectWithFieldOnMethod;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class MemberSelectTreeUtilsTest {

	@CompilationTarget(sourceTypes = MemberSelectWithFieldOnMethod.class)
	@Test
	void testFindEntityWhenVariableOnMethod(AnnotationProcessingRound annotationProcessingRound) {
		List<? extends Pair<CompilationUnitContext, ? extends MemberSelectTree>> pairs = TestTreeUtils
				.getTreesOfClassWithinStaticInitializers(annotationProcessingRound, MemberSelectTree.class);
		//
		Assertions.assertEquals(
				List.of(
						"TEN"),
				pairs.stream()
						.map(pair -> MemberSelectTreeUtils.findEntity(
								pair.getLeft(),
								pair.getRight(),
								EntitySelectionContext.forVariable()))
						.map(String::valueOf)
						.toList());
	}
}