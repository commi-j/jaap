package tk.labyrinth.jaap.handle.element.enhanced;

import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.element.synthetic.SyntheticElementHandleRegistry;
import tk.labyrinth.jaap.enhanced.EnhancedProcessing;
import tk.labyrinth.jaap.enhanced.element.EnhancedTypeElementHandle;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class EnhancedTypeElementHandleTest {

	@Test
	void testProcessingContextGetTypeElementHandle(ProcessingEnvironment processingEnvironment) {
		ProcessingContext processingContext = EnhancedProcessing.createContext(processingEnvironment,
				new SyntheticElementHandleRegistry());
		//
		Class<?> type = String.class;
		String typeName = type.getCanonicalName();
		//
		ContribAssertions.assertInstanceOf(EnhancedTypeElementHandle.class, processingContext.getTypeElementHandle(type));
		ContribAssertions.assertInstanceOf(EnhancedTypeElementHandle.class, processingContext.getTypeElementHandle(typeName));
	}
}
