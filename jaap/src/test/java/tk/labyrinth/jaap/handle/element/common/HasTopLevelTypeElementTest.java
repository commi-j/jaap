package tk.labyrinth.jaap.handle.element.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.Map;

@ExtendWithJaap
class HasTopLevelTypeElementTest {

	@Test
	void testFormalParameterElementHandle(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getMethodElementHandleByName(Map.class, "containsKey")
						.getFormalParameter(0).getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getMethodElementHandleByName(Map.Entry.class, "setValue")
						.getFormalParameter(0).getTopLevelTypeElement());
	}

	@Test
	void testMethodElementHandle(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getMethodElementHandleByName(Map.class, "size").getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getMethodElementHandleByName(Map.Entry.class, "getKey").getTopLevelTypeElement());
	}

	@Test
	void testTypeElementHandle(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getTypeElementHandle(Map.class).getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementHandle(Map.class),
				context.getTypeElementHandle(Map.Entry.class).getTopLevelTypeElement());
	}
}
