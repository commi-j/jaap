package tk.labyrinth.jaap.handle.element;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.test.RepeatableAnnotation;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

@ExtendWithJaap
class ElementHandleExtraTest {

	/**
	 * TRELLO-5 - https://trello.com/c/J0nI6oq6/5<br>
	 * It was given true for any annotation if any repeatable was present.<br>
	 */
	@Test
	void testHasMergedAnnotationDoesNotGivesFalsePositive(ProcessingContext processingContext) {
		{
			Assertions.assertEquals(
					List.of(),
					processingContext.getTypeElementHandle(TestClass.class)
							.getMergedAnnotation(
									Override.class,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations());
			Assertions.assertFalse(
					processingContext.getTypeElementHandle(TestClass.class)
							.hasMergedAnnotation(
									Override.class,
									MergedAnnotationSpecification.javaCore()));
		}
		{
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=0)",
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=1)"),
					processingContext.getTypeElementHandle(TestClass.class)
							.getMergedAnnotation(
									RepeatableAnnotation.class,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
			Assertions.assertTrue(
					processingContext.getTypeElementHandle(TestClass.class)
							.hasMergedAnnotation(
									RepeatableAnnotation.class,
									MergedAnnotationSpecification.javaCore()));
		}
	}

	@RepeatableAnnotation(0)
	@RepeatableAnnotation(1)
	private static class TestClass {
		// empty
	}
}
