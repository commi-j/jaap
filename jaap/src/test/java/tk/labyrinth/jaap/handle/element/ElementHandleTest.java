package tk.labyrinth.jaap.handle.element;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class ElementHandleTest {

	@Disabled("TODO: Element must not have type-aware information, need to distribute to specific tests.")
	@Test
	void testResolveType(ProcessingContext context) {
//		{
//			// Field
//			Assertions.assertEquals(
//					context.getTypeHandle(BigDecimal.class),
//					context.getFieldElementHandle(BigDecimal.class, "ZERO").resolveType());
//		}
//		{
//			// Method
//			Assertions.assertEquals(
//					context.getTypeHandle(int.class),
//					context.getMethodElementHandle(BigDecimal.class, "precision(long,long)").resolveType());
//		}
//		{
//			// Package
//			ContribAssertions.assertThrows(
//					() -> context.getPackageElementHandleOf(BigDecimal.class).resolveType(),
//					fault -> {
//						Assertions.assertEquals(UnsupportedOperationException.class, fault.getClass());
//						Assertions.assertEquals(
//								"type = tk.labyrinth.jaap.model.element.old.handle.lang.LangPackageElementHandle & value = java.math",
//								fault.getMessage());
//					});
//			ContribAssertions.assertThrows(
//					() -> context.getPackageElementHandle("tk.labyrinth", true).resolveType(),
//					fault -> {
//						Assertions.assertEquals(UnsupportedOperationException.class, fault.getClass());
//						Assertions.assertEquals(
//								"type = tk.labyrinth.jaap.model.element.old.handle.synthetic.SyntheticPackageElementHandle & value = tk.labyrinth",
//								fault.getMessage());
//					});
//		}
//		{
//			// Type
//			Assertions.assertEquals(
//					context.getTypeHandle(BigDecimal.class),
//					context.getTypeElementHandle(BigDecimal.class).resolveType());
//		}
	}
}
