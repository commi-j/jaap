package tk.labyrinth.jaap.handle.element.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.langmodel.type.util.TypeMirrorUtils;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.test.model.ExecutableElements;
import tk.labyrinth.jaap.test.model.MethodSignatures;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.Objects;
import java.util.function.Consumer;

@ExtendWithJaap
class ExecutableElementUtilsTest {

	@Test
	void testGetDeclaredInstanceInitializers(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, ExecutableElements.class);
		//
		// TODO: 3 blocks merged into constructors. Find out if there may be explicit initializers left alive.
		Assertions.assertEquals(0, ExecutableElementUtils.getDeclaredInstanceInitializers(typeElement).count());
	}

	@Test
	void testGetDeclaredStaticInitializers(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, ExecutableElements.class);
		//
		// TODO: 2 blocks merged into one. Find out if there may be more than one explicit initializers left alive.
		Assertions.assertEquals(1, ExecutableElementUtils.getDeclaredStaticInitializers(typeElement).count());
	}

	@Test
	void testGetReturnTypeMirrorErasure(ProcessingEnvironment processingEnvironment) {
		TypeMirror returnType = ExecutableElementUtils.getReturnTypeMirrorErasure(processingEnvironment,
				ExecutableElementUtils.resolve(processingEnvironment, Object.class, "toString()"));
		//
		Assertions.assertEquals("java.lang.String", returnType.toString());
		Assertions.assertSame(TypeMirrorUtils.resolve(processingEnvironment, String.class), returnType);
	}

	/**
	 * If we have a method with formal parameter with annotation with {@link Target} with {@link ElementType#TYPE_USE} on it,
	 * this annotation is considered to be part of parameter's type and participates in type's {@link Object#toString()}.
	 * This results in incorrect method's signature string.<br>
	 * To simulate this case we add tested method's class to compilation targets as otherwise this issue won't trigger.<br>
	 *
	 * @param processingEnvironment non-null
	 */
	@CompilationTarget(sourceTypes = MethodSignatures.class)
	@Test
	void testGetSignatureStringWithFormalParameterWithTypeUseAnnotation(ProcessingEnvironment processingEnvironment) {
		{
			ExecutableElement executableElement = ExecutableElementUtils.getDeclaredMethodByName(
					processingEnvironment,
					MethodSignatures.class,
					"withLongFormalParameterWithTypeUseAnnotation");
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:MethodSignatures#withLongFormalParameterWithTypeUseAnnotation(long)",
					ExecutableElementUtils.getSignatureString(processingEnvironment, executableElement));
		}
		{
			ExecutableElement executableElement = ExecutableElementUtils.getDeclaredMethodByName(
					processingEnvironment,
					MethodSignatures.class,
					"withObjectFormalParameterWithTypeUseAnnotation");
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:MethodSignatures#withObjectFormalParameterWithTypeUseAnnotation(java.lang:Object)",
					ExecutableElementUtils.getSignatureString(processingEnvironment, executableElement));
		}
	}

	@Test
	void testGetWithMethod(ProcessingEnvironment processingEnvironment) throws NoSuchMethodException {
		Method method = Objects.class.getDeclaredMethod("equals", Object.class, Object.class);
		Assertions.assertEquals(
				"equals(java.lang.Object,java.lang.Object)",
				String.valueOf(ExecutableElementUtils.get(processingEnvironment, method)));
	}

	@Test
	void testIsEffectivelyAbstract(ProcessingEnvironment processingEnvironment) {
		{
			// Interface method.
			//
			Assertions.assertTrue(ExecutableElementUtils.isEffectivelyAbstract(ExecutableElementUtils
					.getDeclaredMethodByName(processingEnvironment, Consumer.class, "accept")));
		}
		{
			// Default interface method.
			//
			Assertions.assertFalse(ExecutableElementUtils.isEffectivelyAbstract(ExecutableElementUtils
					.getDeclaredMethodByName(processingEnvironment, Consumer.class, "andThen")));
		}
		{
			// Abstract method of abstract class.
			//
			Assertions.assertTrue(ExecutableElementUtils.isEffectivelyAbstract(ExecutableElementUtils
					.getDeclaredMethodByName(processingEnvironment, AbstractList.class, "get")));
		}
		{
			// Method of abstract class.
			//
			Assertions.assertFalse(ExecutableElementUtils.isEffectivelyAbstract(ExecutableElementUtils
					.getDeclaredMethodByName(processingEnvironment, AbstractList.class, "clear")));
		}
	}

	@Test
	void testIsStatic(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, ExecutableElements.class);
		//
		{
			Assertions.assertTrue(ExecutableElementUtils.isStatic(ExecutableElementUtils.resolve(
					processingEnvironment, typeElement, "staticMethod()")));
			Assertions.assertTrue(ExecutableElementUtils.isStatic(ExecutableElementUtils.getDeclaredStaticInitializers(
					typeElement).findFirst().orElseThrow()));
		}
		{
			Assertions.assertFalse(ExecutableElementUtils.isStatic(ExecutableElementUtils.resolve(
					processingEnvironment, typeElement, "instanceMethod()")));
			Assertions.assertFalse(ExecutableElementUtils.isStatic(ExecutableElementUtils.getConstructor(
					processingEnvironment, typeElement)));
		}
	}

	@Test
	void testResolveWithMethodSignature(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"wait()",
				ExecutableElementUtils
						.resolve(processingEnvironment, MethodFullSignature.of("java.lang:Object#wait()"))
						.toString());
		Assertions.assertEquals(
				"wait(long)",
				ExecutableElementUtils
						.resolve(processingEnvironment, MethodFullSignature.of("java.lang:Object#wait(long)"))
						.toString());
		Assertions.assertEquals(
				"wait(long,int)",
				ExecutableElementUtils
						.resolve(processingEnvironment, MethodFullSignature.of("java.lang:Object#wait(long,int)"))
						.toString());
	}

	@Test
	void testResolveWithString(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"wait()",
				ExecutableElementUtils
						.resolve(processingEnvironment, "java.lang:Object#wait()")
						.toString());
		Assertions.assertEquals(
				"wait(long)",
				ExecutableElementUtils
						.resolve(processingEnvironment, "java.lang:Object#wait(long)")
						.toString());
		Assertions.assertEquals(
				"wait(long,int)",
				ExecutableElementUtils
						.resolve(processingEnvironment, "java.lang:Object#wait(long,int)")
						.toString());
	}
}
