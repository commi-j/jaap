package tk.labyrinth.jaap.handle.element.util;

import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.test.model.VarargMethodsAndInheritance;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Set;
import java.util.stream.Stream;

@ExtendWithJaap
class MethodElementHandleUtilsTest {

	@Test
	void testFilterNonOverriden(ProcessingContext processingContext) {
		TypeElementHandle childTypeElementHandle = processingContext.getTypeElementHandle(
				VarargMethodsAndInheritance.Child.class);
		//
		ContribAssertions.assertEquals(
				Set.of(
						"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Child#array(int[])",
						"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Child#vararg(int[])"),
				MethodElementHandleUtils
						.filterNonOverriden(Stream.concat(
								childTypeElementHandle.getDeclaredMethodStream(),
								childTypeElementHandle.getSuperclass().getDeclaredMethodStream()))
						.map(MethodElementHandle::toString));
	}
}
