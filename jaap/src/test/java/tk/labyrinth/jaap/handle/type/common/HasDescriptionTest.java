package tk.labyrinth.jaap.handle.type.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class HasDescriptionTest {

	@Test
	void testGetDescription(ProcessingContext processingContext) {
		// FIXME: Replace TypeHandle with TypeHandle and signature with description.
//		{
//			// Array
//			//
//			Assertions.assertEquals(
//					"java.lang.Object[]",
//					processingContext.getTypeHandle(Object[].class).getSignature().toString());
//			Assertions.assertEquals(
//					"int[][]",
//					processingContext.getTypeHandle(int[][].class).getSignature().toString());
//		}
//		{
//			// Declared
//			//
//			Assertions.assertEquals(
//					"java.util.List[]",
//					processingContext.getTypeHandle(List[].class).getSignature().toString());
//			Assertions.assertEquals(
//					"java.lang.String[]",
//					processingContext.getTypeHandle(String[].class).getSignature().toString());
//		}
//		{
//			// Primitive
//			//
//			Assertions.assertEquals(
//					"boolean",
//					processingContext.getTypeHandle(boolean.class).getSignature().toString());
//			Assertions.assertEquals(
//					"byte",
//					processingContext.getTypeHandle(byte.class).getSignature().toString());
//			Assertions.assertEquals(
//					"char",
//					processingContext.getTypeHandle(char.class).getSignature().toString());
//			Assertions.assertEquals(
//					"double",
//					processingContext.getTypeHandle(double.class).getSignature().toString());
//			Assertions.assertEquals(
//					"float",
//					processingContext.getTypeHandle(float.class).getSignature().toString());
//			Assertions.assertEquals(
//					"int",
//					processingContext.getTypeHandle(int.class).getSignature().toString());
//			Assertions.assertEquals(
//					"long",
//					processingContext.getTypeHandle(long.class).getSignature().toString());
//			Assertions.assertEquals(
//					"short",
//					processingContext.getTypeHandle(short.class).getSignature().toString());
//		}
		{
			// Variable
			//
			// TODO
		}
		{
			// Void
			//
			Assertions.assertEquals(
					TypeDescription.of("void"),
					processingContext.getTypeHandle(void.class).getDescription());
		}
	}
}