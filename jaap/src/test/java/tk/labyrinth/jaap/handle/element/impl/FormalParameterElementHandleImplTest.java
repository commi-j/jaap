package tk.labyrinth.jaap.handle.element.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.test.model.MethodParameters;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class FormalParameterElementHandleImplTest {

	@Test
	void testGetDeclaration(ProcessingContext context) {
		MethodElementHandle handle = context.getMethodElementHandle(String.class, "getBytes(byte[],int,byte)");
		//
		Assertions.assertEquals(3, handle.getFormalParameterCount());
		Assertions.assertEquals("FormalParameterDeclaration(name=dst, type=byte[])", handle.getFormalParameter(0).getDeclaration().toString());
		Assertions.assertEquals("FormalParameterDeclaration(name=dstBegin, type=int)", handle.getFormalParameter(1).getDeclaration().toString());
		Assertions.assertEquals("FormalParameterDeclaration(name=coder, type=byte)", handle.getFormalParameter(2).getDeclaration().toString());
	}

	@Test
	void testGetSignatureString(ProcessingContext context) {
		MethodElementHandle handle = context.getMethodElementHandle(String.class, "getBytes(byte[],int,byte)");
		//
		Assertions.assertEquals(3, handle.getFormalParameterCount());
		Assertions.assertEquals("java.lang:String#getBytes(byte[],int,byte)#0", handle.getFormalParameter(0).getSignatureString());
		Assertions.assertEquals("java.lang:String#getBytes(byte[],int,byte)#1", handle.getFormalParameter(1).getSignatureString());
		Assertions.assertEquals("java.lang:String#getBytes(byte[],int,byte)#2", handle.getFormalParameter(2).getSignatureString());
	}

	@Test
	void testGetTypeHandle(ProcessingContext context) {
		{
			// ParameterizedType with PlainTypeParameter
			//
			TypeHandle typeHandle = context.getMethodElementHandleByName(
					MethodParameters.class, "integerList")
					.getFormalParameter(0).getType();
			//
			Assertions.assertEquals(
					"java.util:List<java.lang:Integer>",
					typeHandle.getDescription().toString());
			Assertions.assertTrue(typeHandle.isParameterizedType());
			Assertions.assertFalse(typeHandle.isPlainType());
			Assertions.assertFalse(typeHandle.isRawType());
		}
		{
			// ParameterizedType with UnboundedWildcardTypeParameter
			//
			TypeHandle typeHandle = context.getMethodElementHandleByName(
					MethodParameters.class, "unboundedWildcardList")
					.getFormalParameter(0).getType();
			//
			Assertions.assertEquals(
					"java.util:List<?>",
					typeHandle.getDescription().toString());
			Assertions.assertTrue(typeHandle.isParameterizedType());
			Assertions.assertFalse(typeHandle.isPlainType());
			Assertions.assertFalse(typeHandle.isRawType());
		}
		{
			// PlainType
			//
			TypeHandle typeHandle = context.getMethodElementHandleByName(
					MethodParameters.class, "string")
					.getFormalParameter(0).getType();
			//
			Assertions.assertEquals("java.lang.String", typeHandle.toString());
			Assertions.assertFalse(typeHandle.isParameterizedType());
			Assertions.assertTrue(typeHandle.isPlainType());
			Assertions.assertFalse(typeHandle.isRawType());
		}
		{
			// RawType
			//
			TypeHandle typeHandle = context.getMethodElementHandleByName(
					MethodParameters.class, "rawtypeList")
					.getFormalParameter(0).getType();
			//
			Assertions.assertEquals(
					"java.util:List",
					typeHandle.getDescription().toString());
			Assertions.assertFalse(typeHandle.isParameterizedType());
			Assertions.assertFalse(typeHandle.isPlainType());
			Assertions.assertTrue(typeHandle.isRawType());
		}
		{
			// VariableType
			//
			TypeHandle typeHandle = context.getMethodElementHandleByName(
					MethodParameters.class, "simpleVariableType")
					.getFormalParameter(0).getType();
			//
			Assertions.assertEquals("E", typeHandle.toString());
			Assertions.assertTrue(typeHandle.isVariableType());
		}
	}
}
