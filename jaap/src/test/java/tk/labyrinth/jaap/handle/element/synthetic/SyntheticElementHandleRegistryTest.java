package tk.labyrinth.jaap.handle.element.synthetic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.jaap.test.model.declaration.TestDeclarations;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.math.BigDecimal;
import java.util.Map;

@ExtendWithJaap
class SyntheticElementHandleRegistryTest {

	@Test
	void testGetMethod(ProcessingContext processingContext) {
		SyntheticElementHandleRegistry syntheticElementHandleRegistry = new SyntheticElementHandleRegistry();
		syntheticElementHandleRegistry.setProcessingContext(processingContext);
		syntheticElementHandleRegistry.registerMethodDeclaration(
				TypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		syntheticElementHandleRegistry.registerMethodDeclaration(
				TypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanInteger());
		//
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.math:BigDecimal)",
				syntheticElementHandleRegistry
						.getMethod("java.math:BigDecimal#greaterThan(java.math:BigDecimal)")
						.getFullSignature().toString());
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.lang:Integer)",
				syntheticElementHandleRegistry
						.getMethod("java.math:BigDecimal#greaterThan(java.lang:Integer)")
						.getFullSignature().toString());
	}

	@Test
	void testGetMethodOfNestedClass(ProcessingContext processingContext) {
		SyntheticElementHandleRegistry syntheticElementHandleRegistry = new SyntheticElementHandleRegistry();
		syntheticElementHandleRegistry.setProcessingContext(processingContext);
		syntheticElementHandleRegistry.registerMethodDeclaration(
				TypeSignature.of(Map.Entry.class),
				TestDeclarations.doNothing());
		//
		// Make sure it works well when type is specified in both binary and canonical form.
		// FIXME: 2020-11-26. Temporarily disabled as we don't support binary names for now.
//		Assertions.assertEquals("java.util.Map.Entry#doNothing()", syntheticElementHandleRegistry.getMethod(
//				"java.util.Map.Entry#doNothing()").getBinarySignature().toString());
//		Assertions.assertEquals("java.util.Map.Entry#doNothing()", syntheticElementHandleRegistry.getMethod(
//				"java.util.Map$Entry#doNothing()").getBinarySignature().toString());
	}
}
