package tk.labyrinth.jaap.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.PackageElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;
import java.util.stream.Collectors;

@CompilationTarget(sourceNames = {
		"tk.labyrinth.jaap.test.Dummy",
		"tk.labyrinth.jaap.test.package-info",
		"tk.labyrinth.jaap.test.pkg.package-info"})
@ExtendWithJaap
class RoundContextImplTest {

	@Test
	void testGetAllTypeElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test:Dummy",
						"tk.labyrinth.jaap.test:Dummy.StaticNested"),
				roundContext.getAllTypeElements()
						.map(TypeElementHandle::toString)
						.sorted()
						.collect(Collectors.toList()));
	}

	@Test
	void testGetPackageElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test",
						"tk.labyrinth.jaap.test.pkg"),
				roundContext.getPackageElements()
						.map(PackageElementHandle::toString)
						.sorted()
						.collect(Collectors.toList()));
	}

	@Test
	void testGetTopLevelElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test",
						"tk.labyrinth.jaap.test.pkg",
						"tk.labyrinth.jaap.test:Dummy"),
				roundContext.getTopLevelElements()
						.map(ElementHandle::toString)
						.sorted()
						.collect(Collectors.toList()));
	}
}
