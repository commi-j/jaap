package tk.labyrinth.jaap.context;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Map;

@ExtendWithJaap
class ProcessingContextImplTest {

	@SuppressWarnings("ConstantConditions")
	@Test
	void testFindDeclaredTypeHandle(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.lang:String",
				processingContext.findDeclaredTypeHandle("java.lang:String").toString());
		Assertions.assertEquals(
				"java.util:Map.Entry",
				processingContext.findDeclaredTypeHandle("java.util:Map.Entry").toString());
		Assertions.assertNull(processingContext.findDeclaredTypeHandle("String"));
	}

	@Test
	void testGetMethodElementHandleWithString(ProcessingContext context) {
		Assertions.assertEquals(
				"java.lang:Object#wait()",
				context.getMethodElementHandle("java.lang:Object#wait()").toString());
		Assertions.assertEquals(
				"java.lang:Object#wait(long)",
				context.getMethodElementHandle("java.lang:Object#wait(long)").toString());
		Assertions.assertEquals(
				"java.lang:Object#wait(long,int)",
				context.getMethodElementHandle("java.lang:Object#wait(long,int)").toString());
		//
		Assertions.assertEquals(
				"java.util:Collection#addAll(java.util:Collection)",
				context.getMethodElementHandle(
								"java.util:Collection#addAll(java.util:Collection)")
						.toString());
		{
			// Vararg
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])",
					context.getMethodElementHandle(
									"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])")
							.toString());
			ContribAssertions.assertThrows(
					() -> context.getMethodElementHandle(
							"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int...)"),
					fault -> Assertions.assertEquals(
							"java.lang.IllegalArgumentException: False TypeSignature: int...",
							fault.toString()));
		}
	}

	@Test
	void testGetName(ProcessingContext context) {
		Assertions.assertEquals("", context.getName("").toString());
		Assertions.assertEquals("foo", context.getName("foo").toString());
		Assertions.assertEquals("foo.bar", context.getName("foo.bar").toString());
		Assertions.assertEquals("foo.Bar", context.getName("foo.Bar").toString());
		//
		ContribAssertions.assertThrows(
				() -> context.getName(null),
				fault -> Assertions.assertEquals(NullPointerException.class, fault.getClass()));
	}

	@Test
	void testGetPackageWhenAbsent(ProcessingContext processingContext) {
		{
			val handle = processingContext.getPackageElementHandle("tk.labyrinth");
			Assertions.assertEquals("tk.labyrinth", handle.getQualifiedName());
			Assertions.assertTrue(handle.isSynthetic());
		}
	}

	@Test
	void testGetTypeElementHandleWithString(ProcessingContext context) {
		Assertions.assertEquals(
				"java.util:Map.Entry",
				context.getElementHandle("java.util:Map.Entry").getSignatureString());
		Assertions.assertEquals(
				"java.util:Map.Entry#getKey()",
				context.getElementHandle("java.util:Map.Entry#getKey()").getSignatureString());
	}

	@Test
	void testGetTypeParameterElementHandleWithClassAndString(ProcessingContext context) {
		Assertions.assertEquals("java.util:Map%K",
				context.getTypeParameterElementHandle(Map.class, "K").toString());
		Assertions.assertEquals("java.util:Map.Entry%V",
				context.getTypeParameterElementHandle(Map.Entry.class, "V").toString());
	}

	@Test
	void testGetTypeParameterElementHandleWithString(ProcessingContext context) {
		Assertions.assertEquals("java.util:Map%K",
				context.getTypeParameterElementHandle("java.util:Map%K").toString());
		Assertions.assertEquals("java.util:Map.Entry%V",
				context.getTypeParameterElementHandle("java.util:Map.Entry%V").toString());
		// TODO: Methods & Constructors
	}
}
