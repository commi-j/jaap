package tk.labyrinth.jaap.annotation.merged;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.test.OmniAnnotation;
import tk.labyrinth.jaap.test.OmniClass;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ExtendWithJaap
class DefaultMergedAnnotationAttributeTest {

	@Test
	void testGetAttributeWithDifferentTypes(ProcessingContext processingContext) {
		MergedAnnotation omniAnnotation = processingContext
				.getElementHandle(OmniClass.class)
				.getMergedAnnotation(
						OmniAnnotation.class,
						MergedAnnotationSpecification.javaCore());
		//
		Assertions.assertEquals(
				List.of(
						"java.lang.Override",
						"java.lang.SuppressWarnings"),
				omniAnnotation.getAttribute("annotationTypes").getValueAsClassList().stream()
						.map(TypeHandle::toString)
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				List.of(
						"java.lang.Integer",
						"java.lang.String"),
				omniAnnotation.getAttribute("types").getValueAsClassList().stream()
						.map(TypeHandle::toString)
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				List.of(
						"@java.lang.SuppressWarnings(value={\"one\"})",
						"@java.lang.SuppressWarnings(value={\"two\"})"),
				omniAnnotation.getAttribute("annotations").getValueAsAnnotationList().stream()
						.map(AnnotationHandle::toString)
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				"one",
				omniAnnotation.getAttribute("string").getValueAsString());
		Assertions.assertEquals(
				List.of(
						"two",
						"three"),
				omniAnnotation.getAttribute("strings").getValueAsStringList());
	}

	/**
	 * There was an issue with implementation where {@link Optional#orElse} led to evaluation of default value
	 * of annotation without default value. Replaced with {@link Optional#orElseGet}.
	 *
	 * @param processingContext injected non-null
	 */
	@Test
	void testGetValueAsStringDoesNotFailWhenNoDefaultValueSpecified(ProcessingContext processingContext) {
		MergedAnnotation omniAnnotation = processingContext
				.getElementHandle(OmniClass.class)
				.getMergedAnnotation(
						OmniAnnotation.class,
						MergedAnnotationSpecification.javaCore());
		//
		Assertions.assertEquals(
				"one.two",
				omniAnnotation.getAttribute("stringWithoutDefault").getValueAsString());
	}
}