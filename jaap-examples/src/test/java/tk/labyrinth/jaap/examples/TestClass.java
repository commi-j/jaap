package tk.labyrinth.jaap.examples;

/**
 * This one is required for {@link CallbackAnnotationProcessorExample} to be triggered
 * during this module's test sources compilation.
 */
public class TestClass {
	// empty
}
