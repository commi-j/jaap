package tk.labyrinth.jaap.examples;

import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.examples.model.MyClass;
import tk.labyrinth.jaap.testing.JaapTestCompiler;

public class ShallowIterateOverFieldsExample {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testIterateOverFields() {
		testCompiler.run(CompilationTarget.ofSourceTypes(MyClass.class),
				new ShallowFieldIteratingAnnotationProcessor());
	}
}
