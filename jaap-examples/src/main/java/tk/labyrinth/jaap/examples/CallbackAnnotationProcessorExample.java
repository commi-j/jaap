package tk.labyrinth.jaap.examples;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;

import javax.annotation.processing.Processor;

@AutoService(Processor.class)
public class CallbackAnnotationProcessorExample extends CallbackAnnotationProcessor {

	{
		onInit(processingEnvironment -> {
			// This is callback equivalent of init method of javax.annotation.processing.Processor.
		});
		onFirstRound(round -> {
			//

			// Annotation Processing Classes (provided by Compiler).
			javax.annotation.processing.ProcessingEnvironment processingEnvironment = round.getProcessingEnvironment();
			javax.annotation.processing.RoundEnvironment roundEnvironment = round.getRoundEnvironment();
			//
			// Context Classes (provided by JAAP).
			tk.labyrinth.jaap.context.ProcessingContext processingContext = ProcessingContext.of(processingEnvironment);
			tk.labyrinth.jaap.context.RoundContext roundContext = RoundContext.of(round);
			//
			// Usage of Factory classes (statically available).
			// TODO
		});
		onEachRound(round -> {
			// This is callback equivalent of process method of javax.annotation.processing.Processor.
		});
	}
}
