package tk.labyrinth.jaap.examples;

import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;

@Slf4j
public class ShallowFieldIteratingAnnotationProcessor extends CallbackAnnotationProcessor {

	{
		onEachRound(round -> {
			RoundContext roundContext = RoundContext.of(round);
			roundContext.getAllTypeElements()
					.filter(typeElement -> typeElement.getPackageQualifiedName().startsWith("tk.labyrinth.jaap.examples.model"))
					.forEach(typeElement -> {
						typeElement.getDeclaredFields().forEach(fieldElement -> {
							logger.info("Field: owner = {}, name = {}, type = {}",
									typeElement.getSignature(),
									fieldElement.getName(),
									fieldElement.getType());
						});
					});
		});
	}
}
