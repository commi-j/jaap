package tk.labyrinth.jaap.examples;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

// This is used to add entry to META-INF/services/javax.annotation.processing.Processor,
// which is the default way of processor discovery.
@AutoService(Processor.class)
//
// By default only if annotations matched this pattern are present, this processor will be invoked.
// Wildcard makes it always work, transforming into "compilation preprocessor".
@SupportedAnnotationTypes("*")
public class ClassicAnnotationProcessorExample extends AbstractProcessor {

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		{
			// Optional method, triggered once before processing is started.
		}
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		{
			// Method is triggered for each round of annotation processing (i.e. for each added set of sources)
			// and once with empty set to indicate last round.
		}
		{
			// Elements here could be TypeElements or PackageElements, one for each source file.
			Set<? extends Element> rootElements = roundEnv.getRootElements();
		}
		return false;
	}
}
