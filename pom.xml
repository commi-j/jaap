<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xmlns="http://maven.apache.org/POM/4.0.0"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<!---->
	<groupId>tk.labyrinth.jaap</groupId>
	<artifactId>jaap-parent</artifactId>
	<version>0.5.1-SNAPSHOT</version>
	<!---->
	<packaging>pom</packaging>
	<!---->
	<name>${project.groupId}:${project.artifactId}</name>
	<description>JAAP Parent</description>
	<url>${project.location.http}</url>
	<!---->
	<licenses>
		<license>
			<name>MIT License</name>
			<url>http://www.opensource.org/licenses/mit-license.php</url>
		</license>
	</licenses>
	<!---->
	<developers>
		<developer>
			<name>Commitman</name>
			<email>commitman@protonmail.com</email>
		</developer>
	</developers>
	<!---->
	<modules>
		<module>jaap</module>
		<module>jaap-core</module>
		<module>jaap-examples</module>
		<module>jaap-misc4j2</module>
		<module>jaap-testing</module>
		<module>test-coverage</module>
	</modules>
	<!---->
	<scm>
		<connection>${project.location.git}</connection>
		<developerConnection>${project.location.git}</developerConnection>
		<url>${project.location.http}</url>
	</scm>
	<!---->
	<properties>
		<!-- VERSIONS BEGIN -->
		<!---->
		<!-- Languages -->
		<version.java>17</version.java>
		<!---->
		<!---->
		<!-- Libraries -->
		<version.auto-service>1.0-rc6</version.auto-service>
		<!---->
		<!-- (Sep 09, 2020) -->
		<version.commons-io>2.8.0</version.commons-io>
		<!---->
		<version.commons-lang>3.10</version.commons-lang>
		<version.guava>29.0-jre</version.guava>
		<version.jpig>0.8.2</version.jpig>
		<version.jsr305>3.0.2</version.jsr305>
		<version.junit5>5.5.2</version.junit5>
		<version.log4j>2.12.1</version.log4j>
		<version.lombok>1.18.20</version.lombok>
		<version.slf4j>1.7.28</version.slf4j>
		<!---->
		<!---->
		<!-- Plugins | Fully checked at 2022-12-08 -->
		<!---->
		<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-compiler-plugin -->
		<version.plugin.compiler>3.10.1</version.plugin.compiler>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.codehaus.mojo/exec-maven-plugin -->
		<version.plugin.exec>3.1.0</version.plugin.exec>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-gpg-plugin -->
		<version.plugin.gpg>3.0.1</version.plugin.gpg>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
		<version.plugin.jacoco>0.8.8</version.plugin.jacoco>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-javadoc-plugin -->
		<version.plugin.javadoc>3.4.1</version.plugin.javadoc>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.sonatype.plugins/nexus-staging-maven-plugin -->
		<version.plugin.nexus-staging>1.6.13</version.plugin.nexus-staging>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-source-plugin -->
		<version.plugin.source>3.2.1</version.plugin.source>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.sonarsource.scanner.maven/sonar-maven-plugin -->
		<version.plugin.sonar>3.9.1.2184</version.plugin.sonar>
		<!---->
		<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-surefire-plugin -->
		<version.plugin.surefire>3.0.0-M7</version.plugin.surefire>
		<!---->
		<!-- VERSIONS END -->
		<!---->
		<!-- CONFIGURATION BEGIN -->
		<jacoco.skip>true</jacoco.skip>
		<!-- CONFIGURATION END -->
		<!---->
		<!-- CORE-SETTINGS BEGIN -->
		<maven.compiler.source>${version.java}</maven.compiler.source>
		<maven.compiler.target>${version.java}</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<!-- CORE-SETTINGS END -->
		<!---->
		<!-- PROJECT-LOCATION BEGIN -->
		<project.location.parent>commitman</project.location.parent>
		<project.location.name>jaap</project.location.name>
		<!---->
		<project.location.path>${project.location.parent}/${project.location.name}</project.location.path>
		<project.location.git>git@gitlab.com:${project.location.path}</project.location.git>
		<project.location.http>https://gitlab.com/${project.location.path}</project.location.http>
		<!-- PROJECT-LOCATION END -->
	</properties>
	<!---->
	<dependencyManagement>
		<dependencies>
			<!-- COMPILE BEGIN -->
			<dependency>
				<groupId>tk.labyrinth.jaap</groupId>
				<artifactId>jaap</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>tk.labyrinth.jaap</groupId>
				<artifactId>jaap-core</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>tk.labyrinth.jaap</groupId>
				<artifactId>jaap-misc4j2</artifactId>
				<version>${project.version}</version>
			</dependency>
			<!-- COMPILE END -->
			<!---->
			<!-- PROVIDED BEGIN -->
			<dependency>
				<groupId>com.google.auto.service</groupId>
				<artifactId>auto-service</artifactId>
				<version>${version.auto-service}</version>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>tk.labyrinth.jaap</groupId>
				<artifactId>jpig</artifactId>
				<version>${version.jpig}</version>
				<scope>provided</scope>
			</dependency>
			<!-- PROVIDED END -->
			<!---->
			<!-- RUNTIME BEGIN -->
			<!-- RUNTIME END -->
			<!---->
			<!-- TEST BEGIN -->
			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-slf4j-impl</artifactId>
				<version>${version.log4j}</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>tk.labyrinth.jaap</groupId>
				<artifactId>jaap-testing</artifactId>
				<version>${project.version}</version>
				<scope>test</scope>
			</dependency>
			<!-- TEST END -->
			<!---->
			<!-- IMPORT BEGIN -->
			<!-- IMPORT END -->
		</dependencies>
	</dependencyManagement>
	<!---->
	<dependencies>
		<!-- COMPILE BEGIN -->
		<dependency>
			<groupId>com.google.code.findbugs</groupId>
			<artifactId>jsr305</artifactId>
			<version>${version.jsr305}</version>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>${version.guava}</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${version.commons-io}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${version.commons-lang}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${version.slf4j}</version>
		</dependency>
		<!-- COMPILE END -->
		<!---->
		<!-- PROVIDED BEGIN -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${version.lombok}</version>
			<scope>provided</scope>
		</dependency>
		<!-- PROVIDED END -->
		<!---->
		<!-- RUNTIME BEGIN -->
		<!-- RUNTIME END -->
		<!---->
		<!-- TEST BEGIN -->
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-slf4j-impl</artifactId>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>${version.junit5}</version>
			<scope>test</scope>
		</dependency>
		<!-- TEST END -->
	</dependencies>
	<!---->
	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>exec-maven-plugin</artifactId>
					<version>${version.plugin.exec}</version>
				</plugin>
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>${version.plugin.jacoco}</version>
				</plugin>
				<plugin>
					<groupId>org.sonatype.plugins</groupId>
					<artifactId>nexus-staging-maven-plugin</artifactId>
					<version>${version.plugin.nexus-staging}</version>
				</plugin>
				<!---->
				<plugin>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${version.plugin.compiler}</version>
				</plugin>
				<plugin>
					<artifactId>maven-gpg-plugin</artifactId>
					<version>${version.plugin.gpg}</version>
				</plugin>
				<plugin>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>${version.plugin.javadoc}</version>
				</plugin>
				<plugin>
					<artifactId>maven-source-plugin</artifactId>
					<version>${version.plugin.source}</version>
				</plugin>
				<plugin>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${version.plugin.surefire}</version>
					<!---->
					<configuration>
						<includes>
							<!-- Include all, because why not? -->
							<include>*</include>
						</includes>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<artifactId>maven-javadoc-plugin</artifactId>
				<!---->
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-source-plugin</artifactId>
				<!---->
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!---->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<!---->
				<executions>
					<execution>
						<id>prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<!---->
	<profiles>
		<profile>
			<id>gpg-sign</id>
			<!---->
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<!---->
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
								<configuration>
									<gpgArguments>
										<!---->
										<!-- If your key has passphrase it should be specified with gpg.passphrase property. -->
										<!-- You may want to specify gpg.keyname property to identify key. -->
										<!-- See .m2/settings.xml for these properties. -->
										<!---->
										<!-- For gpg 2.1+ We need to specify "&#45;&#45;pinentry-mode loopback" to disable passphrase prompt. -->
										<!-- Docs: https://www.gnupg.org/documentation/manuals/gnupg/GPG-Esoteric-Options.html#GPG-Esoteric-Options -->
										<!-- Option name and value must be passed as two different arguments, -->
										<!-- otherwise gpg would say it is invalid option. -->
										<argument>--pinentry-mode</argument>
										<argument>loopback</argument>
										<!---->
									</gpgArguments>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>publish-snapshot</id>
			<!---->
			<distributionManagement>
				<snapshotRepository>
					<id>ossrh</id>
					<url>https://oss.sonatype.org/content/repositories/snapshots</url>
				</snapshotRepository>
			</distributionManagement>
			<!---->
			<build>
				<plugins>
					<plugin>
						<groupId>org.sonatype.plugins</groupId>
						<artifactId>nexus-staging-maven-plugin</artifactId>
						<!---->
						<configuration>
							<serverId>ossrh</serverId>
							<!---->
							<!-- Stage locally with 'deploy', then deploy with 'nexus-staging:deploy-staged'. -->
							<skipRemoteStaging>true</skipRemoteStaging>
						</configuration>
						<extensions>true</extensions>
					</plugin>
				</plugins>
			</build>
			<!---->
		</profile>
		<profile>
			<id>publish-release</id>
			<!---->
			<build>
				<plugins>
					<plugin>
						<!---->
						<!-- You need to specify server with id = ossrh, and username/password credentials for Nexus. -->
						<!---->
						<!-- This plugin replaces default maven-deploy-plugin and during deploy phase it creates -->
						<!-- new Nexus repository, deploys artifacts to it, closes, releases and drops it. -->
						<!---->
						<!-- Docs: https://github.com/sonatype/nexus-maven-plugins/tree/master/staging/maven-plugin -->
						<!-- Docs: https://help.sonatype.com/repomanager2/staging-releases/configuring-your-project-for-deployment#ConfiguringYourProjectforDeployment-DeploymentwiththeNexusStagingMavenPlugin -->
						<!---->
						<groupId>org.sonatype.plugins</groupId>
						<artifactId>nexus-staging-maven-plugin</artifactId>
						<!---->
						<configuration>
							<!---->
							<!-- Release on successful close operation. -->
							<autoReleaseAfterClose>true</autoReleaseAfterClose>
							<!---->
							<!-- Mandatory. Has to point to the base URL of target Nexus. -->
							<nexusUrl>https://oss.sonatype.org</nexusUrl>
							<!---->
							<!-- Mandatory. Has to hold an ID of a <server> section from Maven's settings.xml -->
							<!-- to pick authentication information from. -->
							<serverId>ossrh</serverId>
							<!---->
							<!-- Stage locally with 'deploy', then deploy with 'nexus-staging:deploy-staged'. -->
							<skipRemoteStaging>true</skipRemoteStaging>
						</configuration>
						<extensions>true</extensions>
					</plugin>
				</plugins>
			</build>
			<!---->
		</profile>
	</profiles>
	<!---->
</project>
